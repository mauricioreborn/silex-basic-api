<?php

require __DIR__ . '/../resources/config/bootstrap.php';

if (!array_key_exists('token', $_GET)) {
    exit('Unauthorized');
}

if ($_GET['token'] != 'jenkinsci-notification') {
    exit('Unauthorized');
}

$acmeUrl = getenv('URL');
$environment = getenv('ENV');

$text = sprintf(
    '{"icon_url": "https://www.opopular.com.br/polopoly_fs/1.1250769.1491005015!/image/image.jpg_gen/derivatives/landscape_940/image.jpg", "username": "LADRAUM_BOT", "text": "Hello guys, a new version of ACME API has been released on %s server! Check it out at %s"}',
    $environment,
    $acmeUrl
);

$context = stream_context_create([
    'http' => [
        'method' => 'POST',
        'header' => [
            'Content-Type: application/x-www-form-urlencoded'
        ],
        'content' => $text,
    ]
]);

$slackWebHook = 'https://hooks.slack.com/services/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';
print file_get_contents($slackWebHook, null, $context);