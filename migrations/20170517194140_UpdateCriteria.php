<?php

class UpdateCriteria extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');
        $user = $collection->getDocument(new \MongoId('58dd246096fbde008624d261'));
        for ($i=0; $i<=5; $i++) {
            $user->addToSet('criterias', $this->getCriteria()[$i]);
            $user->addToSet('reviewed', true);
        }
        $user->save();
    }


    public function getUsers()
    {
        return [
            '58dd246096fbde008624d261',
        ];
    }

    public function getCriteria()
    {
        return $array = array (2, 4, 5, 6, 8, 10);
    }
}