<?php

class UpdateApiUser extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $user = $collection->getDocument(new \MongoId('58dd246096fbde008624d261'));
        $user->set('profile', 'student');
        $user->set('state', 'approved');
        $user->save();
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $collection->clearDocumentPool();
    }
}
