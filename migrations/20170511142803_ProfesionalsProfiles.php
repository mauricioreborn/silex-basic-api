<?php

class ProfesionalsProfiles extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $classCode ='591320c0753bd9005006c936';
    private $file;
    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');
        $collection->insert($this->apiUser());
        for ($i =0; $i<10; $i++) {
            $users = $this->getUser($i);
            $collection->insert($users);
        }
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $collection->clearDocumentPool();
    }


    public function getUser($i)
    {
        $profile = array ('pro','educator','recruiter','moderator','pro','educator','recruiter','moderator','admin','admin');
        return [
            '_id' => new \MongoId('590820c0753bd9005006c93'.$i),
            'email' => 'proedu'.$i.'@acme.com.br',
            'password' => md5('123456'),
            'association' => 'classroom',
            'classCode' => new \MongoId($this->classCode),
            'birthDate' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'community' => ['animation','Engineering'],
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'agreement' => 'true',
            'createdAt' => $this->today,
            'gender' => $this->faker->randomElement($array = array ('f','m')),
            'phone' => '3224400088',
            'profile' => $profile[$i],
            'level' => $this->getLevel($profile[$i], 5),
            'userPhoto' => 'https://s3.amazonaws.com/s3-acme/demo/av'.random_int(1, 9).'.png',
            'status' => true,
            'state' => 'approved'
        ];
    }

    public function apiUser()
    {
        return [
            '_id' => new \MongoId('58dd246096fbde008624d261'),
            'email' => 'api@acme.com.br',
            'password' => md5('123456'),
            'association' => 'classroom',
            'classCode' => new \MongoId($this->classCode),
            'birthDate' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'community' => ['animation','Engineering'],
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'agreement' => 'true',
            'createdAt' => $this->today,
            'gender' => $this->faker->randomElement($array = array ('f','m')),
            'phone' => '3224400088',
            'profile' => 'Student Level 3',
            'status' => true,
            'level' => $this->getLevel('Student Level 3', 3),
            'userPhoto' => 'https://s3.amazonaws.com/s3-acme/demo/av'.random_int(1, 9).'.png',
        ];
    }


    /** Method to build a level
     * @param $level
     * @param $number
     * @return array
     */
    public function getLevel($level, $number)
    {
        return [
            'date' => $this->today,
            'alias' => $level,
            'number' => $number
        ];
    }
}