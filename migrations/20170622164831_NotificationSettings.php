<?php

class NotificationSettings extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('notificationSettings', [
                ['alias' => 'comment_work', 'text' => 'Someone like or comment my work', 'status' => true],
                ['alias' => 'work_review', 'text' => 'Receive work Review','status' => true],
                ['alias' => 'level_up', 'text' => 'Level up','status' => true],
                ['alias' => 'co_work', 'text' => "I'm added as a work Collaborator", 'status' => true ],
                ['alias' => 'follow', 'text' => "I'm followed by someone new",'status' => true],
                ['alias' => 'message', 'text' => "I'm sent a suport message",'status' => true],
                ['alias' => 'acme', 'text' => "News about ACME product and feature updates",'status' => true]
            ]);
        });
    }
    
    public function down()
    {
        
    }
}