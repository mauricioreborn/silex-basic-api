<?php

class Level2Fixed extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $today;
    private $faker;
    public function up()
    {
        $this->faker = Faker\Factory::create();
        
        $dt = $this->faker->dateTimeBetween(
            $startDate = '-45 days',
            $endDate = 'now',
            date_default_timezone_get()
        );
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
                ->getDatabase(getenv('DB_DATABASE'))
                ->getCollection('users');

        $work = $collection->getDocument(new \MongoId('58dd246096fbde008624d288'));
        if ($work) {
            $work->Set('level', $this->getLevel('Student Level 3', 3));
            $work->save();
        }
    }


    /** build a level
     * @param $level
     * @param $number
     * @return array
     */
    public function getLevel($level, $number)
    {
        return [
            'date' => $this->today,
            'alias' => $level,
            'number' => $number
        ];
    }
    
    public function down()
    {
        
    }
}