<?php

class CreateWorksWithReviewedFalse extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/00';
    private $today;

    public function up()
    {
        date_default_timezone_set('UTC');

        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $this->faker = Faker\Factory::create();
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        for ($i=0; $i<5; $i++) {
            $work = $this->buildWork($i);
            $collection->insert($work);
        }
    }
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        $collection->clearDocumentPool();
    }


    /** this method create a work to api user with false reviewed field
     * @return array
     */
    public function buildWork($i)
    {
        $uid = new \MongoId();
        $level = random_int(1, 3);
        $iterator = [
            '_id' => $uid,
            'userId' => new \MongoId('58dd246096fbde008624d261'),
            'title' => $this->faker->sentence(3, true),
            'description' => [$this->faker->text(200),$this->faker->text(200)],
            'category' => 'animation',
            'level' => $this->getLevel('Student level '.$level, $level),
            'available' => true,
            'group' => 'work',
            'reviewed' => false,
            'highLight' => $this->faker->randomElement($array = array (true, false)),
            'comments' => [
                [
                    '_id' => new \MongoId('58dd246096fbde008624d233'),
                    'owner' => $uid,
                    'userId' =>  new \MongoId('58dd246096fbde008624d243'),
                    'comment' => 'OMG! greaaat!',
                    'created_at' => $this->today,
                ]
            ],
            'views' => 0,
            'upload_at' => $this->today,
            'attach' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i+1).'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i-1).'.png',
                    'type' => 'png'
                ]

            ],
            'mainFile' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],
            'thumbnail' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],

            'likes' => [
                [
                    '_id' => new \MongoId(),
                    'owner' => new \MongoId('58dd246096fbde008624d243'),
                    'workId' => $uid,
                    'createdAt' => $this->today
                ]
            ]
        ];
        
        return $iterator;
    }


    /** build a level
     * @param $level
     * @param $number
     * @return array
     */
    public function getLevel($level, $number)
    {
        return [
            'date' => $this->today,
            'alias' => $level,
            'number' => $number
        ];
    }
}