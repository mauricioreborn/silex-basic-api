<?php

class ReplyCommentsMigrate extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('replyComment');

        for ($i = 0; $i <=5; $i++) {
            $collection->insert($this->getCommentReply($i));
        }
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('replyComment');

        $collection->clearDocumentPool();
    }

    public function getCommentReply($i)
    {
         $newId = "58dd246096fbde008224d23".$i;
        return [
            '_id' => new \MongoId(),
            'commentId' => new \MongoId($newId),
            'userId' =>  new \MongoId('58dd246096fbde008624d243'),
            'comment' =>  $this->faker->text(80),
            'created_at' => $this->today,
        ];
    }
}