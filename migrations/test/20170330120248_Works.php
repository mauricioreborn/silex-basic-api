<?php

require_once __DIR__ . '/../../vendor/fzaninotto/faker/src/autoload.php';

class Works extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/';
    public $today;

    public function up()
    {
        date_default_timezone_set('UTC');
        $this->faker = Faker\Factory::create();
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        $var =  [];

        $workid = [
            '58ebe2ebd5e73800542e8801',
            '58ebe2ebd5e73800542e8802',
            '58ebe2ebd5e73800542e8803',
            '58ebe2ebd5e73800542e8804',
            '58ebe2ebd5e73800542e8805',
            '58ebe2ebd5e73800542e8806',
            '58ebe2ebd5e73800542e8807',
            '58ebe2ebd5e73800542e8808',
            '58ebe2ebd5e73800542e8809',
            '58ebe2ebd5e73800542e8810',
            '58ebe2ebd5e73800542e8811',
            '58ebe2ebd5e73800542e8812',
            '58ebe2ebd5e73800542e8813',
            '58ebe2ebd5e73800542e8814',
            '58ebe2ebd5e73800542e8815',
            '58ebe2ebd5e73800542e8816',
            '58ebe2ebd5e73800542e8817',
            '58ebe2ebd5e73800542e8818',
            '58ebe2ebd5e73800542e8819',
            '58ebe2ebd5e73800542e8820'
        ];

        $userId = [
            '58dd246096fbde008624d211',
            '58dd246096fbde008624d222',
            '58dd246096fbde008624d233',
            '58dd246096fbde008624d244',
            '58dd246096fbde008624d255',
            '58dd246096fbde008624d266',
            '58dd246096fbde008624d277',
            '58dd246096fbde008624d288',
            '58dd246096fbde008624d299',
            '58dd246096fbde008624d200'
        ];

        $i = 1;
        foreach ($workid as $work) {
            $var[] = $this->getWork($i, $work);
            $i++;
        }
        $u = 1;
        foreach ($userId as $work) {
            $var[] = $this->getWorkWithUserId($u, $work);
            $u++;
        }

        foreach ($var as $work) {
            $collection->insert($work);
        }
    }

    public function getWork($i, $workid)
    {
        $dt = $this->faker->dateTimeBetween(
            $startDate = '-45 days',
            $endDate = 'now',
            date_default_timezone_get()
        );
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $uid = new \MongoId($workid);
        $qtd1 = 0;
        $qtd2 = 0;
        $qtd3 = 0;
        if ($workid === '58ebe2ebd5e73800542e8801') {
            $qtd1 = 5;
            $qtd2 = 3;
            $qtd3 = 2;
        }
        $level = random_int(1, 3);
        $iterator = [
            '_id' => $uid,
            'userId' => new \MongoId('58dd246096fbde008624d261'),
            'title' => $this->faker->sentence(3, true),
            'description' => [$this->faker->text(200),$this->faker->text(200)],
            'category' => 'animation',
            'group' => 'work',
            'level' => $this->getLevel('Student level '.$level, $level),
            'views' => 0,
            'upload_at' => $this->today,
            'attach' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i+1).'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i-2).'.png',
                    'type' => 'png'
                ]

            ],
            'mainFile' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],
            'thumbnail' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],
            'likes' => [
                [
                    '_id' => new \MongoId(),
                    'owner' => new \MongoId('58dd246096fbde008624d243'),
                    'workId' => $uid,
                    'createdAt' => $this->today
                ],
                [
                    '_id' => new \MongoId(),
                    'owner' => new \MongoId('58dd246096fbde008624d243'),
                    'workId' => $uid,
                    'createdAt' => $this->today
                ]
            ]
        ];

        return $iterator;
    }

    public function getWorkWithUserId($i, $uderId)
    {
        $dt = $this->faker->dateTimeBetween(
            $startDate = '-45 days',
            $endDate = 'now',
            date_default_timezone_get()
        );
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $uid = new \MongoId();
        $level = random_int(1, 3);
        $iterator = [
            '_id' => $uid,
            'userId' => new \MongoId($uderId),
            'title' => $this->faker->sentence(3, true),
            'description' => $this->faker->text(200),
            'category' => 'animation',
            'level' => $this->getLevel('Student level '.$level, $level),
            'group' => 'work',
            'highLight' => $this->faker->randomElement($array = array (true, false)),
            'path' => $this->path.$i.'.png',
            'views' => 0,
            'upload_at' => $this->today,
            'attach' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i+1).'.png',
                    'type' => 'png'
                ],
                [
                    'path' => $this->path.($i-2).'.png',
                    'type' => 'png'
                ]

            ],
            'mainFile' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],
            'thumbnail' => [
                [
                    'path' => $this->path.$i.'.png',
                    'type' => 'jpeg'
                ]
            ],

            'likes' => [
                [
                    '_id' => new \MongoId(),
                    'owner' => new \MongoId('58dd246096fbde008624d243'),
                    'workId' => $uid,
                    'createdAt' => $this->today
                ],
                [
                    '_id' => new \MongoId(),
                    'owner' => new \MongoId('58dd246096fbde008624d243'),
                    'workId' => $uid,
                    'createdAt' => $this->today,
                ],
            ]
        ];

        return $iterator;
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        $collection->clearDocumentPool();
    }

    /** build a level
     * @param $level
     * @param $number
     * @return array
     */
    public function getLevel($level, $number)
    {
        return [
            'date' => $this->today,
            'alias' => $level,
            'number' => $number
        ];
    }
}