<?php

require_once __DIR__ . '/../../vendor/fzaninotto/faker/src/autoload.php';

class LikesComments extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public $faker;

    public function up()
    {
        $comment = [
            '58dd246096fbde118624d933',
            '58dd246096fbde118624d932',
            '58dd246096fbde118624d931'
        ];
        date_default_timezone_set('UTC');
        $this->faker = Faker\Factory::create();
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('commentLike');

        $users = $this->getUser();
        $i = 0;
        foreach ($users as $user) {
            if ($i <= 4) {
                $com = $comment[0];
            }
            if (($i > 4) && ($i <= 7)) {
                $com = $comment[1];
            }
            if (($i > 7) && ($i <= 9)) {
                $com = $comment[2];
            }
            $collection->insert($this->getCommentLikes($user, '58ebe2ebd5e73800542e8801', $com));
            $i++;
        }
    }
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

        $collection->clearDocumentPool();
    }

    public function getCommentLikes($user, $work, $comment)
    {
        return  [
            "userId" => new \MongoId($user),
            "workId" => new \MongoId($work),
            "commentId" => new \MongoId($comment),
            "created_at" => $this->getDate()
        ];
    }

    public function getUser()
    {
        return [
            '58dd246096fbde008624d211',
            '58dd246096fbde008624d222',
            '58dd246096fbde008624d233',
            '58dd246096fbde008624d244',
            '58dd246096fbde008624d255',
            '58dd246096fbde008624d266',
            '58dd246096fbde008624d277',
            '58dd246096fbde008624d288',
            '58dd246096fbde008624d299',
            '58dd246096fbde008624d200'
        ];
    }

    public function getDate()
    {
        $dt = $this->faker->dateTimeBetween(
            $startDate = '-10 days',
            $endDate = 'now',
            date_default_timezone_get()
        );
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        return $this->today;
    }

}