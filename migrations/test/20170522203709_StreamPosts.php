<?php

class StreamPosts extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;


    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('posts');

        for ($i = 0; $i <=5; $i++) {
            $collection->insert($this->buildPostLab($i));
        }

        for ($i = 0; $i <=5; $i++) {
            $collection->insert($this->buildPostClassRoom($i));
        }
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('posts');

        $collection->clearDocumentPool();
    }


    public function buildPostLab($i)
    {
        $userId = "58dd246096fbde008624d2".$i.$i;
        $labId = "591cd620c2577603b8285ce".$i;
        $ponstId = "59243799c1fe0100a66e0bd".$i;

        $var =   [
            'userId' => new \MongoId($userId),
            'type' => 'lab',
            'groupId' => new \MongoId($labId),
            'publication' => $this->faker->text(250),
            '_id' => new \MongoId($ponstId),
            'uploadAt' => $this->today,
            'totalLike' => 0,
            'totalComment' => 0
        ];
        return $var;
    }

    public function buildPostClassRoom($i)
    {
        $userId = "58dd246096fbde008624d2".$i.$i;
        $classId = "591cd620c2577603b8285ce".$i;
        $ponstId = "49243799c1fe0100a49e0bd".$i;

        $var =   [
            'userId' => new \MongoId($userId),
            'type' => 'classroom',
            'groupId' => new \MongoId($classId),
            'publication' => $this->faker->text(250),
            '_id' => new \MongoId($ponstId),
            'uploadAt' => $this->today,
            'totalLike' => 0,
            'totalComment' => 0
        ];
        return $var;
    }
}