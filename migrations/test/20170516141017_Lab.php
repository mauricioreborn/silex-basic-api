<?php

class Lab extends \Sokil\Mongo\Migrator\AbstractMigration
{
    protected $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/';
    private $today;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('lab');

        for ($i =0; $i<=5; $i++) {
            $collection->insert($this->getLab($i));
        }
    }

    public function down()
    {
        {
            $collection = $this
                ->getDatabase(getenv('DB_DATABASE'))
                ->getCollection('lab');

            $collection->clearDocumentPool();
        }
    }

    public function getLab($i)
    {
        $lab = '';
        if ($i === 1) {
            for ($increment = 0; $increment<=5; $increment++) {
                $lab[] = $this->getStreamLab($increment);
            }
        }


        $newId = "591cd620c2577603b8285ce".$i;
        return [
            '_id' => new \MongoId($newId),
            'title' => $this->faker->sentence(3, true),
            'description' => $this->faker->text(200),
            'userId' => new \MongoId('590820c0753bd9005006c931'),
            'coverImage' => $this->path.random_int(1, 8).'.png',
            'upload_at' => $this->today,
            'posts' => $lab
        ];
    }

    public function getStreamLab($i)
    {
        return "59243799c1fe0100a66e0bd".$i;
    }
}