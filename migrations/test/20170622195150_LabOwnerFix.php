<?php

class LabOwnerFix extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $today;

    public function up()
    {
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $work = $collection->getDocument(new \MongoId('590820c0753bd9005006c931'));
        if ($work) {
            $work->Set('lab', [
                [
                    "id" => "591cd620c2577603b8285ce0",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "591cd620c2577603b8285ce1",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "591cd620c2577603b8285ce2",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "591cd620c2577603b8285ce3",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "591cd620c2577603b8285ce4",
                    "uploaded_at" => $this->today
                ]
            ]);
            $work->Set('classroom', [
                [
                "id" => "5914b887753bd902b32a7790",
                "uploaded_at" => $this->today
                ],
                [
                    "id" => "5914b887753bd902b32a7791",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "5914b887753bd902b32a7792",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "5914b887753bd902b32a7793",
                    "uploaded_at" => $this->today
                ],
                [
                    "id" => "5914b887753bd902b32a7794",
                    "uploaded_at" => $this->today
                ]
            ]);
            $work->save();
        }
    }
    
    public function down()
    {
        
    }
}