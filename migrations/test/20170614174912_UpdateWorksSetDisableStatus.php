<?php

class UpdateWorksSetDisableStatus extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('status', 'enabled');
        });
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        $collection->clearDocumentPool();
    }
}