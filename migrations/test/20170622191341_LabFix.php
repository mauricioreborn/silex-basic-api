<?php

class LabFix extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('lab');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('status', 'approved');
        });
    }
    
    public function down()
    {
        
    }
}