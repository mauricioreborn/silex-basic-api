<?php

class ReviewWorkCriteria extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $array = array (2, 4, 5, 6, 8, 10);
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

            $user = $collection->getDocument(new \MongoId('58ebe2ebd5e73800542e8803'));
            for ($i=0; $i<=5; $i++) {
                $user->addToSet('criterias', $array[$i]);
            }
            $user->Set('reviewed', true);
            $user->save();
    }
}