<?php

class UpdateUserSetClassroom extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');
        $users = $this->getUsers();

        foreach ($users as $user) {
            $user = $collection->getDocument(new \MongoId($user));
            for ($i=0; $i<5; $i++) {
                $user->addToSet('classroom', $this->getLab($i));
            }
            $user->save();
        }
    }


    public function getUsers()
    {
        return [
            '58dd246096fbde008624d261',
            '58dd246096fbde008624d211',
            '58dd246096fbde008624d222',
            '58dd246096fbde008624d233',
            '58dd246096fbde008624d244',
            '58dd246096fbde008624d255',
            '58dd246096fbde008624d266',
            '58dd246096fbde008624d277',
            '58dd246096fbde008624d288',
            '58dd246096fbde008624d299',
            '58dd246096fbde008624d200'
        ];
    }

    public function getLab($i)
    {
            $newId = "5914b887753bd902b32a779".$i;
           return ['id' => $newId, 'uploaded_at' => $this->today];
    }
}