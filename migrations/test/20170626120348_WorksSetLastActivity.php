<?php

class WorksSetLastActivity extends \Sokil\Mongo\Migrator\AbstractMigration
{
    protected $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/';
    private $today;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('lastActivity', $this->getLastActivity());
        });
    }

    
    public function down()
    {
        
    }

    public function getLastActivity()
    {
        return [
            'userId' => new \MongoId('58dd246096fbde008624d261'),
            'workId' => new \MongoId('58ebe2ebd5e73800542e8801'),
            'date' => $this->today,
            'type' => $this->faker->randomElement($array = array ('new comment', 'new like', 'create')),
            'icon' => $this->faker->randomElement($array = array ('mode_comment', 'favorite')),
        ];
    }
}