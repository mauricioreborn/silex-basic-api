<?php

class FixedLiveSession extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/lv';
    public function up()
    {
        date_default_timezone_set('UTC');
        $this->faker = Faker\Factory::create();
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

            $collection->insert($this->getLiveSession());
    }
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

        $collection->clearDocumentPool();
    }

    public function getLiveSession()
    {
        $dotenv = new \Dotenv\Dotenv(realpath(__DIR__ . '/../../'), '.env');
        $dotenv->load();
        $dataSend = array(
            'url' => getenv('LIVESESSION_URL').'create/session',
            'createdSessionId' => '590820c0753bd9005006c666',
            'speaker' => '590820c0753bd9005006c931',
            'date' => $this->getDate(),
            'startTime' => '01:59 AM',
            'endTime' => '11:59 PM',
            'content' => 'application/json'
        );
        return  [
            '_id' => new \MongoId('590820c0753bd9005006c666'),
            'name' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'description' => $this->faker->text(200),
            'type' => 'public',
            'date' => $dataSend['date'],
            'startTime' => $dataSend['startTime'],
            'endTime' => $dataSend['endTime'],
            'highLight' => $this->faker->randomElement($array = array (true, false)),
            'speaker' => new \MongoId($dataSend['speaker']),
            'moderator' => new \MongoId('590820c0753bd9005006c938'),
            'link' => $this->faker->url,
            'users' => $this->getUser(),
            'created_at' => $this->getDate(),
            'thumbnail' => $this->path.random_int(1, 5).'.png',
        ];
    }

    public function postLiveSession($data)
    {
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-Type: '.$data['content'],
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($opts);
        return file_get_contents($data['url'], false, $context);
    }

    public function getUser()
    {
        return [
            '58dd246096fbde008624d261',
            '58dd246096fbde008624d211',
            '58dd246096fbde008624d222',
            '58dd246096fbde008624d233',
            '58dd246096fbde008624d244',
            '58dd246096fbde008624d255'
        ];
    }

    public function getDate()
    {
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        return new MongoDate($ts);
    }

}