<?php

class Profile extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('profiles');

        $var = array([
            'label' => 'I am a',
            'name' => 'Student',
            'description' => 'I would like to participate with and learn from professionals',
            'order' => '1',
        ],[
            'label' => 'I am an',
            'name' => 'Educator',
            'description' => 'I would like to help students mature and get professional mentorship',
            'order' => '2',
        ],[
            'label' => 'I am a',
            'name' => 'Professional',
            'description' => 'I would like to help prepare students for life as working professionals',
            'order' => '3',
        ],[
            'label' => 'I am a',
            'name' => 'Recruiter',
            'description' => 'I would like to help students find jobs as professional',
            'order' => '4',
        ]);

        foreach ($var as $profile) {
            $collection->insert($profile);
        }
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('profiles');

        $collection->clearDocumentPool();
    }
}