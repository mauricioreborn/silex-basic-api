<?php

class UpdateRecruiter extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');
        $works = $this->getWork();

        foreach ($works as $work) {
            $work = $collection->getDocument(new \MongoId($work));
            if ($work) {
                $work->Set('level', 'recruiter');
                $work->save();
            }
        }
    }

    public function down()
    {

    }

    public function getWork()
    {
        $workid = [
            '590820c0753bd9005006c932',
            '590820c0753bd9005006c936',
        ];

        return $workid;
    }
}