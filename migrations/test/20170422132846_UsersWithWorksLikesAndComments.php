<?php

class UsersWithWorksLikesAndComments extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function avatarRandom()
    {
        return 'https://s3.amazonaws.com/s3-acme/demo/av'.random_int(1, 9).'.png';
    }
    
    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $var2 =[
            [
                '_id' => new \MongoId('58dd246096fbde008624d261'),
                'email' => 'api@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1993-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Jhon',
                'lastName' => 'Doe',
                'agreement' => 'true',
                'createdAt' => $this->today,
                'following' => [
                    ['id' => '58dd246096fbde008624d211', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d222', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d233', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d244', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d255', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d266', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d277', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d288', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d299', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d200', 'date' => $this->today]
                ],
                'followers' => [
                    ['id' => '58dd246096fbde008624d299', 'date' => $this->today],
                    ['id' => '58dd246096fbde008624d200', 'date' => $this->today]
                ],
                'likes' => [
                    [
                        '_id' => new \MongoId(),
                        'owner' => new \MongoId('58dd246096fbde008624d261'),
                        'workId' => '58ebe2ebd5e73800542e8802',
                        'createdAt' => $this->today
                    ],
                    [
                        '_id' => new \MongoId(),
                        'owner' => new \MongoId('58dd246096fbde008624d261'),
                        'workId' => '58ebe2ebd5e73800542e8803',
                        'createdAt' => $this->today
                    ],
                    [
                        '_id' => new \MongoId(),
                        'owner' => new \MongoId('58dd246096fbde008624d261'),
                        'workId' => '58ebe2ebd5e73800542e8804',
                        'createdAt' => $this->today
                    ],
                    [
                        '_id' => new \MongoId(),
                        'owner' => new \MongoId('58dd246096fbde008624d261'),
                        'workId' => '58ebe2ebd5e73800542e8805',
                        'createdAt' => $this->today
                    ],
                    [
                        '_id' => new \MongoId(),
                        'owner' => new \MongoId('58dd246096fbde008624d261'),
                        'workId' => '58ebe2ebd5e73800542e8806',
                        'createdAt' => $this->today
                    ]

                ],
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d211'),
                'email' => 'jhon@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Jhon',
                'lastName' => 'Whick',
                'agreement' => 'true',
                'createdAt' => $this->today,
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d222'),
                'email' => 'jhony@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Jhonny',
                'lastName' => 'Bravo',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 2', 2),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d233'),
                'email' => 'corleone@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Michael',
                'lastName' => 'Corleone',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d244'),
                'email' => 'stark@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Tony',
                'lastName' => 'Stark',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d255'),
                'email' => 'kingofthenorth@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Jhon',
                'lastName' => 'Snow',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d266'),
                'email' => 'nintendo@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Zelda',
                'lastName' => 'Willians',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d277'),
                'email' => 'nintendo@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Mario',
                'lastName' => 'Bros',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d288'),
                'email' => 'lotr@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Frodo',
                'lastName' => 'Baggins',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d299'),
                'email' => 'sw@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Luke',
                'lastName' => 'Skywalker',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'following' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d200'),
                'email' => 'sw@acme.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1985-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => 'Anakin',
                'lastName' => 'Skywalker',
                'agreement' => 'true',
                'followers' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'following' => [
                    ['id' => '58dd246096fbde008624d261', 'date' => $this->today],
                ],
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ],
            [
                '_id' => new \MongoId('58dd246096fbde008624d243'),
                'email' => 'api@acme2.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1993-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
                'agreement' => 'true',
                'createdAt' => $this->today,
                'gender' => 'f',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 2', 2),
                'userPhoto' => $this->avatarRandom()
            ]
        ];

        $var[] =
            [
                '_id' => new \MongoId('58dd246096fbde008624d281'),
                'name' => 'user two',
                'email' => 'utwo@acme.com.br',
                'password' => md5('123456'),
                'association' => 'individual',
                'birthDate' => '1993-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
                'agreement' => 'true',
                'createdAt' => $this->today,
                'gender' => 'm',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 1', 1),
                'userPhoto' => $this->avatarRandom()
            ];
        $var[] =
            [
                '_id' => new \MongoId('58dd246096fbde008624d283'),
                'name' => 'user two',
                'email' => 'uthree@acme2.com.br',
                'password' => md5('123456'),
                'association' => 'classroom',
                 
                'birthDate' => '1993-10-10',
                'community' => ['animation','Engineering'],
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
                'agreement' => 'true',
                'createdAt' => $this->today,
                'gender' => 'f',
                'phone' => '3224400088',
                'profile' => 'Student',
                'level' => $this->getLevel('Student Level 2', 2),
                'userPhoto' => $this->avatarRandom()
            ];

        for ($i= 1; $i <= 5; $i++) {
            $var[] = $this->getUser();
        }

        foreach ($var as $user) {
            $collection->insert($user);
        }

        foreach ($var2 as $user2) {
            $collection->insert($user2);
        }
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $collection->clearDocumentPool();
    }

    public function getUser()
    {
        return [
            '_id' => new \MongoId(),
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => md5('123456'),
            'association' => 'classroom',

            'birthDate' => '1993-10-10',
            'community' => ['animation','Engineering'],
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'agreement' => 'true',
            'createdAt' => $this->today,
            'gender' => 'f',
            'phone' => '3224400088',
            'profile' => 'Student',
            'level' => $this->getLevel('Student Level 2', 2),
            'userPhoto' => $this->avatarRandom()
        ];
    }

    public function getLevel($level, $number)
    {
        return [
            'date' => $this->today,
            'alias' => $level,
            'number' => $number
        ];
    }
}