<?php

class FixedComments extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('comment');

        $works = $this->getWorksId();

        $i = 1;
        foreach ($works as $work) {
            $comment = $this->buildComment($work, $i);
            $collection->insert($comment);
            $i++;
        }
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('comment');

        $collection->clearDocumentPool();
    }



    public function getWorksId()
    {
        $workId = [
            '58ebe2ebd5e73800542e8801',
            '58ebe2ebd5e73800542e8802',
            '58ebe2ebd5e73800542e8803',
        ];
        return $workId;
    }

    public function buildComment($workId, $i)
    {
        $number = $this->faker->randomElement(
            $array = array (
                1, 2, 3, 4, 5, 6, 7, 8
            )
        );

        $user = '58dd246096fbde008624d2'.$number.$number;
        $cid = '58dd246096fbde118624d93'.$i;
        return [
            '_id' => new \MongoId($cid),
            'type' =>  'work',
            'productId' => new \MongoId($workId),
            'comment' => $this->faker->text(125),
            'userId'  =>  new \MongoId($user),
            'created_at' => $this->today,
        ];
    }
}