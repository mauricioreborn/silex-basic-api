<?php

class Community extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('communities');

        $var = array([
            'name' => 'Animation',
            'description' => 'character design, drawing and CGI',
            'order' => '1',
        ],[
            'name' => 'Engineering',
            'description' => 'drafting,mechanics, mechatronics',
            'order' => '2',
        ],[
            'name' => 'Law',
            'description' => 'analysis, rights, public speaking',
            'order' => '3',
        ]);

        foreach ($var as $community) {
            $collection->insert($community);
        }
    }
    
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('communities');

        $collection->clearDocumentPool();
    }
}