<?php

require_once __DIR__ . '/../../vendor/fzaninotto/faker/src/autoload.php';

class LiveSession extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/lv';
    public function up()
    {
        date_default_timezone_set('UTC');
        $this->faker = Faker\Factory::create();
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

        $users = $this->getUser();
        foreach ($users as $user) {
            $collection->insert($this->getLiveSession($user, array_rand($users, 1)));
        }
    }
    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

        $collection->clearDocumentPool();
    }

    public function getLiveSession($speaker, $moderator)
    {
        return  [
            'name' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'description' => $this->faker->text(200),
            'type' => $this->faker->randomElement($array = array ('private','public')),
            'date' => $this->getDate(),
            'startTime' => $this->faker->time($format = 'h:i', $max = 'now').' '.strtoupper($this->faker->amPm($max = 'now')),
            'endTime' => $this->faker->time($format = 'h:i', $max = 'now').' '.strtoupper($this->faker->amPm($max = 'now')),
            'highLight' => $this->faker->randomElement($array = array (true, false)),
            'speaker' => new \MongoId($speaker),
            'moderator' => new \MongoId($moderator),
            'link' => $this->faker->url,
            'users' => $this->getUser(),
            'created_at' => $this->getDate(),
            'thumbnail' => $this->path.random_int(1, 5).'.png',
        ];
    }

    public function getUser()
    {
        return [
                '58dd246096fbde008624d211',
                '58dd246096fbde008624d222',
                '58dd246096fbde008624d233',
                '58dd246096fbde008624d244',
                '58dd246096fbde008624d255',
                '58dd246096fbde008624d266',
                '58dd246096fbde008624d277',
                '58dd246096fbde008624d288',
                '58dd246096fbde008624d299',
                '58dd246096fbde008624d200'
        ];
    }

    public function getDate()
    {
        $dt = $this->faker->dateTimeBetween(
            $startDate = '-10 days',
            $endDate = 'now',
            date_default_timezone_get()
        );
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        return $this->today;
    }

}
