<?php

class CheckStatusUser extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('status', true);
        });
    }
}