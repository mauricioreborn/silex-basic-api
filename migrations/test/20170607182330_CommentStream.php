<?php

class CommentStream extends \Sokil\Mongo\Migrator\AbstractMigration
{
    private $faker;
    private $today;
    private $file;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $this->file = '58d91aff96fbde005a429db2/121f1c7768bfe4786c68ef8b0f0b0aa9.png';
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('comment');

        for ($i =0; $i<5; $i++) {
            $comment = $this->buildComment($i);
            $collection->insert($comment);
        }
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('comment');

        $collection->clearDocumentPool();
    }



    public function buildComment($i)
    {
        $user = '58dd246096fbde008624d21'.$this->faker->randomElement(
                $array = array (
                    1, 2, 3, 4, 5, 6, 7, 8
                )
            );

        $postId = $ponstId = "59243799c1fe0100a66e0bd".$i;

        return [
            'type' =>  'stream',
            'productId' => new \MongoId($postId),
            'comment' => $this->faker->text(125),
            'userId'  =>  new \MongoId($postId),
            'created_at' => $this->today,
        ];
    }
}