<?php

class ClassRoom extends \Sokil\Mongo\Migrator\AbstractMigration
{
    protected $faker;
    public $path = 'https://s3.amazonaws.com/s3-acme/demo/';
    public $today;

    public function up()
    {
        $this->faker = Faker\Factory::create();
        $dt = new DateTime(date('Y-m-d'), new DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new MongoDate($ts);
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('classroom');

        for ($i =0; $i<5; $i++) {
            $collection->insert($this->getLab($i));
        }
    }

    public function down()
    {
        {
            $collection = $this
                ->getDatabase(getenv('DB_DATABASE'))
                ->getCollection('classroom');

            $collection->clearDocumentPool();
        }
    }

    public function getLab($i)
    {
        $lab = '';
        if ($i === 1) {
            for ($increment = 0; $increment<=5; $increment++) {
                $lab[] = $this->getStreamLab($increment);
            }
        }

        $newId = "5914b887753bd902b32a779".$i;
        return [
            '_id' => new \MongoId($newId),
            'title' => $this->faker->sentence(3, true),
            'description' => $this->faker->text(200),
            'userId' => new \MongoId('590820c0753bd9005006c931'),
            'coverImage' => $this->path.random_int(1, 8).'.png',
            'uploaded_at' => $this->today,
            'posts' => $lab
        ];
    }

    public function getStreamLab($i)
    {
        return "49243799c1fe0100a49e0bd".$i;
    }
}