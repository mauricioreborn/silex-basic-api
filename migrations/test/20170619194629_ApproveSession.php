<?php

class ApproveSession extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('session');

        // create new field in all documents of collection
        $collection->updateAll(function ($operator) {
            $operator->set('approved', true);
            $operator->set('moderator', new \MongoId('590820c0753bd9005006c937'));
        });
    }
}