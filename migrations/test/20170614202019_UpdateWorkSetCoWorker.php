<?php

class UpdateWorkSetCoWorker extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('works');
        $works = $this->getWork();

        foreach ($works as $work) {
            $work = $collection->getDocument(new \MongoId($work));
            if ($work) {
                $work->addToSet('coWork', '58dd246096fbde008624d211');
                $work->addToSet('coWork', '58dd246096fbde008624d222');
                $work->addToSet('coWork', '58dd246096fbde008624d233');
                $work->save();
            }
        }
    }
    
    public function down()
    {
        
    }


    public function getWork()
    {
        $workid = [
            '58ebe2ebd5e73800542e8801',
            '58ebe2ebd5e73800542e8802',
            '58ebe2ebd5e73800542e8803',
            '58ebe2ebd5e73800542e8804',
            '58ebe2ebd5e73800542e8805',
            '58ebe2ebd5e73800542e8806',
        ];

        return $workid;
    }
}