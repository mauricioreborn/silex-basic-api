<?php

class Categories extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('categories');

        $var = array(
            [
                'label' => 'Animatics',
                'alias' => 'animatics'
            ],
            [
                'label' => 'Animation',
                'alias' => 'animation'
            ],
            [
                'label' => 'Background',
                'alias' => 'Background'
            ],
            [
                'label' => 'CGI',
                'alias' => 'cgi'
            ],
            [
                'label' => 'Character Design',
                'alias' => 'characterDesign'
            ],
            [
                'label' => 'Drawing',
                'alias' => 'drawing'
            ],
            [
                'label' => 'Layout',
                'alias' => 'layout'
            ],
            [
                'label' => 'Viz Dev',
                'alias' => 'vizDev'
            ],
            [
                'label' => 'All',
                'alias' => 'allCategories'
            ],
        );

        foreach ($var as $profile) {
            $collection->insert($profile);
        }
    }
}