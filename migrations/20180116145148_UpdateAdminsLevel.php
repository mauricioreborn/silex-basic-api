<?php

class UpdateAdminsLevel extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');
        $admins = $collection->find()->where('level.alias', 'Admin');

        foreach ($admins as $admin) {
            $admin->set('level.alias', 'admin');
            $admin->save();
        }
    }

    public function down()
    {
        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('users');

        $collection->clearDocumentPool();
    }
}