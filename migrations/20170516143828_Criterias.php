<?php

class Criterias extends \Sokil\Mongo\Migrator\AbstractMigration
{
    public function up()
    {
        $criterias =[
            [
                '_id' => new \MongoId(),
                'criteria' => 1,
                'title' => 'SQUASH AND STRETCH',
                'description' => 'The first element of animation to master is Squash and Stretch. Living things and many objects change shape when they move or act. Making slight changes, drawing to drawing, will convey the illusion of weight and volume to your animation. Depending on the animation style you are using, too much or too litle Squash and Stretch will enhance or detract from your scene.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 2,
                'title' => 'ANTICIPATION',
                'description' => 'The element of Anticipation allows your audience to prepare for any action that is about to occur. Without a smaller movement, expression or gesture to set up the next movement, the audience is likely to miss it and become confused because you have not created Anticipation, or an expectation that it will happen. Slight backwards movement before a run or jump is one example.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 3,
                'title' => 'STAGING',
                'description' => 'Staging is all the visual choices you make to help the audience see your animation clearly and to follow along with your story. Like a stage design for a theatrical production, staging conveys the setting, mood, and time, so the action is clear and understood. Design elements such as lightiing, texture, line quality, proportion, perspective, symbols, shapes, colors are all important Staging considerations. [Tip: Plan your staging with thumbnail drawings!]',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 4,
                'title' => 'STRAIGHT AHEAD ANIMATION AND POSE TO POSE ANIMATION',
                'description' => 'Deciding which of these animation processes to use in a scene is important for planning, clarity, appeal, vaiety of movement, timing and efficient use of your time as an animator. Straight Ahead Animation starts with your first drawing and continues until your scene. These poses become "Key Drawings" or "Key Frames" that can be "inbetweened" later. Generally, Pose to Pose gives more control to volume, shape and timing of the movement. Straight Ahead is more spontaneous and free-flowing.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 5,
                'title' => 'FOLLOW THROUGH AND OVERLAPPING ANIMATION',
                'description' => 'Watch the natural movement of a character or object in a scene when it stops or changes direction. Notice that its different parts stop at different times. Capes, dresses, long hair, floppy ears, long tails (and even different parts of the main body itself) continue on the path of action until the momentum ends or changes direction. This is called Follow Through. Such elements Overlap at times when the main body of your character or object changes direction, and Follow Through a few frames later.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 6,
                'title' => 'SLOW IN AND SLOW OUT',
                'description' => 'Most animation has many actions, point-of-view, themes, angles, etc. It is important that your audience have enough time to percieve the changes you have planned. Slow In and Slow Out is the principle of placing inbetween frames from scene to scene and action to action, so the audience can locate the action. Sometimes, it is not wise to Slow In and Slow Out between scenes, such as following an action through a scene change, and you want your character to "snap" into movement.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 7,
                'title' => 'ARCS',
                'description' => 'All natural movement occurs on curved, or circular paths of motion. Notice humans, animals or even tree branches blowing in the wind. Contrast these with mechanical/robot kinds of movement. Use the principle of Arcs and your animations will look more natural to your audience.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 8,
                'title' => 'TIMING',
                'description' => 'Knowing when to add (or not to add) drawings (inbetweens) in a scene takes practice. It is called Timing. Quick movement requires fewer drawings and slow movement require more. But it is not only fast or slow. Timing is also speeding up or slowing down-fewer drawings as your character or object accelerates, more as it decelerates.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 9,
                'title' => 'SECONDARY ACTION',
                'description' => 'Secondary Action adds dimension to your animation by adding a second level of movement or opposing movement. For example, a walking bird...as she walks, her head swings forwars...The head movement is Secondary Action. The Primary Action of running controls the Secondary Action.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 10,
                'title' => 'EXAGGERATION',
                'description' => 'Animators are artists, so they characterize reality in a dynamic way. Like novelists or poets, who use language powerfully to bring out the essence of reality and not to copy it, animators use the strength of visuals for this kind of emphasis Exaggeration, or "Pushing" a pose, action, expression or attitude for emphasis at key moments-and not the point of distortion-makes animation compelling and clear.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 11,
                'title' => 'SOLID DRAWING',
                'description' => 'Animation is not just pretty pictures. Whatever look it has, it must be believable. Solid Drawing makes the audience believe that the physical forces of a 3-dimensional world arre actually acting on your character or object. When you draw, believe that your objects and characters exist in a 3-dimensional world.',
            ],
            [
                '_id' => new \MongoId(),
                'criteria' => 12,
                'title' => 'APPEAL',
                'description' => 'Appeal takes Solid Drawing farther. Are your drawings and movements pleasing to the audience? Are your characters and stories compelling? Have you used the visual and storytelling principles to show something that people want to watch?',
            ]

        ];

        $collection = $this
            ->getDatabase(getenv('DB_DATABASE'))
            ->getCollection('criterias');

        foreach ($criterias as $crit) {
            $collection->insert($crit);
        }
    }

    public function down()
    {
        {
            $collection = $this
                ->getDatabase(getenv('DB_DATABASE'))
                ->getCollection('criterias');

            $collection->clearDocumentPool();
        }
    }


}