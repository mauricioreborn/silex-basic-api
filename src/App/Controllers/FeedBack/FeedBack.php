<?php

namespace App\Controllers\FeedBack;

use App\Controller;
use App\Entities\Work;
use App\Notification\NotificationQuerys;
use App\Notification\Reviews;
use App\Services\FeedBack\FeedBackService;
use App\Services\NotificationService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use App\Services\UserService;
use App\Services\Community\CommunityService;
use Symfony\Component\HttpFoundation\Request;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class FeedBack extends Controller
{

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/work/feedback/{workid}")
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/work/feedback/{workid}/{pro}")
     * ),
     * @SWG\Get(
     *     path="/work/feedback/{workid}",
     *     summary="Fetch Work by Id With Criterias to give FeedBack",
     *     tags={"feedback"},
     *     description="Fetch Work by Id With Criterias to give FeedBack",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="workid",
     *         in="query",
     *         description="workid",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function giveFeedBack(Request $request)
    {
        $data['workId'] = $request->get('workid');
        $data['type'] = $request->get('pro');
        $feedService = new FeedBackService();
        $feedService->getFeedBack($this->getEntityManager(), $data);
        return $this->responseJson($feedService->getMessage(), $feedService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/work/feedback/save")
     * ),
     * @SWG\Post(
     *     path="/work/feedback/save",
     *     summary="Save Criterias to give FeedBack",
     *     tags={"feedback"},
     *     description="Save Criterias to give FeedBack",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="criterias",
     *         in="formData",
     *         description="criterias as array with only the criteria number inside",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="workId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="feedBackMessage",
     *         in="formData",
     *         description="feedBackMessage",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function saveFeedBack(Request $request)
    {
        $notificationQuery = new NotificationQuerys();
        $data['postId'] = $request->get('workId');
        $data['owner'] = $notificationQuery->getUserIdOwnerId(
            $this->getEntityManager()->createQueryBuilder(Work::class),
            $data['postId']
        );


        $token = $this->getToken($request->headers->get('token'));
        $feedService = new FeedBackService();
        $feedService->saveFeedBack(
            $this->getEntityManager(),
            $request->get('workId'),
            $request->get('criterias'),
            $request->get('feedBackMessage', null),
            $token->getClaim('context')->user_id,
            $data['owner']
        );

        $data['postId'] = $request->get('workId');
        $data['userId'] = $token->getClaim('context')->user_id;


        $notificationService = new NotificationService(new Reviews());
        $notification = $notificationService->create($data);
        $notificationService->save(
            $this->getEntityManager(),
            $notification,
            $data
        );

        return $this->responseJson($feedService->getMessage(), $feedService->getStatus());
    }
}
