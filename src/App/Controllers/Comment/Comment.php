<?php

namespace App\Controllers\Comment;

use App\Controller;
use App\Entities\Comments;
use App\Entities\ReplyComment;
use App\Services\Comment\CommentService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */

class Comment extends Controller
{


    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/comment/productId/{productId}")
     * ),
     * @SWG\Get(
     *     path="/comment/productId/{productId}",
     *     summary="Fetch token to enter session by userid and sessionid",
     *     tags={"Comment"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="productId",
     *         in="path",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="query",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit of comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         in="query",
     *         description="skip comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="sort a comment by files (type,created_at,_id)",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getComments(Request $request)
    {
        $data = $request->query->all();
        $data['productId'] = $request->get('productId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;


        $commentService = new CommentService();
        $commentService->getComment(
            $this->getEntityManager()->createQueryBuilder(Comments::class),
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($commentService->getMessage(), $commentService->getStatus());
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/comment/user/{userId}")
     * ),
     * @SWG\Get(
     *     path="/comment/user/{userId}",
     *     summary="Fetch token to enter session by userid and sessionid",
     *     tags={"Comment"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="query",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit of comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         in="query",
     *         description="skip comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="sort a comment by files (type,created_at,_id)",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getUserComments(Request $request)
    {
        $data['userId'] = $request->get('userId');
        $skip = $request->get('skip');
        $limit = $request->get('limit');
        $commentService = new CommentService();
        $commentService->getUserComment(
            $this->getEntityManager()->createQueryBuilder(Comments::class),
            $this->getEntityManager(),
            $data
        );
        $comments = array_slice($commentService->getMessage(), (int)$skip, (int)$limit);
        return $this->responseJson(['message' => $comments], $commentService->getStatus());
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/reply/comment/{commentId}")
     * ),
     * @SWG\Get(
     *     path="/reply/comment/{commentId}",
     *     summary="get replys comments by commentId",
     *     tags={"Comment"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="commentId",
     *         in="path",
     *         description="id of the original comment",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="query",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit of comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         in="query",
     *         description="skip comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="sort a comment by files (type,created_at,_id)",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getReplyComments(Request $request)
    {
        $data = $request->query->all();
        $data['commentId'] = $request->get('commentId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $commentService = new CommentService();
        $commentService->getComment(
            $this->getEntityManager()->createQueryBuilder(ReplyComment::class),
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($commentService->getMessage(), $commentService->getStatus());
    }
}