<?php

namespace App\Controllers\Groups;

use App\Entities\Work;
use App\Repository\Group\LabRepository;
use App\Services\Commom\UploadPhoto;
use App\Services\Groups\GroupService;
use App\Services\Groups\LabService;
use App\Entities\Lab as LabEntity;
use App\Services\SignupService;
use App\Validators\Group\LabValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Lab extends Group
{
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/lab/new")
     * ),
     * @SWG\Post(
     *     path="/lab/new",
     *     summary="add a new lab",
     *     tags={"Groups"},
     *     description="Upload a New Lab on Group",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="work title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function lab(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['title'] = ucfirst($request->request->get('title'));
        $data['educatorEmail'] = $request->request->get('educatorEmail', 'empty');
        $data['schoolId'] =  $request->request->get('schoolId', null);
        $data['description'] = $request->request->get('description', 'pending');
        $data['proEmail'] = $request->request->get('proEmail', 'empty');
        $data['type'] = 'lab';
        
        $labValidator = new LabValidator();
        if ($labValidator->validateLabInput($data, $this->getApplication())) {
            $data['userId'] = $token->getClaim('context')->user_id;
            $labService = new LabService();
            $labId = $labService->addLab($data, $this->getEntityManager());

            $groupService = new GroupService();
            $groupService->incrementNewMember(
                $this->getEntityManager()->createQueryBuilder(\App\Entities\ClassRoom::class),
                $labId
            );

            return $this->responseJson($labService->getMessage(), $labService->getStatus());
        }
        return $this->responseJson($labValidator->getErrorMessage(), 400);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/upload/cover")
     * ),
     * @SWG\Post(
     *     path="/group/upload/cover",
     *     summary="Upload Lab Cover",
     *     tags={"Groups"},
     *     description="Upload Lab Cover",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="coverImage",
     *         in="formData",
     *         description="Lab Cover Image as IMAGE",
     *         required=true,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="groupId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="type lab or classroom LOWERCASE",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function uploadCover(Request $request)
    {
        $folder = $request->request->get('groupId').'/cover';
        $userPhoto = $request->files->get('coverImage');
        $photoUpload = new UploadPhoto($this->getApplication(), $folder, $request->request->get('type'));
        $photo = $photoUpload->upload($userPhoto);
        $groupService = new GroupService();
        $response = $groupService->updateCover(
            $this->getEntityManager(),
            [
                'labId' => $request->request->get('groupId'),
                'url' => $photo,
                'type' => $request->request->get('type')
            ]
        );
        if ($response === true) {
            return $this->responseJson(['message' => $photo], $groupService->getStatus());
        }
        return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/group/lab"),
     * )
     * @SWG\Get(
     *     path="/group/lab",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Method to get lab only",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getLab(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 15);
        $data['skip'] = $request->get('skip');
        $data['skip'] = $this->calculateSkip($data);
        $data['sort'] = $request->get('sort');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $labService = new LabService();
        $labRepository = new LabRepository($this->getEntityManager());

        if ($token->getClaim('context')->level === 'admin'
            || $token->getClaim('context')->level === 'pro'
        ) {
            $labService->getLabAdmin($data, $labRepository);
        } else {
            $labService->getLab($data, $this->getEntityManager());
        }
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/lab/join")
     * ),
     * @SWG\Post(
     *     path="/group/lab/join",
     *     summary="method to participate of the lab",
     *     tags={"Groups"},
     *     description="method to participate of the lab",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="labId",
     *         in="formData",
     *         description="work title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function join(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $level = $token->getClaim('context')->level;
        if ($level == 'pro' || $level == 'Student Level 1') {
            $labService = new LabService();
            $labService->joinLab($data, $this->getEntityManager());
            return $this->responseJson($labService->getMessage(), $labService->getStatus());
        }
        return $this->responseJson("the user can't add a user", 400);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/group/lab/sidebar/{groupId}")
     * ),
     * @SWG\Get(
     *     path="/group/lab/sidebar/{groupId}",
     *     summary="add a new lab",
     *     tags={"Groups"},
     *     description="Upload a New Lab on Group",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="path",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getSideBar(Request $request)
    {
        $groupId = $request->get('groupId');
        $sideBar = $this->buildSideBar(
            $this->getEntityManager()->createQueryBuilder(\App\Entities\Lab::class),
            $groupId
        );
        if (empty($sideBar)) {
            return $this->responseJson(['message' => 'Lab Not Found'], 400);
        }
        return $this->responseJson($sideBar, 200);
    }
    
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/lab/task")
     * ),
     * @SWG\Post(
     *     path="/group/lab/task",
     *     summary="add a new task on lab",
     *     tags={"Groups"},
     *     description="Upload a New task on lab",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="labId",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="dueDate",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function createTask(Request $request)
    {
        $data = $request->request->all();

        $labValidator = new LabValidator();
        if ($labValidator->validateTask($data, $this->getApplication())) {
            $token = $this->getToken($request->headers->get('token'));
            $data['userId'] = $token->getClaim('context')->user_id;
            $labService = new LabService();
            $labService->createTask($this->getEntityManager(), $data);
            return $this->responseJson($labService->getMessage(), $labService->getStatus());
        }

        return $this->responseJson($labValidator->getErrorMessage(), 401);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="group/{groupId}/roster"),
     * )
     * @SWG\Get(
     *     path="group/{groupId}/roster",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="groupId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="userName",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         type="string",
     *         in="query",
     *         description="limit ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *        @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         type="string",
     *         in="query",
     *         description="page ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getRoster(Request $request)
    {
        $groupService = new GroupService();
        $data['page'] = $request->get('page');
        $data['groupId'] = $request->get('groupId');
        $data['limit'] = $request->get('limit', 25);
        $data['userName'] = $request->get('userName');
        $data['orderBy'] = $request->get('sort');
        $data['skip'] = $this->calculateSkip($data);


            $userList = $groupService->checkClassCode($this->getEntityManager(), $data);
        if ($userList) {
            $feed = $this->buildRooster(
                $this->getEntityManager(),
                $userList,
                $data
            );
            return $this->responseJson($feed, 200);
        }
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }


    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="lab/{labId}/task"),
     * )
     * @SWG\Get(
     *     path="lab/{labId}/task",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="lab",
     *     description="get lab tasks",
     *     @SWG\Parameter(
     *         name="labId",
     *         type="string",
     *         in="path",
     *         description="labId ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getTasks(Request $request)
    {
        $data['labId'] = $request->get('labId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $labService = new LabService();
        $labService->getTasks($this->getEntityManager(), $data);
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/lab/turnin/task")
     * ),
     * @SWG\Post(
     *     path="/lab/turnin/task",
     *     summary="turnin task",
     *     tags={"Groups"},
     *     description="turnin task",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="labId",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="taskId",
     *         in="formData",
     *         description="taskId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function turninTask(Request $request)
    {
        $turnInInfo = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $turnInInfo['userId'] = $token->getClaim('context')->user_id;

        $labValidator = new LabValidator();

        if ($labValidator->validateTurnin($turnInInfo, $this->getApplication())) {
            $labService = new LabService();
            $labService->turnIn($this->getEntityManager(), $turnInInfo);
            return $this->responseJson($labService->getMessage(), $labService->getStatus());
        }

        return $this->responseJson($labValidator->getErrorMessage(), $labValidator->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="lab/{labId}/{taskId}/submissions"),
     * )
     * @SWG\Get(
     *     path="lab/{labId}/{taskId}/submissions",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="c submissions",
     *     description="c submissions",
     *     @SWG\Parameter(
     *         name="labId",
     *         type="string",
     *         in="path",
     *         description="labId ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="taskId",
     *         type="string",
     *         in="path",
     *         description="taskId ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function seeSubmissions(Request $request)
    {
        $data = $request->attributes->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $labService = new LabService();
        $labService->seeSubmissions($this->getEntityManager(), $data);
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/update")
     * ),
     * @SWG\Post(
     *     path="/group/update",
     *     summary="update a group",
     *     tags={"Groups"},
     *     description="update a group ",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="groupId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="formData",
     *         description="type lab or classroom lowercase",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateLab(Request $request)
    {
        $data = $request->request->all();
        $labService = new LabService();
        $labService->updateLab($data, $this->getEntityManager());
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

    /**
     * Method to delete an embedded task
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/group/lab/task/{taskId}")
     * ),
     * @SWG\Delete(
     *     path="/group/lab/task/{taskId}",
     *     summary="Delete a task by their Id",
     *     tags={"Lab"},
     *     description="Delete post by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="taskId",
     *         in="path",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="type if lab or classroom",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function removeTask(Request $request)
    {
        $taskId = $request->get('taskId');
        $groupService = new GroupService();
        $groupService->removeTask(
            $this->getEntityManager(),
            $taskId
        );
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }


    /**
     * Method to edit a task
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/group/lab/edit/task/{taskId}")
     * ),
     * @SWG\Post(
     *     path="/group/lab/edit/task/{taskId}",
     *     summary="edit a task",
     *     tags={"Lab"},
     *     description="Upload a New task on lab",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="dueDate",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function editTask(Request $request)
    {
        $data = $request->request->all();
        $data['taskId'] =  $taskId = $request->get('taskId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $groupService = new GroupService();
        $groupService->editTask($this->getEntityManager(), $data);
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }
}