<?php

namespace App\Controllers\Groups;


use App\Entities\Work;
use App\Entities\User;
use App\Repository\Group\ClassRoomRepository;
use App\Repository\Group\LabRepository;
use App\Services\Groups\ClassRoomService;
use App\Services\Groups\GroupService;
use App\Validators\Group\LabValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Classroom extends Group
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="classroom/new")
     * ),
     * @SWG\Post(
     *     path="classroom/new",
     *     summary="add a new classroom",
     *     tags={"Groups"},
     *     description="Upload a New classroom on Group",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="work title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="coverImage",
     *         type="string",
     *         in="formData",
     *         description="coverImage URL from s3",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function classRoom(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $labValidator = new LabValidator();
        if ($labValidator->validateLabInput($data, $this->getApplication())) {
            $data['userId'] = $token->getClaim('context')->user_id;
            $ClassRoomService = new ClassRoomService();
            $classId = $ClassRoomService->addClass($data, $this->getEntityManager());
            $groupService = new LabRepository($this->getEntityManager());
            $groupService->updateCount($classId, 'classroom');
            return $this->responseJson(['message' => $ClassRoomService->getMessage()], $ClassRoomService->getStatus());
        }
        return $this->responseJson($labValidator->getErrorMessage(), 400);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="group/classroom"),
     * )
     * @SWG\Get(
     *     path="/group/classroom",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getClass(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 15);
        $data['skip'] = $request->get('skip');
        $data['skip'] = $this->calculateSkip($data);
        $data['sort'] = $request->get('sort');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $ClassRoomService = new ClassRoomService();
        $ClassRoomRepository = new ClassRoomRepository($this->getEntityManager());

        if ($token->getClaim('context')->level === 'admin'
            || $token->getClaim('context')->level === 'pro'
        ) {
            $ClassRoomService->getClassAdmin($data, $ClassRoomRepository);
        } else {
            $ClassRoomService->getClass($data, $this->getEntityManager());
        }
        return $this->responseJson($ClassRoomService->getMessage(), $ClassRoomService->getStatus());
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/group/classroom/sidebar/{groupId}")
     * ),
     * @SWG\Get(
     *     path="/group/classroom/sidebar/{groupId}",
     *     summary="fetch sidebar data for classroom",
     *     tags={"Groups"},
     *     description="fetch sidebar data for classroom",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="path",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getSideBar(Request $request)
    {
        $groupId = $request->get('groupId');

        $groupService = new GroupService();
        $totalMembers = $groupService->getTotalMembers(
            $this->getEntityManager()->createQueryBuilder(User::class),
            $groupId
        );

        $sideBar = $this->buildSideBar(
            $this->getEntityManager()->createQueryBuilder(\App\Entities\ClassRoom::class),
            $groupId
        );

        if (empty($sideBar)) {
            return $this->responseJson(['message' => 'Classroom Not Found'], 400);
        }

        $sideBar['totalMembers'] = $totalMembers;

        return $this->responseJson($sideBar, 200);
    }

    
    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="group/classroom/{groupId}/roster"),
     * )
     * @SWG\Get(
     *     path="group/classroom/{groupId}/roster",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="get classroom users by id ",
     *     @SWG\Parameter(
     *         name="groupId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="userName",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         type="string",
     *         in="query",
     *         description="limit ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *        @SWG\Parameter(
     *         name="sort",
     *         type="string",
     *         in="query",
     *         description="token ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getRoster(Request $request)
    {
        $groupService = new GroupService();
        $data['groupId'] = $request->get('groupId');
        $data['skip'] = $request->get('skip', 0);
        $data['limit'] = $request->get('limit', 25);
        $data['userName'] = $request->get('userName');
        $data['orderBy'] = $request->get('sort');

        $userList = $groupService->checkClassCode($this->getEntityManager(), $data);
        if ($userList) {
            $feed = $this->buildRooster(
                $this->getEntityManager(),
                $userList,
                $data
            );
            return $this->responseJson($feed, 200);
        }
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/classroom/update")
     * ),
     * @SWG\Post(
     *     path="/classroom/update",
     *     summary="update a new classroom",
     *     tags={"Groups"},
     *     description="update a New classroom",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="work title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="classId",
     *         in="formData",
     *         description="classId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="coverImage",
     *         type="string",
     *         in="formData",
     *         description="coverImage URL (utilize route specific)",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateClass(Request $request)
    {
        $data = $request->request->all();
        $labService = new ClassRoomService();
        $labService->updateClass($data, $this->getEntityManager());
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

}