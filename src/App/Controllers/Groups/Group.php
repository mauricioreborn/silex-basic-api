<?php

namespace App\Controllers\Groups;

use App\Controller;
use App\Entities\CommentLike;
use App\Entities\Lab;
use App\Entities\Post;
use App\Entities\User;
use App\Entities\Work;
use App\Notification\Group\Likes;
use App\Notification\NotificationQuerys;
use App\Repository\Group\GroupRepository;
use App\Services\Commom\UploadPhoto;
use App\Services\Groups\GroupService;
use App\Services\NotificationService;
use App\Services\SignupService;
use App\Services\UserService;
use App\Services\Work\CommentLikeService;
use App\Services\Work\LikeService;
use App\Validators\Group\LabValidator;
use App\Validators\Work\LikeValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use App\Notification\Group\Comments;


/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Group extends Controller
{

    /**
     * Method to build a classroom or lab sideBar
     * @param $queryBuilder
     * @param $id Lab or Classroom
     * @return object
     */
    protected function buildSideBar($queryBuilder, $groupId)
    {
        $groupService = new GroupService();
        $sideBar = $groupService->buildSideBar($queryBuilder, $groupId);
        if (count($sideBar)>0) {
            $user = $groupService->buildUser(
                $this->getEntityManager()->createQueryBuilder(User::class),
                $sideBar['userId']
            );
            $sideBar = array_merge($sideBar, $user);
        }
        return $sideBar;
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="group/lab/{groupId}/feed"),
     *      @SLX\Request(method="GET", uri="group/classroom/{groupId}/feed"),
     * )
     * @SWG\Get(
     *     path="group/lab/{groupId}/feed",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="groupId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function buildFeed(Request $request)
    {
        $groupId = $request->get('groupId');
        $groupService = new GroupService();
        $groupService->feedBuilder(
            $this->getEntityManager(),
            $groupId
        );
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }

    /** Method to build a rooster
     * @param $workBuilder
     * @param $user
     * @param $filters
     * @return array
     */
    public function buildRooster($entityManager, $user)
    {
        $groupService = new GroupService();
        $groupFeed = $groupService->buildRooster($user, $entityManager);
        return $groupFeed;
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/group/all"),
     * )
     * @SWG\Get(
     *     path="/group/all",
     *     tags={"Groups"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Method to get lab only",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getLabAndClass(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 15);
        $data['skip'] = $request->get('skip');
        $data['skip'] = $this->calculateSkip($data);
        $data['sort'] = $request->get('sort');

        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $labService = new GroupService();
        $groupRepository = new GroupRepository($this->getEntityManager());
        if ($token->getClaim('context')->level === 'admin'
            || $token->getClaim('context')->level === 'pro'
        ) {
            $labService->getLabAndClassAdmin($data, $groupRepository);
        } else {
            $labService->getLabAndClass($data, $this->getEntityManager());
        }
        return $this->responseJson($labService->getMessage(), $labService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/stream/post")
     * ),
     * @SWG\Post(
     *     path="/group/stream/post",
     *     summary="add a new publication on lab",
     *     tags={"Groups"},
     *     description="Upload a New publication on lab",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="publication",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addPostOnLab(Request $request)
    {
        $data = $request->request->all();

        $labValidator = new LabValidator();
        if ($labValidator->validatePostInput($data, $this->getApplication())) {
            $token = $this->getToken($request->headers->get('token'));
            $data['userId'] = $token->getClaim('context')->user_id;
            $groupService = new GroupService();
            $post = $groupService->addPost($this->getEntityManager(), $data);
            return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
        }
        return $this->responseJson($labValidator->getErrorMessage(), 401);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/link/preview")
     * ),
     * @SWG\Post(
     *     path="/link/preview",
     *     summary="Request a given link details like facebook",
     *     tags={"Groups"},
     *     description="Request a given link details like facebook",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="url",
     *         in="formData",
     *         description="url",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function urlCheck(Request $request)
    {
        $array1 = array();
        $array1['url'] = $request->request->get('url');
        $check = filter_var($array1['url'], FILTER_VALIDATE_URL);
        if ($check === false) {
            return $this->responseJson(['message' => 'URL not valid'], 400);
        }
        libxml_use_internal_errors(true);
        $c = file_get_contents($array1['url']);
        $d = new \DomDocument();
        $d->loadHTML($c);
        $xp = new \domxpath($d);

        foreach ($xp->query("//meta[@property='og:title']") as $el) {
            $array1['title'] = $el->getAttribute("content");
        }
        foreach ($xp->query("//meta[@property='og:description']") as $el) {
            $array1['description'] = $el->getAttribute("content");
        }
        foreach ($xp->query("//meta[@property='og:image']") as $el) {
            $array1['image'] = $el->getAttribute("content");
        }

        return  $this->responseJson($array1, 200);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/stream/comment")
     * ),
     * @SWG\Post(
     *     path="/stream/comment",
     *     summary="Post Comment On POSTS",
     *     tags={"Groups"},
     *     description="Request a given link details like facebook",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="commentId",
     *         in="formData",
     *         description="commentId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="postId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="comment",
     *         in="formData",
     *         description="comment",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addCommentStream(Request $request)
    {
        $data = $request->request->all();
        $data['postId'] = $request->request->get('id');
        $data['type'] = 'stream';
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $groupService = new GroupService();
        if (!empty($data['commentId'])) {
            $groupService->postReplyComment($this->getEntityManager(), $data);
            return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
        }
        $groupService->postComment(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $this->getEntityManager(),
            $data
        );
        

        $notificationQuery = new NotificationQuerys();

        $data['owner'] = $notificationQuery->getUserId(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $data['postId']
        );

        $notificationService = new NotificationService(new Comments());
        $notification = $notificationService->create($data);
        $notificationService->save(
            $this->getEntityManager(),
            $notification,
            $data
        );
        return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="stream/{id}/comment/{commentId}/like")
     * ),
     * @SWG\Post(
     *     path="posts/{id}/comment/{commentId}/like",
     *     summary="add a like on comment",
     *     tags={"Stream"},
     *     description="add a like on comment",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="comment id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="productId",
     *         in="path",
     *         description="lab id or classroom id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */

    public function addLike(Request $request)
    {
        $data['commentId'] = $request->get('commentId');
        $data['productId'] = $request->get('id');
        $data['type'] = 'group';
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        if ($this->validateId($data['commentId'])) {
            $commentService = new CommentLikeService();
            if (!$commentService->verifyLike(
                $this->getEntityManager(),
                $data
            )) {
                $commentService->addLike($this->getEntityManager(), $data);
                return $this->responseJson($commentService->getMessage(), $commentService->getStatus());
            }
        }

        $notificationQuery = new NotificationQuerys();

        $data['owner'] = $notificationQuery->getUserIdOwnerId(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $data['postId']
        );


        $notificationService = new NotificationService(new Likes());
        $notification = $notificationService->create($data);
        $notificationService->save(
            $this->getEntityManager(),
            $notification,
            $data
        );


        return $this->responseJson('the user already liked this comment or This comment does not belong to work', 400);
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/join")
     * ),
     * @SWG\Post(
     *     path="/group/join",
     *     summary="method to participate of the lab or classroom",
     *     tags={"Groups"},
     *     description="method to participate of the lab",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="group id (class or lab)",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function join(Request $request)
    {
        $data = $request->request->all();

        if (\MongoId::isValid($data['groupId'])) {
            $token = $this->getToken($request->headers->get('token'));
            $data['userId'] = $token->getClaim('context')->user_id;
            $data['level'] = $token->getClaim('context')->levelNumber;

            $userService = new UserService();

            if (!$userService->verifyAlreadyJoin(
                $this->getEntityManager()->createQueryBuilder(User::class),
                $data
            )
            ) {
                $groupService = new GroupService();
                $groupService->joinGroup(
                    $this->getEntityManager(),
                    $data
                );

                return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
            }
            return $this->responseJson($userService->getMessage(), $userService->getStatus());
        }
        return $this->responseJson(['message' => 'ClassCode Invalid'], 401);
    }



    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="stream/like/{id}")
     * ),
     * @SWG\Post(
     *     path="stream/like/{id}",
     *     summary="method to add new like on post",
     *     tags={"Stream"},
     *     description="method to add new like on post",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="post id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function like(Request $request)
    {
        $data['productId'] = $request->get('id');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $groupService = new GroupService();
        if (!$groupService->findLike(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $data
        )) {
            $groupService->addLikeOnPost($this->getEntityManager(), $data);
            return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
        }
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/stream/like/{id}")
     * ),
     * @SWG\Delete(
     *     path="/stream/like/{id}",
     *     summary="remove a like",
     *     tags={"Stream"},
     *     description="Upload a New Work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="path",
     *         description="work Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function removeLike(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $likeValidator = new LikeValidator();
        $data['productId'] = $request->get('id');
        $data['userId'] = $token->getClaim('context')->user_id;
        if ($likeValidator->userLikedWorkOnPost(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $data
        )
        ) {
            $likeService  = new LikeService();
            $likeService->deleteLikeOnPost(
                $this->getEntityManager(),
                $data
            );
            $userService = new UserService();
            $userService->removePostLike($this->getEntityManager(), $data);
            return $this->responseJson($likeService->getMessage(), $likeService->getStatus());
        }
        return $this->responseJson('like not found', 400);
    }


    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/delete/group/{groupId}/{type}")
     * ),
     * @SWG\Delete(
     *     path="/delete/group/{groupId}/{type}",
     *     summary="Delete group by id",
     *     tags={"Groups"},
     *     description="Delete group by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="type if lab or classroom",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteGroup(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['groupId'] = $request->attributes->get('groupId');
        $data['type'] =  $request->attributes->get('type');
        $groupService = new GroupService();
        $groupService->deleteGroup($this->getEntityManager(), $data);
        return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/group/add/user")
     * ),
     * @SWG\Post(
     *     path="/group/add/user",
     *     summary="add a new user on group by email",
     *     tags={"Groups"},
     *     description="add a new user on group by email",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *         @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="lab or classroom as string",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addUserOnGroup(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['level'] = $token->getClaim('context')->level;
        $groupService = new GroupService();
        $groupService->addUserOnGroup($this->getEntityManager(), $data);
        return $this->responseJson(['message' => $groupService->getMessage()], $groupService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/post/{postId}")
     * ),
     * @SWG\Delete(
     *     path="/post/{postId}",
     *     summary="Delete group by id",
     *     tags={"Groups"},
     *     description="Delete post by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="postId",
     *         in="query",
     *         description="type if lab or classroom",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteStream(Request $request)
    {
        $postId = $request->get('postId');
        $groupService = new GroupService();
        $groupService->removeAPost(
            $this->getEntityManager()->createQueryBuilder(Post::class),
            $postId
        );
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }

    /**
     * Remove a user from the group
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/remove/{type}/{groupId}/{userId}")
     * ),
     * @SWG\Delete(
     *     path="/remove",
     *     summary="Delete group by id",
     *     tags={"Groups"},
     *     description="Delete post by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="postId",
     *         in="query",
     *         description="type if lab or classroom",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteRosterUser(Request $request)
    {
        $data = $request->attributes->all();
        $groupService = new GroupService();
        $groupService->removeUserRoster(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }
}