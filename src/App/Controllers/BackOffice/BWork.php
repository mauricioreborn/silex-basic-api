<?php

namespace App\Controllers\BackOffice;

use App\Controllers\BackOffice\BackOffice;
use App\Entities\Comments;
use App\Services\Comment\CommentService;
use App\Services\Work\WorkService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BWork extends BackOffice
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/select/{sessionId}/works")
     * ),
     * @SWG\Get(
     *     path="/select/session/works",
     *     summary="get all works submitted",
     *     tags={"work"},
     *     description="get all works submitted",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="query",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getSubmittedWorks(Request $request)
    {
        $data['sessionId'] = $request->attributes->get('sessionId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $workService = new WorkService();
        $workService->submittedList(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/select/session/works")
     * ),
     * @SWG\Post(
     *     path="/select/session/works",
     *     summary="Select Session Works",
     *     tags={"LiveSession"},
     *     description="Select Session Works",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="live session id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="ARRAY containing ONLY workids = [id1,id2,id3...]",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function selectWorkSession(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $workService = new WorkService();
        $workService->selectSubmittedWork(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/comment/{commentId}")
     * ),
     * @SWG\Delete(
     *     path="/comment/{commentId}",
     *     summary="delete a comment by Id",
     *     tags={"work"},
     *     description="get all filters to the discover page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="commentId",
     *         in="path",
     *         description="workId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function deleteAComment(Request $request)
    {
        $commentId = $request->get('commentId');
        $commentService = new CommentService();
        $commentService->deleteCommentById(
            $this->getEntityManager()->createQueryBuilder(Comments::class),
            $commentId
        );
        return $this->responseJson($commentService->getMessage(), 200);
    }

}