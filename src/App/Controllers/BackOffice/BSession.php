<?php

namespace App\Controllers\BackOffice;

use App\Controllers\BackOffice\BackOffice;
use App\Services\BackOffice\BOService;
use App\Services\Events\EventService;
use App\Services\UserService;
use App\Services\Work\WorkService;
use App\Validators\BackOffice\Moderator;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BSession extends BackOffice
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/fetch/sessions")
     * ),
     * @SWG\Get(
     *     path="/fetch/sessions",
     *     summary="get all session that need approval submitted",
     *     tags={"backOffice/session"},
     *     description="get all session that need approval submitted",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="month",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listSessions(Request $request)
    {
        $data['month'] = $request->attributes->get('schoolId', 'all');
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $event = new BOService();
        $event->getEventListBackOffice(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($event->getMessage(), $event->getStatus());
    }

    /**
     * Method to save sessions
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/session/update")
     * ),
     * @SWG\Post(
     *     path="/session/update",
     *     summary="Save Session By Admin",
     *     tags={"backOffice/session"},
     *     description="Approve Session By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="approved",
     *         in="formData",
     *         description="approval as approved or denied",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="speakerId",
     *         in="formData",
     *         description="moderatorId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="moderatorId",
     *         in="formData",
     *         description="moderatorId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="highlight",
     *         in="formData",
     *         description="true or false boolean",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="type",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         description="type",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Session saved, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Session not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function updateSession(Request $request)
    {
        $data = $request->request->all();
        $dt = new \DateTime(date($data['date']), new \DateTimeZone('UTC'));
        $newTime = (string)$dt->format('Y-m-d').' '.$data['startTime'];
        $d = \DateTime::createFromFormat('Y-m-d H:i A', $newTime);
        $data['date'] = $d;

        if (empty($data['moderatorId']) && $data['approved']) {
            return $this->responseJson(['message' => 'Moderator Required'], 400);
        }
        $event = new BOService();
        $event->updateSession(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($event->getMessage(), $event->getStatus());
    }

    /**
     * Method to save sessions
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/session/delete")
     * ),
     * @SWG\Post(
     *     path="/session/delete",
     *     summary="Save Session By Admin",
     *     tags={"backOffice/session"},
     *     description="Remove Session By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Session deleted, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Session not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function excludeSession(Request $request)
    {
        $data = $request->request->all();

        $event = new BOService();
        $event->excludeSession(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($event->getMessage(), $event->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/fetch/moderators")
     * ),
     * @SWG\Get(
     *     path="/fetch/moderators",
     *     summary="get all moderators",
     *     tags={"backOffice/session"},
     *     description="get all session that need approval submitted",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listAvailableModerators()
    {
        $BoService = new BOService();
        $BoService->getModeratorList(
            $this->getEntityManager()
        );
        return $this->responseJson($BoService->getMessage(), $BoService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/session/ban/list")
     * ),
     * @SWG\Get(
     *     path="/session/ban/list",
     *     summary="get all banned users",
     *     tags={"backOffice/session"},
     *     description="get all banned users",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function bannedUsers(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $data['name'] = $request->get('search', 'all');
        $data['type'] = $request->get('type', 'all');

        $BoService = new BOService();
        $BoService->bannedUsers(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($BoService->getMessage(), $BoService->getStatus());
    }

    /**
     * Method to save sessions
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/session/remove/ban")
     * ),
     * @SWG\Post(
     *     path="/session/remove/ban",
     *     summary="Save Session By Admin",
     *     tags={"backOffice/session"},
     *     description="Remove Session By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Session deleted, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Session not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function unBanUser(Request $request)
    {
        $data = $request->request->all();
        $BoService = new BOService();
        $BoService->unBanUser(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($BoService->getMessage(), $BoService->getStatus());
    }

}
