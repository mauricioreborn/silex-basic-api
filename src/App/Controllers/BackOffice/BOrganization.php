<?php

namespace App\Controllers\BackOffice;

use App\Controllers\BackOffice\BackOffice;
use App\EmailTemplate\OrganizationEmails;
use App\EmailTemplate\ProEduAccountBackOffice;
use App\Services\BackOffice\BOService;
use App\Services\SignupService;
use App\Entities\User;
use App\Validators\BackOffice\BackOffice as BOValidator;
use App\Services\Events\EventService;
use App\Services\UserService;
use App\Services\Work\WorkService;
use App\Validators\BackOffice\Moderator;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BOrganization extends BackOffice
{

    /**
     * Fetch all organizations based on the parameters sent on the query 
     * string.
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/list/organizations")
     * ),
     * @SWG\Get(
     *     path="/backoffice/list/organizations",
     *     summary="get all organizations that need approval submitted",
     *     tags={"backOffice/organization"},
     *     description="get all organizations that need approval submitted",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         in="query",
     *         description="status can be PENDING, DENIED, APPROVED, ALL",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="page",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit defaults to 25",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="orderBy defaults to DESC",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getListOrganization(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $data['status'] = $request->get('status', 'all');
        $data['search'] = $request->get('search');

        $boService = new BOService();
        $boService->getListOrganization(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/organization/approval")
     * ),
     *  @SWG\Post(
     *     path="/organization/approval",
     *     summary="Approve or Deny a organization pending request",
     *     tags={"backOffice/organization"},
     *     description="Approve or Deny a organization pending request",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="organizationId",
     *         in="formData",
     *         description="sessionId",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="approval",
     *         in="formData",
     *         description="approved or denied as STRING lowercase",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function organizationApproveDeny(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $org = $boService->organizationApproveDeny(
            $this->getEntityManager(),
            $data
        );
        $orgDetails['email'] = $boService->getMessage()['email'];
        $orgDetails['Name'] = $boService->getMessage()['firstName'];
        $orgDetails['educationalStage'] = $boService->getMessage()['lastName'];
        $orgDetails['orgName'] = $boService->getMessage()['name'];
        $orgDetails['Community'] = $boService->getMessage()['community'];
        if ($org === true) {
            $templateBuilder = new OrganizationEmails();
            $emailBuilder['title'] = 'ACME - Your '.$boService->getMessage()['message'];
            $emailBuilder['email'] = $boService->getMessage()['email'];
            $newAccount = $this->registerProEduAuto($orgDetails, 'organization', $boService->getMessage()['email']);
            $orgDetails['password'] = $newAccount['passWord'];
            $emailBuilder['message'] = $templateBuilder->getTemplateApproved($orgDetails);
            $this->sendEmail($emailBuilder);
        } else {
            $templateBuilder = new OrganizationEmails();
            $emailBuilder['title'] = 'ACME - Your '.$boService->getMessage()['message'];
            $emailBuilder['email'] = $boService->getMessage()['email'];
            $emailBuilder['message'] = $templateBuilder->getTemplateDenied($boService->getMessage()['name']);
            $this->sendEmail($emailBuilder);
        }
        return $this->responseJson(['message' => $boService->getMessage()['message']], $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/organization/details/{organizationId}")
     * ),
     * @SWG\Get(
     *     path="/organization/details/{organizationId}",
     *     summary="get all organizations details by id",
     *     tags={"backOffice/organization"},
     *     description="get all organizations details by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="organizationId",
     *         in="query",
     *         description="organizationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function organizationDetails(Request $request)
    {
        $boService = new BOService();
        $boService->organizationDetails(
            $this->getEntityManager(),
            $request->attributes->get('organizationId')
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/organization/school/new")
     * ),
     *  @SWG\Post(
     *     path="/organization/school/new",
     *     summary="Approve or Deny a organization pending request",
     *     tags={"backOffice/organization"},
     *     description="Approve or Deny a organization pending request",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="organizationId",
     *         in="formData",
     *         description="organizationId",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="schoolName",
     *         in="formData",
     *         description="schoolName",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="educatorEmail",
     *         in="formData",
     *         description="educatorEmail",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="proEmail",
     *         in="formData",
     *         description="proEmail",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="numberClassrooms",
     *         in="formData",
     *         description="numberClassrooms",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="numberStudents",
     *         in="formData",
     *         description="numberStudents",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function organizationSchoolNew(Request $request)
    {
        $data = $request->request->all();
        $validator = new BOValidator();
        if ($validator->checkSchools($this->getEntityManager(), $data['schoolName'])) {
            $boService = new BOService();
            $boService->organizationSchoolNew(
                $this->getEntityManager(),
                $data
            );
            return $this->responseJson(['message' => 'School Registered'], 200);
        }
        return $this->responseJson(['message' => 'School Already Exists'], 400);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/list/organization/schools/{organizationId}")
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/list/organization/schools")
     * ),
     * @SWG\Get(
     *     path="/list/organization/schools/{organizationId}",
     *     summary="get all schools by organization id",
     *     tags={"backOffice/organization"},
     *     description="get all schools by organization id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="organizationId",
     *         in="query",
     *         description="organizationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="page",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit defaults to 25",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="orderBy defaults to DESC",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getListOrganizationSchools(Request $request)
    {
        $data['organizationId'] = $request->attributes->get('organizationId', 'all');
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $data['search'] = $request->get('search');
        
        $boService = new BOService();
        $boService->getListOrganizationSchools(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/organization/school/details/{schoolId}")
     * ),
     * @SWG\Get(
     *     path="/organization/school/details/{schoolId}",
     *     summary="get all school details by id",
     *     tags={"backOffice/organization"},
     *     description="get all school details by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="schoolId",
     *         in="query",
     *         description="organizationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function organizationSchoolDetails(Request $request)
    {
        $boService = new BOService();
        $boService->organizationSchoolDetails(
            $this->getEntityManager(),
            $request->attributes->get('schoolId')
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/organization/school/classroom/new")
     * ),
     *  @SWG\Post(
     *     path="/organization/school/classroom/new",
     *     summary="Approve or Deny a organization pending request",
     *     tags={"backOffice/organization"},
     *     description="Approve or Deny a organization pending request",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="schoolId",
     *         in="formData",
     *         description="schoolId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="classroomName",
     *         in="formData",
     *         description="classroomName",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="educatorEmail",
     *         in="formData",
     *         description="educatorEmail",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="proEmail",
     *         in="formData",
     *         description="proEmail",
     *         required=false,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function organizationClassroomNew(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['title'] = $request->request->get('classroomName');
        $data['educatorEmail'] = $request->request->get('educatorEmail');
        $data['schoolId'] =  $request->request->get('schoolId');
        $data['description'] = $request->request->get('description', 'pending');
        $data['proEmail'] = $request->request->get('proEmail', 'empty');
        $data['type'] = 'classroom';

        $boService = new BOService();
        $validator = new BOValidator();
        if ($validator->checkSchoolsById($this->getEntityManager(), $data['schoolId'])) {
            $data['organizationId'] = $validator->getMessage();

            $users = $boService->verifyUserExist(
                $this->getEntityManager(),
                $data
            );



            $response = $users['emailEdu'] === false ? $response = $this->registerProEduAuto(
                $boService->getMessage()['orgDetails'],
                'educator',
                $data['educatorEmail']
            ) : $users['emailEdu'];


            $data['userId'] = $response;

            if (isset($response['userId'])) {

                $data['userId'] = $response['userId'];
            }


            $data['classId'] = $boService->organizationClassroomNew(
                $this->getEntityManager(),
                $data
            );

            if (isset($response['userId'])) {
                $emailSend['title'] = 'ACME - ' . $response['orgName'] . ' Created an Account for you';
                $emailSend['email'] = $response['email'];
                $emailSend['message'] = 'ACME Platform Created you an account, use the following credentials to access our platform:
            - login: ' . $response['email'] . '
            - password: ' . $response['passWord'] . '
            
            Remember to update your personal information.';
                $data['userId'] = $response['userId'];
                $this->sendEmail($emailSend);
            }
            return $this->responseJson(['message' => 'Classroom Registered'], $boService->getStatus());
        }

        return $this->responseJson(['message' => 'school not found'], $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/list/organization/schools/classrooms/{schoolId}")
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/list/classrooms")
     * ),
     * @SWG\Get(
     *     path="/list/organization/schools/classrooms/{schoolId}",
     *     summary="get all schools by organization id",
     *     tags={"backOffice/organization"},
     *     description="get all schools by organization id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="schoolId",
     *         in="query",
     *         description="organizationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="page",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit defaults to 25",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="orderBy defaults to DESC",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getListOrganizationSchoolClassrooms(Request $request)
    {
        $data['schoolId'] = $request->attributes->get('schoolId', 'all');
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $data['search'] = $request->get('search');
        
        $boService = new BOService();
        $boService->getListOrganizationSchoolClassrooms(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to get the information about the classroom and students related to that classroom.
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/classroom/detail/{classroomId}")
     * ),
     * @SWG\GET(
     *     path="/classroom/detail/{classroomId}",
     *     summary="Get classroom details",
     *     tags={"backOffice/classroom"},
     *     description="Get classroom details and students.",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="classroomId",
     *         in="path",
     *         description="the id of a classroom",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function getClassroomDetails(Request $request)
    {
        $data['classroomId'] = $request->attributes->get('classroomId');
        $BOService = new BOService(); 
        $classInformation = $BOService->getClassRoomById($this->getEntityManager(), $data);

        if($classInformation != false) {
           $classMembers = $BOService->getMembersClassRoom(
               $this->getEntityManager()->createQueryBuilder(User::class),
               $classInformation
           );

            if($classMembers != false) {
                $classDetails["classroom"] = $classInformation;
                $classDetails['members'] = $classMembers;
                return $this->responseJson($classDetails, 200);
            }
                return $this->responseJson($classInformation, 200);
        }

        return $this->responseJson($BOService->getMessage(), $BOService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/organization/delete")
     * ),
     * @SWG\Delete(
     *     path="/organization/delete",
     *     summary="Delete Organization and all of its children",
     *     tags={"backOffice/organization"},
     *     description="Enroll Events User",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="orgId",
     *         in="formData",
     *         description="org id",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteOrganization(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data = $request->request->all();
        $data['userId'] = $token->getClaim('context')->user_id;
        $boService = new BOService();
        $boService->deleteOrganization(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/school/delete")
     * ),
     * @SWG\Delete(
     *     path="/school/delete",
     *     summary="Delete school and all of its children",
     *     tags={"backOffice/organization"},
     *     description="Delete school and all of its children",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="schoolId",
     *         in="formData",
     *         description="org id",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteSchool(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data = $request->request->all();
        $data['userId'] = $token->getClaim('context')->user_id;
        $boService = new BOService();
        $boService->deleteSchool(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }
    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/classroom/delete")
     * ),
     * @SWG\Delete(
     *     path="/classroom/delete",
     *     summary="Delete classroom and all of its children",
     *     tags={"backOffice/organization"},
     *     description="Delete school and all of its children",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="classId",
     *         in="formData",
     *         description="org id",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteClassroom(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data = $request->request->all();
        $data['userId'] = $token->getClaim('context')->user_id;
        $boService = new BOService();
        $boService->deleteClassroom(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/organization/details/update")
     * ),
     * @SWG\Post(
     *     path="/organization/details/update",
     *     summary="Update Organization Details",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="orgId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="firstName",
     *         in="formData",
     *         description="Organization Contact First Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         description="Organization Contact Last Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Organization Contact Email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="Organization Contact Title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="Name",
     *         in="formData",
     *         description="The Name of the Organization",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Organization Contact Phone",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="educationalStage",
     *         in="formData",
     *         description="Type of Education Level the organization teaches",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="duration",
     *         in="formData",
     *         description="Duration for the classes in the organization",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="Community",
     *         in="formData",
     *         description="Organization is interested in which acmes community ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateOrganization(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->updateOrganization(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/organization/school/update")
     * ),
     * @SWG\Put(
     *     path="/organization/school/update",
     *     summary="Update Organization Details",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="schoolId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="Name",
     *         in="formData",
     *         description="Organization Contact First Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateSchool(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->updateSchool(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/organization/classroom/update")
     * ),
     * @SWG\Put(
     *     path="/organization/classroom/update",
     *     summary="Update Organization Details",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="classId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="Name",
     *         in="formData",
     *         description="Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="educatorEmail",
     *         in="formData",
     *         description="educatorEmail",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateClassroom(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->updateClassroom(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/details/{userId}")
     * ),
     * @SWG\Get(
     *     path="/user/details/{userId}",
     *     summary="Update Organization Details",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function userDetails(Request $request)
    {
        $data = $request->attributes->all();
        $boService = new BOService();
        $boService->userDetails(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/update")
     * ),
     * @SWG\Post(
     *     path="/user/update",
     *     summary="Update User Details",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="firstName",
     *         in="formData",
     *         description="Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         description="Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="photo",
     *         in="formData",
     *         description="photo",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="educatorEmail",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="level",
     *         in="formData",
     *         description="level",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateUser(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->updateUser(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/organization/filter/{name}")
     * ),
     * @SWG\Get(
     *     path="/organization/filter/{name}",
     *     summary="Filter Organization By Name",
     *     tags={"backOffice/organization"},
     *     description="Update Organization Details",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="Organization Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function filterOrganizationByName(Request $request)
    {
        $data = $request->attributes->all();
        $boService = new BOService();
        $boService->filterOrganizationByName(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/upload/user/avatar")
     * ),
     * @SWG\Post(
     *     path="/upload/user/avatar",
     *     summary="Upload Avatar",
     *     tags={"signup"},
     *     description="Upload Avatar",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userPhoto",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function BackOfficeSendAvatar(Request $request)
    {
        $userPhoto = $request->files->get('userPhoto');
        $signUpService = new SignupService($this->getApplication());
        $signUpService->uploadAvatar($userPhoto);
        return $this->responseJson($signUpService->getMessage(), $signUpService->getStatus());
    }
}
