<?php

namespace App\Controllers\BackOffice;

use App\Controllers\BackOffice\BackOffice;
use App\EmailTemplate\ProEmails;
use App\Services\BackOffice\BOService;
use App\Services\Groups\GroupService;
use App\Services\Setting\SettingService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BRegister extends BackOffice
{
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/new")
     * ),
     *  @SWG\Post(
     *     path="/user/new",
     *     summary="Register a new user by admin choosing it's type",
     *     tags={"backOffice/register"},
     *     description="Register a new user by admin choosing it's type",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="profile",
     *         in="formData",
     *         description="one of the following: admin,educator,moderator,pro,recruiter,Student Level X",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="community",
     *         type="string",
     *         in="formData",
     *         description="User Selected Community",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="firstName",
     *         in="formData",
     *         description="Users First Name",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         description="Users Last Name",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User Email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function registerByAdmin(Request $request)
    {
        $data['Name'] = $request->request->get('firstName');
        $data['educationalStage'] = $request->request->get('lastName');
        $data['Community'] = $request->request->get('community');
        $email = $request->request->get('email');
        $type = $request->request->get('profile');
        $userEmail = $this->registerProEduAuto($data, $type, $email);
        $userEmail['orgName'] = 'We';
        $emailSend['title'] = 'ACME - '.$userEmail['orgName'].' Created an Account for you';
        $emailSend['email'] = $userEmail['email'];
        $emailSend['message'] = 'ACME Platform Created you an account, use the following credentials to access our platform:
            - login: '.$userEmail['email'].'
            - password: '.$userEmail['passWord'].'
            
            Remember to update your personal information.';
        $this->sendEmail($emailSend);
        return $this->responseJson(['message' => 'User Created, Email sent with credentials'], 200);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="list/users")
     * ),
     * @SWG\Get(
     *     path="list/users",
     *     summary="get all users by type or needing approval",
     *     tags={"backOffice/register"},
     *     description="get all users by type or needing approval",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         in="query",
     *         description="status pending or approved",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="type of user like admin, student level x. recruiter etc..",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="page",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit defaults to 25",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="orderBy",
     *         in="query",
     *         description="orderBy defaults to DESC",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listProsApproval(Request $request)
    {
        $data = $request->query->all();
        $data['status'] = $request->get('status', 'all');
        $data['type'] = $request->get('type', 'all');
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        if (!is_null($request->get('search'))) {
            $data['name'] = $request->get('search');
        }

        $boService = new BOService();
        $labs = $boService->listProsApproval($this->getEntityManager(), $data);
        if ($labs === true) {
            return $this->responseJson(['message' => $boService->getMessage()['users'], $boService->getMessage()['pagination']], $boService->getStatus());
        }
        return $this->responseJson(['message' => $boService->getMessage()], $boService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/pro/approve")
     * ),
     * @SWG\Post(
     *     path="/pro/approve",
     *     summary="Approve pro By Admin",
     *     tags={"backOffice/register"},
     *     description="Approve pro By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="proId",
     *         in="formData",
     *         description="labId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="approval",
     *         in="formData",
     *         description="approval as approved or denied",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="boolean"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function approveOrDenyPro(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->approveOrDenyPro(
            $this->getEntityManager(),
            $data
        );
        $sendData = $boService->getMessage();
        $retorno = $sendData['status'] ? 'approved' : 'denied';
        $templateBuilder = new ProEmails();
        $emailBuilder['title'] = 'ACME - Your Professional Account Was '.$retorno;
        $emailBuilder['email'] = $sendData['email'];
        $emailBuilder['message'] = $templateBuilder->getProTemplate($retorno);
        $this->sendEmail($emailBuilder);

        return $this->responseJson(['message' => 'Pro Account '. $retorno ], $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/pro/details/{proId}")
     * ),
     * @SWG\Get(
     *     path="/pro/details/{proId}",
     *     summary="get all pro details by id",
     *     tags={"backOffice/register"},
     *     description="get all pro details by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="proId",
     *         in="query",
     *         description="organizationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function proDetails(Request $request)
    {
        $boService = new BOService();
        $boService->proDetails(
            $this->getEntityManager(),
            $request->attributes->get('proId')
        );
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/remove/{type}/{groupId}/{userId}")
     * ),
     * @SWG\Delete(
     *     path="/remove/{type}/{groupId}/{userId}",
     *     summary="Delete group by id",
     *     tags={"Groups"},
     *     description="Delete post by id",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="groupId",
     *         in="formData",
     *         description="groupId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="query",
     *         description="userId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="lab or classroom",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function deleteBackOfficeRosterUser(Request $request)
    {
        $data = $request->attributes->all();
        $groupService = new GroupService();
        $groupService->removeUserRoster(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($groupService->getMessage(), $groupService->getStatus());
    }
}