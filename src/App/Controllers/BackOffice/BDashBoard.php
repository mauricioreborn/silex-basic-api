<?php

namespace App\Controllers\BackOffice;

use App\Services\BackOffice\BOService;
use App\Services\Events\EventService;
use App\Services\UserService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BDashBoard extends BackOffice
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/dashboard/data")
     * ),
     * @SWG\Get(
     *     path="/dashboard/data",
     *     summary="get all data for dashboard",
     *     tags={"backOffice/dashboard"},
     *     description="get all data for dashboard",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="startDate",
     *         in="path",
     *         description="start date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endDate",
     *         in="path",
     *         description="end Date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function dataMining(Request $request)
    {
        $data['startDate'] = $request->get('startDate', date('Y-01-01'));
        $data['endDate'] = $request->get('endDate', date('Y-12-t'));
        $boService = new BOService();
        $dm = $boService->dashboardData($this->getEntityManager(), $data);
        if ($dm === true) {
            return $this->responseJson(['message' => $boService->getMessage()], 200);
        }
        return $this->responseJson(['message' => $boService->getMessage()], 400);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/dashboard/events")
     * ),
     * @SWG\Get(
     *     path="/dashboard/events",
     *     summary="get all events for dashboard",
     *     tags={"backOffice/dashboard"},
     *     description="get all data for dashboard",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="startDate",
     *         in="path",
     *         description="start date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endDate",
     *         in="path",
     *         description="end Date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function dataMiningEvents(Request $request)
    {
        $data['startDate'] = $request->get('startDate', date('Y-m-d'));
        $data['endDate'] = $request->get('endDate', date('Y-m-d'));
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 0);
        $data['skip'] = $request->get('skip');
        $data['skip'] = $this->calculateSkip($data);

        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->getEventList(
                $this->getEntityManager(),
                $data,
                $token->getClaim('context')->user_id
            );
            return $this->responseJson($event->getMessage(), $event->getStatus());
        }
        return $this->responseJson('Invalid User ID', 400);
    }

}