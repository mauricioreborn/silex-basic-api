<?php

namespace App\Controllers\BackOffice;

use App\Entities\User;
use App\Repository\UserRepository;
use App\Repository\WorkRepository;
use App\Services\FlagService;
use App\Services\Setting\SettingService;
use App\Services\UserService;
use App\Services\Comment\CommentService;
use App\Entities\Comments;
use App\Services\Work\RefineByFactory;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BSettings extends BackOffice
{
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/flags")
     * ),
     * @SWG\Get(
     *     path="/flags",
     *     summary="get all flags",
     *     tags={"backOffice/settings"},
     *     description="get all flags",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getAllFlags(Request $request)
    {
        $settingService = new SettingService();
        $settingService->getFLags($this->getEntityManager());
        return $this->responseJson($settingService->getMessage(), $settingService->getStatus());
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param Request $request validate an user.
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/user/delete/{userId}")
     * ),
     *
     * @SWG\Delete(
     *     path="backoffice/user/delete/{userId}",
     *     tags={"user"},
     *     operationId="remove an user by email",
     *     summary="remove an user",
     *     description="remove an user by email",
     *     @SWG\Parameter(
     *         name="email",
     *         type="string",
     *         in="formData",
     *         description="user email ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="user has been removed",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function delete(Request $request)
    {
        $data['user'] = $request->get('userId');

        $commentService = new CommentService();
        $userRepository = new UserRepository($this->getEntityManager()->createQueryBuilder(User::class));
        $user = $userRepository->findAnUser($data['user']);
        if (count($user)>0) {
            $commentService->deleteCommentByUserId($this->getEntityManager()->createQueryBuilder(Comments::class), $data['user']);
            
            $filters['myLevel'] = $user->getLevel()->getAlias();
            $filters['skip'] = 0;
            $filters['waitingReview'] = false;
            $filters['user'] = $data['user'];

            if ($request->get('browse') == "waitingReview") {
                $filters['waitingReview'] = true;
            }

            $refine = new RefineByFactory($this->getEntityManager(), $filters['myLevel']);
            $query = $refine->startSearch($filters);

            $workRepository = new WorkRepository($this->getEntityManager());
            $works = $workRepository->getAllWork($query);

            foreach ($works as $work) {
                $commentService->deleteCommentByWorkId($this->getEntityManager()->createQueryBuilder(Comments::class), $work["id"]);
            }

            $userService = new UserService();
            $userService->deleteUser($this->getEntityManager(), $data['user']);

            return $this->responseJson($userService->getMessage(), $userService->getStatus());
        }
        return $this->responseJson('user not found', 200);
    }
}