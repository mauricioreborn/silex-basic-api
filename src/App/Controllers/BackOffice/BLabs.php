<?php

namespace App\Controllers\BackOffice;

use App\Controllers\BackOffice\BackOffice;
use App\Services\BackOffice\BOService;
use App\Validators\BackOffice\BackOffice as BOValidator;
use App\Services\Events\EventService;
use App\Services\UserService;
use App\Services\Work\WorkService;
use App\Validators\BackOffice\Moderator;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BLabs extends BackOffice
{
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/labs/approval/list")
     * ),
     * @SWG\Get(
     *     path="/labs/approval/list",
     *     summary="get all labs needing approval",
     *     tags={"backOffice/labs"},
     *     description="get all labs needing approval",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listLabsApproval(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 25);
        $data['orderBy'] = $request->get('sort', 'DESC');
        $data['skip'] = $this->calculateSkip($data);
        $boService = new BOService();
        $labs = $boService->listLabsApproval($this->getEntityManager(), $data);
        if ($labs === true) {
            return $this->responseJson(['message' => $boService->getMessage()['labs'], $boService->getMessage()['pagination']], $boService->getStatus());
        }
        return $this->responseJson(['message' => $boService->getMessage()], $boService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/lab/approve")
     * ),
     * @SWG\Post(
     *     path="/lab/approve",
     *     summary="Approve labs By Admin",
     *     tags={"backOffice/labs"},
     *     description="Approve labs By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="labId",
     *         in="formData",
     *         description="labId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="approval",
     *         in="formData",
     *         description="approval as approved or denied",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function approveOrDenyLab(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->approveOrDenyLab(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['message' => $boService->getMessage()], $boService->getStatus());
    }

}