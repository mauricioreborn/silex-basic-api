<?php

namespace App\Controllers\BackOffice;

use App\Controller;
use App\Entities\Level;
use App\Entities\User;
use App\Services\BackOffice\BackOfficeService;
use App\Services\Email\EmailBuilder;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BackOffice extends Controller
{

    /**
     * Method to send a email
     * @param $data[email,title,message]
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function sendEmail($data)
    {
        $emailBuider = new EmailBuilder($this->getApplication()['mailer']);
        $emailBuider->setTo($data['email']);
        $emailBuider->setSubject($data['title']);
        $emailBuider->setFrom('info@theacmenetwork.org');
        $emailBuider->setContent($data['message']);
        $message = $emailBuider->sendEmail() === 1
            ? 'email has been sent' : 'error to send email';
        return $this->responseJson(['message' => $message], 200);
    }

    /**
     * Method to generate a mongoDate
     * @param null $date
     * @return \MongoDate
     */
    public function getMongoDate($date = null)
    {
        $date = $date == null ? "Y-m-d H:i:s" : $date;
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $today = new \MongoDate($ts);
        return $today;
    }

    public function registerProEduAuto($data, $type, $email)
    {
        $user = new User();
        $user->setFirstName($data['Name']);
        $user->setLastName(ucfirst($data['educationalStage']));
        $user->setEmail($email);
        $user->setProfile($type);
        $level = new Level();
        $level->setDate($this->getMongoDate());
        $level->setAlias($type);
        $level->setNumber(5);
        $user->setLevel($level);
        foreach ($data['Community'] as $com) {
            $user->setCommunity($com);
        }
        $user->setStatus(true);
        $user->setAgreement(true);
        $generatedPass = uniqid();
        $user->setPassword(md5($generatedPass));
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        
        return [
            'passWord' => $generatedPass,
            'email' => $user->getEmail(),
            'orgName' => $data['Name'],
            'userId' => $user->getId()
        ];
    }

    /**
     * Executes a search based on the type given as a parameter.
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/search/{type}/{search}")
     * ),
     *  @SWG\Get(
     *     path="/search/{type}/{search}",
     *     summary="Method to do a search on BackOffice",
     *     tags={"backOffice"},
     *     description="This method do a search on platform searching this parameters by type",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="what do you want to find ? organization,user,work,classroom,schools",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="search",
     *         in="path",
     *         description="the words to search",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit of rows",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="skip",
     *         in="query",
     *         description="total rows to skip",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : array , status : 200}",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : {type} not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function searchOnPlatform(Request $request)
    {
        $data['type'] = $request->get('type');
        $data['name'] = $request->get('search');

        $data['limit'] = is_null($request->query->get('limit')) ? 25 :$request->query->get('limit');
        $data['skip'] = is_null($request->query->get('limit')) ? 0 :$request->query->get('limit');
        $backOfficeService = new BackOfficeService();
        $backOfficeService->searchOnBackOffice($data, $this->getEntityManager());

        return $this->responseJson($backOfficeService->getMessage(), $backOfficeService->getStatus());
    }
}
