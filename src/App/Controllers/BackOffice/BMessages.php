<?php

namespace App\Controllers\BackOffice;

use App\Controller;
use App\Services\BackOffice\BOService;
use App\Services\LiveSession\LiveSessionService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use App\Services\UserService;
use App\Services\Events\EventService;
use Symfony\Component\HttpFoundation\Request;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */
class BMessages extends Controller
{

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/send/message")
     * ),
     * @SWG\Post(
     *     path="/user/send/messages",
     *     summary="Send Message to Users",
     *     tags={"backoffice/Messages"},
     *     description="Send Message to Users",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="User id REQUIRED if new message to new user optional if reply",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="message",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="chatId",
     *         in="formData",
     *         description="chatId i'm replying to",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function sendCustomMessage(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $request->request->get('userId');
        $data['originId'] = $token->getClaim('context')->user_id;
        $data['message'] = $request->request->get('message');
        $data['chatId'] = $request->request->get('chatId', 'new');
        $data['typeOrigin'] = 'admin';
        $sessionService = new BOService();
        $sessionService->saveChat(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/list/messages")
     * ),
     * @SWG\Get(
     *     path="/user/list/messages",
     *     summary="get all messages",
     *    tags={"backoffice/Messages"},
     *     description="get all messages",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listMessages(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['type'] = 'admin';
        $boService = new BOService();
        $labs = $boService->listMessages($this->getEntityManager(), $data);
        if ($labs === true) {
            return $this->responseJson($boService->getMessage(), $boService->getStatus());
        }
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/read/message/{chatId}")
     * ),
     *     @SWG\Get(
     *     path="/user/read/message/{chatId}",
     *     summary="get all messages by id and set read true",
     *     tags={"backoffice/Messages"},
     *     description="get all messages by id and set read true",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="chatId",
     *         in="formData",
     *         description="chatId chatId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function backOfficeReadMessage(Request $request)
    {
        $data['chatId'] = $request->attributes->get('chatId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new BOService();
        $sessionService->readMessage(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/search/user")
     * ),
     * @SWG\Post(
     *     path="/search/user",
     *     summary="search Users",
     *    tags={"backoffice/Messages"},
     *     description="search Users",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="searchUser",
     *         in="formData",
     *         description="searchUser",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="formData",
     *         description="limit",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="skip",
     *         in="formData",
     *         description="skip",
     *         required=false,
     *         type="integer",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="formData",
     *         description="page",
     *         required=false,
     *         type="integer",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function searchUserSendMessage(Request $request)
    {
        $searchUser = explode(' ', $request->request->get('searchUser'));
        $data['firstName'] = $searchUser[0];
        $lastName = '';
        foreach ($searchUser as $idx => $names) {
            if ($idx > 0) {
                $lastName .=' '.$names;
            }
        }
        $data['lastName'] = $lastName;
        $data['limit'] = $request->get('limit', 5);
        $data['page'] = $request->get('page');
        $data['skip'] = $this->calculateSkip($data);
        $sessionService = new LiveSessionService();
        $sessionService->searchUserSendMessage(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

}