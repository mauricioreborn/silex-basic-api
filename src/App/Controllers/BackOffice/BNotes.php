<?php

namespace App\Controllers\BackOffice;

use App\Controller;
use App\Entities\Notes;
use App\Services\Notes\NotesService;
use App\Validators\Settings\NotesValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/backoffice/")
 */

class BNotes extends Controller
{

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param Request $request validate an user.
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/notes")
     * )
     *
     * @SWG\Post(
     *     path="/notes",
     *     tags={"Notes"},
     *     operationId="notes",
     *     summary="method to create a new custom note",
     *     description="method to create a new custom note",
     *     @SWG\Parameter(
     *         name="message",
     *         type="string",
     *         in="formData",
     *         description="custom message",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="title",
     *         type="string",
     *         in="formData",
     *         description="custom message title",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="group",
     *         type="string",
     *         in="formData",
     *         description="group[pro,studentlevel 1, recruiter]",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *       @SWG\Parameter(
     *         name="password",
     *         type="string",
     *         in="formData",
     *         description="user password ",
     *         required=true,
     *
     *        @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *     )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function notes(Request $request)
    {
        $data['title'] = $request->request->get('title');
        $data['message'] = $request->request->get('message');
        $data['group'] = $request->request->get('group');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $notesValidator = new NotesValidator();

        if ($notesValidator->validateNotesInputs(
            $this->getApplication(),
            $data
        ) === true) {
            $notesService = new NotesService();
            $notesService->addNotes($this->getEntityManager(), $data);
            return $this->responseJson($notesService->getMessage(), $notesService->getStatus());
        }
        return $this->responseJson($notesValidator->getErrorMessage(), 401);
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/note/{noteId}")
     * ),
     * @SWG\Delete(
     *     path="/note",
     *     summary="remove an announcement",
     *     tags={"backOffice/Notes"},
     *     description="remove an announcement",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="noteId",
     *         in="path",
     *         description="announcement Id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function noteRemove(Request $request)
    {
        $data['noteId'] = $request->get('noteId');
        $notesService = new NotesService();
        $notesService->noteRemove($this->getEntityManager(), $data);
        return $this->responseJson($notesService->getMessage(), $notesService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/notes")
     * ),
     * @SWG\Get(
     *     path="/notes",
     *     summary="remove an announcement",
     *     tags={"backOffice/Notes"},
     *     description="remove an announcement",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getNotes(Request $request)
    {
        $notesService = new NotesService();
        $notesService->getAllNotes($this->getEntityManager()->createQueryBuilder(Notes::class));
        return $this->responseJson($notesService->getMessage(), $notesService->getStatus());
    }
}