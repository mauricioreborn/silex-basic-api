<?php

namespace App\Controllers;

use App\Controller;
use App\Entities\User;
use App\Services\Commom\TokenRead;
use App\Services\ForgotPassword\ForgotPasswordService;
use App\Validators\LoginValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class ForgotPassword extends Controller
{
    /**
     * Method to Forgot Password
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/password/forgot")
     * )
     *    @SWG\Post(
     *     path="/password/forgot",
     *     summary="Forgot Password Route",
     *     tags={"password"},
     *     description="Forgot Password Route",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         type="string",
     *         in="formData",
     *         description="User Email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function passwordForgot(Request $request)
    {

        $data = $request->request->all();

        $loginValidator = new LoginValidator($this->getApplication());

        $validator = $loginValidator->validEmail($data['email']);
        $validate = new ForgotPasswordService();
        if ($validator === true) {
            $validate->forgotPassword(
                $this->getEntityManager(),
                $request->request->get('email'),
                $this->getApplication()['mailer']
            );
        }
        return $this->responseJson(['message' => $validate->getMessage()], $validate->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/password/reset/{token}")
     * )
     *
     *  @SWG\Get(
     *     path="/password/reset/{token]",
     *     summary="Reset Password Route",
     *     tags={"password"},
     *     description="Reset Password Route",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="formData",
     *         description="User Token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Token Valid , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message :Token Invalid, status : 400}",
     *     ),
     *     ),
     *
     * @return JsonResponse
     */
    public function resetPassword(Request $request)
    {
        $token = $this->getToken($request->get('token'));
        $userId = $token->getClaim('context')->user_id;
        $expiration = $token->getClaims()['exp']->getValue();
        $validate = new ForgotPasswordService();
        $validate->validateForgotToken($this->getEntityManager(), $userId, $token, $expiration);
        return $this->responseJson(['message' => $validate->getMessage()], $validate->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/password/update")
     * )
     *
     *  @SWG\Post(
     *     path="/password/update",
     *     summary="Update Password Route",
     *     tags={"password"},
     *     description="Reset Password Route",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="password",
     *         type="string",
     *         in="formData",
     *         description="User new password",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Token Valid , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message :Token Invalid, status : 400}",
     *     ),
     *     ),
     *
     * @return JsonResponse
     */
    public function updatePassword(Request $request)
    {
        $data = $request->request->all();

        $token = $this->getToken($data['token']);
        $loginValidator = new LoginValidator($this->getApplication());
        $validator = $loginValidator->validPassword($data['password']);
        $validate = new ForgotPasswordService();

        if ($validator  === true) {
            $validate->updatePassword(
                $this->getEntityManager(),
                $token->getClaim('context')->user_id,
                $data['password']
            );
        }
        return $this->responseJson(['message' => $validate->getMessage()], $validate->getStatus());
    }

}
