<?php

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Controllers;

use App\Controller;

use App\Entities\Organization;
use App\Services\Commom\UploadPhoto;
use App\Services\SignupService;
use App\Services\TokenService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Validators\LoginValidator;
use App\Services\Auth\TokenGenerator;
use App\Services\Auth\TokenBuilder;
use App\Services\Email\EmailBuilder;
use App\Entities\User;
use Swagger\Annotations as SWG;

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Login extends Controller
{

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param Request $request validate an user.
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/login")
     * )
     *
     * @SWG\Post(
     *     path="/login",
     *     tags={"login"},
     *     operationId="getUser",
     *     summary="getUser",
     *     description="verify if an user exist",
     *     @SWG\Parameter(
     *         name="email",
     *         type="string",
     *         in="formData",
     *         description="user email",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *       @SWG\Parameter(
     *         name="password",
     *         type="string",
     *         in="formData",
     *         description="user password ",
     *         required=true,
     *
     *        @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *     )
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid input",
     *     ),
     * ),
     * @SWG\Response(
     *         response=200,
     *         description="token",
     *     @SWG\Schema(
     *     type="json",
     * @SWG\Items(ref="#/definoitions/Pet")
     * ),
     *     ),
     * )
     *
     * @return json
     */
    public function login(Request $request)
    {
        $loginValidator = new LoginValidator($this->getApplication());
        $validator = $loginValidator->validateLogin($request->request->all());

        if ($validator === true) {
            $repository = $this->getEntityManager()->getRepository(User::class);
            $user = $repository->findOneBy(
                [
                    'email' => $request->request->get('email'),
                    'password' => md5($request->request->get('password'))
                ]
            );

            if (count($user) === 0) {
                return new JsonResponse(['message' => 'Email or Password incorrect'], 401);
            }

            if ($user->getStatus() === false) {
                $saveUser = new SignUp($this->getApplication());
                $saveUser->sendEmail($request->get('email'), $user->getId(), $user->getProfile());
                return new JsonResponse(['message' => 'User Account Not Confirmed:  A new email was sent.'], 401);
            }
            $orgId = null;
            if ($user->getLevel()->getAlias() == 'organization') {
                $repo = $this->getEntityManager()->getRepository(Organization::class);
                $org = $repo->findOneBy(
                    [
                        'email' => $request->request->get('email'),
                    ]
                );
                if ($org) {
                    $orgId = $org->getId();
                }
            }
            $tokenService = new TokenService();
            $token = $tokenService->tokenBuild($user, $orgId);
            return new JsonResponse(
                [
                    'token' => $token
                ],
                200
            );
        }
        return new JsonResponse(['message' => $validator], 400);
    }
}
