<?php


namespace App\Controllers;


use App\Controller;
use App\Controllers\Work\Work;
use App\Entities\Notes;
use App\Entities\Notification;
use App\Notification\Users;
use App\Repository\UserRepository;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;
use App\Services\Notes\NotesService;
use App\Services\NotificationService;
use App\Services\UserFactory;
use App\Validators\Settings\NotesValidator;
use App\Validators\UserValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Services\UserService;
use Swagger\Annotations as SWG;
use DDesrosiers\SilexAnnotations\Annotations as SLX;

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class User extends Controller
{
    
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/name/{name}/profile/{profile}")
     *
     * ),
     * @SWG\Get(
     *     path="/user/name/{name}/profile/{type}",
     *     summary="get an user by your first or last name or email and by type",
     *     tags={"user"},
     *     description="get an user by your first or last name or email and by type",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="name",
     *         in="path",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="profile",
     *         in="path",
     *         description="type (Student,pro,educator,recruter)",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{user:userObject}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getUsers(Request $request)
    {
        $userValidator = new UserValidator();

        if ($userValidator->validateMongoId($request->get('name'))) {
            $data['uniqClassCode'] = $request->get('name');
        } else {
            $data['name'] = $request->get('name');
        }

        $data['profile'] = $request->get('profile');
        $type = strpos($data['profile'], 'Student');
        $tokenGenerator = new TokenGenerator(new TokenBuilder());
        $token = $tokenGenerator->readToken($request->headers->get('token'));

        $data['id'] = $token->getClaim('context')->user_id;
        if ($type !== false && $token->getClaim('context')->level !== 'pro') {
            $tokenGenerator = new TokenGenerator(new TokenBuilder());
            $token = $tokenGenerator->readToken($request->headers->get('token'));
            $data['classCode'] = $token->getClaim('context')->classCode;
        }
        
        $userService = new UserService();
        $userService->getUsers($this->getEntityManager(), $data);
        return $this->responseJson($userService->getMessage(), $userService->getStatus());
    }

    /**
     * Method to search users by name
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/search/{name}")
     *
     * ),
     * @SWG\Get(
     *     path="/user/search/{name}",
     *     summary="get users by first name and last name",
     *     tags={"user"},
     *     description="get users by first name and last name",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of the users to be search",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="skip",
     *         in="header",
     *         description="number of skips, this is an optional parameter",
     *         required=false,
     *         type="int",
     *         maxLength=100,
     *         @SWG\Items(type="int"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="An array of users",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function searchUsers(Request $request)
    {
        $searchData['name'] = $request->get('name');
        $searchData['skip'] = $request->headers->get('skip');

        if (is_null($searchData['skip'])) {
            $searchData['skip'] = 0;
        }

        $userService = new UserService();
        $userService->searchUsers($this->getEntityManager(), $searchData);

        return $this->responseJson($userService->getMessage(), $userService->getStatus());

    }
    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/notification")
     *
     * ),
     * @SWG\Get(
     *     path="/user/notification",
     *     summary="get an user by your first or last name or email and by type",
     *     tags={"user"},
     *     description="get an user by your first or last name or email and by type",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{user:userObject}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getNotification(Request $request)
    {
        $tokenGenerator = new TokenGenerator(new TokenBuilder());
        $token = $tokenGenerator->readToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['limit'] = $request->get('limit', 0);
        $data['skip'] = $request->get('skip', 0);
        $data['orderBy'] = 'createdAt';
        $notificationService = new NotificationService(new Users());
        $notification['notification'] = $notificationService->getNotification(
            $this->getEntityManager(),
            $data
        );

        $noteService = new NotesService();
        $notes = $noteService->getNotes($this->getEntityManager(), $data);
        if (count($notes) + count($notification['notification']) >0) {
            $notification['unseen'] = true;
              $notification['totalNews'] = count($notes) + count($notification['notification']);

        }

        $userRepository = new UserRepository($this->getEntityManager()->createQueryBuilder(\App\Entities\User::class));
        $user = $userRepository->findAnUser($data['userId']);
        
        $myNotes = $noteService->getNoteById($this->getEntityManager()->createQueryBuilder(Notes::class), $user->getNotes());

        $notes = array_merge($notes, $myNotes);
        
        $notification['announcements'] = $notes;
        $noteService->getMessage();
        return $this->responseJson($notification, $notificationService->getStatus());
    }



    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/notification")
     *
     * ),
     * @SWG\Post(
     *     path="/user/notification",
     *     summary="This method set a notification how read",
     *     tags={"Notification"},
     *     description="This method set a notification how read",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="notificationId",
     *         in="formData",
     *         description="notificationId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="type (notification or announcement)",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{user:userObject}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function readNotification(Request $request)
    {
        $data['notificationId'] = $request->request->get('notificationId');
        $data['type'] = $request->request->get('type');
        $notificationValidator = new NotesValidator();

        if ($notificationValidator->readNotification($this->getApplication(), $data)) {
            $token = $this->getToken($request->headers->get('token'));

            $data['userId'] = $token->getClaim('context')->user_id;

            if ($data['type'] == 'notification') {
                $notificationService = new NotificationService(new Users());
                $notificationService->setReadNotification(
                    $this->getEntityManager()->createQueryBuilder(Notification::class),
                    $data
                );

                return $this->responseJson(
                    ['message' => $notificationService->getMessage()],
                    $notificationService->getStatus()
                );
            }

            $userRepository = new UserRepository($this->getEntityManager()->createQueryBuilder(\App\Entities\User::class));
            $user = $userRepository->findAnUser($data['userId']);

            if (!in_array($data['notificationId'], $user->getNotes())) {
                $notesService = new NotesService();
                $notesService->setNoteHowRead($this->getEntityManager(), $data);

                return $this->responseJson(
                    $notesService->getMessage(),
                    $notesService->getStatus()
                );
            }
            return $this->responseJson(['message' => 'notification has been updated'], 200);
        }
        return $this->responseJson($notificationValidator->getErrorMessage(), 401);
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/review")
     *
     * ),
     * @SWG\Get(
     *     path="/user/review",
     *     summary="verify if an user can send a work to receive a feedback",
     *     tags={"user"},
     *     description="verify if an user can send a work to receive a feedback",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{user:userObject}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function canSendToReview(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $userService = new UserService();
        $userService->canSendToReview(
            $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
            $data
        );

        return $this->responseJson($userService->getMessage(), $userService->getStatus());
    }
}
