<?php

namespace App\Controllers\Messages;

use App\Controller;
use App\Services\BackOffice\BOService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Messages extends Controller
{

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/send/message")
     * ),
     * @SWG\Post(
     *     path="/user/send/message",
     *     summary="Send Message to Users",
     *     tags={"Messages"},
     *     description="Send Message to Users",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="message",
     *         in="formData",
     *         description="Message To The User",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="chatId",
     *         in="formData",
     *         description="chatId required for students, optional for recruiter and admin",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId required for students, optional for recruiter and admin",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function userSaveMessage(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['originId'] = $token->getClaim('context')->user_id;
        $data['message'] = $request->request->get('message');
        $data['userId'] = $request->request->get('userId');
        $data['chatId'] = $request->request->get('chatId', 'new');
        $data['typeOrigin'] = 'user';
        $level =  $token->getClaim('context')->level;
        $allowed = ['recruiter', 'admin'];
        if ((!in_array($level, $allowed)) && ($data['chatId'] === 'new')) {
                return $this->responseJson(['message' => 'New Message Not Allowed'], 400);
        }
        $sessionService = new BOService();
        $sessionService->saveChat(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/read/message/{chatId}")
     * ),
     * @SWG\Get(
     *     path="/users/read/message/{chatId}",
     *     summary="check if a work can to submitted to portfolio ",
     *     tags={"messages"},
     *     description="check if a work can to submitted to portfolio",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="chatId",
     *         in="path",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message :this work may be sent to portfolio, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : this work cannot be sent to portfolio , status : 401}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function readMessage(Request $request)
    {
        $data['chatId'] = $request->attributes->get('chatId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new BOService();
        $sessionService->readMessage(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/list/messages")
     * ),
     * @SWG\Get(
     *     path="/users/list/messages",
     *     summary="get all messages",
     *     tags={"messages"},
     *     description="get all messages",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function listMessages(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $level = $token->getClaim('context')->level;
        $allowed = ['recruiter', 'admin'];
        $data['type'] = 'user';
        if (in_array($level, $allowed)) {
            $data['type'] = 'admin';
        }
        $boService = new BOService();
        $labs = $boService->listMessages($this->getEntityManager(), $data);
        if ($labs === true) {
            return $this->responseJson($boService->getMessage(), $boService->getStatus());
        }
        return $this->responseJson(['message' => $boService->getMessage()], $boService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/networkcheck")
     * ),
     * @SWG\Get(
     *     path="/networkcheck",
     *     summary="networkcheck",
     *     tags={"messages"},
     *     description="networkcheck",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function netWorkCheck()
    {
        return $this->responseJson(['message' => 'OK'], 200);
    }
}