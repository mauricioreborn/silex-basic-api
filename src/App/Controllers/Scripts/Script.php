<?php

namespace App\Controllers\Scripts;

use App\Controller;
use App\Repository\Script\ScriptRepository;
use App\Validators\Work as WorkValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Script extends Controller {

    /**
     **
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/script/comment")
     * ),
     * @SWG\Get(
     *     path="/script/comment",
     *     summary="get all works",
     *     tags={"work"},
     *     description="get all works by idUser",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function updateComments(Request $request)
    {
        
    }


    /**
     **
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/script/uppercase")
     * ),
     * @SWG\Get(
     *     path="/script/uppercase",
     *     summary="get all works",
     *     tags={"work"},
     *     description="get all works by idUser",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function setUpperCase()
    {
        $labRepository = new ScriptRepository($this->getEntityManager());
        $labs = $labRepository->getAllLabs();
        $this->flushGroup($labs);
        $class = $labRepository->getAllClassrooms();
        $this->flushGroup($class);


        return $this->responseJson('script has been executed',200);
    }



    private function flushGroup($groups)
    {
        foreach ($groups as $group)
        {
            $group->setTitle(ucfirst($group->getTitle()));
            $this->getEntityManager()->persist($group);
            $this->getEntityManager()->flush();
        }
    }

    /**
     **
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/script/workhighlightdate")
     * ),
     * @SWG\Get(
     *     path="/script/workhighlighdate",
     *     summary="get all works",
     *     tags={"work"},
     *     description="get all works by idUser",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function setWorkHighlightDate()
    {
        $workRepository = new ScriptRepository($this->getEntityManager());
        $works = $workRepository->getAllWorks();
        
        foreach($works as $work) {

            if($work->getHighlight() == true && $work->getHighLightDate() == null) {
                $work->setHighLightDate($work->getUploadAt());
                $work->setUtcOffset("+0000");
                $this->getEntityManager()->persist($work);
                $this->getEntityManager()->flush();
            }
        }
        return $this->responseJson('script has been executed',200);
    }
    /**
     **
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/script/sessionhighlightdate")
     * ),
     * @SWG\Get(
     *     path="/script/sessionhighlightdate",
     *     summary="get all works",
     *     tags={"work"},
     *     description="get all works by idUser",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function setSessionHighlightDate()
    {
        $liveSessionRepository = new ScriptRepository($this->getEntityManager());
        $livesessions = $liveSessionRepository->getAllLiveSessions();

        foreach($livesessions as $livesession) {

            if($livesession->getHighlight() == true && $livesession->getHighLightDate() == null) {
                $livesession->setHighLightDate($livesession->getCreatedAt());
                $this->getEntityManager()->persist($livesession);
                $this->getEntityManager()->flush();
            }
        }
        return $this->responseJson('script has been executed',200);
    }
}