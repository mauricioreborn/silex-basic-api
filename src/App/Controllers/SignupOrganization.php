<?php

/**
 * SignUp For Organization Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Controllers;

use App\Controller;
use App\EmailTemplate\AccountOrganizationEmailConfirm;
use App\Entities\Organization;
use App\Services\Email\EmailBuilder;
use App\Services\SignupService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Validators\OrganizationValidator;
use Swagger\Annotations as SWG;

/**
 * SignUp For Organization Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class SignupOrganization extends Controller
{

    /**
     * Method to send data to register new organization
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="organization")
     * ),
     * @SWG\Post(
     *     path="/organization",
     *     summary="Register a New Organization",
     *     tags={"Organization"},
     *     description="Register a New Organization",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="contactFirstName",
     *         in="formData",
     *         description="Organization Contact First Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="contactLastName",
     *         in="formData",
     *         description="Organization Contact Last Name",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="contactEmail",
     *         in="formData",
     *         description="Organization Contact Email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="contactTitle",
     *         in="formData",
     *         description="Organization Contact Title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="organizationName",
     *         in="formData",
     *         description="The Name of the Organization",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="numberOfSchools",
     *         in="formData",
     *         description="Number of Schools the organization has",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="numberOfClassrooms",
     *         in="formData",
     *         description="Total Classrooms the organization has",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Organization Contact Phone",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="educationalStage",
     *         in="formData",
     *         description="Type of Education Level the organization teaches",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="duration",
     *         in="formData",
     *         description="Duration for the classes in the organization",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="involvement",
     *         in="formData",
     *         description="Organization is involved with high schools or college ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="interestInvolvement",
     *         in="formData",
     *         description="Organization is interested in
     * beeing involved with high schools or college ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="interestedCommunity",
     *         in="formData",
     *         description="Organization is interested in which acmes community ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="foundOut",
     *         in="formData",
     *         description="Organization found out about acme how",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="comments",
     *         in="formData",
     *         description="Comments",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function signupOrganization(Request $request)
    {


        $organizationValidator = new OrganizationValidator($this->getApplication());
        $entityManager = $this->getEntityManager();
        $data = $request->request->all();

        $validator = $organizationValidator->validateRegisterOrganization($data, $entityManager);

        if ($validator === true) {
            $signupService = new SignupService($this->getApplication());
            $signupService->registerOrganization($data, $this->getEntityManager());
            $this->sendEmail($request);
            return $this->responseJson(['message' => $signupService->getMessage()], 200);
        }
        return new JsonResponse(['message' => $validator]);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="email")
     * )
     *
     * @return JsonResponse
     */
    public function sendEmail(Request $request)
    {
        $templateBuilder = new AccountOrganizationEmailConfirm();
        $data = $request->request->all();
        $emailBuider = new EmailBuilder($this->getApplication()['mailer']);
        $emailBuider->setTo($data['contactEmail']);
        $emailBuider->setSubject('ACME Organization Register');
        $emailBuider->setFrom('info@theacmenetwork.org');
        $emailBuider->setContent(
            $templateBuilder->getTemplate()
        );
        $message = $emailBuider->sendEmail() === 1
            ? 'email has been sent' : 'error to send email';
        return new JsonResponse($message);
    }
}
