<?php

namespace App\Controllers\Events;

use App\Controller;
use App\Entities\LiveSession;
use App\Entities\Work;
use App\Services\LiveSession\LiveSessionService;
use App\Services\Work\WorkService;
use App\Validators\EventValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Documents\Event;
use Swagger\Annotations as SWG;
use App\Services\UserService;
use App\Services\Events\EventService;
use Symfony\Component\HttpFoundation\Request;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Events extends Controller
{

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/list/{startDate}/{endDate}")
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/list"),
     * )
     * @SWG\Get(
     *     path="/events/list",
     *     summary="get all events",
     *     tags={"events"},
     *     description="get all events",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="startDate",
     *         in="path",
     *         description="start date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endDate",
     *         in="path",
     *         description="end Date Y-m-d",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function eventsList(Request $request)
    {
        $data['startDate'] = $request->get('startDate', date('Y-m-d'));
        $data['endDate'] = $request->get('endDate', date('Y-m-d'));
        $data['page'] = $request->get('page');
        $data = $this->buildPaginate($data, $request);
        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->getEventList(
                $this->getEntityManager(),
                $data,
                $token->getClaim('context')->user_id
            );
            return $this->responseJson($event->getMessage(), $event->getStatus());
        }
        return $this->responseJson('Invalid User ID', 400);
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/enrolled/list/{startDate}/{endDate}")
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/enrolled/list"),
     * ),
     * @SWG\Get(
     *     path="/events/enrolled/list",
     *     summary="get all events enrolled list",
     *     tags={"events"},
     *     description="get all events enrolled list",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="month",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function eventsEnrolledList(Request $request)
    {
        $data['startDate'] = $request->get('startDate', date('Y-m-d'));
        $data['endDate'] = $request->get('endDate', date('Y-m-d'));

        $data = $this->buildPaginate($data, $request);

        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->getEventEnrolledList(
                $this->getEntityManager(),
                $data,
                $token->getClaim('context')->user_id
            );
            return $this->responseJson($event->getMessage(), $event->getStatus());
        }
        return $this->responseJson('Invalid User ID', 400);
    }


    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/events/enroll")
     * ),
     * @SWG\Post(
     *     path="/events/enroll",
     *     summary="get all events enrolled list",
     *     tags={"events"},
     *     description="get all events enrolled list",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="eventId",
     *         in="formData",
     *         description="Enroll Events User",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function enroll(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->setEnroll(
                $this->getEntityManager(),
                $request->request->get('eventId'),
                [$token->getClaim('context')->user_id]
            );
            return $this->responseJson(['message' => $event->getMessage()], $event->getStatus());
        }
        return $this->responseJson(['message' => 'Invalid User ID'], 400);
    }

    /**
     * Method to add participants to an event
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/events/add/participants")
     * ),
     * @SWG\Post(
     *     path="/events/add/participants",
     *     summary="add multiple or a single participant to an event",
     *     tags={"events"},
     *     description="add multiple or a single participant to an event",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="eventId",
     *         in="formData",
     *         description="Id of the event that will be inserted the user",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="An array of users ids to be added to the event.",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="array"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : User added to event , status : 200}",
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="{message : User is already in this event , status : 403}",
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="{message : This user is not an admin , status : 401}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addParticipants(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));

        if ($token->getClaim("context")->level == "admin") {
            $eventService = new EventService();
            $eventService->setEnroll(
                $this->getEntityManager(),
                $request->get("eventId"),
                $request->get("userId")
            );

            return $this->responseJson($eventService->getMessage(), $eventService->getStatus());
        }

        return $this->responseJson(["message" => "This user is not an admin"], 401);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/events/enroll/{eventId}")
     * ),
     * @SWG\Delete(
     *     path="/events/enroll/{eventId}",
     *     summary="Enroll Events User",
     *     tags={"events"},
     *     description="Enroll Events User",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="eventId",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : User Disenrolled , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Event not valid , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function unenroll(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->setUnEnrollWork(
                $this->getEntityManager(),
                $request->attributes->get('eventId'),
                $token->getClaim('context')->user_id
            );

            $event->setUnEnroll(
                $this->getEntityManager(),
                $request->attributes->get('eventId'),
                $token->getClaim('context')->user_id
            );
            
            return $this->responseJson(['message' => $event->getMessage()], $event->getStatus());
        }
        return $this->responseJson(['message' => 'Invalid User ID'], 400);
    }

    /**
     * Method to set the deadline of an event.
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/events/deadline/{eventId}")
     * ),
     * @SWG\Post(
     *     path="/events/deadline/{eventId}",
     *     summary="This route will set the deadline to users be able or not to submit works",
     *     tags={"events"},
     *     description="The admin is able to enable or disable the dateline date to students submit works",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="deadlineStatus",
     *         in="formData",
     *         description="Status will define if the deadline is enable or not",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="boolean"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="deadlineDate",
     *         in="formData",
     *         description="date is the final date to submit works to an event",
     *         required=true,
     *         type="date",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="deadlineTime",
     *         in="formData",
     *         description="The time to be use to create a DateTime to deadline",
     *         required=true,
     *         type="date",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : DeadLine was changed, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/Events")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : This event was not found, status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function deadLine(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['eventId'] = $request->attributes->get('eventId');
        $data['deadlineStatus'] = (boolean) $request->get('deadlineStatus');

        if (!is_null($request->get('deadlineDate'))) {
            $data['deadlineTime'] = $request->get('deadlineTime');
            $date = new \DateTime(date($request->get('deadlineDate')), new \DateTimeZone('UTC'));
            $date = (string)$date->format('Y-m-d') . ' ' . $data['deadlineTime'];
            $data['deadlineDate'] =  \DateTime::createFromFormat('Y-m-d H:i A', $date);

        }

        if ($token->getClaim('context')->level == 'admin') {
            $eventService = new EventService();
            $eventService->setDeadLine($this->getEntityManager(), $data);
            return $this->responseJson(['message' => $eventService->getMessage()], $eventService->getStatus());
        }

        return $this->responseJson(['message' => 'You need to be an admin to access this function'], 200);
    }

    /**
     * Method to remove a participant to an event
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/events/remove/participants")
     * ),
     * @SWG\Post(
     *     path="/events/remove/participants",
     *     summary="Remove an participant from an event",
     *     tags={"events"},
     *     description="Remove an participant from an event",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="eventId",
     *         in="header",
     *         description="Id of the event that will have the user removed",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="header",
     *         description="The id of the user that will be removed from the event",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : User removed from the event , status : 200}",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Event not valid, status : 400}",
     *     ),
     *      @SWG\Response(
     *         response="401",
     *         description="{message : This user is not an admin , status : 401}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function removeParticipant(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));

        if ($token->getClaim('context')->level == "admin") {
            $eventService = new EventService();

            $eventService->setUnEnrollWork(
                $this->getEntityManager(),
                $request->headers->get('eventId'),
                $request->headers->get('userId')
            );

            $eventService->setUnEnroll(
                $this->getEntityManager(),
                $request->headers->get('eventId'),
                $request->headers->get('userId')
            );

            return $this->responseJson($eventService->getMessage(), $eventService->getStatus());
        }

        return $this->responseJson(["message" => "This user is not an admin"], 401);
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/details/{eventId}")
     * ),
     * @SWG\Get(
     *     path="/events/details/{eventId}",
     *     summary="get all events enrolled list",
     *     tags={"events"},
     *     description="get all events enrolled list",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="eventId",
     *         in="query",
     *         description="User TO Follow Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function eventsDetails(Request $request)
    {
        $id = $request->get('eventId');
        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $event = new EventService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $event->getEventDetails(
                $this->getEntityManager(),
                $id,
                $token->getClaim('context')->user_id
            );
            return $this->responseJson(['message' => $event->getMessage()], $event->getStatus());
        }
        return $this->responseJson(['message' => 'Invalid User ID'], 400);
    }



    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/events/details/{eventId}/works")
     * ),
     * @SWG\Get(
     *     path="/events/details/{eventId}/works",
     *     summary="get all events enrolled list",
     *     tags={"events"},
     *     description="get all events enrolled list",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="eventId",
     *         in="query",
     *         description="User TO Follow Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function getSubmittedWorks(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['eventId']  = $request->get('eventId');
        $data['limit'] = $request->get('limit', 25);
        $data['skip'] = $request->get('skip', 0);
        
        $eventService = new EventService();
        $events = $eventService->getEventDetails(
            $this->getEntityManager(),
            $data['eventId'],
            $data['userId']
        );
        if (count($events['works']) > 0) {
            $workService = new WorkService();
            $works = $workService->getWorkByRange($this->getEntityManager(), $data, $events['works']);
            return $this->responseJson($works, 200);
        }
        return $this->responseJson('work not found', 200);
    }

    /**
     * Method to sort works submitted on a live session
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT", uri="/events/works/order")
     * ),
     * @SWG\Put(
     *     path="/events/works/order",
     *     summary="Request to update work queue order",
     *     tags={"events"},
     *     description="Request to update work queue order",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="eventId",
     *         in="formData",
     *         description="live session Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="work identifier",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="formData",
     *         description="order number",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been sorted, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/events")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function sessionWorksOrder(Request $request)
    {
        $data = $request->request->all();
        $eventService = new EventService();
        $eventService->sessionWorksOrder($this->getEntityManager(), $data);
        return $this->responseJson($eventService->getMessage(), $eventService->getStatus());
    }

    /**
     * Method to get works of enrolled users
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/event/{eventId}/users/works")
     * ),
     * @SWG\Get(
     *     path="/event/{eventId}/users/works",
     *     summary="get all works of enrolled users",
     *     tags={"events"},
     *     description="get all events enrolled list",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="eventId",
     *         in="path",
     *         description="session identifier ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : works object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/Events")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function getWorksOfEnrolledUsers(Request $request)
    {
        $data['limit'] = $request->query->get('limit',10);
        $data['sessionId'] = $request->get('eventId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['level'] = $token->getClaim('context')->level;
        $eventValidator = new EventValidator();
        $eventValidator->validateGetEnrolledUsersWork($this->getApplication(), $data);
        if (count($eventValidator->getErrorMessage()) > 0) {
            return $this->responseJson($eventValidator->getErrorMessage(), 400);
        }
        $eventService = new EventService();
        $eventService->getWorksOfEnrolledUsers($this->getEntityManager(), $data);
        return $this->responseJson($eventService->getMessage(), $eventService->getStatus());
    }
}

