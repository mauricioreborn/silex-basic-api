<?php

/**
 * SignUp Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Controllers;

use App\Controller;
use App\EmailTemplate\AccountEmailConfirm;
use App\Entities\ClassRoom;
use App\Entities\Community;
use App\Entities\User;
use App\Repository\UserRepository;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;
use App\Services\Community as CommunityService;
use App\Services\Email\EmailBuilder;
use App\Services\Profile as ProfileService;
use App\Services\TokenService;
use App\Validators\Email;
use App\Validators\LoginValidator;
use App\Validators\SignUpValidator;
use App\Validators\Token;
use App\Validators\UserValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Services\SignupService;
use App\Repository\ClassRoomRepository;

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class SignUp extends Controller
{

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/signup")
     * ),
     * @SWG\Post(
     *     path="/signup",
     *     summary="Register a New User",
     *     tags={"signup"},
     *     description="Register a New User",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="profile",
     *         in="formData",
     *         description="User Selected Profile",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="community",
     *         type="string",
     *         in="formData",
     *         description="User Selected Community",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="association",
     *         type="string",
     *         in="formData",
     *         description="User Association with classroom or not",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userPhoto",
     *         in="formData",
     *         description="User Uploaded Photo URL",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="firstName",
     *         in="formData",
     *         description="Users First Name",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         description="Users Last Name",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="proTitle",
     *         in="formData",
     *         description="Professional/Recruiter Users Title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="proCompany",
     *         in="formData",
     *         description="Professional/Recruiter Users Company",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="gender",
     *         in="formData",
     *         description="Users gender",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="birthDate",
     *         in="formData",
     *         description="Users Birth Day",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Users Phone Number",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User Email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="agreement",
     *         in="formData",
     *         description="User agreement accepted",
     *         required=true,
     *         type="boolean",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function signUp(Request $request)
    {
        $message = [
            'error' => true,
            'message' => 'profile not valid',
            'status' => 400
        ];
        
        $data = $request->request->all();

        $students = ['student','educator'];
        $professionals = ['professional','recruiter'];
        
        $saveUser = new SignupService($this->getApplication());
        
        if (in_array($request->get('profile'), $students)) {
            $returnMsg = $saveUser->registerStudent($data, $this->getEntityManager());
            $message = $returnMsg;
        }

        if (in_array($request->get('profile'), $professionals)) {
            $returnMsg = $saveUser->registerProfessional($data, $this->getEntityManager());
            $message = $returnMsg;
        }

        if (!empty($returnMsg['id'])) {
            $this->sendEmail($request->get('email'), $returnMsg['id'], $request->get('profile'));
        }

        return new JsonResponse(
            $message,
            $message['status']
        );
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     * @param Request $userId  Generated user id
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="email")
     * )
     *
     * @return JsonResponse
     */
    public function sendEmail($email, $userId, $profile)
    {
        $tokenGenerator = new TokenGenerator(new TokenBuilder());

        $userRepository = new UserRepository($this->getEntityManager()
        ->createQueryBuilder(User::class));

        $user = $userRepository->findAnUser($userId);

        $tokenService = new TokenService();
        $token = $tokenService->tokenBuild($user);
        $templateBuilder = new AccountEmailConfirm();
        if ($profile == 'professional') {
            $template = $templateBuilder->getProTemplate($token);
        } else {
            $template = $templateBuilder->getTemplate($token);
        }
        $tokenGenerator->setExpiration(86400);
        $emailBuider = new EmailBuilder($this->getApplication()['mailer']);
        $emailBuider->setTo($email);
        $emailBuider->setSubject('ACME Platform Registration');
        $emailBuider->setFrom('info@theacmenetwork.org');
        $emailBuider->setContent(
            $template
        );
        $message = $emailBuider->sendEmail() === 1
            ? 'email has been sent' : 'error to send email';
        return new JsonResponse($message);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="onboarding/confirm/account/{tokenId}")
     * )
     * @SWG\Get(
     *     path="onboarding/confirm/account/{tokenId}",
     *     tags={"signup"},
     *     operationId="Get communities and profiles",
     *     summary="get communities and profiles",
     *     description="verify if is an available email",
     *     @SWG\Parameter(
     *         name="tokenId",
     *         type="string",
     *         in="formData",
     *         description="community name ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{community : array , profiles : array}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function emailConfirm(Request $request)
    {
        $token = $this->getToken($request->get('tokenId'));
        $confirm = new SignupService($this->getApplication());
        $confirm->confirmEmail($this->getEntityManager(), $token->getClaim('context')->user_id);

        return $this->responseJson(['message' => $confirm->getMessage()], $confirm->getStatus());
    }



    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param Request $request validate an user.
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/signup/data")
     * )
     *
     * @SWG\Get(
     *     path="/signup/data",
     *     tags={"signup"},
     *     operationId="Get communities and profiles",
     *     summary="get communities and profiles",
     *     description="verify if is an available email",
     *     @SWG\Parameter(
     *         name="comunnity",
     *         type="string",
     *         in="formData",
     *         description="community name ",
     *         required=false,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{community : array , profiles : array}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getProfilesAndCommunities()
    {

        $profile = new ProfileService();
        $community =  new CommunityService();
        $entity = $this->getEntityManager();

        $profiles = $profile->getProfiles($entity);

        $communities = $community->getCommunities(
            $entity->getRepository(
                Community::class
            )
        );

        return new JsonResponse(['profile'=>$profiles, 'communities' => $communities]);
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param Request $request validate an user.
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/signup/email/{email}")
     * )
     *
     * @return JsonResponse
     */
    public function verifyEmail(Request $request)
    {
        $emailValidator = new Email();
        $loginValidator = new LoginValidator($this->getApplication());
        $entityManager = $this->getEntityManager();
        if ($emailValidator->existEmail($entityManager->getRepository(User::class), $request->get('email'))
            || !$loginValidator->validEmail($request->get('email'))) {
            return new JsonResponse(
                [
                    'error' => 'this email was used by another user or not is a valid email',
                    'message' => 'email unavailable',
                ],
                400
            );
        }

        return new JsonResponse(
            [
                'error' => false,
                'message' => 'email available',
            ],
            200
        );
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/resend/confirmation/{email}")
     * )
     *
     *  @SWG\Get(
     *     path="/resend/confirmation/{email}",
     *     summary="Resend Confirmation Email",
     *     tags={"signup"},
     *     description="Resend Confirmation Email",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         type="string",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : email sent, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message :email not valid, status : 400}",
     *     ),
     *     ),
     *
     * @return JsonResponse
     */
    public function resendConfirmEmail(Request $request)
    {
        $saveUser = new SignupService($this->getApplication());
        $send = $saveUser->resendEmail($this->getEntityManager(), $request->get('email'));
        if ($send === false) {
            return $this->responseJson(['message' => $saveUser->getMessage()], $saveUser->getStatus());
        }
        $this->sendEmail($request->get('email'), $send->getId(), $send->getProfile());
        return $this->responseJson(['message' => $saveUser->getMessage()], $saveUser->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/classcode/{classCode}")
     * )
     *
     *  @SWG\Get(
     *     path="/classcode/{classcode}",
     *     summary="verify if is a valid classCode",
     *     tags={"signup"},
     *     description="method to verify if is a valid classCode",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="classCode",
     *         type="string",
     *         in="path",
     *         description="classCode Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message: is a valid classCode, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message :not is a valid classcode, status : 400}",
     *     ),
     *     ),
     *
     * @return JsonResponse
     */
    public function verifyClassCode(Request $request)
    {
        $userValidator = new UserValidator();
        if ($userValidator->validateMongoId($request->get('classCode'))) {
            $signUpService = new SignupService($this->getEntityManager()->createQueryBuilder(ClassRoom::class));
            $signUpService->verifyClassCode($request->get('classCode'));
            return $this->responseJson($signUpService->getMessage(), $signUpService->getStatus());
        }
        return $this->responseJson($userValidator->getErrorMessage(), $userValidator->getStatus());
    }

    /**
     * This method returns all classrooms with their teachers
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/signup/classroom/information")
     * )
     *
     *  @SWG\Get(
     *     path="/signup/classroom/information",
     *     summary="return all classrooms and their teachers",
     *     tags={"signup"},
     *     description="method returns all classrooms with their teachers",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="limit",
     *         type="integer",
     *         in="path",
     *         description="limit of classrooms to get",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="skip",
     *         type="integer",
     *         in="path",
     *         description="number of classrooms to be skipped",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/Classrooms")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : There is not Classrooms, status : 400}",
     *     ),
     *  ),
     *
     * @return JsonResponse
     */
    public function getClassInformation(Request $request)
    {
        $data['limit'] = $request->get('limit');
        $data['skip'] = $request->get('skip');

        $signupValidator = new SignUpValidator();
        $validData = $signupValidator->validateGetClassQuery(
            $data,
            $this->getApplication()
        );

        if ($validData == true) {
            $signupService = new SignupService($this->getApplication());
            $classroomRepository = new ClassRoomRepository(
                $this->getEntityManager()
            );
            $userRepository = new UserRepository($this->getEntityManager());

            $classrooms = $signupService->classroomsInformation(
                $data,
                $classroomRepository,
                $userRepository
            );

            if ($classrooms == false) {
                return $this->responseJson(
                    $signupService->getMessage(),
                    $signupService->getStatus()
                );
            }

            return $this->responseJson(
                $classrooms,
                $signupService->getStatus()
            );
        }

        return $this->responseJson($signupValidator->getErrorMessage(), 400);
    }
    
    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/upload/avatar")
     * ),
     * @SWG\Post(
     *     path="/upload/avatar",
     *     summary="Upload Avatar",
     *     tags={"signup"},
     *     description="Upload Avatar",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userPhoto",
     *         in="formData",
     *         description="User Uploaded Photo",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function sendAvatar(Request $request)
    {
        $userPhoto = $request->files->get('userPhoto');
        $signUpService = new SignupService($this->getApplication());
        $signUpService->uploadAvatar($userPhoto);
        return $this->responseJson($signUpService->getMessage(), $signUpService->getStatus());
    }
}
