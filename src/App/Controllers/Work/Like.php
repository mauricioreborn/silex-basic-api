<?php

namespace App\Controllers\Work;

use App\Entities\User;
use App\Entities\Work as WorkEntity;
use App\Notification\Work\Likes;
use App\Notification\NotificationQuerys;
use App\Services\NotificationService;
use App\Services\UserService;
use App\Services\Work\LikeService;
use App\Services\Work\WorkService;
use App\Validators\UserValidator;
use App\Validators\Work\LikeValidator;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Like extends Work
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/work/like/{workId}")
     * ),
     * @SWG\Post(
     *     path="/work/like",
     *     summary="add a new like",
     *     tags={"work"},
     *     description="Upload a New Work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function setLike(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $likeValidator = new LikeValidator();
        $data['workId'] = $request->get('workId');
        $data['userId'] = $token->getClaim('context')->user_id;

        if ($likeValidator->userLikedWork($this->getEntityManager()->createQueryBuilder(WorkEntity::class),
                $data) || $this->workExist($data['workId']) == false) {
            return $this->responseJson('Invalid WorkId or This work has already been liked.', 400);
        }
        $likeService  = new LikeService();
        $likeService->addLike(
            $this->getEntityManager(),
            $data
        );
        $likeService->addLikeOnUser($this->getEntityManager(), $data);

        $notificationQuery = new NotificationQuerys();

        $data['owner'] = $notificationQuery->getUserIdOwnerId(
            $this->getEntityManager()->createQueryBuilder(WorkEntity::class),
            $data['workId']
        );

        $data['type'] = 'new like';
        $data['postId'] = $data['workId'];
        $data['icon'] = 'favorite';

        $workService = new WorkService();

        $workService->updateWorActivity(
            $this->getEntityManager(),
            $data
        );

        $notificationService = new NotificationService(new Likes());
        $notification = $notificationService->create($data);
        $notificationService->save(
            $this->getEntityManager(),
            $notification,
            $data
        );
        return $this->responseJson($likeService->getMessage(), $likeService->getStatus());
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/work/like/{workId}")
     * ),
     * @SWG\Delete(
     *     path="/work/like/{workId}",
     *     summary="remove a like",
     *     tags={"work"},
     *     description="Upload a New Work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="path",
     *         description="work Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function removeLike(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userValidator = new UserValidator();
        $likeValidator = new LikeValidator();
        $data['workId'] = $request->get('workId');
        $data['userId'] = $token->getClaim('context')->user_id;
        if ($likeValidator->userLikedWork(
                $this->getEntityManager()->createQueryBuilder(WorkEntity::class),
                $data
            )
        ) {
            $likeService  = new LikeService();
            $likeService->deleteLike(
                $this->getEntityManager(),
                $data
            );
            $userService = new UserService();
            $userService->removeLikes($this->getEntityManager(), $data);
            return $this->responseJson($likeService->getMessage(), $likeService->getStatus());
        }
        return $this->responseJson('like not found', 400);
    }

}