<?php

namespace App\Controllers\Work;

use App\Entities\CommentLike;
use App\Entities\ReplyComment;
use App\Entities\User;
use App\Entities\Work as WorkEntity;
use App\Notification\NotificationQuerys;
use App\Notification\ReplyComments;
use App\Notification\Users;
use App\Repository\UserRepository;
use App\Services\NotificationService;
use App\Services\UserService;
use App\Services\Work\CommentLikeService;
use App\Services\Work\CommentService;
use App\Services\Work\WorkService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use App\Notification\Work\Comments;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Comment extends Work
{

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/work/comment")
     * ),
     * @SWG\Post(
     *     path="/work/comment",
     *     summary="inset new comment at work",
     *     tags={"work"},
     *     description="Upload a New Comment at Work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="commentId",
     *         in="formData",
     *         description="CommentId to reply",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="comment",
     *         type="string",
     *         in="formData",
     *         description="comment description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : comment has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definition/comment")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Error Message , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function comment(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['levelNumber'] = $token->getClaim('context')->levelNumber;
        $data['postId'] = $request->request->get('id');
        $data['type'] = 'work';
        $data['status'] = 'enabled';
            $workService = new WorkService();
        $work = $workService->findWork(
            $this->getEntityManager()->createQueryBuilder(WorkEntity::class),
            $data['postId']
        );

        if (count($work) > 0) {

            $checkWorkToComment = new CommentService();
            $cwt = $checkWorkToComment->checkWorkComment(
                $this->getEntityManager()->createQueryBuilder(\App\Entities\Comments::class),
                $data
            );

            if ((int)$data['levelNumber'] < (int)$work->getLevel()->getNumber()) {
                return $this->responseJson(['message' => "you can't comment on this work"], 401);
            }
            if (!empty($data['commentId'])) {
                $workService->postReplyComment($this->getEntityManager(), $data);

                $notificationQuery = new NotificationQuerys();

                $data['owner'] = $notificationQuery->getUserIdOwnerId(
                    $this->getEntityManager()->createQueryBuilder(\App\Entities\Comments::class),
                    $data['commentId']
                );

                $notificationService = new NotificationService(new ReplyComments());
                $notification = $notificationService->create($data);
                $notificationService->save(
                    $this->getEntityManager(),
                    $notification,
                    $data
                );
                return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
            }

            $workService->postComment(
                $this->getEntityManager()->createQueryBuilder(WorkEntity::class),
                $this->getEntityManager(),
                $data
            );

            $data['type'] = 'new comment';
            $data['icon'] = 'mode_comment';

            $workService->updateWorActivity(
                $this->getEntityManager(),
                $data
            );


            if ($work->getUserId() != $data['userId']) {
                if ($cwt == 0) {
                    $workService->updateTotalWorkComment(
                        $this->getEntityManager()->createQueryBuilder(User::class),
                        $data['userId']
                    );
                }

                $notificationQuery = new NotificationQuerys();

                $data['owner'] = $notificationQuery->getUserIdOwnerId(
                    $this->getEntityManager()->createQueryBuilder(WorkEntity::class),
                    $data['postId']
                );

                $notificationService = new NotificationService(new Comments());
                $notification = $notificationService->create($data);
                $notificationService->save(
                    $this->getEntityManager(),
                    $notification,
                    $data
                );
            }

            $userService = new UserService();

            if ($userService->canSendToReview(
                $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
                $data
            )) {
                $userRepository = new UserRepository($this->getEntityManager()->createQueryBuilder(User::class));
                if ($userService->verifyAlreadyNotification(
                    $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
                    $data['userId'])
                ) {
                    $notificationService = new NotificationService(new Users());
                    $notification = $notificationService->allowToReview($data);
                    $notificationService->save(
                        $this->getEntityManager(),
                        $notification,
                        $data
                    );
                    $userRepository->setAlreadyNotified($data['userId']);
                }
            }
        }
        return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/work/{id}/comment/{commentId}/like")
     * ),
     * @SWG\Post(
     *     path="/work/{id}/comment/{commentId}/like",
     *     summary="add a like on comment",
     *     tags={"work"},
     *     description="add a like on comment",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="work Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addLike(Request $request)
    {
        $data['productId'] = $request->get('id');
        $data['commentId'] = $request->get('commentId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        if ($this->validateId($data['commentId'])) {
            $commentService = new CommentLikeService();
            if (!$commentService->verifyLike(
                $this->getEntityManager(),
                $data
            )
            ) {
                $commentService->addLike($this->getEntityManager(), $data);
                return $this->responseJson($commentService->getMessage(), $commentService->getStatus());
            }
        }
        return $this->responseJson('the user already liked this comment or This comment does not belong to work', 400);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/work/{id}/comment/{commentId}/like"),
     *      @SLX\Request(method="DELETE", uri="/stream/{id}/comment/{commentId}/like"),
     * ),
     * @SWG\Delete(
     *     path="/work/{id}/comment/{commentId}/like",
     *     summary="remove a like on comment",
     *     tags={"comment"},
     *     description="remove a like on comment",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="work Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *       @SWG\Parameter(
     *         name="commentId",
     *         in="path",
     *         description="Comment Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         type="string",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function deleteLike(Request $request)
    {
        $data['productId'] = $request->get('id');
        $data['commentId'] = $request->get('commentId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        if ($this->validateId($data['commentId'])) {
            $commentService = new CommentLikeService();
            if ($commentService->verifyLike(
                $this->getEntityManager(),
                $data
            )
            ) {
                $commentService->removeLike(
                    $this->getEntityManager()
                        ->createQueryBuilder(CommentLike::class),
                    $data
                );
                return $this->responseJson($commentService->getMessage(), $commentService->getStatus());
            }
            return $this->responseJson('commentId not is valid', 400);
        }
    }
}