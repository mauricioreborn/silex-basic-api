<?php

namespace App\Controllers\Work;

use App\Entities\Work as WorkEntity;
use App\Services\Work\ViewService;
use Symfony\Component\HttpFoundation\Request;
use App\Entities\View as ViewEntity;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class View extends Work
{

    /**
     * Method to add more one view
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/work/view")
     * ),
     * @SWG\Post(
     *     path="/work/view",
     *     summary="inset new view on work",
     *     tags={"work"},
     *     description="inset new view on work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="work title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : comment has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Error Message , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function addView(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['workId'] = $request->request->get('workId');
        $viewService = new ViewService();

        if ($viewService->userViewedWork(
            $data,
            $this->getEntityManager()->createQueryBuilder(ViewEntity::class)
        )) {
            return $this->responseJson($viewService->getMessage(), $viewService->getStatus());
        }
        $viewService->addView($data, $this->getEntityManager());
        $viewService->addViewOnWork($data, $this->getEntityManager());
        return $this->responseJson($viewService->getMessage(), $viewService->getStatus());
    }
}