<?php

namespace App\Controllers\Work;

use App\Controller;
use App\Entities\Comments;
use App\Entities\ReplyComment;
use App\Entities\User;
use App\Notification\Users;
use App\Notification\Works;
use App\Repository\UserRepository;
use App\Repository\WorkRepository;
use App\Services\Comment\CommentService;
use App\Services\NotificationService;
use App\Services\UserService;
use App\Services\Work\RefineByFactory;
use App\Services\Work\WorkService;
use App\Validators\Profile\ProfileValidator;
use App\Validators\SessionValidator;
use App\Validators\UserValidator;
use Symfony\Component\HttpFoundation\Request;
use App\Validators\Work as WorkValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use App\Services\Commom\UploadPhoto;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Work extends Controller
{
    private $fileStyles = [
        "jpg",
        "jpeg",
        "png",
        "gif",
        "mp4",
        "mov",
        "avi",
        "wmv",
        "pdf",
        "doc",
        "docx",
        "xls",
        "xlsx",
        "ppt",
        "pptx"
    ];

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/getwork")
     * ),
     * @SWG\Get(
     *     path="/getwork",
     *     summary="get all works",
     *     tags={"work"},
     *     description="get all works by idUser",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     )
     * )
     */
    public function getWorks(Request $request)
    {
        $workRepository = new WorkRepository($this->getEntityManager());
        $filters = $request->query->all();
        $filters['waitingReview'] = false;

        if ($request->get('browse') == 'waitingReview') {
            $filters['waitingReview'] = true;
            unset($filters['browse']);
        }

        if (!isset($filters['skip'])) {
            $filters['skip'] = 0;
        }

        if (isset($filters['search'])) {
            if ($filters['search'] == '') {
                unset($filters['search']);
            }
        }

        $filters['isPrivate'] = false;

        $token = $this->getToken($request->headers->get('token'));
        $filters['myLevel'] = $token->getClaim('context')->level;
        
        $refine = new RefineByFactory($this->getEntityManager(), $token->getClaim('context')->level);
        $queryFiltered = $refine->startSearch($filters);
        return $this->responseJson($workRepository->getAllWork($queryFiltered), 200);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/work/{workId}"),
     * ),
     * @SWG\Get(
     *     path="/work/{workId}",
     *     summary="get a work by workId",
     *     tags={"work"},
     *     description="get a work by workId",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="workId",
     *         in="query",
     *         description="workId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getWorkById(Request $request)
    {
        $workService = new WorkService();
        $userService = new UserService();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['workId'] = $request->get('workId');
        if ($userService->validateId($request->get('workId'))) {
            $work = $workService->getWorkById(
                $this->getEntityManager(),
                $data
            );
            $work['versions'] = $workService->getWorkVersion($this->getEntityManager(),
                $data);

            return $this->responseJson($work, 200);
        }
        return $this->responseJson('invalid workId', 400);
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="/check/{type}/{userId}/{id}"),
     * ),
     * @SWG\Get(
     *     path="/check/{type}/{userId}/{id}",
     *     summary="Check Likes",
     *     tags={"work"},
     *     description="check Likes",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="type",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="query",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="id",
     *         in="query",
     *         description="id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function checkLikes(Request $request)
    {
        $data = $request->attributes->all();
        $workService = new WorkService();
        $userService = new ProfileValidator();
        if ($userService->validateProfile($data, $this->getEntityManager())) {
            $workService->checkLikes(
                $this->getEntityManager(),
                $data['type'],
                $data['userId'],
                $data['id']
            );
            return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
        }
        return $this->responseJson(['message' => $userService->getErrorMessage()], $userService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/refine/{type}")
     * ),
     * @SWG\Get(
     *     path="/refine",
     *     summary="get all filters",
     *     tags={"work"},
     *     description="get all filters to the discover page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="type",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getRefineBy(Request $request)
    {
        $data = $request->attributes->all();
        $token = $this->getToken($request->headers->get('token'));
        $ws = new WorkService();
        $ws->fetchCategories($this->getEntityManager(), $token->getClaim('context')->user_id);
        $level = $token->getClaim('context')->acl;
        $options = [
            [
                'label' => 'Popular', 'alias' => 'popular'
            ],
            [
                'label' => 'Recent', 'alias' => 'recent'
            ],
            [
                'label' => 'Need Feedback', 'alias' => 'feedback'
            ],
            [
                'label' => 'Reviewed', 'alias' => 'reviewed'
            ]
        ];
        if ($level == 'pro') {
            $options = [
                [
                    'label' => 'Popular', 'alias' => 'popular'
                ],
                [
                    'label' => 'Recent', 'alias' => 'recent'
                ],
                [
                    'label' => 'Need Feedback', 'alias' => 'feedback'
                ],
                [
                    'label' => 'Reviewed', 'alias' => 'reviewed'
                ],
                [
                    'label' => 'Waiting Review', 'alias' => 'waitingReview'
                ],
                [
                    'label' => 'Best Rated', 'alias' => 'bestRated'
                ]
            ];
        }
        $communities = $ws->getMessage();
        $refine = [
            [
                'section' => 'Browse',
                'alias' => 'browse',
                'options' => $options

            ],
            [
                'section' => 'Time',
                'alias' => 'time',
                'options' =>
                    [
                        [
                            'label' => 'Today', 'alias' => 'today'
                        ],
                        [
                            'label' => 'This Week', 'alias' => 'week'
                        ],
                        [
                            'label' => 'This Year', 'alias' => 'year'
                        ],
                        [
                            'label' => 'All the Time', 'alias' => 'allTime'
                        ],

                    ],
            ],
            [
                'section' => 'Categories',
                'alias' => 'category',
                'options' => $communities,

            ]
        ];
        if ($data['type'] === 'works') {
            $refine[] = [
                'section' => 'Student Level',
                'alias' => 'level',
                'options' =>
                    [
                        [
                            'label' => 'All levels', 'alias' => 'allLevel'
                        ],
                        [
                            'label' => 'Level 1', 'alias' => 'level1'
                        ],
                        [
                            'label' => 'Level 2', 'alias' => 'level2'
                        ],
                        [
                            'label' => 'Level 3', 'alias' => 'level3'
                        ],
                    ],
            ];
        }
        return $this->responseJson($refine, 200);
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/upload/file")
     * ),
     * @SWG\Post(
     *     path="/upload/file",
     *     summary="Upload Work File To Get Url",
     *     tags={"files"},
     *     description="Upload Work File To Get Url",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="file",
     *         in="formData",
     *         description="User Uploaded File",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function addWorkFile(Request $request)
    {
            $token = $this->getToken($request->headers->get('token'));
            $photoService = new UploadPhoto($this->getApplication(), $token->getClaim('context')->user_id);
            $work['path'] = $s3Response = $photoService->upload($request->files->get('file'));
            $work['type'] = $request->files->get('file')->getClientOriginalExtension();
            $work['name'] = $request->files->get('file')->getClientOriginalName();
            return $this->responseJson($work, 200);
    }


    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/upload/work")
     * ),
     * @SWG\Post(
     *     path="/upload/work",
     *     summary="Upload a new work",
     *     tags={"Work"},
     *     description="Upload a new work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="preciso terminar essa documentacao",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error message , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function addWork(Request $request)
    {
        $data = $request->request->all();
        if (empty($data['thumbnail']) || $data['thumbnail'] == null) {
            $data['thumbnail'] = $data['mainFile'];
        }
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['studentLevel'] = $token->getClaim('context')->level;
        $data['levelNumber'] = $token->getClaim('context')->levelNumber;
        $data['availableReview'] = $request->request->get('availableReview') ?
            $request->request->get('availableReview') :
            false;
        $data['sendPortfolio'] = $request->request->get('sendPortfolio') ?
            $request->request->get('sendPortfolio') :
            false;

        $data['isPrivate'] = (bool) $request->request->get('isPrivate');

        if (!is_null($request->request->get('labId'))) {
            $data['labId'] = $request->request->get('labId');
        }

        $data['group'] = $request->request->get('group') ?
            $request->request->get('group') :
            'work';
        $workService = new WorkService();

        $data['type'] = 'new';
        $workValidator = new WorkValidator();
        if ($workValidator->validateWorkInputs($this->getApplication(), $data)) {
            if (isset($data['workId'])) {
                $data['group'] = 'work-version';
                $workService->setWorkVersion($this->getEntityManager(), $data);
                return $this->responseJson(
                    $workService->getMessage(),
                    $workService->getStatus()
                );
            }

            $workService->setWork($this->getEntityManager(), $data);

            $this->reviewRules($data);
            return $this->responseJson(
                $workService->getMessage(),
                $workService->getStatus()
            );
        }
        return $this->responseJson(
            $workValidator->getErrorMessage(),
            $workValidator->getStatus()
        );
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/work/{workId}")
     * ),
     * @SWG\Delete(
     *     path="/work/{workId}",
     *     summary="delete a work by Id",
     *     tags={"work"},
     *     description="get all filters to the discover page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="workId",
     *         in="path",
     *         description="workId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function deleteWork(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['workId'] = $request->get('workId');
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['level'] = $token->getClaim('context')->level;
        $data['type'] = 'delete';
        $userService = new UserService();
        if ($userService->validateId($data['workId'])) {
            $workService = new WorkService();
            $workService->deleteWork($this->getEntityManager()->createQueryBuilder(\App\Entities\Work::class), $data);
            $commentService = new CommentService();

            $commentIds = $commentService->getIdCommentsByProductId(
                $this->getEntityManager()->createQueryBuilder(Comments::class),
                $data['workId']
            );

            if ($commentIds) {
                $commentService->deleteReplyCommentsByCommentId(
                    $this->getEntityManager()->createQueryBuilder(ReplyComment::class),
                    $commentIds
                );

                $commentService->deleteCommentsByProductId(
                    $this->getEntityManager()->createQueryBuilder(Comments::class),
                    $data['workId']
                );
            }
            return $this->responseJson($workService->getMessage(), $workService->getStatus());
        }
    }


    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/update/work/{workId}")
     * ),
     * @SWG\Put(
     *     path="/update/work/{workId}",
     *     summary="Upload a new work",
     *     tags={"Work"},
     *     description="Upload a new work",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="path",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="category",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="work description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="mainFile",
     *         in="formData",
     *         description="main file object",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="thumbnail",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="available",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error message , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function editWork(Request $request)
    {
        $data = $request->request->all();
        $data['workId'] = $request->get('workId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['type'] = 'edit';
        $data['isPrivate'] = !is_null($request->request->get('isPrivate')) ?
            $request->request->get('isPrivate') :
            false;
        $workService = new WorkService();

        if ($workService->workHasReviewed(
            $this->getEntityManager()->createQueryBuilder(\App\Entities\Work::class),
            $data['workId']
        )
        ) {
            return $this->responseJson($workService->getMessage(), $workService->getStatus());
        }
        $workService->editWork(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($workService->getMessage(), $workService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/user/works/{workName}")
     * ),
     * @SWG\Get(
     *     path="/user/works/{workName}",
     *     summary="Fetch user works by name",
     *     tags={"work"},
     *     description="Fetch user works by name",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="workName",
     *         in="query",
     *         description="workName",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function userWorks(Request $request)
    {
        $data['workName'] = $request->attributes->get('workName');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $workService = new WorkService();
        $workService->userWorksByName(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
    }

    /**
     * Method to add a work on a session
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/submit/session/work")
     * ),
     * @SWG\Post(
     *     path="/submit/session/work",
     *     summary="Submit a Work to a Specific Live Session",
     *     tags={"events"},
     *     description="Submit a Work to a Specific Live Session",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="User Work Name",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="session identifier",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="work creator identifier",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="action",
     *         in="formData",
     *         description="remove or add ",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been added/removed , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function submitSessionWork(Request $request)
    {
        $data = $request->request->all();
        $data['workOwnerId'] = $data['userId'];
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['type'] = $token->getClaim('context')->level;
        $workService = new WorkService();
        $sessionValidator = new SessionValidator();
        $validator = $sessionValidator->validateSubmitWork($this->getApplication(), $data);
        if ($validator == false) {
            return $this->responseJson($sessionValidator->getErrorMessage(), 400);
        }
        $workService->submitSessionWork(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
    }

    /** Method to check rules to review
     * @param $data
     */
    public function reviewRules($data)
    {
        $workService = new WorkService();
        $userService = new UserService();
        $userRepository = new UserRepository($this->getEntityManager()->createQueryBuilder(User::class));


        if ($userService->canSendToReview(
            $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
            $data
        )
        ) {
            if ($workService->workOnReview(
                    $this->getEntityManager()->createQueryBuilder(\App\Entities\Work::class),
                    $data['userId']
                ) && $data['availableReview'] == true
            ) {
                $userService->removeTotalCommentsAndWorks(
                    $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
                    $data['userId']
                );
            }

            if ($userService->verifyAlreadyNotification(
                $this->getEntityManager()->createQueryBuilder(\App\Entities\User::class),
                $data['userId'])
            ) {
                $notificationService = new NotificationService(new Users());
                $notification = $notificationService->allowToReview($data);
                $notificationService->save(
                    $this->getEntityManager(),
                    $notification,
                    $data
                );
                $userRepository->setAlreadyNotified($data['userId']);
            }
        }
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/portfolio/work/{workId}")
     * ),
     * @SWG\Get(
     *     path="/portfolio/work/{workId}",
     *     summary="check if a work can to submitted to portfolio ",
     *     tags={"work"},
     *     description="check if a work can to submitted to portfolio",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="workId",
     *         in="path",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message :this work may be sent to portfolio, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : this work cannot be sent to portfolio , status : 401}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function sendToPortfolio(Request $request)
    {
        $data['workId'] = $request->get('workId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $workService = new WorkService();
        $userService = new UserService();
        
        if ($userService->validateId($data['workId'])) {
            $workService->sendToPortfolio(
                $this->getEntityManager()->createQueryBuilder(\App\Entities\Work::class),
                $data['workId'],
                $data['userId']
            );
            return $this->responseJson(['message' => $workService->getMessage()], $workService->getStatus());
        }
        return $this->responseJson(['message' => 'invalid work id'], $workService->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/profile/user/{userId}/cowork")
     * ),
     * @SWG\Get(
     *     path="/profile/user/{userId}/cowork",
     *     summary="get works that an user help to build",
     *     tags={"work"},
     *     description="get works that an user help to build",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="path",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="limit of work",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="skip",
     *         in="query",
     *         description="skip of work",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message :works array, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : empty array, status : 401}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getMyCoWorks(Request $request)
    {
        $data['userId'] = $request->get('userId');
        $data['limit'] = $request->get('limit',25);
        $data['skip'] = $request->get('skip',0);
        $workService = new WorkService();
        $workService->getMyCoWorks($this->getEntityManager(), $data);
        return $this->responseJson($workService->getMessage(), $workService->getStatus());
    }
}