<?php

namespace App\Controllers\Settings;

use App\Controller;
use App\Entities\Comments;
use App\Entities\User;
use App\Entities\Work;
use App\Notification\Flag;
use App\Notification\NotificationQuerys;
use App\Services\Comment\CommentService;
use App\Services\FlagService;
use App\Services\NotificationService;
use App\Services\Setting\SettingService;
use App\Services\TokenService;
use App\Services\Work\WorkService;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use App\Entities\Flag as FlagEntity;

/**
 * Work Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Settings extends Controller
{


    /**
     * Method to send a edit user account information
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="setting/user/account")
     * ),
     * @SWG\Put(
     *     path="setting/user/account",
     *     summary="Update user account information",
     *     tags={"Setting"},
     *     description="Update user account information",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="firstName",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         description="f or m",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *       @SWG\Parameter(
     *         name="userPhoto",
     *         in="formData",
     *         description="userPhoto",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="bio",
     *         in="formData",
     *         description="bio",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="gender",
     *         in="formData",
     *         description="f or m",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="birthDate",
     *         in="formData",
     *         description="birth Date",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="phone number",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="community",
     *         in="formData",
     *         description="community array",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error message , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function account(Request $request)
    {
        $data = $request->request->all();
        $data['userPhoto'] = $request->request->get('userPhoto', null);
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $settingService = new SettingService();
        $settingService->changeAccount(
            $this->getEntityManager()->createQueryBuilder(User::class),
            $data
        );

        $repository = $this->getEntityManager()->getRepository(User::class);
        $user = $repository->findOneBy(
            [
                '_id' => new \MongoId($data['userId'])
            ]
        );

        $tokenService = new TokenService();
        $token = $tokenService->tokenBuild($user);

        $this->updateOtherEnvironment($request->request->all());
        return $this->responseJson(
            [
            'message' =>$settingService->getMessage(),
            'token' => $token
            ],
            $settingService->getStatus()
        );
    }



    /**
     * Method to send a edit user account information
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="setting/user/password")
     * ),
     * @SWG\Post(
     *     path="/setting/user/password",
     *     summary="Update user personal information",
     *     tags={"Setting"},
     *     description="Update user personal information",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="newPassword",
     *         in="formData",
     *         description="f or m",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="bio",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error message , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function password(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $settingService = new SettingService();
        if ($settingService->validatePassword(
            $this->getEntityManager(),
            $data
        )) {
            $settingService->changePassword(
                $this->getEntityManager()->createQueryBuilder(User::class),
                $data
            );
            return $this->responseJson(['message' => $settingService->getMessage()], $settingService->getStatus());
        }
        return $this->responseJson(['message' => $settingService->getMessage()], $settingService->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="setting/user")
     * )
     * @SWG\Get(
     *     path="setting/user",
     *     tags={"Setting"},
     *     operationId="Setting",
     *     summary="Setting",
     *     description="Get user data information",
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function getSettingsData(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $settingService = new SettingService();
        $settingService->getUserData(
            $this->getEntityManager()->createQueryBuilder(User::class),
            $data
        );
        return $this->responseJson($settingService->getMessage(), $settingService->getStatus());
    }

    /**
     * Method to send a edit user account information
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="setting/user/notification")
     * ),
     * @SWG\Put(
     *     path="setting/user/notification",
     *     summary="Update user personal information",
     *     tags={"Setting"},
     *     description="Update user personal information",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="notification",
     *         in="formData",
     *         description="work id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="bio",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work has been saved , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : error message , status : 400}",
     *     ),
     *     ),
     * @return  JsonResponse
     */
    public function notification(Request $request)
    {
        $data['notification'] = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $settingService = new SettingService();
        $settingService->changeNotification(
            $this->getEntityManager()->createQueryBuilder(User::class),
            $data
        );

        return $this->responseJson(['message' => $settingService->getMessage()], $settingService->getStatus());
    }



    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="flag/{type}/{productId}")
     * )
     * @SWG\Post(
     *     path="setting/flag/{type}/{productId}",
     *     tags={"Setting"},
     *     operationId="Setting",
     *     summary="Setting",
     *     description="flag a work or comment",
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="productId",
     *         in="path",
     *         description="workId or commentId",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="work or comment",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */
    public function flags(Request $request)
    {
        $data['productId'] = $request->get('productId');
        $data['type'] = $request->get('type');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $service = $data['type'] == 'work' ?
            new WorkService() :
            new CommentService();

        $flagService = new FlagService();

        if ($service->checkWasFlag($this->getEntityManager(), $data)) {
            return $this->responseJson(
                $service->getMessage(),
                $service->getStatus()
            );
        }
        
        $settingService = new SettingService();
        $settingService->setFlag($this->getEntityManager(), $data);


        $flagContent = $flagService->createFlagContent(
            $this->getEntityManager()->createQueryBuilder(FlagEntity::class),
            $data['userId']
        );

        $service->setFlag(
            $this->getEntityManager(),
            $flagContent,
            $data['productId']
        );

        return $this->responseJson(
            $service->getMessage(),
            $service->getStatus()
        );

        $notificationQuery = new NotificationQuerys();

        if ($data['type'] == 'work') {
            $data['owner'] = $notificationQuery->getUserIdOwnerId(
                $this->getEntityManager()->createQueryBuilder(Work::class),
                $data['productId']
            );
        } else {
            $data['owner'] = $notificationQuery->getUserIdOwnerId(
                $this->getEntityManager()->createQueryBuilder(Comments::class),
                $data['productId']
            );
        }

        $notificationService = new NotificationService(new Flag());
        $notification = $notificationService->create($data);
        $notificationService->save(
            $this->getEntityManager(),
            $notification,
            $data
        );
        return $this->responseJson(
            [
                'message' => $settingService->getMessage()
            ],
            $settingService->getStatus());
    }


    public function updateOtherEnvironment($data)
    {
        $data['url'] = 'https://law-api.acmeconnect.org/setting/user/account';
        $data['content'] = 'application/json';
        $content = isset($data['content']) ? $data['content'] : 'application/json';
        $opts = array('http' =>
            array(
                'method'  => 'PUT',
                'header'  => 'Content-Type: '.$content,
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );

        $data['url'] = 'https://eng-api.acmeconnect.org/setting/user/account';
        $data['content'] = 'application/json';
        $content = isset($data['content']) ? $data['content'] : 'application/json';

        $opts2 = array('http' =>
            array(
                'method'  => 'PUT',
                'header'  => 'Content-Type: '.$content,
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($opts);
        $context2 = stream_context_create($opts2);
        file_get_contents($data['url'], false, $context);
        file_get_contents($data['url'], false, $context2);
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="/flag/{type}/{productId}")
     * ),
     * @SWG\Delete(
     *     path="/flags",
     *     summary="remove a flag ",
     *     tags={"settings"},
     *     description="remove a flag of the product",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="query",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *        @SWG\Parameter(
     *         name="productId",
     *         in="path",
     *         description="string work id or string comment Id",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message :Flag has been removed , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/flag")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Flag not found , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function removeFlag(Request $request)
    {
        $data['type'] = $request->get('type');
        $data['productId'] = $request->get('productId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $data['level'] = $token->getClaim('context')->level;

        $settingService = new SettingService();
        $settingService->removeFlag($this->getEntityManager(), $data);
        return $this->responseJson($settingService->getMessage(), $settingService->getStatus());
    }
}