<?php

namespace App\Controllers\LiveSession;

use App\Controller;
use App\Repository\LiveSessionRepository;
use App\Services\Commom\UploadPhoto;
use App\Services\Groups\ClassRoomService;
use App\Services\LiveSession\LiveSessionService AS LiveService;
use App\Services\LiveSession\LiveSessionService;
use App\Validators\SessionValidator;
use App\Validators\LiveSession\SessionValidator as LiveSessionValidator;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use OpenTok\MediaMode;
use Swagger\Annotations as SWG;
use App\Entities\LiveSession as LiveSessionModel;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use DateTimeZone;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class LiveSession extends Controller
{
    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/session")
     * ),
     * @SWG\Get(
     *     path="/session",
     *     summary="get all session",
     *     tags={"Session"},
     *     description="get all recentactivity to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getLiveSession(Request $request)
    {
        $sessionService = new LiveSessionService();
        $sessionService->getSessions(
            $this->getEntityManager()->createQueryBuilder(
                LiveSessionModel::class
            )
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }


    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/session")
     * ),
     * @SWG\Post(
     *     path="/session",
     *     summary="add a new live session",
     *     tags={"Session"},
     *     description="add a new live session",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="live session title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         type="string",
     *         in="formData",
     *         description="live session description",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="formData",
     *         description="live session type (public,private)",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="speaker",
     *         type="string",
     *         in="formData",
     *         description="live session speaker",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="participants",
     *         type="string",
     *         in="formData",
     *         description="array of optional participans containing only id as ['id1','id2','id3',...]",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         description="live session date",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="startTime",
     *         in="formData",
     *         description="start Time (15:30)",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endTime",
     *         in="formData",
     *         description="end Time (17:30)",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="thumbnail",
     *         in="formData",
     *         description="cover image url",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="deadlineStatus",
     *         in="formData",
     *         description="This will define if the deadline is enabled or not",
     *         required=false,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="deadlineDate",
     *         in="formData",
     *         description="This will be the day of the deadline",
     *         required=false,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="deadlineTime",
     *         in="formData",
     *         description="This will be the time of the deadline and will be use do create a DateTime",
     *         required=false,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function createSession(Request $request)
    {
        $data = $request->request->all();
        $dt = new DateTime(date($data['date']), new DateTimeZone('UTC'));
        $newTime = (string)$dt->format('Y-m-d').' '.$data['startTime'];
        $d = \DateTime::createFromFormat('Y-m-d H:i A', $newTime);
        $data['date'] = $d;

        if (isset($data['deadlineDate'])) {
            $date = new DateTime(date($data['deadlineDate']), new DateTimeZone('UTC'));
            $newTime = (string)$date->format('Y-m-d') . ' ' . $data['deadlineTime'];
            $data['deadlineDate'] = DateTime::createFromFormat('Y-m-d H:i A', $newTime);
        }

        $data['deadlineStatus'] = (boolean) $request->get('deadlineStatus');

        $sessionValidator = new SessionValidator();
        if ($sessionValidator->requiredFields($this->getApplication(), $data)) {
            $sessionService = new LiveService();
            $sessionService->createSession(
                $this->getEntityManager(),
                $data
            );
            return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
        }
        return $this->responseJson(['message' => $sessionValidator->getErrorMessage()], $sessionValidator->getStatus());
    }

    /**
     * Method to edit livesession's cover image
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/session/upload/cover")
     * ),
     * @SWG\Post(
     *     path="/session/upload/cover",
     *     summary="Upload liveSession Cover",
     *     tags={"LiveSession"},
     *     description="Upload LiveSession Cover",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="coverImage",
     *         in="formData",
     *         description="LiveSession Cover Image as IMAGE",
     *         required=true,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="The unique identifier of liveSession",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Photo URL , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/liveSession")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Cover not uploaded , status : 400}",
     *     ),
     * ),
     * @return JsonResponse
     */
    public function uploadCover(Request $request)
    {
        $validator = new LiveSessionValidator();
        $validate['sessionId'] = is_null($request->get('sessionId')) ?
            false :
            $request->get('sessionId');

        $validate['coverImage'] = is_null($request->files->get('coverImage')) ?
            false :
            $request->files->get('coverImage');

        if ($validator->uploadCoverValidator(
            $this->getApplication(),
            $validate
        )
        ) {
            $folder = $request->request->get('sessionId') . '/cover';
            $token = $this->getToken($request->headers->get('token'));
            $photo = $request->files->get('coverImage');
            $uploadPhoto = new UploadPhoto(
                $this->getApplication(),
                $folder,
                'livesession'
            );

            $sessionInfo['cover'] = $uploadPhoto->upload($photo);
            $sessionInfo['sessionId'] = $request->get('sessionId');
            $sessionInfo['userId'] = $token->getClaim('context')->user_id;
            $sessionInfo['level'] = $token->getClaim('context')->level;

            $sessionService = new LiveSessionService();
            $sessionRepository = new LiveSessionRepository(
                $this->getEntityManager()
            );

            $sessionService->uploadCover(
                $sessionInfo,
                $sessionRepository
            );

            $sessionCover = $sessionService->getStatus() == 200 ?
                $sessionInfo['cover'] :
                $sessionService->getMessage();

            return $this->responseJson(
                ['message' => $sessionCover],
                $sessionService->getStatus()
            );
        }

        return $this->responseJson(
            [
                'message' => $validator->getErrorMessage()
            ],
            400
        );
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/session/token/{sessionId}")
     * ),
     * @SWG\Get(
     *     path="/session/token/{sessionId}",
     *     summary="Fetch token to enter session by userid and sessionid",
     *     tags={"Session"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="query",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Session not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function generateSessionToken(Request $request)
    {
        $data['sessionId'] = $request->get('sessionId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;

        $sessionService = new LiveService();
        $sessionService->generateSessionToken(
            $this->getEntityManager(),
            $data
        );

        $response = $sessionService->getMessage();
        if(!empty($response["users"])) {
            $classService = new ClassRoomService();
            $dataSend["limit"] = 1;
            $dataSend["skip"] = 0;
            $usersInformation = array();
            $viewers = $response["users"];

            foreach($viewers as $viewer) {
                $dataSend["userId"] = $viewer["id"];

                $classService->getClass($dataSend, $this->getEntityManager());
                $classroomInfo = $classService->getMessage();

                $viewer['classroom'] = [];

                if (array_key_exists(0, $classroomInfo) && array_key_exists('title', $classroomInfo[0])) {
                    $viewer['classroom'] = $classroomInfo[0]['title'];
                }

                $usersInformation[] = $viewer;
            }
            $response["users"] = $usersInformation;
            return $this->responseJson($response, $sessionService->getStatus());
        }
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/raise/hand")
     * ),
     * @SWG\Post(
     *     path="/raise/hand",
     *     summary="User Raise Your Hand",
     *     tags={"Session"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function raiseHand(Request $request)
    {
        $data['sessionId'] = $request->request->get('sessionId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new LiveService();
        $sessionService->raiseHand(
            $data
        );
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/raise/hand/{sessionId}")
     * ),
     * @SWG\Get(
     *     path="/raise/hand/{sessionId}",
     *     summary="Fetch list of everyone who raised their hand",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function raiseYourHandList(Request $request)
    {
        $data['sessionId'] = $request->get('sessionId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new LiveService();
        $sessionService->raiseHandList(
            $data
        );
        return $this->responseJson(['list' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE",          uri="/raise/hand/{sessionId}/{userId}")
     * ),
     * @SWG\Delete(
     *     path="/raise/hand/{sessionId}/{userId}",
     *     summary="Remove Specific User From Specific Session raised Hand Queue",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function delRaiseYourHand(Request $request)
    {
        $data = [
            'sessionId' => $request->attributes->get('sessionId'),
            'userId' => $request->attributes->get('userId')
        ];
        $sessionService = new LiveService();
        $sid = $sessionService->delRaiseYourHand(
            $data
        );

        if ($sid) {
            return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
        }
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/promote/speaker")
     * ),
     * @SWG\Put(
     *     path="/promote/speaker",
     *     summary="Promote Speaker",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function promoteSpeaker(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->promoteSpeaker(
            $data
        );
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/return/speaker")
     * ),
     * @SWG\Put(
     *     path="/return/speaker",
     *     summary="Return Original Speaker",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function unPromoteSpeaker(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->unPromoteSpeaker(
            $data
        );
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/start/session/archive")
     * ),
     * @SWG\Post(
     *     path="/start/archive",
     *     summary="Start Archive Opentok",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="openTokSessionId",
     *         in="formData",
     *         description="live session id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionTitle",
     *         in="formData",
     *         description="live session title",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function starArchive(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->startRecording(
            $data
        );
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/stop/session/archive")
     * ),
     * @SWG\Post(
     *     path="/stop/archive",
     *     summary="Stop Archive Session",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="archiveId",
     *         in="formData",
     *         description="live session id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function stopArchive(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->stopRecording(
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }


    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/get/session/archive")
     * ),
     * @SWG\Post(
     *     path="/get/archive",
     *     summary="Download Archive Session",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="archiveId",
     *         in="formData",
     *         description="live session id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getArchive(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->getRecording(
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/promote/work")
     * ),
     * @SWG\Put(
     *     path="/promote/work",
     *     summary="Promote work",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="workId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function promoteWork(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->promoteWork(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="PUT",          uri="/return/work")
     * ),
     * @SWG\Put(
     *     path="/return/work",
     *     summary="Return Original work",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function unPromoteWork(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->unPromoteWork(
            $data
        );
        return $this->responseJson(['message' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/works/list/{sessionId}")
     * ),
     * @SWG\Get(
     *     path="/works/list/{sessionId}",
     *     summary="Fetch list of every work submitted to the livesession",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function workList(Request $request)
    {
        $data['sessionId'] = $request->get('sessionId');
        $token = $this->getToken($request->headers->get('token'));
        $data['level'] = $token->getClaim('context')->level;
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new LiveService();
        $sessionService->workList(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['list' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/works/list/{sessionId}/{userId}")
     * ),
     * @SWG\Get(
     *     path="/works/list/{sessionId}/{userId}",
     *     summary="Fetch list of every work submitted to the livesession BY USER",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function workListByUser(Request $request)
    {
        $data['sessionId'] = $request->get('sessionId');
        $data['userId'] = $request->get('userId');
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new LiveService();
        $sessionService->workList(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson(['list' => $sessionService->getMessage()], $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/save/chat")
     * ),
     * @SWG\Post(
     *     path="/save/chat",
     *     summary="Save Chat Message",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="originId",
     *         in="formData",
     *         description="message origin ID",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="flagged",
     *         in="formData",
     *         description="flagged TRUE OR FALSE not required",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="message",
     *         in="formData",
     *         description="message content",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function saveChat(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->saveChat(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/flag/chat")
     * ),
     * @SWG\Post(
     *     path="/flag/chat",
     *     summary="delete Chat Message",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="originId",
     *         in="formData",
     *         description="message origin ID",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="messageId",
     *         in="formData",
     *         description="messageId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function flagChat(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->flagChat(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to save a drawover operation
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST", uri="/save/drawover")
     * ),
     * @SWG\Post(
     *     path="/save/drawover",
     *     summary="Save Chat Message",
     *     tags={"LiveSession"},
     *     description="Create Live Session Within OpenTok Service",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="originId",
     *         in="formData",
     *         description="message origin ID",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="tagId",
     *         in="formData",
     *         description="tagId for pulling or updating",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="operation",
     *         in="formData",
     *         description="push or pull",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="tag",
     *         in="formData",
     *         description="tag contents",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function saveDrawoverOperation(Request $request)
    {
        $data = $request->request->all();
        $sessionService = new LiveSessionService();
        $sessionService->saveDrawoverOperation(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/chat/history/{sessionId}/{limit}/{skip}")
     * ),
     * @SWG\Get(
     *     path="/chat/history/{sessionId}/{limit}/{skip}",
     *     summary="Fetch chat history of the livesession",
     *     tags={"LiveSession"},
     *     description="Fetch chat history of the livesession",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="formData",
     *         description="limit",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="skip",
     *         in="formData",
     *         description="skip",
     *         required=false,
     *         type="integer",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function getChatHistory(Request $request)
    {
        $data['sessionId'] = $request->get('sessionId');
        $data['limit'] = $request->get('limit', 0);
        $data['skip'] = $request->get('skip', 0);
        $sessionService = new LiveSessionService();
        $chat = $sessionService->getChatHistory(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($chat, $sessionService->getStatus());
    }

    /**
     * Method to save sessions
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/session/ban")
     * ),
     * @SWG\Post(
     *     path="/session/ban",
     *     summary="Save Session By Admin",
     *     tags={"LiveSession"},
     *     description="Remove Session By Admin",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Session deleted, status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Session not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function banUser(Request $request)
    {
        $data =  $request->request->all();
        $BoService = new LiveSessionService();
        $BoService->banUser(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($BoService->getMessage(), $BoService->getStatus());
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/terminate/session")
     * ),
     * @SWG\Post(
     *     path="/terminate/session",
     *     summary="Terminate Session",
     *     tags={"Session"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="sessionId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function terminateSession(Request $request)
    {
        $data = $request->request->all();
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $sessionService = new LiveSessionService();
        $sessionService->terminateSession(
            $this->getEntityManager(),
            $data
        );
        return $this->responseJson($sessionService->getMessage(), $sessionService->getStatus());
    }


    /**
     * Method to duplicate a existent live session
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/duplicate/session")
     * ),
     * @SWG\Post(
     *     path="/duplicate/session",
     *     summary="duplicate a live session",
     *     tags={"Session"},
     *     description="duplicate a live session",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sessionId",
     *         in="formData",
     *         description="the id of the live session that will be duplicated.",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         description="live session date",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="startTime",
     *         in="formData",
     *         description="start Time (15:30)",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="endTime",
     *         in="formData",
     *         description="end Time (17:30)",
     *         required=true,
     *         type="string",
     *         maxLength=50,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : session has been created , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : Invalid object ID , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function duplicateSession(Request $request)
    {
        $data = $request->request->all();

        $dateTime = new DateTime(date($data['date']), new DateTimeZone('UTC'));
        $newTime = (string) $dateTime->format('Y-m-d') . ' ' . $data['startTime'];
        $data['date'] = DateTime::createFromFormat('Y-m-d H:i A', $newTime);

        $liveSessionService = new liveSessionService();
        $sessionInformation = $liveSessionService->getSession(
            $this->getEntityManager(),
            $data
        );

        $data = [
            'type' => $sessionInformation->getType(),
            'title' => $sessionInformation->getName(),
            'description' => $sessionInformation->getDescription(),
            'speaker' => $sessionInformation->getSpeaker(),
            'thumbnail' => $sessionInformation->getThumbnail(),
            'participants' => $sessionInformation->getUsers(),
            'utcOffset' => $sessionInformation->getUtcOffset(),
            'moderator' => $sessionInformation->getModerator(),
            'deadlineStatus' => $sessionInformation->getDeadlineStatus(),
            'date' => $data['date'],
            'startTime' => $data['startTime'],
            'endTime' => $data['endTime'],
            'originalSessionId' => $data['sessionId'],
        ];

        $sessionValidator = new SessionValidator();

        if ($sessionValidator->requiredFields($this->getApplication(), $data)) {
            $liveSessionService->createSession(
                $this->getEntityManager(),
                $data
            );

            return $this->responseJson([
                'message' => $liveSessionService->getMessage()
            ], $liveSessionService->getStatus());
        }

        return $this->responseJson([
            'message' => $sessionValidator->getErrorMessage()
        ], $sessionValidator->getStatus());
    }
}
