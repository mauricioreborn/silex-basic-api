<?php

/**
 * Resource File Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Controllers;

use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Resource File Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Resource
{
    /**
     * API index GET
     *
     * @return mixed
     *
     *  @SLX\Route(
     *      @SLX\Request(method="GET", uri="")
     * )
     */
    public function index()
    {
        return new JsonResponse(
            [
                'error' => false,
                'message' => 'Welcome to your API!',
            ]
        );
    }

    /**
     * API RESOURCES
     *
     * @return mixed
     *
     *  @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="")
     * )
     */
    public function delete()
    {
        return new JsonResponse(
            [
                'error' => false,
                'message' => 'YAY I can delete it!'
            ]
        );
    }

    /**
     * API index
     *
     * @param mixed $request request.
     *
     * @return mixed
     *
     *  @SLX\Route(
     *      @SLX\Request(method="POST", uri="")
     * )
     */
    public function post(Request $request)
    {
        return new JsonResponse(
            [
                'error' => false,
                'message' => 'Posting you data!',
                'data' => $request->get('data'),
            ]
        );
    }

    /**
     * API index
     *
     * @return mixed
     *
     *  @SLX\Route(
     *      @SLX\Request(method="PUT", uri="")
     * )
     */
    public function put()
    {
        return new JsonResponse(
            [
                'error' => false,
                'message' => 'Updating you data!',
            ]
        );
    }
}
