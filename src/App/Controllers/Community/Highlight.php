<?php

namespace App\Controllers\Community;

use App\Controller;
use App\Services\BackOffice\BOService;
use App\Services\UserService;
use App\Services\Community\CommunityService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Highlight extends Controller
{
    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/set/highlight")
     * ),
     * @SWG\Post(
     *     path="/set/highlight",
     *     summary="Enroll Events User",
     *     tags={"events"},
     *     description="Enroll Events User",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="id of work or livesession",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="work or livesession lowercase",
     *         required=false,
     *         type="file",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function setHighLight(Request $request)
    {
        $data = $request->request->all();
        $boService = new BOService();
        $boService->setHighLight($this->getEntityManager(), $data);
        return $this->responseJson($boService->getMessage(), $boService->getStatus());
    }

}