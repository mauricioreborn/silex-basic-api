<?php

namespace App\Controllers\Community;

use App\Controller;
use App\Services\UserService;
use App\Services\Community\CommunityService;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;

/**
 * Community Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class RecentActivity extends Controller
{

    /**
     * Method to send a Community Recent Activity
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/recentactivity")
     * ),
     * @SWG\Get(
     *     path="/recentactivity",
     *     summary="get all recentactivity",
     *     tags={"community"},
     *     description="get all recentactivity to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function recentActivity(Request $request)
    {
        $data['page'] = $request->get('page');
        $data['limit'] = $request->get('limit', 15);
        $data['skip'] = $request->get('skip');
        $data['skip'] = $this->calculateSkip($data);
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $token->getClaim('context')->user_id;
        $userService = new UserService();
        $community = new CommunityService();
        if ($userService->validateId($data['userId'])) {
            $community->getRecentActivity(
                $this->getEntityManager(),
                $data
            );
            return $this->responseJson($community->getMessage(), $community->getStatus());
        }
        return $this->responseJson('Invalid User ID', 400);
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/interesting/comments")
     * ),
     * @SWG\Get(
     *     path="/interesting/comments",
     *     summary="get all Interesting Comments",
     *     tags={"community"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function interestingComments(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userService = new UserService();
        $community = new CommunityService();
        if ($userService->validateId($token->getClaim('context')->user_id)) {
            $community->getInterestingComments(
                $this->getEntityManager()
            );
            return $this->responseJson($community->getMessage(), $community->getStatus());
        }
        return $this->responseJson('Invalid User ID', 400);
    }

    /**
     * Method to send a Community Interesting Comments
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="/highlights")
     * ),
     * @SWG\Get(
     *     path="/highlights",
     *     summary="get all Interesting Comments",
     *     tags={"community"},
     *     description="get all Interesting Comments to the community page",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=false,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : filters object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function highlights(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $community = new CommunityService();
            $community->getHighlights(
                $this->getEntityManager(),
                $token->getClaim('context')->user_id,
                $token->getClaim('context')->level
            );
        return $this->responseJson(['highlights' => $community->getMessage()], $community->getStatus());
    }
}
