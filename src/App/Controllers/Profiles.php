<?php

namespace App\Controllers;

use App\Notification\Follows;
use App\Notification\Likes;
use App\Notification\NotificationQuerys;
use App\Services\NotificationService;
use App\Services\Profiles\ProfileService;
use App\Controller;
use App\Entities\User;
use App\Services\Commom\TokenRead;
use App\Services\UserService;
use App\Validators\Profile\ProfileValidator;
use Symfony\Component\HttpFoundation\Request;
use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Swagger\Annotations as SWG;
use App\Validators\UserValidator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Profiles Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class Profiles extends Controller
{

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/profile/{userId}")
     * )
     * @SWG\Get(
     *     path="user/profile/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function userProfile(Request $request)
    {
        $ProfileValidator = new ProfileValidator();
        $validator= $ProfileValidator->validateProfile($request->attributes->all(), $this->getEntityManager());
        if ($validator) {
            $token = $this->getToken($request->headers->get('token'));
            $data['userId'] = $request->get('userId');
            if (empty($data['userId']) or $data['userId'] == null) {
                $data['userId'] = $token->getClaim('context')->user_id;
                $data['level'] =  $token->getClaim('context')->level;
            }
            $profileService = new ProfileService();
            $profileService->getSidebar($this->getEntityManager(), $data);
            return $this->responseJson($profileService->getMessage(), $profileService->getStatus());
        }
        return $this->responseJson([
            'message' => $ProfileValidator->getErrorMessage()[0]
        ], $ProfileValidator->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/work/{userId}")
     * )
     * @SWG\Get(
     *     path="user/work/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function userWork(Request $request)
    {
        $ProfileValidator = new ProfileValidator();
        $validator= $ProfileValidator->validateProfile($request->attributes->all(), $this->getEntityManager());
        if ($validator) {
            $token = $this->getToken($request->headers->get('token'));
            $data['userId'] = $request->get('userId');
            if (empty($data['userId']) || $data['userId'] == null) {
                $data['userId'] = $token->getClaim('context')->user_id;
            }
            $data = $this->buildPaginate($data, $request);
            $followers = new ProfileService();
            $followers->getProfileWorks($this->getEntityManager(), $data);
            return $this->responseJson($followers->getMessage(), $followers->getStatus());
        }
        return $this->responseJson([
            'message' => $ProfileValidator->getErrorMessage()[0]
        ], $ProfileValidator->getStatus());
    }


    /**
     * Method to follow user
     *
     * @param Request $request request data
     *,
     * @SLX\Route(
     *      @SLX\Request(method="POST",          uri="/user/follow")
     * ),
     * @SWG\Post(
     *     path="/user/follow",
     *     summary="Follow a new User",
     *     tags={"profiles"},
     *     description="Follow a new User",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="followId",
     *         in="formData",
     *         description="User TO Follow Id",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : User Followed , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User Followed , status : 400}",
     *     ),
     *     ),
     * @return                              JsonResponse
     */
    public function follow(Request $request)
    {
        $ProfileValidator = new ProfileValidator();
        $validator= $ProfileValidator->validateProfile($request->request->all(), $this->getEntityManager());
        if ($validator) {
            $token = $this->getToken($request->headers->get('token'));
            $ids = [
                'userId' => $token->getClaim('context')->user_id,
                'followId' => $request->get('followId')
            ];
            $userValidator = new UserValidator();


            if ($userValidator->followUser($this->getEntityManager(), $ids)) {
                $userService = new UserService();
                $check = $userService->insertFollow($this->getEntityManager(), $ids);
                if ($check === true) {
                    $userService->insertFollower($this->getEntityManager(), $ids);
                }


                $notificationQuery = new NotificationQuerys();

                $data['userId'] = $token->getClaim('context')->user_id;
                $data['followId'] =  $request->get('followId');

                $data['owner'] = $notificationQuery->getId(
                    $this->getEntityManager()->createQueryBuilder(User::class),
                    $data['followId']
                );

                $notificationService = new NotificationService(new Follows());
                $notification = $notificationService->create($data);
                $notificationService->save(
                    $this->getEntityManager(),
                    $notification,
                    $data
                );

                return $this->responseJson($userService->getMessage(), $userService->getStatus());
            }
            return $this->responseJson($userValidator->getErrorMessage(), $userValidator->getStatus());
        }
        return $this->responseJson([
            'message' => $ProfileValidator->getErrorMessage()[0]
        ], $ProfileValidator->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="user/followers/{userId}"),
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="user/followers"),
     * )
     * @SWG\Get(
     *     path="user/followers/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function followers(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $request->get('userId');
        $data = $this->buildPaginate($data, $request);

        if (empty($data['userId'])) {
            $data['userId'] = $token->getClaim('context')->user_id;
        }
        $followers = new ProfileService();
        $followers->fetchFollowers($this->getEntityManager(), $data, $token->getClaim('context')->user_id);
        return $this->responseJson($followers->getMessage(), $followers->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="user/following/{userId}"),
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="user/following"),
     * )
     * @SWG\Get(
     *     path="user/following/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function following(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $request->get('userId');
        $data = $this->buildPaginate($data, $request);
        if (!$data['userId']) {
            $data['userId'] = $token->getClaim('context')->user_id;
        }
        $followers = new ProfileService();
        $followers->fetchFollowing($this->getEntityManager(), $data, $token->getClaim('context')->user_id);
        return $this->responseJson($followers->getMessage(), $followers->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     *
     * @SLX\Route(
     *      @SLX\Request(method="DELETE", uri="user/unfollow/{userId}"),
     * )
     * @SWG\Delete(
     *     path="user/unfollow/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="userId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function unFollow(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userId = $request->get('userId');
        $followerId = $token->getClaim('context')->user_id;
        $followers = new ProfileService();
        $followers->unFollow($this->getEntityManager(), $followerId, $userId);
        return $this->responseJson($followers->getMessage(), $followers->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/likes/{userId}"),
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/likes"),
     * ),
     * @SWG\Get(
     *     path="user/likes/",
     *     summary="get a user likes by workId",
     *     tags={"profiles"},
     *     description="get a user likes by workId",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function profileLikes(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $request->get('userId');
        $data = $this->buildPaginate($data, $request);
        if (empty($userId)) {
            $data['userId'] = $token->getClaim('context')->user_id;
        }
        $followers = new ProfileService();
        $followers->fetchLikes($this->getEntityManager(), $data);
        return $this->responseJson($followers->getMessage(), $followers->getStatus());
    }

    /**
     * Method to send a signup datas
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET",          uri="check/follow/{followId}"),
     * ),
     * @SWG\Get(
     *     path="check/follow/{userId}",
     *     tags={"profiles"},
     *     operationId="profile",
     *     summary="profile",
     *     description="Fetch profile",
     *     @SWG\Parameter(
     *         name="followId",
     *         type="string",
     *         in="path",
     *         description="token ",
     *         required=true,
     *         @SWG\Schema(
     *           type="json",
     *            @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         maxLength=100,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="{message : Email has been Sent , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : User not found , status : 400}",
     * ),
     * )
     *
     * @return string
     */

    public function checkFollow(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $userId = $token->getClaim('context')->user_id;
        $followId = $request->get('followId');
        $check = new ProfileService();
        $check->checkFollow($this->getEntityManager(), $userId, $followId);
        return $this->responseJson(['status' => $check->getMessage()], $check->getStatus());
    }

    /**
     * Method to send a work
     *
     * @param Request $request request data
     *
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/portfolio/{userId}"),
     * ),
     * @SLX\Route(
     *      @SLX\Request(method="GET", uri="user/portfolio"),
     * ),
     * @SWG\Get(
     *     path="user/portfolio/",
     *     summary="get a user likes by workId",
     *     tags={"profiles"},
     *     description="get a user likes by workId",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="header",
     *         description="token",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *      @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         description="userId",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="{message : work object , status : 200}",
     *         @SWG\Schema(
     *             type="json",
     *             @SWG\Items(ref="#/definoitions/Pet")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="{message : work not found , status : 400}",
     *     ),
     *     ),
     * @return JsonResponse
     */
    public function portFolio(Request $request)
    {
        $token = $this->getToken($request->headers->get('token'));
        $data['userId'] = $request->get('userId', $token->getClaim('context')->user_id);
        $data['skip'] = $request->get('skip', 0);
        $data['limit'] = $request->get('limit', 9);
        $followers = new ProfileService();
        $followers->portfolio($this->getEntityManager(), $data);
        return $this->responseJson($followers->getMessage(), $followers->getStatus());
    }
}
