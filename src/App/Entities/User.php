<?php

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="users")
 */
class User
{

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $email;

    /**
     * @ODM\EmbedMany(targetDocument="Work")
     */
    private $works;

    /**
     * @ODM\EmbedMany(targetDocument="UserLike")
     */
    private $likes;

    /**
     * Password
     *
     * @ODM\Field(type="string")
     */
    private $password;

    /**
     * Association
     *
     * @ODM\Field(type="string")
     */
    private $association;

    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * Profile
     *
     * @ODM\Field(type="string")
     */
    private $profile;

    /**
     * Classroom
     *
     * @ODM\Field(type="collection")
     */
    private $classroom;


    /**
     * notes
     *
     * @ODM\Field(type="collection")
     */
    private $notes;


    /**
     * Notification
     *
     * @ODM\Field(type="collection")
     */
    private $notification;

    /**
     * Profile
     *
     * @ODM\Field(type="string")
     */
    private $classCode;

    /**
     * Community
     *
     * @ODM\Field(type="collection")
     */
    private $community;

    /**
     * Criterias
     *
     * @ODM\Field(type="collection")
     */
    private $criterias;


    /**
     * Criterias
     *
     * @ODM\Field(type="collection")
     */
    private $notificationSettings;

    /**
     * FirstName
     *
     * @ODM\Field(type="date")
     */
    private $lastActivity;
    /**
     * Laboratory
     *
     * @ODM\Field(type="collection")
     */
    private $lab;


    /**
     * @ODM\EmbedMany(targetDocument="Follow")
     */
    private $followers;

    /**
     * @ODM\EmbedMany(targetDocument="Notification")
     */
    private $notifications;

    /**
     * @ODM\EmbedMany(targetDocument="Follow")
     */
    private $following;

    /**
     * Photo
     *
     * @ODM\Field(type="string")
     */
    private $userPhoto;

    /**
     * FirstName
     *
     * @ODM\Field(type="string")
     */
    private $firstName;


    /**
     * totalWorkComment
     *
     * @ODM\Field(type="string")
     */
    private $totalComment;

    /**
     * totalWorkComment
     *
     * @ODM\Field(type="string")
     */
    private $totalCoWork;

    /**
     * totalWork
     *
     * @ODM\Field(type="string")
     */
    private $totalWork;
    /**
     * LastName
     *
     * @ODM\Field(type="string")
     */
    private $lastName;

    /**
     * Gender
     *
     * @ODM\Field(type="string")
     */
    private $gender;

    /**
     * Birthday
     *
     * @ODM\Field(type="date")
     */
    private $birthDate;

    /**
     * Phone
     *
     * @ODM\Field(type="integer")
     */
    private $phone;

    /**
     * Agreement
     *
     * @ODM\Field(type="boolean")
     */
    private $agreement;


    /**
     * Verify if an user already notified
     *
     * @ODM\Field(type="boolean")
     */
    private $alreadyNotified;


    /**
     * Verify if an user already notified
     *
     * @ODM\Field(type="boolean")
     */
    private $firstReview;

    /**
     * Status
     *
     * @ODM\Field(type="boolean")
     */
    private $status;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $state;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $title;

    /**
     * Token
     *
     * @ODM\Field(type="string")
     */
    private $tokenPass;


    /**
     * @ODM\EmbedOne(targetDocument="Level")
     */
    private $level;


    /**
     * Avatar
     *
     * @ODM\Field(type="string")
     */
    private $avatar;

    /**
     * Biography
     *
     * @ODM\Field(type="string")
     */
    private $bio;

    /**
     * FeedBackMessage
     *
     * @ODM\Field(type="string")
     */
    private $feedbackmessage;

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getFeedBackMessage()
    {
        return $this->feedbackmessage;
    }

    /**
     * @param mixed $feedbackmessage
     */
    public function setFeedBackMessage($feedbackmessage)
    {
        $this->feedbackmessage = $feedbackmessage;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return mixed
     */
    public function getTokenPass()
    {
        return $this->tokenPass;
    }

    /**
     * @param mixed $tokenPass
     */
    public function setTokenPass($tokenPass)
    {
        $this->tokenPass = $tokenPass;
    }

    /**
     * Get Title
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get Company
     *
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set Company
     *
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     *
     *
     * @ODM\Field(type="string")
     */
    private $company;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param mixed $agreement
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;
    }


    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return mixed
     */
    public function getCriterias()
    {
        return $this->criterias;
    }

    /**
     * @param mixed $criterias
     */
    public function setCriterias($criterias)
    {
        $this->criterias[] = $criterias;
    }

    /**
     * @return mixed
     */
    public function getCommunity()
    {
        return $this->community;
    }

    /**
     * @param mixed $community
     */
    public function setCommunity($community)
    {
        $this->community[] = $community;
    }

    /**
     * @return mixed
     */
    public function getAssociation()
    {
        return $this->association;
    }

    /**
     * @param mixed $association
     */
    public function setAssociation($association)
    {
        $this->association = $association;
    }

    /**
     * @return mixed
     */
    public function getUserPhoto()
    {
        return $this->userPhoto;
    }

    /**
     * @param mixed $userPhoto
     */
    public function setUserPhoto($userPhoto)
    {
        $this->userPhoto = $userPhoto;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get the user id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to set the user id.
     *
     * @param string $id id of user
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Method to get the user email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Method to set the user email.
     *
     * @param string $email email of user
     *
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Method to get the user password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Method to set the user password.
     *
     * @param string $password of user.
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Method to get the date of the user has been created.
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Method to set the datetime that an user will be created.
     *
     * @param string $createdAt of user
     *
     * @return void
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Construct to set the createdAt how time now.
     * @return void
     */
    public function __construct()
    {
        $this->state = 'pending';
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed $works
     */
    public function getWorks()
    {
        return $this->works;
    }

    /**
     * @param mixed $work
     */
    public function setWork(Work $work)
    {
        $work->setOwner($this);
        $this->works[] = $work;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel(Level $level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param mixed $followers
     */
    public function setFollowers(Follow $followers)
    {
        $this->followers[] = $followers;
    }

    /**
     * @return mixed
     */
    public function getFollowing()
    {
        return $this->following;
    }

    public function deleteFollower($id)
    {
        foreach ($this->followers as $idx => $follow) {
            if ($id == $follow->getId()) {
                unset($this->followers[$idx]);
                break;
            }
        }
    }

    public function deleteFollowing($id)
    {
        foreach ($this->following as $idx => $follow) {
            if ($id == $follow->getId()) {
                unset($this->following[$idx]);
                break;
            }
        }
    }

    public function deleteLab($id)
    {
        foreach ($this->lab as $idx => $lab) {
            if ($id == $lab['id']) {
                unset($this->lab[$idx]);
                break;
            }
        }
    }

    public function deleteClassroom($id)
    {
        foreach ($this->classroom as $idx => $class) {
            if ($id == $class['id']) {
                unset($this->classroom[$idx]);
                break;
            }
        }
    }

    /**
     * @param mixed $following
     */
    public function setFollowing(Follow $following)
    {
        $this->following[] = $following;
    }

    public function setComment(Work $work)
    {
        $this->work['comment'][] = $work;
    }

    /**
     * @return mixed
     */
    public function getClassRoom()
    {
        return $this->classroom;
    }

    /**
     * @param mixed $classRoom
     */
    public function setClassRoom($classroom)
    {
        $this->classroom[] = $classroom;
    }

    /**
     * @return mixed
     */
    public function getClassCode()
    {
        return $this->classCode;
    }

    /**
     * @param mixed $classCode
     */
    public function setClassCode($classCode)
    {
        $this->classCode = $classCode;
    }

    public function __unset($key)
    {
        unset($this->following[$key]);
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed
     */
    public function setLikes(UserLike $like)
    {
        $like->setOwner($this);
        $this->likes[] = $like;
    }

    /**
     * @return mixed
     */
    public function getLabs()
    {
        return $this->lab;
    }

    /**
     * @param mixed $labs
     */
    public function setLabs($lab)
    {
        $this->lab[] = $lab;
    }

    /**
     * @return mixed
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param mixed $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    }

    public function removeLike($workId)
    {
        foreach ($this->getLikes() as $idx => $like) {
            if ($workId == $like->getWorkId()) {
                unset($this->likes[$idx]);
                break;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param mixed $notifications
     */
    public function setNotifications(Notification $notifications)
    {
        $this->notifications[] = $notifications;
    }

    /**
     * @return mixed
     */
    public function getNotificationSettings()
    {
        return $this->notificationSettings;
    }

    /**
     * @param mixed $notificationSettings
     */
    public function setNotificationSettings($notificationSettings)
    {
        $this->notificationSettings[] = $notificationSettings;
    }

    /**
     * @return mixed
     */
    public function getTotalWork()
    {
        return $this->totalWork;
    }

    /**
     * @param mixed $totalWork
     */
    public function setTotalWork($totalWork)
    {
        $this->totalWork = $totalWork;
    }

    /**
     * @return mixed
     */
    public function getTotalComment()
    {
        return $this->totalComment;
    }

    /**
     * @param mixed $totalComment
     */
    public function setTotalComment($totalComment)
    {
        $this->totalComment = $totalComment;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes[] = $notes;
    }

    /**
     * @return mixed
     */
    public function getAlreadyNotified()
    {
        return $this->alreadyNotified;
    }

    /**
     * @param mixed $alreadyNotified
     */
    public function setAlreadyNotified($alreadyNotified)
    {
        $this->alreadyNotified = $alreadyNotified;
    }

    /**
     * @return mixed
     */
    public function getFirstReview()
    {
        return $this->firstReview;
    }

    /**
     * @param mixed $firstReview
     */
    public function setFirstReview($firstReview)
    {
        $this->firstReview = $firstReview;
    }

    /**
     * @return mixed
     */
    public function getTotalCoWork()
    {
        return $this->totalCoWork;
    }

    /**
     * @param mixed $totalCoWork
     */
    public function setTotalCoWork($totalCoWork)
    {
        $this->totalCoWork = $totalCoWork;
    }
    
    
}
