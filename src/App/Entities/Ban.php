<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="ban")
 */
class Ban
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * originId
     *
     * @ODM\Field(type="object_id")
     */
    private $sessionId;

    /**
     * userId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * Construct to set the createdAt how time now.
     */
    public function __construct()
    {

        $dt = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->createdAt = new \MongoDate($ts);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }



}