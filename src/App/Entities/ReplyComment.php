<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="replyComment")
 */
class ReplyComment
{

    /**
     * ProductId
     *
     * @ODM\Field(type="object_id")
     */
    private $productId;

    /**
     * Comment Type (work, stream,etc)
     *
     * @ODM\Field(type="string")
     */
    private $type;

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;


    /**
     * flagged
     *
     * @ODM\Field(type="boolean")
     */
    private $flagged;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $comment;

    /**
     * Birthday
     *
     * @ODM\Field(type="date")
     */
    private $created_at;
    /**
     * Birthday
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * OriginalComment
     *
     * @ODM\Field(type="object_id")
     */
    private $owner;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $like;

    /**
     * embed document to save information about flag on comments
     * @ODM\EmbedMany(targetDocument="FlagContent")
     */
    private $flag;
    
    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $create_at
     */
    public function setCreateAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * Method to get Flagged status
     * @return boolean true as flagged and false as not flagged
     */
    public function getFlagged()
    {
        return $this->flagged;
    }

    /**
     * Method to set a comment as flagged
     * @param boolean $flagged
     */
    public function setFlagged(bool $flagged)
    {
        $this->flagged = $flagged;
    }

    /**
     * @param mixed $like
     */
    public function setLike($like)
    {
        $this->like = $like;
    }

    /**
     *  Method to remove all flags of the comment
     */
    public function removeAllFlag()
    {
        unset($this->flag);
    }

    /**
     * Method to get all embed flag documents of the comment
     * @return FlagContent object
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Method to set a flag inside comment document
     * @param FlagContent $flag A flagContent object with all parameters filled
     */
    public function setFlag(FlagContent $flag)
    {
        $this->flag[] = $flag;
    }

    /**
     * Method to get total flags with true status.
     * @return string total flags enabled
     */
    public function getTotalFlags()
    {
        $totalFlag = '';
        foreach ($this->getFlag() as $flag) {
            if ($flag->getStatus() == true) {
                $totalFlag ++;
            }
        }
        return $totalFlag;
    }

    /**
     * Method to check if the current logged user already flagged this work
     * @param string $userId user mongo Identifier
     * @return bool          True if the user already flagged it, false if the
     *                       user did not flag it.
     */
    public function checkIFlagged(string $userId)
    {
        foreach ($this->getFlag() as $flag) {
            if ($flag->getUserId() == $userId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to remove a flag of the work
     * @param string $userId User identifier on database
     */
    public function removeUserFlag(string $userId)
    {
        foreach ($this->getFlag() as $index => $flagContent) {
            if ($flagContent->getUserId() == $userId) {
                unset($this->flag[$index]);
            }
        }
    }
}