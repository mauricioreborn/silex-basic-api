<?php

/**
 * Entity Organization File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity Organization File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="organizations")
 */
class Organization
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /** @ODM\Field(type="string") */
    private $firstName;

    /** @ODM\Field(type="string") */
    private $lastName;
    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $email;

    /** @ODM\Field(type="string") */
    private $title;

    /** @ODM\Field(type="string") */
    private $organizationName;

    /** @ODM\Field(type="integer") */
    private $numbersOfSchool;

    /** @ODM\Field(type="integer") */
    private $numbersOfClassroom;

    /** @ODM\Field(type="integer") */
    private $phone;

    /** @ODM\Field(type="string") */
    private $educationalStage;

    /** @ODM\Field(type="string") */
    private $duration;

    /** @ODM\Field(type="string") */
    private $involvement;

    /** @ODM\Field(type="string") */
    private $interestInvolvement;

    /**
     * interestedCommunity
     *
     * @ODM\Field(type="collection")
     */
    private $interestedCommunity;

    /** @ODM\Field(type="string") */
    private $foundOut;

    /** @ODM\Field(type="string") */
    private $comments;

    /** @ODM\Field(type="boolean") */
    private $status;

    /** @ODM\Field(type="boolean") */
    private $deleted;

    /** @ODM\Field(type="string") */
    private $approval;

    /**
     * @return mixed
     */
    public function getApproval()
    {
        return $this->approval;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @param mixed $approval
     */
    public function setApproval($approval)
    {
        $this->approval = $approval;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * @param mixed $organizationName
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;
    }

    /**
     * @return mixed
     */
    public function getNumbersOfSchool()
    {
        return $this->numbersOfSchool;
    }

    /**
     * @param mixed $numbersOfSchool
     */
    public function setNumbersOfSchool($numbersOfSchool)
    {
        $this->numbersOfSchool = $numbersOfSchool;
    }

    /**
     * @return mixed
     */
    public function getNumbersOfClassroom()
    {
        return $this->numbersOfClassroom;
    }

    /**
     * @param mixed $numbersOfClassroom
     */
    public function setNumbersOfClassroom($numbersOfClassroom)
    {
        $this->numbersOfClassroom = $numbersOfClassroom;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEducationalStage()
    {
        return $this->educationalStage;
    }

    /**
     * @param mixed $educationalStage
     */
    public function setEducationalStage($educationalStage)
    {
        $this->educationalStage = $educationalStage;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getInvolvement()
    {
        return $this->involvement;
    }

    /**
     * @param mixed $involvement
     */
    public function setInvolvement($involvement)
    {
        $this->involvement = $involvement;
    }

    /**
     * @return mixed
     */
    public function getInterestInvolvement()
    {
        return $this->interestInvolvement;
    }

    /**
     * @param mixed $interestInvolvement
     */
    public function setInterestInvolvement($interestInvolvement)
    {
        $this->interestInvolvement = $interestInvolvement;
    }

    /**
     * @return mixed
     */
    public function getInterestedCommunity()
    {
        return $this->interestedCommunity;
    }

    /**
     * @param mixed $interestedCommunity
     */
    public function setInterestedCommunity($interestedCommunity)
    {
        $this->interestedCommunity = $interestedCommunity;
    }

    /**
     * @return mixed
     */
    public function getFoundOut()
    {
        return $this->foundOut;
    }

    /**
     * @param mixed $foundOut
     */
    public function setFoundOut($foundOut)
    {
        $this->foundOut = $foundOut;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * Method to get the date of the user has been created.
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Method to set the datetime that an user will be created.
     *
     * @param string $createdAt of user
     *
     * @return void
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Construct to set the createdAt how time now.
     *
     * @return void
     */
    public function __construct()
    {
        $dt = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->createdAt = new \MongoDate($ts);
        $this->deleted = false;
    }
}
