<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * Entity workView File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="view")
 */
class View
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * workId
     *
     * @ODM\Field(type="object_id")
     */
    private $workId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param mixed $workId
     */
    public function setWorkId($workId)
    {
        $this->workId = $workId;
    }
    
}