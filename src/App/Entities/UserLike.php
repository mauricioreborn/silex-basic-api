<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class UserLike
{
    /**
     * @ODM\ReferenceOne(simple=true, targetDocument="User")
     */
    private $owner;

    /**
     * WorkId
     *
     * @ODM\Field(type="object_id")
     */
    private $workId;

    /**
     * CreateAt
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param mixed $workId
     */
    public function setWorkId($workId)
    {
        $this->workId = $workId;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}