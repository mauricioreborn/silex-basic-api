<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="lab")
 */
class Lab
{
    /**
     * @ODM\ReferenceOne(simple=true, targetDocument="User")
     */
    public $owner;
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $userCounter;

    /**
     * schoolId
     *
     * @ODM\Field(type="object_id")
     */
    private $schoolId;

    /**
     * @ODM\EmbedMany(targetDocument="Task")
     */
    private $tasks;


    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks(Task $tasks)
    {
        $this->tasks[] = $tasks;
    }

    public function removeTask($taskId)
    {
        foreach ($this->getTasks() as $key => $task) {
            if ($task->getId() == $taskId) {
                unset($this->tasks[$key]);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getSchoolId()
    {
        return $this->schoolId;
    }

    /**
     * @param mixed $schoolId
     */
    public function setSchoolId($schoolId)
    {
        $this->schoolId = $schoolId;
    }

    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * Construct to set the createdAt how time now.
     */
    public function __construct()
    {

        $dt = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->createdAt = new \MongoDate($ts);
    }

    /**
     * status
     *
     * @ODM\Field(type="string")
     */
    private $status;



    /**
     * status
     *
     * @ODM\Field(type="integer")
     */
    private $totalMembers;


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUserCounter()
    {
        return $this->userCounter;
    }

    /**
     * @param mixed $userCounter
     */
    public function setUserCounter($userCounter)
    {
        $this->userCounter = $userCounter;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * created_at
     *
     * @ODM\Field(type="date")
     */
    private $upload_at;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $title;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $coverImage;
    /**
     * Description
     *
     * @ODM\Field(type="string")
     */
    private $description;

    /**
     * Users
     *
     * @ODM\Field(type="collection")
     */
    private $posts;

    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $emailEducator;

    /**
     * @return mixed
     */
    public function getEmailEducator()
    {
        return $this->emailEducator;
    }

    /**
     * @param mixed $emailEducator
     */
    public function setEmailEducator($emailEducator)
    {
        $this->emailEducator = $emailEducator;
    }

    /**
     * @return mixed
     */
    public function getEmailPro()
    {
        return $this->emailPro;
    }

    /**
     * @param mixed $emailPro
     */
    public function setEmailPro($emailPro)
    {
        $this->emailPro = $emailPro;
    }

    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $emailPro;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUploadAt()
    {
        return $this->upload_at;
    }

    /**
     * @param mixed $upload_at
     */
    public function setUploadAt($upload_at)
    {
        $this->upload_at = $upload_at;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @param mixed $coverImage
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;
    }

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts[] = $posts;
    }

    /**
     * @return mixed
     */
    public function getTotalMembers()
    {
        return $this->totalMembers;
    }

    /**
     * @param mixed $totalMembers
     */
    public function setTotalMembers($totalMembers)
    {
        $this->totalMembers = $totalMembers;
    }


    public function removeATask($taskId)
    {
        foreach ($this->getTasks() as $key => $value) {
            if ($value->getId() == new \MongoId($taskId)) {
                unset($this->tasks[$key]);
            }
        }
    }
}