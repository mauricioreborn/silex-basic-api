<?php


namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use MongoId;
use MongoDate;

/**
 * @ODM\EmbeddedDocument
 */

class FlagContent
{
    /**
     * UserId current logged user
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * Date
     *
     * @ODM\Field(type="date")
     */
    private $created_at;

    /**
     * status
     *
     * @ODM\Field(type="boolean")
     */
    private $status;

    /**
     * Method to get user id
     * @return string id of who flagged
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Method to set user id
     * @param MongoId $userId mongo user id to save on database
     */
    public function setUserId(MongoId $userId)
    {
        $this->userId = $userId;
    }

    /**
     * method to get date of the insertion
     * @return MongoDate an object mongo datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * method to set insertion date
     * @param MongoDate $created_at set now date
     */
    public function setCreatedAt(MongoDate $created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * Method to get flag content status
     * @return boolean with true for enabled and false as disabled
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Method to set flag status
     * @param boolean $status True as active and false as inactive
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }
}