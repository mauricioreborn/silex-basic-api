<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="messages")
 */
class Messages
{

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * chatId
     *
     * @ODM\Field(type="object_id")
     */
    private $chatId;

    /**
     * userId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * originId
     *
     * @ODM\Field(type="object_id")
     */
    private $originId;

    /**
     * read
     *
     * @ODM\Field(type="boolean")
     */
    private $read;

    /**
     * Message
     *
     * @ODM\Field(type="string")
     */
    private $message;


    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getOriginId()
    {
        return $this->originId;
    }

    /**
     * @param mixed $originId
     */
    public function setOriginId($originId)
    {
        $this->originId = $originId;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getChatId()
    {
        return $this->chatId;
    }

    /**
     * @param mixed $chatId
     */
    public function setChatId($chatId)
    {
        $this->chatId = $chatId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * @param mixed $read
     */
    public function setRead($read)
    {
        $this->read = $read;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }



    /**
     * Construct to set the createdAt how time now.
     */
    public function __construct()
    {

        $dt = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->createdAt = new \MongoDate($ts);
    }
}
