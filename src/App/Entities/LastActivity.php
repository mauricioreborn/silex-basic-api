<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */

class LastActivity
{

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;
    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $workId;
    /**
     * UserId
     *
     * @ODM\Field(type="date")
     */
    private $date;

    /**
     * UserId
     *
     * @ODM\Field(type="string")
     */
    private $message;

    /**
     * Icon
     *
     * @ODM\Field(type="string")
     */
    private $icon;

    /**
     * UserId
     *
     * @ODM\Field(type="string")
     */
    private $type;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param mixed $workId
     */
    public function setWorkId($workId)
    {
        $this->workId = $workId;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}