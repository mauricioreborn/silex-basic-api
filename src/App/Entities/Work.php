<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use MongoId;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="works")
 * ODM\@Index(keys={"title"="text"})
 */
class Work
{

    /**
     * @ODM\ReferenceOne(simple=true, targetDocument="User")
     */
    public $owner;

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $originalWork;


    /**
     * @ODM\EmbedMany(targetDocument="FlagContent")
     */
    private $flag;

    /**
     * flagged
     *
     * @ODM\Field(type="boolean")
     */
    private $flagged;

    /**
     * labId will be used to identify the lab that the work was created
     * @ODM\Field (type="object_id")
     */
    private $labId;

    /**
     * isPrivate identifies if the work is public or not.
     * @ODM\Field (type="boolean")
     */
    private $isPrivate;

    /**
     * This method will set the isPrivate attribute
     * @param bool $isPrivate An boolean to indicate if the work is public or
     *                        not.
     */
    public function setIsPrivate(bool $isPrivate)
    {
        $this->isPrivate = $isPrivate;
    }

    /**
     * This method will return the isPrivate attribute
     * @return string Return if the work is private or not.
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * This method is used to set the labId
     * @param string $labId the id of the lab that this work was created
     */
    public function setLabId(string $labId)
    {
        $this->labId = $labId;
    }

    /**
     * This method will return the labId of a work.
     * @return string   return a labId.
     */
    public function getLabId()
    {
        return $this->labId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @ODM\EmbedMany(targetDocument="File")
     */
    private $attach;


    /**
     * @ODM\EmbedOne(targetDocument="LastActivity")
     */
    private $lastActivity;

    /**
     * @return mixed
     */
    public function getUploadAt()
    {
        return $this->upload_at;
    }

    /**
     * @param mixed $upload_at
     */
    public function setUploadAt($upload_at)
    {
        $this->upload_at = $upload_at;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel(Level $level)
    {
        $this->level = $level;
    }

    /**
     * Upload_at
     *
     * @ODM\Field(type="date")
     */
    private $upload_at;

    /**
     * @ODM\EmbedOne(targetDocument="Level")
     */
    private $level;


    /**
     * Status
     *
     * @ODM\Field(type="string")
     */
    private $status;

    /**
     * Level
     *
     * @ODM\Field(type="collection")
     */
    private $coWork;

    /**
     * @ODM\EmbedMany(targetDocument="File")
     */
    private $thumbnail;

    /**
     * Criterias
     *
     * @ODM\Field(type="collection")
     */
    private $criterias;


    /**
     * @ODM\EmbedOne(targetDocument="FeedBackGrid")
     */
    private $feedback;


    /**
     * feedbackauthor
     *
     * @ODM\Field(type="boolean")
     */
    private $reviewed;


    /**
     * Status
     *
     * @ODM\Field(type="integer")
     */
    private $countCriterias;

    /**
     * @return mixed
     */
    public function getCountCriterias()
    {
        return $this->countCriterias;
    }

    /**
     * @param mixed $countCriterias
     */
    public function setCountCriterias($countCriterias)
    {
        $this->countCriterias = $countCriterias;
    }

    /**
     * @return mixed
     */
    public function getCriterias()
    {
        return $this->criterias;
    }

    /**
     * @param mixed $criterias
     */
    public function setCriterias($criterias)
    {
        $this->criterias[] = $criterias;
    }

    /**
     * highLight
     *
     * @ODM\Field(type="boolean")
     */
    private $highLight;

    /**
     * @return mixed
     */
    public function getHighLight()
    {
        return $this->highLight;
    }

    /**
     * @param mixed $highLight
     */
    public function setHighLight($highLight)
    {
        $this->highLight = $highLight;
    }


    /**
     * highLightDate
     *
     * @ODM\Field(type="date")
     */
    private $highLightDate;

    /**
     * @return mixed
     */
    public function getHighLightDate()
    {
        return $this->highLightDate;
    }

    /**
     * @param mixed $highLightDate
     */
    public function setHighLightDate($highLightDate)
    {
        $this->highLightDate = $highLightDate;
    }

    /**
     * offset
     *
     * @ODM\Field(type="string")
     */
    private $utcOffset;

    /**
     * @return mixed
     */
    public function getUtcOffset()
    {
        return $this->utcOffset;
    }

    /**
     * @param mixed $utcOffset
     */
    public function setUtcOffset($utcOffset)
    {
        $this->utcOffset = $utcOffset;
    }

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $title;
    /**
     * Description
     *
     * @ODM\Field(type="collection")
     */
    private $description;

    /**
     * Type
     *
     * @ODM\Field(type="string")
     */
    private $type;

    /**
     * Category
     *
     * @ODM\Field(type="string")
     */
    private $category;

    /**
     * @ODM\EmbedMany(targetDocument="Comment")
     */
    private $comments;

    /**
     * @ODM\EmbedMany(targetDocument="Like")
     */
    private $likes;

    /**
     * sendToPortfolio
     *
     * @ODM\Field(type="boolean")
     */
    private $sendPortfolio;

    /**
     * @return mixed
     */
    public function getSendPortfolio()
    {
        return $this->sendPortfolio;
    }

    /**
     * @param mixed $sendPortfolio
     */
    public function setSendPortfolio($sendPortfolio)
    {
        $this->sendPortfolio = $sendPortfolio;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comment
     */
    public function setComment(Comment $comment)
    {
        $comment->setOwner($this);
        $this->comments[] = $comment;
    }


    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAttach()
    {
        return $this->attach;
    }

    /**
     * @param mixed $file
     */
    public function setAttach(File $file)
    {
        $this->attach[] = $file;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     */
    public function setThumbnail(File $thumbnail)
    {
        $this->thumbnail[] = $thumbnail;
    }

    public function removeImagesToEdit()
    {
        foreach ($this->attach as $key => $value) {
            unset($this->attach[$key]);
        }

        foreach ($this->thumbnail as $key => $value) {
            unset($this->thumbnail[$key]);
        }

        foreach ($this->mainFile as $key => $value) {
            unset($this->mainFile[$key]);
        }
        
    }

    /**
     * @return mixed
     */
    public function getThumbnailPath()
    {
        return $this->thumbnailPath;
    }
    

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $like
     */
    public function setLikes(Like $like)
    {
        $like->setOwner($this);
        $this->likes[] = $like;
    }

    public function deleteLike($id)
    {
        foreach ($this->getLikes() as $idx => $like) {
            if ($id == $like->getUserId()) {
                unset($this->likes[$idx]);
                break;
            }
        }
    }

    /**
     * Total likes views comments
     *
     * @ODM\Field(type="integer")
     */
    private $popular;

    /**
     * Views
     *
     * @ODM\Field(type="integer")
     */
    private $views;

    /**
     * @return mixed
     */
    public function getPopular()
    {
        return $this->popular;
    }

    /**
     * @param mixed $popular
     */
    public function setPopular($popular)
    {
        $this->popular = $popular;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @ODM\EmbedMany(targetDocument="File")
     */
    private $mainFile;

    /**
     * @return mixed
     */
    public function getMainFile()
    {
        return $this->mainFile;
    }

    /**
     * @param mixed $mainFile
     */
    public function setMainFile(File $mainFile)
    {
        $this->mainFile[] = $mainFile;
    }

    /**
     * Group
     *
     * @ODM\Field(type="string")
     */
    private $group;

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * Level
     *
     * @ODM\Field(type="boolean")
     */
    private $available;

    /**
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * @param mixed $available
     */
    public function setAvailable($available)
    {
        $this->available = $available;
    }

    public function updateAllComment($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getCoWork()
    {
        return $this->coWork;
    }

    /**
     * @param mixed $coWork
     */
    public function setCoWork($coWork)
    {
        $this->coWork[] = $coWork;
    }

    /**
     * @return mixed
     */
    public function getReviewed()
    {
        return $this->reviewed;
    }

    /**
     * @param mixed $reviewed
     */
    public function setReviewed($reviewed)
    {
        $this->reviewed = $reviewed;
    }

    /**
     * @return mixed
     */
    public function getOriginalWork()
    {
        return $this->originalWork;
    }

    /**
     * @param mixed $originalWork
     */
    public function setOriginalWork($originalWork)
    {
        $this->originalWork = $originalWork;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param mixed $lastActivity
     */
    public function setLastActivity(LastActivity $lastActivity)
    {

        $this->lastActivity = $lastActivity;
    }

    /**
     * Method to get boolean flagged status
     * @return boolean true as enabled and false as disabled
     */
    public function getFlagged()
    {
        return $this->flagged;
    }

    /**
     * Method to set a boolean status
     * @param boolean $flagged
     */
    public function setFlagged(bool $flagged)
    {
        $this->flagged = $flagged;
    }

    public function __construct()
    {
        $this->highLight = false;

    }

    /**
     * @return mixed
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Method to get a FlagContent object
     * @return FlagContent object
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Method to set a flagContent when an user to flag a work
     * @param FlagContent $flag with all parameters filled
     */
    public function setFlag(FlagContent $flag)
    {
        $this->flag[] = $flag;
    }


    /**
     * Method to set if a work is for feedback
     * @param FeedBackGrid $feedback An object FeedBackGrid
     */
    public function setFeedback(FeedBackGrid $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * Method to remove a flag of the work
     * @param string $userId User id identifier on database
     */
    public function removeUserFlag(string $userId)
    {
        foreach ($this->getFlag() as $index => $flagContent) {
            if ($flagContent->getUserId() == $userId) {
                unset($this->flag[$index]);
            }
        }
    }

    
    /**
     * Method to get total flags with true status.
     * @return string total flags enabled
     */
    public function getTotalFlags()
    {
        $totalFlag = '';
        foreach ($this->getFlag() as $flag) {
            if ($flag->getStatus() == true) {
                $totalFlag ++;
            }
        }
        return $totalFlag;
    }

    /**
     * Method to check if the current logged user already flagged this work
     * @param string $userId user mongo Identifier
     * @return bool          True if the user already flagged it, false if the
     *                       user did not flag it.
     */
    public function checkIFlagged(string $userId)
    {
        foreach ($this->getFlag() as $flag) {
            if ($flag->getUserId() == $userId) {
                return true;
            }
        }
        return false;
    }

}


