<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class File
{

    /**
     * @ODM\ReferenceOne(simple=true, targetDocument="Work")
     */
    private $owner;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $path;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $type;

    /**
     * Title
     *
     * @ODM\Field(type="string")
     */
    private $name;
    /**
     * Title
     *
     * @ODM\Field(type="date")
     */
    private $upload_at;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUploadAt()
    {
        return $this->upload_at;
    }

    /**
     * @param mixed $upload_at
     */
    public function setUploadAt($upload_at)
    {
        $this->upload_at = $upload_at;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}