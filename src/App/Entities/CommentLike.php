<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="commentLike")
 */
class CommentLike
{
    /**
     * Birthday
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;
    /**
     * Birthday
     *
     * @ODM\Field(type="date")
     */
    private $created_at;

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;
    /**
     * Birthday
     *
     * @ODM\Field(type="object_id")
     */
    private $workId;


    /**
     * ProductId workId or streamId or livesessionId
     *
     * @ODM\Field(type="object_id")
     */
    private $productId;

    /**
     * Birthday
     *
     * @ODM\Field(type="object_id")
     */
    private $commentId;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getWorkId()
    {
        return $this->workId;
    }

    /**
     * @param mixed $workId
     */
    public function setWorkId($workId)
    {
        $this->workId = $workId;
    }

    /**
     * @return mixed
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * @param mixed $commentId
     */
    public function setCommentId($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }
}