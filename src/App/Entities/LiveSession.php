<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="session")
 */
class LiveSession
{

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * Owner(userId)
     *
     * @ODM\Field(type="object_id")
     */
    private $owner;

    /**
     * Type
     *
     * @ODM\Field(type="string")
     */
    private $type;
    /**
     * Name
     *
     * @ODM\Field(type="string")
     */
    private $name;
    /**
     * Description
     *
     * @ODM\Field(type="string")
     */
    private $description;
    /**
     * Date
     *
     * @ODM\Field(type="date")
     */
    private $date;
    /**
     * StartTime
     *
     * @ODM\Field(type="string")
     */
    private $startTime;
    /**
     * EndTime
     *
     * @ODM\Field(type="string")
     */
    private $endTime;
    /**
     * HighLight
     *
     * @ODM\Field(type="boolean")
     */
    private $highLight;
    /**
     * HighLight
     *
     * @ODM\Field(type="date")
     */
    private $highLightDate;
    /**
     * AllDay
     *
     * @ODM\Field(type="boolean")
     */
    private $allDay;
    /**
     * Repeating
     *
     * @ODM\Field(type="boolean")
     */
    private $repeating;
    /**
     * Speaker
     *
     * @ODM\Field(type="object_id")
     */
    private $speaker;
    /**
     * Moderator
     *
     * @ODM\Field(type="object_id")
     */
    private $moderator;
    /**
     * Link
     *
     * @ODM\Field(type="string")
     */
    private $link;

    /**
     * approved
     *
     * @ODM\Field(type="boolean")
     */
    private $approved;

    /**
     * EndTime
     *
     * @ODM\Field(type="string")
     */
    private $status;

    /**
     * Case this session was duplicated, this will be the ID of the original session.
     *
     * @ODM\Field(type="object_id")
     */
    private $originalSessionId;

    /**
     * A boolean the will be the status of the deadline
     * @ODM\Field(type="boolean")
     */
    private $deadLineStatus;

    /**
     * A boolean the will be the status of the deadline
     * @ODM\Field(type="date")
     */
    private $deadLineDate;

    /**
     * A boolean the will be the status of the deadline
     * @ODM\Field(type="string")
     */
    private $deadLineTime;

    /**
     * @return mixed
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param mixed $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * thumbnail
     *
     * @ODM\Field(type="string")
     */
    private $thumbnail;

    /**
     * Users
     *
     * @ODM\Field(type="collection")
     */
    private $users;
    /**
     * createdAt
     *
     * @ODM\Field(type="date")
     */
    private $created_at;

    /**
     * OpenTokId
     *
     * @ODM\Field(type="string")
     */
    private $opentokId;

    /**
     * archiveUrl
     *
     * @ODM\Field(type="string")
     */
    private $archiveUrl;

    /**
     * workQueue
     *
     * @ODM\Field(type="collection")
     */
    private $workQueue;

    /**
     * archiveUrl
     *
     * @ODM\Field(type="string")
     */
    private $activeWork;

    /**
     * offset
     *
     * @ODM\Field(type="string")
     */
    private $utcOffset;

    /**
     * @return mixed
     */
    public function getUtcOffset()
    {
        return $this->utcOffset;
    }

    /**
     * @param mixed $utcOffset
     */
    public function setUtcOffset($utcOffset)
    {
        $this->utcOffset = $utcOffset;
    }
    

    /**
     * @return mixed
     */
    public function getWorkQueue()
    {
        return $this->workQueue;
    }

    /**
     * @param mixed $workQueue
     */
    public function setWorkQueue($workQueue)
    {
        $this->workQueue[] = $workQueue;
    }

    /**
     * @return mixed
     */
    public function selectWorkQueue($id)
    {
        foreach ($this->getWorkQueue() as $idx => $work) {
            if ($id == $work['workId']) {
                $work['selected'] = true;
                $this->workQueue[$idx] = $work;
                break;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }



    /**
     * @return mixed
     */
    public function getActiveWork()
    {
        return $this->activeWork;
    }

    /**
     * @param mixed $activeWork
     */
    public function setActiveWork($activeWork)
    {
        $this->activeWork = $activeWork;
    }
    /**
     * @return mixed
     */
    public function getArchiveUrl()
    {
        return $this->archiveUrl;
    }

    /**
     * @param mixed $archiveUrl
     */
    public function setArchiveUrl($archiveUrl)
    {
        $this->archiveUrl = $archiveUrl;
    }

    /**
     * @return mixed
     */
    public function getOpentokId()
    {
        return $this->opentokId;
    }

    /**
     * @param mixed $opentokId
     */
    public function setOpentokId($opentokId)
    {
        $this->opentokId = $opentokId;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return mixed
     */
    public function getHighLight()
    {
        return $this->highLight;
    }

    /**
     * @param mixed $highLight
     */
    public function setHighLight($highLight)
    {
        $this->highLight = $highLight;
    }

    /**
     * @return mixed
     */
    public function getHighLightDate()
    {
        return $this->highLightDate;
    }

    /**
     * @param mixed $highLightDate
     */
    public function setHighLightDate($highLightDate)
    {
        $this->highLightDate = $highLightDate;
    }

    /**
     * @return mixed
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * @param mixed $allDay
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;
    }

    /**
     * @return mixed
     */
    public function getRepeating()
    {
        return $this->repeating;
    }

    /**
     * @param mixed $repeating
     */
    public function setRepeating($repeating)
    {
        $this->repeating = $repeating;
    }

    /**
     * @return mixed
     */
    public function getSpeaker()
    {
        return $this->speaker;
    }

    /**
     * @param mixed $speaker
     */
    public function setSpeaker($speaker)
    {
        $this->speaker = $speaker;
    }

    /**
     * @return mixed
     */
    public function getModerator()
    {
        return $this->moderator;
    }

    /**
     * @param mixed $moderator
     */
    public function setModerator($moderator)
    {
        $this->moderator = $moderator;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users[] = $users;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOriginalSessionId()
    {
        return $this->originalSessionId;
    }

    /**
     * @param mixed $originalSessionid
     */
    public function setOriginalSessionId($originalSessionId)
    {
        $this->originalSessionId = $originalSessionId;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @param $deadLineStatus
     */
    public function setDeadLineStatus($deadLineStatus)
    {
        $this->deadLineStatus = $deadLineStatus;
    }

    /**
     * @return boolean
     */
    public function getDeadLineStatus()
    {
        return $this->deadLineStatus;
    }

    /**
     * @param $deadLineDate
     */
    public function setDeadLineDate($deadLineDate)
    {
        $this->deadLineDate = $deadLineDate;
    }

    /**
     * @return mixed
     */
    public function getDeadLineDate()
    {
        return $this->deadLineDate;
    }

    /**
     * @param $deadLineTime
     */
    public function setDeadLineTime($deadLineTime)
    {
        $this->deadLineTime = $deadLineTime;
    }

    /**
     * @return mixed
     */
    public function getDeadLineTime()
    {
        return $this->deadLineTime;
    }

    public function deleteEnroll($id)
    {
        foreach ($this->getUsers() as $idx => $user) {
            if ($id == $user) {
                unset($this->users[$idx]);
                break;
            }
        }
    }

    public function deleteWorkQueue($id)
    {
        foreach ($this->workQueue as $idx => $data) {
            if ($id == $data['workId']) {
                unset($this->workQueue[$idx]);
                break;
            }
        }
    }

    public function deleteUserWorkQueue(string $id)
    {
        foreach ($this->workQueue as $idx => $data) {
            if ($id == $data['userId']) {
                unset($this->workQueue[$idx]);
                break;
            }
        }
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return array(
          'name' => $this->name,
          'description' => $this->description,
          'date' => $this->date,
          'startTime' => $this->startTime,
          'endTime' => $this->endTime,
          'speaker' => $this->speaker,
          'moderator' => $this->moderator,
          'users' => $this->users
        );
    }

    public function __construct()
    {
        $this->status = 'pending';
        $this->highLight = false;
    }

    /**
     * Method to update work queue order
     * @param array $data[workId,order]
     */
    public function updateWorkQueue(array $data)
    {
        foreach ($this->getWorkQueue() as $index => $work) {
            if ($data['workId'] == $work['workId']) {
                $work['order'] = $data['order'];
                $this->workQueue[$index] = $work;
                break;
            }
        }
    }
}
