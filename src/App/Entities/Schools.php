<?php

/**
 * Entity Organization File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity Organization File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="schools")
 */
class Schools
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * organizationId
     *
     * @ODM\Field(type="object_id")
     */
    private $organizationId;

    /** @ODM\Field(type="string") */
    private $name;

    /**
     * Email
     *
     * @ODM\Field(type="boolean")
     */
    private $deleted;

    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $emailEducator;

    /**
     * Email
     *
     * @ODM\Field(type="string")
     */
    private $emailPro;

    /** @ODM\Field(type="integer") */
    private $numbersOfStudent;

    /** @ODM\Field(type="integer") */
    private $numbersOfClassroom;

    /**
     * status
     *
     * @ODM\Field(type="string")
     */
    private $status;

    /**
     * Created date
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Construct to set the createdAt how time now.
     */
    public function __construct()
    {

        $dt = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->createdAt = new \MongoDate($ts);
        $this->status = "active";
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }



    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param mixed $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmailEducator()
    {
        return $this->emailEducator;
    }

    /**
     * @param mixed $emailEducator
     */
    public function setEmailEducator($emailEducator)
    {
        $this->emailEducator = $emailEducator;
    }

    /**
     * @return mixed
     */
    public function getEmailPro()
    {
        return $this->emailPro;
    }

    /**
     * @param mixed $emailPro
     */
    public function setEmailPro($emailPro)
    {
        $this->emailPro = $emailPro;
    }


    /**
     * @return mixed
     */
    public function getNumbersOfClassroom()
    {
        return $this->numbersOfClassroom;
    }

    /**
     * @param mixed $numbersOfClassroom
     */
    public function setNumbersOfClassroom($numbersOfClassroom)
    {
        $this->numbersOfClassroom = $numbersOfClassroom;
    }

    /**
     * @return mixed
     */
    public function getNumbersOfStudent()
    {
        return $this->numbersOfStudent;
    }

    /**
     * @param mixed $numbersOfStudent
     */
    public function setNumbersOfStudent($numbersOfStudent)
    {
        $this->numbersOfStudent = $numbersOfStudent;
    }

}