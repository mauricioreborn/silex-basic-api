<?php

/**
 * Entity Flag
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use DateTime;
use MongoDate;
use MongoId;
use DateTimeZone;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="flag")
 */
class Flag
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * Type of the flag if is a work or a comment
     *
     * @ODM\Field(type="string")
     */
    private $type;

    /**
     * Flag status is true(enabled) false (disabled)
     *
     * @ODM\Field(type="boolean")
     */
    private $status;

    /**
     * Date of creation the flag
     *
     * @ODM\Field(type="date")
     */
    private $created_at;

    /**
     * Method to get type of the flag
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Method to set type of flag
     *
     * @param mixed $type A string with type of flag (work or comment)
     *
     * @return null
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * Method to get flag document id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to set an identifier on flag
     *
     * @param MongoId $id MongoId object to set on flag
     *
     * @return null
     */
    public function setId(MongoId $id)
    {
        $this->id = $id;
    }

    /**
     * Method to get the creation flag date
     *
     * @return MongoDate Object
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Method to set a creation date
     *
     * @param mixed $created_at A date parameter to set when this flag was
     *                          created
     *
     * @return null
     */
    public function setCreatedAt($created_at = null)
    {
        if ($created_at == null) {
            $date = "Y-m-d H:i:s";
            $dt = new DateTime(date($date), new DateTimeZone('UTC'));
            $ts = $dt->getTimestamp();
            $created_at = new MongoDate($ts);
        }
        $this->created_at = $created_at;
    }

    /**
     * Method to get user id of the user collection
     *
     * @return string id of user that flagged
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Method to set the user id  of user that flagged something
     *
     * @param string $userId user id on user document
     *
     * @return null
     */
    public function setUserId(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Method to get work or comment id reference the work or comment flagged
     *
     * @return string ProductId reference a work or comment flagged
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Method to set which product is flagged work or comment
     *
     * @param string $productId Work or comment id on work/comment collection
     *
     * @return null
     */
    public function setProductId(string $productId)
    {
        $this->productId = $productId;
    }

    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;

    /**
     * ProductId
     *
     * @ODM\Field(type="object_id")
     */
    private $productId;

    /**
     * Method to get flag status
     *
     * @return boolean true for active and false to inactive
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Method to set status of the flag.
     *
     * @param boolean $status True for enabled or False to disabled
     *
     * @return null
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    
}