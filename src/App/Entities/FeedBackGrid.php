<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class FeedBackGrid
{

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /**
     * feedbackauthor
     *
     * @ODM\Field(type="string")
     */
    private $feedbackauthor;


    /**
     * feedbackdate
     *
     * @ODM\Field(type="date")
     */
    private $feedbackdate;


    /**
     * FeedBackMessage
     *
     * @ODM\Field(type="string")
     */
    private $feedbackmessage;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFeedbackauthor()
    {
        return $this->feedbackauthor;
    }

    /**
     * @param mixed $feedbackauthor
     */
    public function setFeedbackauthor($feedbackauthor)
    {
        $this->feedbackauthor = $feedbackauthor;
    }

    /**
     * @return mixed
     */
    public function getFeedbackdate()
    {
        return $this->feedbackdate;
    }

    /**
     * @param mixed $feedbackdate
     */
    public function setFeedbackdate($feedbackdate)
    {
        $this->feedbackdate = $feedbackdate;
    }

    /**
     * @return mixed
     */
    public function getFeedbackmessage()
    {
        return $this->feedbackmessage;
    }

    /**
     * @param mixed $feedbackmessage
     */
    public function setFeedbackmessage($feedbackmessage)
    {
        $this->feedbackmessage = $feedbackmessage;
    }
}