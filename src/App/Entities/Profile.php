<?php

/**
 * Entity Profile Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity Profile Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="profiles")
 */
class Profile
{

    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;

    /** @ODM\Field(type="string") */
    private $label;

    /** @ODM\Field(type="string") */
    private $name;

    /** @ODM\Field(type="string") */
    private $description;

    /** @ODM\Field(type="integer") */
    private $order;


    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order oder
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }


    /**
     * Method to get the date of the user has been created.
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->_createdAt;
    }

    /**
     * Method to set the datetime that an user will be created.
     *
     * @param string $createdAt of user
     *
     * @return void
     */
    public function setCreatedAt($createdAt)
    {
        $this->_createdAt = $createdAt;
    }

    /**
     * Construct to set the createdAt how time now.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_createdAt = new \DateTime();
    }
}
