<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Entity User File Doc Comment
 *
 * PHP version 7
 *
 * @category Entity
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @ODM\Document(collection="posts")
 */
class Post
{
    /**
     * LabId or classRoomId
     *
     * @ODM\Field(type="object_id")
     */
    private $groupId;
    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;
    /**
     * publication
     *
     * @ODM\Field(type="string")
     */
    private $publication;
    /**
     * Id
     *
     * @ODM\Id()
     */
    private $id;

    /**
     * UserId
     *
     * @ODM\Field(type="date")
     */
    private $uploadAt;
    /**
     * @ODM\EmbedMany(targetDocument="Like")
     */
    private $likes;
    /**
     * WorkId
     *
     * @ODM\Field(type="string")
     */
    private $totalComment;

    /**
     * WorkId
     *
     * @ODM\Field(type="string")
     */
    private $totalLike;

    /**
     * @return mixed
     */
    public function getTotalLike()
    {
        return $this->totalLike;
    }

    /**
     * @param mixed $totalLike
     */
    public function setTotalLike($totalLike)
    {
        $this->totalLike = $totalLike;
    }

    /**
     * WorkId
     *
     * @ODM\Field(type="string")
     */
    private $type;


    /**
     * @ODM\EmbedMany(targetDocument="Comment")
     */
    private $comments;

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $owner
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param mixed $publication
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUploadAt()
    {
        return $this->uploadAt;
    }

    /**
     * @param mixed $upload_at
     */
    public function setUploadAt($upload_at)
    {
        $this->uploadAt = $upload_at;
    }

    /**
     * @return mixed
     */
    public function getLike()
    {
        return $this->likes;
    }

    /**
     * @param mixed $like
     */
    public function setLike(Like $like)
    {
        $this->likes[] = $like;
    }

    /**
     * @return mixed
     */
    public function getTotalComment()
    {
        return $this->totalComment;
    }

    /**
     * @param mixed $totalComment
     */
    public function setTotalComment($totalComment)
    {
        $this->totalComment = $totalComment;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComment(Comment $comment)
    {
        $this->comments[] = $comment;
    }

    public function updateAllComment($comments)
    {
        $this->comments = $comments;
    }



    public function deleteLike($id)
    {
        foreach ($this->getLike() as $idx => $like) {
            if ($id == $like->getUserId()) {
                unset($this->likes[$idx]);
                break;
            }
        }
    }

    /**
     * Total likes views comments
     *
     * @ODM\Field(type="integer")
     */
    private $popular;

    /**
     * @return mixed
     */
    public function getPopular()
    {
        return $this->popular;
    }

    /**
     * @param mixed $popular
     */
    public function setPopular($popular)
    {
        $this->popular = $popular;
    }

    



}