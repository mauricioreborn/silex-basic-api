<?php

namespace App\Entities;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class Task
{
    /**
     * Indentfy
     *
     * @ODM\Id
     */
    private $id;
    /**
     * UserId
     *
     * @ODM\Field(type="object_id")
     */
    private $userId;
    /**
     * UserId
     *
     * @ODM\Field(type="string")
     */
    private $title;
    /**
     * UserId
     *
     * @ODM\Field(type="string")
     */
    private $description;
    /**
     * dueDate
     *
     * @ODM\Field(type="date")
     */
    private $dueDate;
    /**
     * UserId
     *
     * @ODM\Field(type="date")
     */
    private $uploadAt;

    /**
     * turnedIn
     *
     * @ODM\Field(type="collection")
     */
    private $turnedIn;

    /**
     * @return mixed
     */
    public function getTurnedIn()
    {
        return $this->turnedIn;
    }

    /**
     * @param mixed $turnedIn
     */
    public function setTurnedIn($turnedIn)
    {
        $this->turnedIn[] = $turnedIn;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUploadAt()
    {
        return $this->uploadAt;
    }

    /**
     * @param mixed $uploadAt
     */
    public function setUploadAt($uploadAt)
    {
        $this->uploadAt = $uploadAt;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param mixed $dueDate
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}