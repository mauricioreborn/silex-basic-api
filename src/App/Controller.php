<?php

/**
 * Abstract  Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App;

use App\Entities\Work;
use App\Repository\WorkRepository;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;
use App\Services\UserService;
use Doctrine\ORM\EntityManager;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Abstract  Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SWG\Info(
 *   title="ACME API",
 *   description="ACME API endpoints",
 *   version="1.0.0",
 *   @SWG\Contact(
 *     email="contact@2mundos.net",
 *     name="2Mundos ACME API Team",
 *     url="http://2mundos.net"
 *   ),
 *   @SWG\License(
 *     name="MIT",
 *     url="http://github.com/gruntjs/grunt/blob/master/LICENSE-MIT"
 *   ),
 *   termsOfService="http://swagger.io/terms/"
 * )
 * @SWG\Swagger(
 *   host=URL,
 *   schemes={"http"},
 *   produces={"application/json"},
 *   consumes={"application/json"},
 *   @SWG\ExternalDocumentation(
 *     description="find more info here",
 *     url="https://swagger.io/about"
 *   )
 * )
 */
class Controller
{

    /**
     * EntityManager
     *
     * @var EntityManager
     */
    private $entityManager;

    /**
     * Application
     *
     * @var Application
     */
    private $application;

    /**
     * Construct to receive an instance of application.
     *
     * @param Application $app instance of application
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->entityManager = $app['mongodbodm.dm'];
        $this->application = $app;
    }

    /**
     * GetEntityMaganer
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }



    /**
     * GetApplication
     *
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * SetApplication
     *
     * @param Application $application instance of application
     *
     * @return void
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * SetEntityMaganer
     *
     * @param EntityManager $entityManager EntityManager
     *
     * @return Controller
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }


    public function responseJson($message, $status)
    {
        return new JsonResponse($message, $status);
    }

    public function getToken($token)
    {
        $tokenGenerator = new TokenGenerator(new TokenBuilder());
        return $tokenGenerator->readToken($token);
    }

    public function hasPermission($token, $type)
    {
        $token = $this->getToken($token);
        if ($token->getClaim('context')->level === $type) {
            return [
                'permission' => true,
                'token' => $token
            ];
        }
        return ['permission' => false];
    }

    /**
     * Method to validate mongo Ids
     * @param $id
     * @return bool
     */
    public function validateId($id)
    {
        $userService = new UserService();
        if ($userService->validateId($id)) {
            return true;
        }
        return false;
    }

    public function workExist($workId)
    {
        $workRepository = new WorkRepository($this->getEntityManager()->createQueryBuilder(Work::class));
        return $workRepository->existWork($workId);
    }

    public function calculateSkip($data)
    {
        if (!empty($data['page'])) {
            return ($data['page'] * $data['limit']) - $data['limit'];
        }
        if (empty($data['page'])) {
            if (empty($data['skip'])) {
                return 0;
            }
            return $data['skip'];
        }
    }


    /** Method to create a defaultPagination
     * @param $data
     * @param $request
     * @return mixed
     */
    public function buildPaginate($data, $request)
    {
        $data['limit'] = $request->get('limit', 1000);
        $data['skip'] = $request->get('skip', 0);
        $data['skip'] = $this->calculateSkip($data);
        return $data;
    }
    
}
