<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 05/07/17
 * Time: 02:58
 */

namespace App\EmailTemplate;


class AccountOrganizationEmailConfirm
{

    public function getTemplate()
    {
        return '
<p>Thank you for your interest in entering as an organization!</p>
<p>We received your information successfully. Please wait while we analyze your company. Soon you will receive a contact from one of our staff members.</p>
<p>ACME Team.</p>
        ';
    }

}