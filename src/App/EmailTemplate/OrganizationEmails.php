<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 05/07/17
 * Time: 03:15
 */

namespace App\EmailTemplate;


class OrganizationEmails
{
    public function getTemplateApproved($data)
    {
        $link = '<a href="mailto:support@acme.com">Support</a>';
        return '
<p>Hi '.$data['orgName'].' ,</p>
<p>Your Organization on ACME Platform has been approved.</p>
<p>Please user the credentials below to enter our platform:</p>
<p></p>
<p>Login: '.$data['email'].'</p>
<p>Password: '.$data['password'].'</p>
<p></p>
<p>If you any trouble or doubt, please contact our '.$link.' .</p>
<p>ACME Team.</p>
        ';
    }

    public function getTemplateDenied($name)
    {
        $link = '<a href="mailto:support@acme.com">Support</a>';
        return '
<p>Hi '.$name.' ,</p>
<p>Thanks for your registration on the Platform but sadly we couldn’t approve.</p> 
<p>We are willing to talk and have your organization on our Platform.</p>
<p>For more information, please contact our '.$link.' .</p>
<p>ACME Team.</p>
        ';
    }
}