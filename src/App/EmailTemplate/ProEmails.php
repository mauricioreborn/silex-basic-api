<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 02/08/17
 * Time: 03:59
 */

namespace App\EmailTemplate;


class ProEmails
{
    public function getProTemplate($status)
    {
        $link = '<a href="mailto:contact@acme.com">Support</a>';
        return '
<p>Welcome to ACME Platform, </p>
<p>Your Professional Account in our platform was '.$status.'</p>
<p>if you have any doubts please contact us: '.$link;
    }
}