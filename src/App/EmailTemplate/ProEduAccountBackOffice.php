<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 05/07/17
 * Time: 03:24
 */

namespace App\EmailTemplate;


class ProEduAccountBackOffice
{
    public function getTemplateEducator($response)
    {
        return
        '<p>ACME Platform Created you an account, use the following credentials to access our platform:</p>
            <p>- login: '.$response['email'].'</p>
            <p>- password: '.$response['passWord'].'</p>
            
            <p>Remember to update your personal information.</p>
            <p>ACME Team.</p>
            ';
    }

    public function getTemplatePro($response)
    {
        return
            '<p>ACME Platform Created you an account, use the following credentials to access our platform:</p>
            <p>- login: '.$response['email'].'</p>
            <p>- password: '.$response['passWord'].'</p>
            
            <p>Remember to update your personal information.</p>
            <p>ACME Team.</p>
            ';
    }
}