<?php

namespace App\EmailTemplate;

/**
 * Login Controller Doc Comment
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Acme
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 *
 * @SLX\Controller(prefix="/")
 */
class AccountEmailConfirm
{
    public function getTemplate($data)
    {
        //$url = getenv('FRONTEND_URL').'/onboarding/confirm/account?token='.$data;
        $url = getenv('FINAL_URL').'onboarding/confirm/account?token='.$data;
        return '
<p>Welcome to ACME Platform, </p>
<p>You can confirm your account email through the link below:</p>
<p><a href="'.$url.'" target="_blank">Confirm Account</a></p>
<p>If you didn’t sign up for an account on ACME Platform, please ignore this email.</p>';
    }

    public function getProTemplate()
    {
        return '
<p>Welcome to ACME Platform, </p>
<p>Your Professional Account is pending approval</p>
<p>Our Team Will look into it as soon as possible.</p>';
    }

}