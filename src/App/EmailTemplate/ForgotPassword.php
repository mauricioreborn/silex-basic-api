<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 05/07/17
 * Time: 03:02
 */

namespace App\EmailTemplate;


class ForgotPassword
{
    public function getTemplate($data)
    {
//        $url=getenv('FRONTEND_URL').'/onboarding/reset?token='.$data;
        $url= getenv('FINAL_URL').'onboarding/reset?token='.$data;
        return '
<p>Hello, </p>
<p>We have received a request to reset your password on ACME Platform. To reset your password, please click this link:</p>
<p><a href="'.$url.'" target="_blank">Confirm Account</a></p>
<p>If you didn’t request to change your password, please ignore this email.</p>
        ';
    }
}