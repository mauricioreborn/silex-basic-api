<?php
namespace App\Repository;

use App\Entities\Criterias;
use App\Entities\FeedBackGrid;
use App\Entities\Level;
use App\Entities\User;
use App\Entities\Work;
use App\Entities\LiveSession;
use \Doctrine\ODM\MongoDB\DocumentRepository;
use Sokil\Mongo\Exception;

class FeedBackRepository extends Repository
{

    public function getUser($userId)
    {
        $user = $this->getRepository()->createQueryBuilder(User::class)
            ->field('_id')
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        if (count($user) > 0) {
            if (!empty($user->getCriterias())) {
                $ucrit = $user->getCriterias();
            } else {
                $ucrit = array();
            }
            return [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'profile' => $user->getProfile(),
                'level' => $user->getLevel()->getAlias(),
                'avatar' => $user->getUserPhoto(),
                'criterias' => $ucrit
            ];
        }
        return '';
    }

    public function getAllCriterias()
    {

    }

    public function checkCriteria($userId, $check)
    {


        $repo = $this->getRepository()->createQueryBuilder(User::class);
        $status = $repo
            ->field('_id')
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();


        if ($status->getCriterias()) {
            if (in_array($check, $status->getCriterias())) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function checkWorkCriteria($workId, $check)
    {
        $repo = $this->getRepository()->createQueryBuilder(Work::class);
        $status = $repo
            ->field('_id')
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->getSingleResult();

        if ($status->getCriterias()) {
            if (in_array($check, $status->getCriterias())) {
                return true;
            }
            return false;
        }
        return false;
    }


    /**
     * @param null $productId
     * @param string $type
     * @return array
     */
    public function criterias($workId, $userId, $type = 'user')
    {
        $criteriaRepository = $this->getRepository()->createQueryBuilder(Criterias::class);

        $crit = $criteriaRepository
            ->sort('criterias.criteria', 1)
            ->getQuery()
            ->execute();

        $type = 'user';

        $workRepository = $this->getRepository()->createQueryBuilder(Work::class)
            ->field("_id")
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->getSingleResult();

        $type = count($workRepository->getCriterias())>0 ? 'work' : 'user';


        $criterias = array();
        foreach ($crit as $criteria) {
            $data['id'] = $criteria->getId();
            $data['criteria'] = $criteria->getCriteria();
            $data['title'] = $criteria->getTitle();
            $data['description'] = $criteria->getDescription();

            if ($type == 'user') {
                $data['status'] = $this->checkCriteria($userId, $criteria->getCriteria());
            }

            if ($type == 'work') {
                $data['status'] = $this->checkWorkCriteria($workId, $criteria->getCriteria());
            }

            $criterias[] = $data;
        }
        return $criterias;
    }

    public function fetchName($id)
    {
        $userRepo = $this->getRepository()->getRepository(User::Class);
        $user = $userRepo->findOneBy(
            [
                '_id' => new \MongoId($id)
            ]
        );

        return $user->getFirstName().' '.$user->getLastName();
    }

    public function workIterator($id)
    {
        $repo = $this->getRepository()->createQueryBuilder(Work::class);
        return $repo
            ->field('_id')->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function getFeedBack($data)
    {
        $work = $this->workIterator($data['workId']);

        $workFeed = $this->buildWork($work);
        $workFeed['user'] = $this->getUser($work->getUserId());

        if ($work->getReviewed() == true || $data['type'] == 'pro') {
            $workFeed['criterias'] = $this->criterias($work->getId(), $work->getUserId());
        }

        $workFeed['feedback'] = '';
        if (count($work->getFeedback()) >0) {
            $workFeed['feedback'] = $this->buildFeedback($work->getFeedback());
            $workFeed['comment'] = $work->getFeedback()->getFeedbackmessage();
        }
        return $workFeed;
    }

    /**
     * @param Work $work
     * @param $criteria
     * @param $fid
     * @param $msg
     * @return bool
     */
    public function saveWorkFeedBack(Work $work, $criteria, $fid, $msg)
    {
        $em = $this->getRepository();
        $feeBackGrid = new FeedBackGrid();
        $feeBackGrid->setFeedbackauthor(new \MongoId($fid));
        $feeBackGrid->setFeedbackdate($this->getMongoDate());
        $feeBackGrid->setFeedbackmessage($msg);
        if (count($work->getCriterias()) > 0 || !empty($work->getCriterias())) {
            foreach ($criteria as $val) {
                if (!in_array($val, $work->getCriterias())) {
                    $work->setCriterias($val);
                }
            }
            $work->setCountCriterias(count($criteria));
            $work->setFeedback($feeBackGrid);
            $work->setReviewed(true);
            $work->setAvailable(false);
            $em->persist($work);
            $em->flush();
            return true;
        }
        if (count($criteria) == 0) {
            $work->setFeedback($feeBackGrid);
            $work->setReviewed(true);
            $work->setAvailable(false);
            $work->setStatus('reproved');
            $em->persist($work);
            $em->flush();
            return true;
        }
        foreach ($criteria as $crit) {
            $work->setCriterias($crit);
        }
        $work->setCountCriterias(count($criteria));
        $work->setFeedback($feeBackGrid);
        $work->setReviewed(true);
        $work->setAvailable(false);
        $em->persist($work);
        $em->flush();
        return true;
    }

    public function upgradeLevel($userId, $level)
    {
        if ($level == 'Student Level 1') {
            $newLevel = 'Student Level 2';
            $number = 2;
        }
        if ($level == 'Student Level 2') {
            $newLevel = 'Student Level 3';
            $number = 3;
        }
        if ($level == 'Student Level 3') {
            $newLevel = 'Student Level 3';
            $number = 3;
        }

        $level = new Level();
        $level->setNumber($number);
        $level->setAlias($newLevel);
        $level->setDate($this->getMongoDate());

        $check = $this->getRepository()->createQueryBuilder(User::Class)
            ->updateOne()
            ->field('level')->set($level)
            ->field('criterias')->set('')
            ->field('_id')->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
        if ($check) {
            return true;
        }
        return false;
    }

    public function updateUserFeedBack($userId, $criteria)
    {

        $em = $this->getRepository();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBy(
            [
                '_id' => new \MongoId($userId)
            ]
        );
        $sorted = [];
        if (count($user)>0) {
            if (count($user->getCriterias()) > 0) {
                $newArray = array_merge($user->getCriterias(), $criteria);

                if (count($newArray) >= 12) {
                    $this->upgradeLevel($userId, $user->getLevel()->getAlias());
                    $em->createQueryBuilder(User::class)
                        ->updateOne()
                        ->field("totalWork")
                        ->set(0)
                        ->field("totalComment")
                        ->set(0)
                        ->field("alreadyNotified")
                        ->set(false)
                        ->field("criterias")
                        ->set([])
                        ->field("_id")
                        ->equals(new \MongoId($userId))
                        ->getQuery()
                        ->execute();
                    return true;
                }
                foreach ($newArray as $val) {
                    if (!in_array($val, $user->getCriterias())) {
                        $user->setCriterias($val);
                        $sorted[] = $val;
                    }
                }
                $em->persist($user);
                $em->flush();
                return true;
            }
            if (count($criteria) >= 12) {
                $this->upgradeLevel($userId, $user->getLevel()->getAlias());
                $em->createQueryBuilder(User::class)
                    ->updateOne()
                    ->field("totalWork")
                    ->set(0)
                    ->field("totalComment")
                    ->set(0)
                    ->field("alreadyNotified")
                    ->set(false)
                    ->field("criterias")
                    ->set([])
                    ->field("_id")
                    ->equals(new \MongoId($userId))
                    ->getQuery()
                    ->execute();
                return true;
            }
            foreach ($criteria as $crit) {
                $user->setCriterias($crit);
            }
            $em->persist($user);
            $em->flush();
            return true;
        }
        return false;
    }

    public function saveFeedBack($workId, $criteria, $msg, $fid)
    {
        $work = $this->workIterator($workId);
        $sw = $this->saveWorkFeedBack($work, $criteria, $fid, $msg);
        $uu = $this->updateUserFeedBack($work->getUserId(), $criteria);
        if ($sw == true && $uu == true) {
            return true;
        }
        return false;
    }


    /** Method to build a feedback array
     * @param FeedBackGrid $feedback
     * @return mixed
     */
    public function buildFeedback(FeedBackGrid $feedback)
    {
        $data['message'] = $feedback->getFeedbackmessage();
        $data['date'] = $feedback->getFeedbackdate();
        return $data;
    }
}