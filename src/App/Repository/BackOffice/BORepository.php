<?php

namespace App\Repository\BackOffice;

use App\Entities\Ban;
use App\Entities\ClassRoom;
use App\Entities\Comment;
use App\Entities\Comments;
use App\Entities\Lab;
use App\Entities\Chat;
use App\Entities\Level;
use App\Entities\LiveSession;
use App\Entities\Messages;
use App\Entities\Organization;
use App\Entities\Schools;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\EventRepository;
use App\Repository\Group\ClassRoomRepository;
use App\Repository\Group\GroupRepository;
use App\Repository\Repository;
use App\Services\Groups\GroupService;
use App\Services\UserFactory;
use phpDocumentor\Reflection\Types\Array_;
use MongoRegex;

class BORepository extends Repository
{

    public function getEventListBackOffice()
    {
        $repo = $this->queryBuilder->createQueryBuilder(LiveSession::class);
        $sessions = $repo->field('status')->notEqual('deleted')->getQuery()->execute();
        $nevents = array();
        $eventRepo = new EventRepository($this->getRepository());
        foreach ($sessions as $session) {
            $nevents[] = $eventRepo->buildEvents($session);
        }
        $listEvents['sessions'] = $nevents;
        return $listEvents;
    }

    public function updateSession($data)
    {
        $session = $this->getRepository()
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['sessionId'])
                ]
            );

        if (count($session) > 0) {
            if (isset($data['moderatorId'])) {
                $session->setModerator(new \MongoId($data['moderatorId']));
            }
            if ($data['approved']) {
                $session->setApproved(true);
                $session->setStatus('approved');
            } else {
                $session->setApproved(false);
                $session->setStatus('denied');
            }
            $session->setSpeaker($data['speakerId']);
            $session->setName($data['name']);
            $session->setType($data['type']);
            $session->setHighLight($data['highLight']);
            $session->setHighLightDate($this->getMongoDate());
            $session->setDescription($data['description']);
            $session->setDate($data['date']);
            $session->setStartTime($data['startTime']);
            return $session;
        }
        return false;
    }

    public function excludeSession($data)
    {
        $session = $this->getRepository()
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['sessionId'])
                ]
            );

        if (count($session) > 0) {
              $session->setStatus('deleted');
              return $session;
        }
        return false;
    }

    public function buildModeratorAttendingSessions($id)
    {
        $sessions = $this->getRepository()->getRepository(LiveSession::class)
            ->findBy(
                [
                    'moderator' => new \MongoId($id)
                ]
            );
        $sessionArray = array();
        foreach ($sessions as $session) {
            $sessionArray[] = [
                'sessionId' => $session->getId(),
                'name' => $session->getName(),
                'type' => $session->getType(),
                'date' => $session->getDate()->format('m-d-Y'),
                'startTime' => $session->getStartTime(),
            ];
        }
        return $sessionArray;
    }

    public function buildModerator(User $mod)
    {
        $userBuilded['firstName'] = $mod->getFirstName();
        $userBuilded['id'] = $mod->getId();
        $userBuilded['lastName'] = $mod->getLastName();
        $userBuilded['level'] = ucfirst($mod->getLevel()->getAlias());
        $userBuilded['avatar'] = $mod->getUserPhoto();
        $userBuilded['upload_at'] = $mod->getCreatedAt();
        $userBuilded['lastActivity'] = $mod->getLastActivity();
        $userBuilded['sessions'] = $this->buildModeratorAttendingSessions($mod->getId());
        $userBuilded['attending'] = count($userBuilded['sessions']);
        return $userBuilded;
    }

    public function getModerators()
    {
        $users = $this->getRepository()->getRepository(User::class)
            ->findBy(
                [
                    '$or' => array(
                        array('level.alias' => 'moderator'),
                        array('level.alias' => 'pro'),
                        array('level.alias' => 'admin')
                    ),
                ]
            );
        $mods = array();
        if (count($users) > 0) {
            foreach ($users as $mod) {
                $buildedMods = $this->buildModerator($mod);
                $mods['moderators'][] = $buildedMods;
                $mods['speakers'][] = $buildedMods;
            }
            return $mods;
        }
        return false;
    }

    public function buildOrganizationDetails(Organization $org)
    {
        $orgBuilded['id'] = $org->getId();
        $orgBuilded['firstName'] = $org->getFirstName();
        $orgBuilded['lastName'] = $org->getLastName();
        $orgBuilded['email'] = $org->getEmail();
        $orgBuilded['title'] = $org->getTitle();
        $orgBuilded['Name'] = $org->getOrganizationName();
        $orgBuilded['Schools'] = $org->getNumbersOfSchool();
        $orgBuilded['Classrooms'] = $org->getNumbersOfClassroom();
        $orgBuilded['phone'] = $org->getPhone();
        $orgBuilded['educationalStage'] = $org->getEducationalStage();
        $orgBuilded['duration'] = $org->getDuration();
        $orgBuilded['involvement'] = $org->getInvolvement();
        $orgBuilded['interestInvolvement'] = $org->getInterestInvolvement();
        foreach ($org->getInterestedCommunity() as $com) {
            $comunities[] = ucfirst($com);
        }
        $orgBuilded['Community'] =  $comunities;
        $orgBuilded['foundOut'] = $org->getFoundOut();
        $orgBuilded['comments'] = $org->getComments();
        $orgBuilded['status'] = $org->getStatus();
        $orgBuilded['Registered'] = $org->getCreatedAt();
        $orgBuilded['approval'] = $org->getApproval();
        return $orgBuilded;
    }

    public function buildOrganizationSchoolDetails(Schools $org)
    {
        return $this->buildSchool($org);
    }

    public function buildOrganization(Organization $org)
    {
        $comunities = array();
        $orgBuilded['id'] = $org->getId();
        $orgBuilded['Name'] = $org->getOrganizationName();
        $orgBuilded['Registered'] = $org->getCreatedAt();
        foreach ($org->getInterestedCommunity() as $com) {
            $comunities[] = ucfirst($com);
        }
        $orgBuilded['Community'] =  $comunities;
        $orgBuilded['Schools'] = $org->getNumbersOfSchool();
        $orgBuilded['Classrooms'] = $org->getNumbersOfClassroom();
        return $orgBuilded;
    }

    public function buildSchool(Schools $org)
    {
        $organization['status'] = ['_id' => $org->getOrganizationId()];
        $organization['orderBy'] = 'DESC';
        $organization['limit'] = 0;
        $organization['skip'] = 0;
        $orgObj = $this->getListOrganization($organization);
        $schoolBuilded['id'] = $org->getId();
        $schoolBuilded['Name'] = $org->getName();
        $schoolBuilded['Organization'] = $orgObj;
        $schoolBuilded['Classrooms'] = $org->getNumbersOfClassroom();
        $schoolBuilded['Students'] = $org->getNumbersOfStudent();
        return $schoolBuilded;
    }

    /**
     * Method to find all organizations in the database based on the 
     * filter given via the $data parameter.
     * @param $data array
     */
    public function getListOrganization(array $data)
    {
        $order = [
            'createdAt' => $data['orderBy']
        ];

        $where = [];

        if (is_array($data['status']) && 
            array_key_exists('approval', $data['status']) &&
            array_key_exists('deleted', $data['status'])) {
            $where['approval'] = $data['status']['approval'];
            $where['deleted'] = $data['status']['deleted'];
        }
        
        if (array_key_exists('search', $data) && $data['search']) {
            $regex = new MongoRegex('/.*' . $data['search']  . '.*/i');
            $where['organizationName'] = $regex;
        }

        $orgs = $this->getRepository()->getRepository(Organization::class)
            ->findBy(
                $where,
                $order
            );

        $organizations = [];
        
        if (count($orgs) > 0) {
            foreach ($orgs as $org) {
                $organizations[] = $this->buildOrganizationDetails($org);
            }

            return $organizations;
        }

        return false;
    }

    public function getListOrganizationSchools(array $data)
    {
        if (array_key_exists('search', $data) && $data['search']) {
            $data['param']['name'] = new MongoRegex('/.*' . $data['search'] . '.*/i');
        }
        
        $schools = $this->getRepository()->getRepository(Schools::class)
            ->findBy($data['param'], [
                'createdAt' => $data['orderBy']
            ]);

        $organizations = [];

        if (count($schools) > 0) {
            foreach ($schools as $school) {
                $organizations[] = $this->buildSchool($school);
            }
            return $organizations;
        }
        return false;
    }

    public function countClass($id)
    {
        $classes = $this->getRepository()->getRepository(User::class)
            ->findBy(
                []
            );
        $counter = 0;
        if (count($classes) > 0) {
            foreach ($classes as $class) {
                if (!empty($class->getClassRoom())) {
                    $ids = array_column($class->getClassRoom(), 'id');
                    if (in_array($id, $ids)) {
                        $counter += 1;
                    }
                }
            }
        }
        return $counter;
    }

    /** Method to return a classroom's information using the id
     * @param $data
     * @return mixed
     */
    public function getClassroomById($data)
    {
        $class = $this->getRepository()->getRepository(ClassRoom::class)
        ->findBy(
            [
                '_id' => $data['classroomId']
            ]
        );
        if (count($class) > 0) {
            foreach($class as $classInfo) {
                $classInformation = $this->buildClassroom($classInfo);
            }

            return $classInformation;
        }
        return false;

    }

    public function buildClassroom(ClassRoom $class)
    {
        $organization['param'] = ['_id' => $class->getSchoolId()];
        $organization['orderBy'] = 'DESC';
        $organization['limit'] = 0;
        $organization['skip'] = 0;
        $schoolObj = $this->getListOrganizationSchools($organization);
        $classBuilded['id'] = $class->getId();
        $classBuilded['Name'] = $class->getTitle();
        $classBuilded['code'] = $class->getId();
        $classBuilded['Educator'] = $class->getEmailEducator();
        $classBuilded['Pro'] = $class->getEmailPro();
        $classBuilded['Students'] = $this->countClass($class->getId());
        $classBuilded['School'] = $schoolObj;
        return $classBuilded;
    }

    public function getListOrganizationSchoolClassrooms($data)
    {
        if (array_key_exists('search', $data) && $data['search']) {
            $data['param']['title'] = new MongoRegex('/.*' . $data['search'] . '.*/i');
        }

        $class = $this->getRepository()->getRepository(ClassRoom::class)
            ->findBy(
                $data['param'],
                [
                    'createdAt' => $data['orderBy']
                ]
            );
        $organizations = array();
        if (count($class) > 0) {
            foreach ($class as $clas) {
                $organizations[] = $this->buildClassroom($clas);
            }
            return $organizations;
        }
        return false;
    }

    public function organizationApproveDeny($data)
    {
        $orgs = $this->getRepository()->getRepository(Organization::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['organizationId']),
                ]
            );
        if (count($orgs) > 0) {
            if ($data['approval'] === 'approved') {
                $status = true;
                $approval = 'approved';
                $this->setMessage([
                    'message' => 'organization was approved',
                    'email' => $orgs->getEmail(),
                    'name' => $orgs->getOrganizationName(),
                    'firstName' => $orgs->getFirstName(),
                    'lastName' => $orgs->getlastName(),
                    'community' => $orgs->getInterestedCommunity()
                ]);
            }
            if ($data['approval'] === 'denied') {
                $status = false;
                $approval = 'denied';
                $this->setMessage([
                    'message' => 'organization was denied',
                    'email' => $orgs->getEmail(),
                    'name' => $orgs->getOrganizationName()
                ]);
            }
            $orgs->setApproval($approval);
            $orgs->setStatus($status);
            $this->getRepository()->persist($orgs);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function organizationDetails($data)
    {
        $orgs = $this->getRepository()->getRepository(Organization::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data),
                ]
            );
        if (count($orgs) > 0) {
            return $this->buildOrganizationDetails($orgs);
        }
        return false;
    }

    public function organizationSchoolDetails($data)
    {
        $school = $this->getRepository()->getRepository(Schools::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data),
                ]
            );
        if (count($school) > 0) {
            return $this->buildOrganizationSchoolDetails($school);
        }
        return false;
    }

    public function checkProEducator($email)
    {
        $orgs = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    'email' => $email,
                ]
            );
        if (count($orgs) > 0) {
            $this->setMessage($orgs->getId());
            return $orgs->getId();
        }
        return false;
    }

    public function organizationSchoolNew($data)
    {
        $school = new Schools();
        $school->setOrganizationId($data['organizationId']);
        $school->setName($data['schoolName']);
        if (!empty($data['educatorEmail'])) {
            $school->setEmailEducator($data['educatorEmail']);
        }
        if (!empty($data['proEmail'])) {
            $school->setEmailPro($data['proEmail']);
        }
        if (empty($data['educatorEmail'])) {
            $data['educatorEmail'] = 'empty';
        }
        if (empty($data['proEmail'])) {
            $data['proEmail'] = 'empty';
        }
        $school->setNumbersOfClassroom($data['numberClassrooms']);
        $school->setNumbersOfStudent($data['numberStudents']);
        $this->getRepository()->persist($school);
        $this->getRepository()->flush();
        return true;
    }

    public function organizationClassroomNew($data)
    {
        $repo = $this->getRepository();
        $userRepo = $repo->createQueryBuilder(User::class);
        $classRoomRepository = new ClassRoomRepository($this->queryBuilder);

        $userBy = $this->findUserByEmail($userRepo, $data['educatorEmail']);

        if (count($userBy) > 0) {
                $data['userId'] = $userBy->getId();
        }

        $classRoom = $classRoomRepository->buildClass($data);

        $repo->persist($classRoom);
        $repo->flush();

        $userBy->setAssociation('classroom');
        $userBy->setClassCode($classRoom->getId());
        $userBy->setClassRoom([
            'id' => $classRoom->getId(),
            'uploaded_at' => $this->getMongoDate()
        ]);
        $repo->persist($userBy);
        $repo->flush();

        return $classRoom->getId();
    }


    /** Method to verify if a pro, edu and org exist
     * @param $data
     */
    public function verifyUsersExist($data)
    {
        $this->setMessage([
            'emailEdu' => $this->checkProEducator($data['educatorEmail']),
            'emailPro' => $this->checkProEducator($data['proEmail']),
            'orgDetails' => $this->organizationDetails($data['organizationId']),
        ]);
    }

    public function listLabsApproval()
    {
        $labs = $this->getRepository()->getRepository(Lab::class)
            ->findBy(
                [
                    'status' => 'pending'
                ]
            );
        $labss = array();
        if (count($labs) > 0) {
            $gRepo = new GroupRepository($this->getRepository());
            foreach ($labs as $lab) {
                $labss[] = $gRepo->buildLabOrClass($lab, 'lab');
            }
            $this->setMessage($labss);
            return true;
        }
        return false;
    }

    public function approveOrDenyLab($data)
    {
        $lab = $this->getRepository()->getRepository(Lab::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['labId'])
                ]
            );
        if (count($lab) > 0) {
            $lab->setStatus($data['approval']);
            $this->getRepository()->persist($lab);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function buildMessages($id, $read = false)
    {
        $msgs = $this->getRepository()->getRepository(Messages::class)
            ->findBy(
                [
                    'chatId' => new \MongoId($id)
                ]
            );
        $messagesArray = array();
        if (count($msgs) > 0) {
            foreach ($msgs as $msg) {
                if ($read === true) {
                    $msg->setRead(true);
                    $this->getRepository()->persist($msg);
                    $this->getRepository()->flush();
                }
                $messages['id'] = $msg->getId();
                $messages['chatId'] = $msg->getChatId();
                $messages['message'] = $msg->getMessage();
                $messages['createdAt'] = $msg->getCreatedAt()->format(\DateTime::ISO8601);
                $messages['user'] = $this->buildMain(['userId' => $msg->getOriginId()]);

                $messagesArray[] = $messages;
            }
        }
        return $messagesArray;
    }

    public function buildMain($userData)
    {
        $users = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($userData['userId'])
                ]
            );
        $user = array();
        if (count($users) > 0) {
            $user['id'] = $users->getId();
            $user['firstName'] = $users->getFirstName();
            $user['lastName'] = $users->getLastName();
            $user['level'] = ucfirst($users->getLevel()->getAlias());
            $user['avatar'] = $users->getUserPhoto();
            $user['upload_at'] = $users->getCreatedAt();
            $user['lastActivity'] = $users->getLastActivity();
        }
        return $user;
    }

    public function listMessagesUser($data)
    {
        $param =  [
            'userId' => new \MongoId($data['userId']),
            'reply' => false
        ];
        if ($data['type'] === 'admin') {
            $param =  [
                'reply' => false
            ];
        }
        $chats = $this->getRepository()->getRepository(Chat::class)
                ->findBy(
                    $param
                );

        $arrayMsg = array();
        if (count($chats) > 0) {
            foreach ($chats as $chat) {
                $userData = $this->fetchChatDetails(['originId' => $data['userId'], 'chatId' => $chat->getId()]);
                if ($userData === false) {
                    return false;
                }
                $messageArr = $this->buildMessages($chat->getId());
                $lasIdx = count($messageArr) - 1;
                $message['main'] = $this->buildMain($userData);
                $message['main']['message'] = $messageArr[$lasIdx]['message'];
                $message['main']['messageDate'] = $messageArr[$lasIdx]['createdAt'];
                $message['main']['read'] = $chat->getRead();
                $message['main']['chatId'] = $messageArr[$lasIdx]['chatId'];
                $arrayMsg[] = $message;
            }
            $this->setMessage($arrayMsg);
            return true;
        }
        return false;
    }

    public function readMessage($data)
    {
        $chat = $this->getRepository()->getRepository(Chat::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['chatId'])
                ]
            );
        $arrayMsg = array();
        if (count($chat) > 0) {
            $chat->setRead(true);
            $this->getRepository()->persist($chat);
            $this->getRepository()->flush();
                $userData = $this->fetchChatDetails(['originId' => $data['userId'], 'chatId' => $chat->getId()]);
            if ($userData === false) {
                return false;
            }
            $messageArr = $this->buildMessages($chat->getId(), true);
            $lasIdx = count($messageArr) - 1;
            $message['main'] = $this->buildMain($userData);
            $message['main']['message'] = $messageArr[$lasIdx]['message'];
            $message['main']['read'] = $chat->getRead();
            $message['main']['chatId'] = $messageArr[$lasIdx]['chatId'];
            $message['details'] = $messageArr;
            $arrayMsg[] = $message;
            $this->setMessage($arrayMsg);
            return true;
        }
        return false;
    }

    /**
     *  Method to list users on backoffice making a dinamic query
     * @param $data
     * @return bool
     */
    public function listUsers($data)
    {
        $userFactory = new UserFactory($this->getRepository());
        $pros = $userFactory->searchAllFilters($data);
        $proList = array();
        if (count($pros) > 0) {
            $repo = $this->getRepository();
            foreach ($pros as $pro) {
                $this->setRepository($this->getRepository()->createQueryBuilder(User::class));
                $user = $this->buildUser($pro->getId());
                $this->setRepository($repo);
                $user['classroom'] = '';
                if (count($pro->getClassRoom())>0) {
                    $orgId = array_reverse($pro->getClassRoom())[0]['id'];
                    if (\MongoId::isValid($orgId)) {
                        $user['organization'] = $this->getListOrganizationSchoolClassrooms([
                            'param' => ['_id' => new \MongoId($orgId)],
                            'skip' => 0,
                            'limit' => 0,
                            'orderBy' => 'DESC'
                        ]);
                    }
                }
                $proList['users'][] = $user;
            }

            if (isset($data['name'])) {
                $proList['total'] = count($proList['users']);
            } else {
                $proList['total'] = $this->totalUsers($data);
            }

            $this->setMessage($proList);
            return true;
        }
        return false;
    }

    public function totalUsers($data)
    {
        if ($data['type'] == 'all') {
            if ($data['status'] != 'all') {
                return $this->getRepository()
                    ->createQueryBuilder(User::class)
                    ->field('state')
                    ->equals($data['status'])
                    ->getQuery()
                    ->execute()
                    ->count();
            }
            return $this->getRepository()
                ->createQueryBuilder(User::class)
                ->getQuery()
                ->execute()
                ->count();
        } else {
            return $this->getRepository()->createQueryBuilder(User::class)
                ->field('level.alias')
                ->equals($data['type'])
                ->getQuery()->execute()->count();
        }
    }
    
    public function approveOrDenyPro($data)
    {
        $pro = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['proId'])
                ]
            );
        if (count($pro) > 0) {
            if ($data['approval'] === 'approved') {
                $pro->setStatus(true);
                $pro->setState($data['approval']);
            } else {
                $pro->setStatus(false);
                $pro->setState($data['approval']);
            }
            $this->getRepository()->persist($pro);
            $this->getRepository()->flush();
            $this->setMessage([
                'status' => $pro->getStatus(),
                'email' => $pro->getEmail()
            ]);
            return true;
        }
        return false;
    }

    public function proDetails($data)
    {
        $pro = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data),
                ]
            );
        if (count($pro) > 0) {
            $this->setRepository($this->getRepository()->createQueryBuilder(User::class));
            return $this->buildUser($data);
        }
        return false;
    }

    public function fetchChatId($id)
    {
        $chat = $this->getRepository()->getRepository(Chat::class)
            ->findOneBy(
                [
                    'userId' => new \MongoId($id),
                ]
            );
        if (count($chat) > 0) {
            return $chat->getId();
        }
        return 'new';
    }


    public function buildNewChat($data)
    {
        $chat = new Chat();
        $chat->setOriginId($data['originId']);
        if (!empty($data['userId'])) {
            $chat->setUserId($data['userId']);
        }
        $chat->setMessage('New User Chat');
        $chat->setType('messages');
        $chat->setReply(false);
        $chat->setRead(false);
        return $chat;
    }

    public function fetchChatDetails($data, $read = false)
    {
        $chat = $this->getRepository()->getRepository(Chat::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['chatId']),
                ]
            );
        if (count($chat) > 0) {
            if ($read === true) {
                $chat->setRead(false);
                $this->getRepository()->persist($chat);
                $this->getRepository()->flush();
            }
            $userId = $chat->getUserId();
            if ($userId == $data['originId']) {
                $userId = $chat->getOriginId();
            }
               return [
                   'originId' => $data['originId'],
                   'userId' => $userId,
               ];
        }
        return false;
    }

    public function saveNewMessage($data)
    {
        $userData = $this->fetchChatDetails($data, true);
        if ($userData === false) {
            return false;
        }
        $chat = new Messages();
        $chat->setUserId($userData['userId']);
        $chat->setOriginId($userData['originId']);
        $chat->setChatId($data['chatId']);
        $chat->setRead(false);
        $chat->setMessage($data['message']);
        return $chat;
    }

    public function deleteSchools($id)
    {
        $orgs = $this->getRepository()->getRepository(Schools::class)
            ->findBy(
                [
                    'organizationId' => new \MongoId($id),
                ]
            );
        if (count($orgs) >0) {
            foreach ($orgs as $org) {
                $org->setStatus('deleted');
                $this->deleteClasses($org->getId());
                $this->getRepository()->persist($org);
                $this->getRepository()->flush();
            }
            return true;
        }
        return false;
    }

    public function deleteClasses($id)
    {
        $orgs = $this->getRepository()->getRepository(ClassRoom::class)
            ->findBy(
                [
                    'schoolId' => new \MongoId($id),
                ]
            );
        if (count($orgs) >0) {
            foreach ($orgs as $org) {
                $org->setStatus('deleted');
                $this->getRepository()->persist($org);
                $this->getRepository()->flush();
            }
            return true;
        }
        return false;
    }

    public function deleteOrganization($data)
    {
        $org = $this->getRepository()->getRepository(Organization::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['orgId']),
                ]
            );
        if (count($org) >0) {
            $org->setDeleted(true);
            $this->deleteSchools($org->getId());
            $this->getRepository()->persist($org);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function deleteSchool($data)
    {
        $org = $this->getRepository()->getRepository(Schools::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['schoolId']),
                ]
            );
        if (count($org) >0) {
            $org->setStatus('deleted');
            $this->deleteClasses($org->getId());
            $this->getRepository()->persist($org);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function deleteClassroom($data)
    {
        $org = $this->getRepository()->getRepository(ClassRoom::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['classId']),
                ]
            );
        if (count($org) >0) {
            $org->setStatus('deleted');
            $this->getRepository()->persist($org);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function updateClassUserId($labArray)
    {
        $classroom = $this->getRepository()->getRepository(ClassRoom::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($labArray['classId']),
                ]
            );
        if (count($classroom) >0) {
            $classroom->setUserId($labArray['userId']);
            $this->getRepository()->persist($classroom);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function getOrgDetailsByClasscode($classId)
    {
        $class = $this->getRepository()->getRepository(ClassRoom::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($classId),
                ]
            );
        if (count($class) > 0) {
            $school = $this->getRepository()->getRepository(Schools::class)
                ->findOneBy(
                    [
                        '_id' => new \MongoId($class->getSchoolId()),
                    ]
                );
            if (count($school) > 0) {
                $org = $this->getRepository()->getRepository(Organization::class)
                    ->findOneBy(
                        [
                            '_id' => new \MongoId($school->getOrganizationId()),
                        ]
                    );
                return $org->getEducationalStage();
            }
        }
        return 'empty';
    }

    public function getOrgDetailsBySchool($classId)
    {
        $school = $this->getRepository()->getRepository(Schools::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($classId),
                ]
            );
        if (count($school) > 0) {
            $org = $this->getRepository()->getRepository(Organization::class)
                ->findOneBy(
                    [
                        '_id' => new \MongoId($school->getOrganizationId()),
                    ]
                );
            return $org->getEducationalStage();
        }
        return 'empty';
    }

    public function getEducators()
    {
        $educatorsArray = $this->getRepository()->getRepository(User::class)
            ->findBy(
                [
                    'level.alias' => 'educator',
                ]
            );
        $educators['individual'] = 0;
        $educators['College'] = 0;
        $educators['HighSchool'] = 0;
        $educators['MiddleSchool'] = 0;
        if (count($educators) > 0) {
            foreach ($educatorsArray as $edu) {
                if (($edu->getAssociation() == 'individual') || (empty($edu->getAssociation()))) {
                    $educators['individual'] += 1;
                } else {
                    $orgLevel = $this->getOrgDetailsByClasscode($edu->getClassCode());
                    if ($orgLevel == 'college') {
                        $educators['College'] += 1;
                    } elseif ($orgLevel == 'middle_school') {
                        $educators['MiddleSchool'] += 1;
                    } elseif ($orgLevel == 'high_school') {
                        $educators['HighSchool'] += 1;
                    } elseif ($orgLevel == 'empty') {
                        $educators['individual'] += 1;
                    } else {
                        $educators['College'] += 0;
                        $educators['HighSchool'] += 0;
                        $educators['MiddleSchool'] += 0;
                    }
                }
            }
        }
        return $educators;
    }

    public function getClassDash()
    {
        $classArray = $this->getRepository()->getRepository(ClassRoom::class)
            ->findBy(
                []
            );
        $classes['College'] = 0;
        $classes['HighSchool'] = 0;
        $classes['MiddleSchool'] = 0;
        $data['total'] = 0;
        if (count($classArray) > 0) {
            foreach ($classArray as $class) {
                $orgLevel = $this->getOrgDetailsByClasscode($class->getId());
                if ($orgLevel == 'college') {
                    $classes['College'] += 1;
                } elseif ($orgLevel == 'middle_school') {
                    $classes['MiddleSchool'] += 1;
                } elseif ($orgLevel == 'high_school') {
                    $classes['HighSchool'] += 1;
                } else {
                    $classes['College'] += 0;
                    $classes['HighSchool'] += 0;
                    $classes['MiddleSchool'] += 0;
                }
            }
            $data[] = $classes;
            $data['total'] += count($classArray);
        }
        return $data;
    }

    public function getSchoolsDash()
    {
        $schoolArray = $this->getRepository()->getRepository(Schools::class)
            ->findBy(
                []
            );
        $schools['College'] = 0;
        $schools['HighSchool'] = 0;
        $schools['MiddleSchool'] = 0;
        $data['total'] = 0;
        if (count($schoolArray) > 0) {
            foreach ($schoolArray as $school) {
                $orgLevel = $this->organizationDetails($school->getOrganizationId())['educationalStage'];
                if ($orgLevel == 'college') {
                    $schools['College'] += 1;
                } elseif ($orgLevel == 'middle_school') {
                    $schools['MiddleSchool'] += 1;
                } elseif ($orgLevel == 'high_school') {
                    $schools['HighSchool'] += 1;
                } else {
                    $schools['College'] += 0;
                    $schools['HighSchool'] += 0;
                    $schools['MiddleSchool'] += 0;
                }
            }
            $data[] = $schools;
            $data['total'] += count($schoolArray);
        }
        return $data;
    }

    public function getOrgsDash()
    {
        $orgArray = $this->getRepository()->getRepository(Organization::class)
            ->findBy(
                []
            );
        $organizations['College'] = 0;
        $organizations['HighSchool'] = 0;
        $organizations['MiddleSchool'] = 0;
        $data['total'] = 0;
        if (count($orgArray) > 0) {
            foreach ($orgArray as $org) {
                $orgLevel = $org->getEducationalStage();
                if ($orgLevel == 'college') {
                    $organizations['College'] += 1;
                } elseif ($orgLevel == 'middle_school') {
                    $organizations['MiddleSchool'] += 1;
                } elseif ($orgLevel == 'high_school') {
                    $organizations['HighSchool'] += 1;
                } else {
                    $organizations['College'] += 0;
                    $organizations['HighSchool'] += 0;
                    $organizations['MiddleSchool'] += 0;
                }
            }
            $data[] = $organizations;
            $data['total'] += count($orgArray);
        }
        return $data;
    }

    public function getWorksDash($type)
    {
        $workArray = $this->getRepository()->getRepository(Work::class)
            ->findBy(
                [
                    'group' => $type
                ]
            );
        $commentArray = $this->getRepository()->getRepository(Comments::class)
            ->findBy(
                [
                    'type' => $type
                ]
            );
        $data['reviewed'] = 0;
        $data['likes'] = 0;
        $data['views'] = 0;
        $data['comments'] = 0;
        $data['total'] = 0;
        $counterReviewed = 0;
        $counterViews = 0;
        $counterLikes =0;
        if (count($workArray) > 0) {
            foreach ($workArray as $work) {
                if ($work->getReviewed() == true) {
                    $counterReviewed += 1;
                }
                $counterViews += (int)$work->getViews();
                $counterLikes += count($work->getLikes());
                $data['reviewed'] = $counterReviewed;
                $data['likes'] = $counterLikes;
                $data['views'] += $counterViews;
            }
            $data['comments'] = count($commentArray);
            $data['total'] += count($workArray);
        }
        return $data;
    }

    public function getMainGraph($data)
    {
        $dt = new \DateTime(date('Y-m-1', strtotime($data['startDate'])), new \DateTimeZone('UTC'));

        $fdt = new \DateTime(date('Y-m-t', strtotime($data['endDate'])), new \DateTimeZone('UTC'));

        $ts = $dt->getTimestamp();
        $firstDayOfMonth = new \MongoDate($ts);
        $ts2 = $fdt->getTimestamp();

        $lastDayOfMonth = new \MongoDate($ts2);

        $users = $this->getRepository()->createQueryBuilder(User::class)
            ->field('lastActivity')
            ->range($firstDayOfMonth, $lastDayOfMonth)
            ->group(array('lastActivity' => 1,'level.alias' => 1), array(
                'count' => 0,
                'date' => 'date'
            ))
            ->reduce('function (obj, prev) {
            prev.count++;
            prev.date = obj.lastActivity;
            }')
            ->getQuery()
            ->execute();
        $months = [
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0
        ];
        $studentsProfiles = ['StudentLevel1', 'StudentLevel3', 'StudentLevel2'];
        $userCount = $users->getCommandResult()['retval'];
        @$students['total'] = 0;
        foreach ($userCount as $user) {
            if (!empty($user['level.alias'])) {
                $index = str_replace(' ', '', $user['level.alias']);
                if (in_array($index, $studentsProfiles)) {
                    @$students['students'] = $months;
                } else {
                    @$students[$index] = $months;
                }
            }
        }
        foreach ($userCount as $user) {
            if (!empty($user['level.alias'])) {
                $index = str_replace(' ', '', $user['level.alias']);
                if (in_array($index, $studentsProfiles)) {
                    $students['students'][date('M', $user['date']->sec)] += 1;
                } else {
                    $students[$index][date('M', $user['date']->sec)] += 1;
                }

            }
        }
        @$students['total'] = count($userCount);
        return @$students;
    }

    public function dashboardData($dado)
    {
        /*mount donut graphs*/
        $users = $this->getRepository()->createQueryBuilder(User::class)
            ->group(array('level.alias' => 1), array(
                'count' => 0,
                'classCode' => 'classCode',
                'association' => 'association'
            ))
            ->reduce('function (obj, prev) {
            prev.count++;
            prev.classCode = obj.classCode;
            prev.association = obj.association;
            }')
            ->getQuery()
            ->execute();
        $userCounts = $users->getCommandResult()['retval'];
        $students['total'] = 0;
        $educators['total'] = 0;
        $students['StudentLevel1'] = [
            'level' => 'Student Level 1',
            'count' => 0
        ];
        $students['StudentLevel2'] = [
            'level' => 'Student Level 2',
            'count' => 0
        ];
        $students['StudentLevel3'] = [
            'level' => 'Student Level 3',
            'count' => 0
        ];
        foreach ($userCounts as $user) {
            if (!empty($user['level.alias'])) {
                $index = str_replace(' ', '', $user['level.alias']);
                $students[$index] = [
                    'level' => $user['level.alias'],
                    'count' => $user['count']
                ];
                $students['total'] += $user['count'];
                if (strtolower($user['level.alias']) == 'educator') {
                    $educators[] = $this->getEducators();
                    $educators['total'] += $user['count'];
                }
            }
        }
        $data['main'] = $this->getMainGraph($dado);
        $data['users'] = $students;
        $data['educators'] = $educators;
        $data['schools'] = $this->getSchoolsDash();
        $data['classrooms'] = $this->getClassDash();
        $data['organization'] = $this->getOrgsDash();
        $data['works'] = $this->getWorksDash('work');
        $data['proTips'] = $this->getWorksDash('proTip');
        $data['resource'] = $this->getWorksDash('resource');
        $this->setMessage($data);
        return true;
    }

    public function updateOrganization($data)
    {
        $org = $this->getRepository()->getRepository(Organization::class)
            ->findOneBy(
                [
                    '_id' => $data['orgId']
                ]
            );
        if (count($org)>0) {
            $org->setFirstName($data['firstName']);
            $org->setLastName($data['lastName']);
            $org->setEmail($data['email']);
            $org->setTitle($data['title']);
            $org->setOrganizationName($data['Name']);
            $org->setPhone($data['phone']);
            $org->setEducationalStage($data['educationalStage']);
            $org->setDuration($data['duration']);
            $org->setInterestedCommunity($data['Community']);
            $this->getRepository()->persist($org);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function updateSchool($data)
    {
        $school = $this->getRepository()->getRepository(Schools::class)
            ->findOneBy(
                [
                    '_id' => $data['schoolId']
                ]
            );
        if (count($school)>0) {
            $school->setName($data['Name']);
            $this->getRepository()->persist($school);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function updateClassroom($data)
    {

        $repo = $this->getRepository();
        $userRepo = $repo->createQueryBuilder(User::class);

        $userBy = $this->findUserByEmail($userRepo, $data['educatorEmail']);

        if (count($userBy) > 0) {
            $data['userId'] = $userBy->getId();
        }

        $classRoom = $this->getRepository()->getRepository(ClassRoom::class)
            ->findOneBy(
                [
                    '_id' => $data['classId']
                ]
            );
        if (count($classRoom)>0) {
            $classRoom->setTitle($data['Name']);
            $classRoom->setEmailEducator($data['educatorEmail']);
            $classRoom->setUserId($data['userId']);
            $this->getRepository()->persist($classRoom);
            $this->getRepository()->flush();
        }

        $userBy->setAssociation('classroom');
        $userBy->setClassCode($classRoom->getId());
        $userBy->setClassRoom([
            'id' => $classRoom->getId(),
            'uploaded_at' => $this->getMongoDate()
        ]);
        $repo->persist($userBy);
        $repo->flush();

        return true;
    }

    public function userDetails($data)
    {
        $user = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => $data['userId']
                ]
            );
        if (count($user)>0) {
            $arrayUser = $this->buildUserByObject($user);
            if ($user->getLevel()->getNumber() <= 3) {
                $groupService = new GroupService();
                $data['skip'] = 0;
                $data['limit'] = 0;
                $groupService->getLabAndClass($data, $this->getRepository());
                $arrayUser['groups'] = $groupService->getMessage();
            }
            $this->setMessage($arrayUser);
            $this->setStatus(200);
            return true;
        }
        return false;
    }

    public function updateUser($data)
    {
        $user = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => $data['userId']
                ]
            );
        if (count($user)>0) {
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setUserPhoto($data['photo']);
            $user->setEmail($data['email']);
            if ($user->getLevel()->getAlias() !== $data['level']) {
                $levelArray = array_reverse(explode(' ', $data['level']));
                $level = new Level();
                $level->setAlias($data['level']);
                $level->setNumber((int)$levelArray[0]);
                $level->setDate($this->getMongoDate());
                $user->setLevel($level);
            }
            $this->getRepository()->persist($user);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function filterOrganizationByName($data)
    {
        $orgs = $this->getRepository()->getRepository(Organization::class)
            ->findBy(
                [
                    'organizationName' => new MongoRegex('/.*'.$data['name'].'.*/i')
                ]
            );
        $orgArray = array();
        if (count($orgs)>0) {
            foreach ($orgs as $org) {
                $orgArray[] = $this->buildOrganizationDetails($org);
            }
            $this->setMessage($orgArray);
            return true;
        }
        return false;
    }

    public function setHighLight($data)
    {
        if ($data['type'] == 'work') {
            $highlight = $this->getRepository()->getRepository(Work::class);
        } else {
            $highlight = $this->getRepository()->getRepository(LiveSession::class);
        }

        $object = $highlight->findOneBy(
            [
                '_id' => new \MongoId($data['id'])
            ]
        );
        if (count($object)>0) {
            if ($object->getHighLight() == true && !empty($object->getHighLight())) {
                $object->setHighLight(false);
                $this->setMessage('Highlight Removed From Object');
            } else {
                $object->setHighLight(true);
                $object->setHighlightDate($this->getMongoDate());
                $this->setMessage('This Object Was Highlighted');
            }
            $this->getRepository()->persist($object);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    /**
     * Return a list of banned users and theirs session
     * @param array $data   This array will have all parameters that will be add to the query. Skip, name, limit, page,
     *                      type and orderby.
     * @return bool
     */
    public function bannedUsers(array $data)
    {
        $users = [];
        if ($data['name'] != 'all') {
            $this->listUsers($data);
            $users = $this->getMessage();
            $users = $users['users'];
        }
        
        $bans = $this->getRepository()->getRepository(Ban::class)
            ->findBy(
                []
            );
        $banss = [];
        if (count($bans) > 0) {
            $eventRepo = new EventRepository($this->getRepository());
            $this->setRepository($this->getRepository()->createQueryBuilder(User::class));
            foreach ($bans as $ban) {
                $obj['user'] = $this->buildUser($ban->getUserId())['user'];
                $obj['session'] = $eventRepo->getEventDetails($ban->getSessionId(), $ban->getUserId());
                $obj['banDate'] = $ban->getCreatedAt();
                $banss[] = $obj;
            }
            if (count($users) > 0) {
                $banss = $this->searchBannedUsers($users, $banss);
                if (is_null($banss)) {
                    return false;
                }
            }
            $this->setMessage($banss);
            return true;
        }
        return false;
    }

    /**
     * Compare an array of users sorted by name and an array of banned users to return the banned user with that name
     * @param array $users          An array of users and sessions objects built as an array.
     * @param array $bannedUsers    An array of users' object built as an array.
     * @return array
     */
    public function searchBannedUsers(array $users, array $bannedUsers)
    {
        foreach ($users as $user) {
            foreach ($bannedUsers as $bannedUser) {
                if ($bannedUser['user']['id'] == $user['user']['id']) {
                    $bannedUserFound[] = $bannedUser;
                }
            }
        }

        return $bannedUserFound;
    }

    public function unBanUser($data)
    {
        $bans = $this->getRepository()->getRepository(Ban::class)
            ->findBy(
                [
                    'userId' => new \MongoId($data['userId']),
                    'sessionId' => new \MongoId($data['sessionId'])
                ]
            );
        if (count($bans) > 0) {
            foreach ($bans as $ban) {
                $this->getRepository()->createQueryBuilder(Ban::class)
                    ->remove()
                    ->field("_id")
                    ->equals(new \mongoId($ban->getId()))
                    ->getQuery()
                    ->execute();
            }
            return true;
        }
        return false;
    }
}
