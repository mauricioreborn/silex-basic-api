<?php

namespace App\Repository;

use App\Entities\Ban;
use App\Entities\DrawoverOperation;
use App\Entities\Chat;
use App\Entities\LiveSession;
use App\Entities\User;
use App\Services\Work\WorkService;

class LiveSessionRepository extends Repository
{
    public function getSessions($type = 'public')
    {
        $session = $this->getRepository()->getQuery()->execute();
        if (count($session) > 0) {
            return $this->buildSession($session);
        }
    }

    public function buildSession($session)
    {
        $sessions = '';
        foreach ($session as $liveSession) {
            $data['title'] = $liveSession->getName();
            $data['type'] = $liveSession->getType();
            $data['description'] = $liveSession->getDescription();
            $data['date'] = $liveSession->getDate();
            $data['startTime'] = $liveSession->getStartTime();
            $data['endTime'] = $liveSession->getEndTime();
            $data['url'] = $liveSession->getLink();
            $data['coverImage'] = $liveSession->getThumbnail();
            $sessions[] = $data;
        }
        return $sessions;
    }

    public function getSession($data)
    {
        $sessionRepo  = $this->getRepository()->getRepository(LiveSession::Class);
        return $sessionRepo->findOneBy(
            [
                '_id' => new \MongoId($data['sessionId'])
            ]
        );
    }

    /**
     * This method update the cover of a liveSession.
     * @param array $sessionInformation An array with information to update the
     *                                  liveSession cover like, liveSession
     *                                  identifier in database and the url of
     *                                  liveSession cover.
     * @return bool                     A boolean that informs if the cover was
     *                                  changed (true) or not (false).
     */
    public function uploadCover(array $sessionInformation)
    {
        $liveSession = $this->getSession($sessionInformation);

        if (count($liveSession) > 0) {
            if ($liveSession->getModerator() == $sessionInformation['userId']
                || $liveSession->getSpeaker() == $sessionInformation['userId']
                || strtolower($sessionInformation['level']) == 'admin'
            ) {
                $liveSession->setThumbnail($sessionInformation['cover']);
                $this->getRepository()->persist($liveSession);
                $this->getRepository()->flush();

                return true;
            }
        }

        return false;
    }

    public function generateSessionToken($data)
    {
        $sessionRepo  = $this->getRepository()->getRepository(LiveSession::Class);
        $session = $sessionRepo->findOneBy(
            [
                '_id' => new \MongoId($data['sessionId'])
            ]
        );
        if (count($session) === 0) {
            return 0;
        }

        if ($session->getSpeaker() == $data['userId']) {
            $this->setMessage(array('role' => 'speaker', 'session' => $session));
            return true;
        }

        if ($session->getModerator() == $data['userId']) {
            $this->setMessage(array('role' => 'moderator', 'session' => $session));
            return true;
        }

        if (!empty($session->getUsers())) {
            if (in_array($data['userId'], $session->getUsers())) {
                $this->setMessage(array('role' => 'viewer', 'session' => $session));
                return true;
            }
        }

        if ($session->getType() === 'public') {
            $this->setMessage(array('role' => 'viewer', 'session' => $session));
            return true;
        }

        return false;
    }

    /**
     * @param array $data
     * @return LiveSession
     */
    public function createSession(array $data)
    {
        $liveSession = new LiveSession();
        $liveSession->setCreatedAt($this->getMongoDate());
        $liveSession->setHighLightDate($this->getMongoDate());
        $liveSession->setSpeaker(new \MongoId($data['speaker']));
        $liveSession->setDescription($data['description']);
        $liveSession->setUtcOffset($data['utcOffset']);
        $liveSession->setName($data['title']);
        $liveSession->setDate($data['date']);
        $liveSession->setApproved(false);
        $liveSession->setStatus('pending');
        if (!empty($data['participants'])) {
            foreach ($data['participants'] as $user) {
                $liveSession->setUsers($user);
            }
        }
        $liveSession->setDeadLineStatus($data['deadlineStatus']);
        if (!empty($data['deadlineDate'])) {
            $liveSession->setDeadLineDate($data['deadlineDate']);
            $liveSession->setDeadLineTime($data['deadlineTime']);
        }
        $liveSession->setStartTime($data['startTime']);
        $liveSession->setEndTime($data['endTime']);
        $liveSession->setThumbnail($data['thumbnail']);
        $liveSession->setType($data['type']);
        if (!empty($data['moderator'])) {
            $liveSession->setModerator($data['moderator']);
        }
        if (!empty($data['originalSessionId'])) {
            $liveSession->setOriginalSessionId($data['originalSessionId']);
        }
        return $liveSession;
    }

    public function saveChat($data)
    {
        $chat = new Chat();
        $chat->setOriginId($data['originId']);
        $chat->setUserId($data['userId']);
        $chat->setMessage($data['message']);
        if (!empty($data['flagged'])) {
            $chat->setFlag($data['flagged']);
        }
        if (empty($data['flagged'])) {
            $chat->setFlag(false);
        }
        $chat->setType('session');
        return $chat;
    }

    public function flagChat($data)
    {
      $chatRepo = $this->getRepository()->getRepository(Chat::Class);
      $msg = $chatRepo->findOneBy(
          [
              'originId' => new \MongoId($data['originId']),
              'id' => new \MongoId($data['messageId'])
          ]
      );
      if ($msg) {
        $msg->setFlag(true);
      }
      return $msg;
    }

    public function buildGetChat($msgs)
    {
        $this->setRepository($this->getRepository()->createQueryBuilder(User::class));
        $history = array();
        foreach ($msgs as $msg) {
            if ($msg->getFlag()) continue;
            $user = $this->buildUser($msg->getUserId())['user'];
            $data['id'] = $msg->getId();
            $data['originId'] = $msg->getOriginId();
            $data['message'] = $msg->getMessage();
            $data['user'] = [
                'firstName' => $user['firstName'],
                'lastName' => $user['lastName'],
                'id' => $user['id']
            ];
            $data['createdAt'] = $msg->getCreatedAt();
            $history[] = $data;
        }
        return $history;
    }

    public function getChatHistory($data)
    {
        $chatRepo = $this->getRepository()->getRepository(Chat::Class);
        $chats = $chatRepo->findBy(
            [
                'originId' => new \MongoId($data['sessionId']),
            ],
            [
                'createdAt' => 'ASC'
            ],
            $data['limit'],
            $data['skip']
        );

        if (count($chats) > 0) {
            return ['history' => $this->buildGetChat($chats)];
        }
        return ['history' => 'No Chat History'];
    }

    public function buildUserByObject(User $user)
    {
        $userBuilded['firstName'] = $user->getFirstName();
        $userBuilded['id'] = $user->getId();
        $userBuilded['lastName'] = $user->getLastName();
        $userBuilded['level'] = ucfirst($user->getLevel()->getAlias());
        $userBuilded['avatar'] = $user->getUserPhoto();
        $userBuilded['upload_at'] = $user->getCreatedAt();
        $userBuilded['lastActivity'] = $user->getLastActivity();
        $userBuild['user'] = $userBuilded;
        return $userBuild;
    }


    public function searchUserSendMessage($data)
    {
        $userRepo = $this->getRepository()->getRepository(User::Class);
        $chatRepo = $this->getRepository()->getRepository(Chat::Class);
        $users = $userRepo->findBy(
            [
                'firstName' => new \MongoRegex('/.*'.$data['firstName'].'.*/i'),
                'lastName' => new \MongoRegex('/.*'.$data['lastName'].'.*/i')
            ],
            [
                'createdAt' => 'ASC'
            ],
            $data['limit'],
            $data['skip']
        );
        $resultUsers = array();
        if (count($users) > 0) {
            foreach ($users as $user) {
                $chat = $chatRepo->findOneBy(
                    [
                        'userId' => new \MongoId($user->getId()),
                    ]
                );
                $array['user'] = $this->buildUserByObject($user)['user'];
                $array['chatId'] = '';
                if (count($chat) > 0) {
                    $array['chatId'] = $chat->getId();
                }
                $resultUsers[] = $array;
            }
            $this->setMessage($resultUsers);
            return true;
        }
        return false;
    }

    public function saveDrawoverOperation($data)
    {
        $op = new DrawoverOperation();
        $op->setOriginId($data['originId']);
        $op->setTag($data['tag']);
        $op->setOperation($data['operation']);
        return $op;
    }

    public function buildGetDrawover($ops)
    {
        $this->setRepository($this->getRepository()->createQueryBuilder(User::class));
        $history = array();
        foreach ($ops as $op) {
            $data['originId'] = $op->getOriginId();
            $data['tag'] = $op->getTag();
            $data['operation'] = $op->getOperation();
            $data['createdAt'] = $op->getCreatedAt();
            $history[] = $data;
        }
        return $history;
    }

    public function getDrawoverHistory($data)
    {
        $drawRepo = $this->getRepository()->getRepository(DrawoverOperation::Class);
        $ops = $drawRepo->findBy(
            [
                'originId' => new \MongoId($data['sessionId']),
            ],
            [
                'createdAt' => 'ASC'
            ],
            $data['limit'],
            $data['skip']
        );

        if (count($ops) > 0) {
            return ['history' => $this->buildGetDrawover($ops)];
        }
        return ['history' => 'No Drawover History'];
    }

    public function workList($data)
    {
        $lsRepo = $this->getRepository()->getRepository(LiveSession::class);
        $liveSession = $lsRepo->findOneBy(
            [
                '_id' => new \MongoId($data['sessionId'])
            ]
        );
        $list = array();
        if (count($liveSession) > 0) {
            if ($liveSession->getWorkQueue()) {
                $workService = new WorkService();
                foreach ($liveSession->getWorkQueue() as $val) {
                    $workResponce = $workService->getWorkById($this->getRepository(), $val);
                    if ($workResponce != false) {
                        $list[] = $workResponce;
                    }
                }
                $this->setMessage($list);
                $this->setStatus(200);
                return true;
            }
        }
        return false;
    }

    public function checkBan($data)
    {
        $lsRepo = $this->getRepository()->getRepository(Ban::class);
        $liveSession = $lsRepo->findBy(
            [
                'userId' => new \MongoId($data['userId']),
                'sessionId' => new \MongoId($data['sessionId'])
            ]
        );
        if (count($liveSession) > 0) {
            return true;
        }
        return false;
    }

    public function banUser($data)
    {
        $ban = new Ban();
        $ban->setUserId(new \MongoId($data['userId']));
        $ban->setSessionId(new \MongoId($data['sessionId']));
        $this->getRepository()->persist($ban);
        $this->getRepository()->flush();
        return true;
    }

    public function terminateSession($data)
    {
        $lsRepo = $this->getRepository()->getRepository(LiveSession::class);
        $liveSession = $lsRepo->findOneBy(
            [
                '_id' => new \MongoId($data['sessionId'])
            ]
        );
        if (count($liveSession) > 0) {
            $liveSession->setStatus('ended');
            $this->getRepository()->persist($liveSession);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }
}
