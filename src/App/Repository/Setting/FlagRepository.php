<?php

/**
 * Entity Flag
 *
 * PHP version 7
 *
 * @category Service
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Repository\Setting;

use App\Entities\Comments;
use App\Entities\Flag;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\Repository;
use \MongoId;

/**
 * Entity Flag
 *
 * PHP version 7
 *
 * @category Service
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
class FlagRepository extends Repository
{

    /**
     * Method to create a new Flag
     *
     * @param array $data All content to build a flag
     *
     * @return Flag Object with all fields filled
     */
    public function flagBuild(array $data)
    {
        $flag = new Flag();
        $flag->setCreatedAt($this->getMongoDate());
        $flag->setProductId($data['productId']);
        $flag->setType($data['type']);
        $flag->setUserId($data['userId']);
        $flag->setStatus(true);
        return $flag;
    }

    /**
     * Method to get all flags sorted by created_at field on decreasing order
     *
     * @return array|bool With flag contents
     */
    public function getFlags()
    {
        $entity = $this->queryBuilder;
        $flagEntity = $this->getRepository()->createQueryBuilder(Flag::class);

        $flags = $flagEntity
            ->field('status')
            ->equals(true)
            ->sort('created_at', 'desc')
            ->getQuery()
            ->execute();

        $flagIds = [];
        $newFlags = [];

        foreach ($flags as $flagContent) {
            if (in_array($flagContent->getProductId(), $flagIds)) {
                continue;
            }
            $flagIds[] = $flagContent->getProductId();
            $newFlags[] = $flagContent;
        }

        $arrayFlag  = array();
        if (count($newFlags) > 0) {
            foreach ($newFlags as $flag) {
                $data = array();
                $data['id'] = $flag->getId();
                $data['type'] = $flag->getType();
                $data['productId'] = $flag->getProductId();
                $data['createdAt'] = $flag->getCreatedAt();

                if ($data['type'] == 'work') {
                    $this->queryBuilder = $entity->createQueryBuilder(
                        Work::class
                    );
                    $work = $this->findAnUser($data['productId']);

                    if (count($work) > 0) {
                        if ($work->getStatus() == 'enabled') {
                            $data['work'] = $this->getWorkData($work);
                            $this->queryBuilder = $entity->createQueryBuilder(
                                User::class
                            );
                            $user = $this->findAnUser($data['work']['userId']);

                            if (count($user) > 0) {
                                $data['user'] = $this->getUserData($user);
                            }
                            $arrayFlag[] = $data;
                        }
                    }
                } else if ($data['type'] == 'comment') {
                    $this->queryBuilder = $entity->createQueryBuilder(
                        Comments::class
                    );
                    $comment = $this->findAnUser($data['productId']);
                    if (count($comment) > 0) {

                        $data['comment'] = $this->getCommentData($comment);
                        $commentUser = $data['comment']['userId'];
                        $this->queryBuilder = $entity->createQueryBuilder(
                            User::class
                        );
                        $user = $this->findAnUser($commentUser);

                        if (count($user) > 0) {
                            $data['user'] = $this->getUserData($user);
                        }
                        $arrayFlag[] = $data;
                    }
                }

            }

            return $arrayFlag;
        }
        return false;
    }


    /**
     * Method to get work information
     *
     * @param Work $work Work Entity object to build an array
     *
     * @return array An array with work information
     */
    public function getWorkData(Work $work)
    {
        $data['workId'] = $work->getId();
        $data['workTitle'] = $work->getTitle();
        $data['userId'] = $work->getUserId();
        $data['totalFlag'] = count($work->getFlag());
        return $data;
    }

    /**
     * Method to build a comment array
     *
     * @param Comments $comment Comment Entity object to build an array
     *
     * @return array   $data    An array with comment information
     */
    public function getCommentData(Comments $comment)
    {
        $data['commentId'] = $comment->getId();
        $data['content'] = $comment->getComment();
        $data['userId'] = $comment->getUserId();
        $data['workId'] = $comment->getProductId();
        $data['totalFlag'] = count($comment->getFlag());
        return $data;
    }

    /**
     * Method to build an user array
     *
     * @param User $user User entity object to get information
     *
     * @return array An array with user information
     */
    public function getUserData(User $user)
    {
        $data['userId'] = $user->getId();
        $data['firstName'] = $user->getFirstName();
        $data['lastName'] = $user->getLastName();
        $data['communities'] = $user->getCommunity();
        return $data;
    }

    /**
     * Method to set a flag as false.
     *
     * @param string $productId product (work or comment) database identifier
     *
     * @return null
     */
    public function disableAllFlags(string $productId)
    {
        return $this->getRepository()
            ->updateMany()
            ->field('status')
            ->set(false)
            ->field('create_at')
            ->set($this->getMongoDate())
            ->field('productId')
            ->equals(new MongoId($productId))
            ->getQuery()
            ->execute();
    }

    /**
     * Method to set a flag as false.
     *
     * @param array $flagContent array with information to disable a flag
     *                           how product id and user identify on database
     *
     * @return null
     */
    public function disableFlag(array $flagContent)
    {
        $this->getRepository()
            ->updateMany()
            ->field('status')
            ->set(false)
            ->field('productId')
            ->equals(new MongoId($flagContent['productId']))
            ->field('userId')
            ->equals(new MongoId($flagContent['userId']))
            ->field('create_at')
            ->set($this->getMongoDate())
            ->getQuery()
            ->execute();
    }
}