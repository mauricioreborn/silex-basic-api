<?php

namespace App\Repository\Setting;

use App\Entities\User;
use App\Repository\Repository;

class SettingRepository extends Repository
{


    /**
     * Method to change user account information on database
     * @param $data
     * @return mixed
     */
    public function changeAccount($data)
    {
        return $this->getRepository()
            ->updateOne()
            ->field('firstName')->set($data['firstName'])
            ->field('lastName')->set($data['lastName'])
            ->field('userPhoto')->set($data['userPhoto'])
            ->field('bio')->set($data['bio'])
            ->field('gender')->set($data['gender'])
            ->field('birthDate')->set($this->getMongoDate($data['birthDate']))
            ->field('phone')->set($data['phone'])
            ->field('community')->set($data['community'])
            ->field('_id')->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->execute();
    }

    /**
     * Method to verify if is the current password
     * @param $data
     * @return mixed
     */
    public function verifyUserPassword($data)
    {
        $user = $this->getRepository()->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($data['userId']),
                    'password' => md5($data['password'])
                ]
            );
        if (count($user) > 0) {
            return true;
        }
        return false;
    }

    /** Method to change the password
     * @param $data[new password, userId]
     * @return mixed
     */
    public function changePassword($data)
    {
         $this->getRepository()
            ->updateOne()
            ->field("password")->set(md5($data['newPassword']))
            ->field("_id")
             ->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to get User datas to settings
     * @param $userId
     * @return bool
     */
    public function getUserInformation($userId)
    {
        $myUser = $this->getRepository()
        ->field("_id")
        ->equals(new \MongoId($userId))
        ->getQuery()
        ->getSingleResult();
        $info = array();
        if (count($myUser)>0) {
            $user['email'] = $myUser->getEmail();
            $user['firstName'] = $myUser->getFirstName();
            $user['lastName'] = $myUser->getLastName();
            $user['gender'] = $myUser->getGender();
            $user['birthDate'] = $myUser->getBirthDate();
            $user['phone'] = $myUser->getPhone();
            $user['bio'] = $myUser->getBio();
            $user['photo'] = $myUser->getUserPhoto();
            $user['community'] = $myUser->getCommunity();
            $info['account'] = $user;
            $arrayNotifications = $myUser->getNotificationSettings();
            for ($i=0; $i<=5; $i++) {
                $info['notification'][] = $arrayNotifications[$i];
            }
            $info['updates'] = $arrayNotifications[6];
            $this->setMessage($info);
            return true;
        }
        return false;
    }

    public function changeNotification($data)
    {
          $this->getRepository()
            ->updateOne()
            ->field('notificationSettings')
            ->set($data['notification']['notification'])
            ->field("_id")
            ->equals($data['userId'])
            ->getQuery()
            ->execute();

        return true;
    }
}