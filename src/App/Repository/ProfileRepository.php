<?php
namespace App\Repository;

use App\Entities\ClassRoom;
use App\Entities\Comments;
use App\Entities\Follow;
use App\Entities\Following;
use App\Entities\User;
use App\Entities\Work;
use App\Services\Work\RefineByFactory;
use App\Services\Work\WorkService;
use Doctrine\ODM\MongoDB\Cursor;

class ProfileRepository extends Repository
{
    protected $message;
    protected $status;
    private $returnObject;
    private $secondaryObject;
    private $entityManager;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager);
    }
    /**
     * @return mixed
     */
    public function getReturnObject()
    {
        return $this->returnObject;
    }

    /**
     * @param mixed $returnObject
     */
    public function setReturnObject($returnObject)
    {
        $this->returnObject = $returnObject;
    }

    /**
     * @return mixed
     */
    public function getSecondaryObject()
    {
        return $this->secondaryObject;
    }

    /**
     * @param mixed $secondaryObject
     */
    public function setSecondaryObject($secondaryObject)
    {
        $this->secondaryObject = $secondaryObject;
    }


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getTotalCommentsByUser($id)
    {
        $qty = $this->queryBuilder->createQueryBuilder(Comments::class)
            ->field('userId')
            ->equals(new \MongoId($id))
            ->getQuery()
            ->execute();
        return count($qty);
    }

    /**
     * @param $userId
     * @param User $user
     * @param $collaborationTotal
     * @return array
     */
    public function getSidebar($userId, User $user, $collaborationTotal = null)
    {
        $countLikes = 0;
        $countViews = 0;
        $countComments = $this->getTotalCommentsByUser($user->getId());
        $refine = new \App\Services\Work\Refine\Student\User();
        $works = $refine->startSearch(
            $this->queryBuilder->createQueryBuilder(Work::class),
            $userId
        )
        ->field("status")->equals("enabled")
        ->getQuery()->execute();

        $commentsMade = $this->countUserComments(
            $this->queryBuilder->createQueryBuilder(Comments::class),
            $userId
        );

        if (count($works) > 0) {
            foreach ($works as $work) {
                $countLikes += count($work->getLikes());
                $countViews += $work->getViews();
            }
        }



        $classes = array();
        if (count($user->getClassRoom()) > 0) {
            foreach ($user->getClassRoom() as $class) {
                $details = $this->getClassroomDetails($class['id']);
                $array['id'] = $class['id'];
                if (count($details) > 0) {
                    $array['name'] = $details->getTitle();
                    $array['joinedAt'] = $class['uploaded_at'];
                }
                $classes[] = $array;
            }
        }

        $message =  [
            'userPhoto' => $user->getUserPhoto(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'bio' => $user->getBio(),
            'level' => $user->getLevel()->getAlias(),
            'totalLikes' => $countLikes,
            'totalViews' => $countViews,
            'totalComments' => $countComments,
            'classRoom' => $user->getAssociation(),
            'classes' => $classes,
            'joined' => $user->getCreatedAt(),
            'progress' => count($user->getCriterias()),
            'menu' => [
                'works' => count($works) > 0 ?
                    $this->checkWorks($works) :
                    count($works),
                'likes' => $this->checkLike($user->getLikes()),
                'following' => $this->checkUsers($user->getFollowing()),
                'followers' => $this->checkUsers($user->getFollowers()),
                'collaborations' => is_null($collaborationTotal) ? $user->getTotalCoWork() : $collaborationTotal,
                'comments' => $commentsMade
            ]

        ];
        return $message;
    }

    /**
     * This method check the all user's work and return the quantity of public
     * works that this user has.
     * @param Cursor $works An object of Cursor that have all works of the users
     * @return int          The quantity of public works.
     */
    public function checkWorks(Cursor $works)
    {
        $totalWorks = 0;
        foreach ($works as $work) {
            if ($work->getIsPrivate() != true) {
                $totalWorks ++;
            }
        }

        return $totalWorks;
    }

    public function getClassroomDetails($classId)
    {
        return $this->queryBuilder->getRepository(ClassRoom::class)->findOneBy(
            [
                '_id' => new \MongoId($classId)
            ]
        );
    }

    public function countUserComments($queryBuilder, $userId)
    {
        $comments = $queryBuilder
            ->field("userId")
            ->equals(new \MongoId($userId))
            ->field('status')
            ->notIn(['disabled'])
            ->getQuery()->execute();
        return count($comments);
    }

    /** Method to get profile by works than an educator liked
     * @param Work $works, $data
     * @return array
     */
    public function getEducatorWorks($works, $data)
    {
        $queryBuilder = $this->queryBuilder;
        $this->setRepository($queryBuilder->createQueryBuilder(Work::class));
        $worksArray = array();
        foreach ($works as $work) {
            $this->setRepository($queryBuilder->createQueryBuilder(Work::class));
            $work = $this->findAnUser($work->getWorkId());
            if (count($work)>0 ) {
                $this->setRepository($queryBuilder);
                $worksArray[] = $this->buildWork($work);
            }
        }
        return array_slice($worksArray, $data['skip'], $data['limit']);
    }

    /** Method to get my works on profile page.
     * @param $userId
     * @return array|int
     */
    public function getProfileWorks($data)
    {
        $worksArray = [];
        $waitingreview = $this->getRepository()->getRepository(Work::class)->findBy(
            [
                'userId' => new \MongoId($data['userId']),
                'available' => true,
                'reviewed' => false,
                'status' => 'enabled'
            ],
            [
                'upload_at' => 'DESC'
            ]
        );
        $reviewed = $this->getRepository()->getRepository(Work::class)->findBy(
            [
                'userId' => new \MongoId($data['userId']),
                'reviewed' => true,
                'status' => 'enabled'
            ],
            [
                'upload_at' => 'DESC'
            ]
        );
        $newer = $this->getRepository()->getRepository(Work::class)->findBy(
            [
                'userId' => new \MongoId($data['userId']),
                'status' => 'enabled',
                'reviewed' => false
            ],
            [
                'upload_at' => 'DESC'
            ]
        );
        if (count($waitingreview) > 0) {
            foreach ($waitingreview as $wr) {
                $workbuilded = $this->buildWork($wr);

                if ($workbuilded['isPrivate'] == false) {
                    $worksArray[] = $workbuilded;
                }
            }
        }
        if (count($reviewed) > 0) {
            foreach ($reviewed as $rw) {
                $workbuilded = $this->buildWork($rw);

                if ($workbuilded['isPrivate'] == false) {
                    $worksArray[] = $workbuilded;
                }
            }
        }
        if (count($newer) > 0) {
            foreach ($newer as $nw) {
                $workbuilded = $this->buildWork($nw);

                if ($workbuilded['isPrivate'] == false) {
                    $worksArray[] = $workbuilded;
                }
            }
        }

        if (count($worksArray) == 0) {
            return 0;
        } else {
            $returnArray = array();
            foreach ($worksArray as $work) {
                if (!in_array($work['id'], array_column($returnArray, 'id'))) {
                    $returnArray[] = $work;
                }
            }
        }

        return array_slice($worksArray, $data['skip'], $data['limit']);
    }

    public function checkMutual($profileId, $profileFollowing, $visitorId, $id)
    {
        $followings = $profileFollowing;
        if ($profileId !== $visitorId) {
            $visitor = $this->entityManager->findOneBy(
                [
                    '_id' => $visitorId
                ]

            );
            $followings = $this->getFollow($visitor->getFollowing());
        }
        if (!empty($followings)) {
            $checkFollowings = array_column($followings, 'id');
            return in_array($id, $checkFollowings);
        }
        return false;
    }

    /** Method to get followers
     * @param $data[userId,limit,skip]
     * @param $tokenId
     * @return array|int
     */
    public function fetchFollowers($data, $tokenId)
    {
        $user = $this->queryBuilder->findOneBy(
            [
                '_id' => $data['userId']
            ],
            [

            ]
        );

        if ($user->getFollowers()) {
            $followVar = array();
            $followers = $this->getFollow($user->getFollowers());
            if (!empty($followers)) {
                if (isset($data['limit']) && isset($data['skip'])) {
                  $followers = array_slice($followers, $data['skip'], $data['limit']);
                }
                foreach ($followers as $follower) {
                    $follow = $this->buildUser($follower["id"]);

                    $followVar[] = [
                        'userId' => $follower['id'],
                        'userPhoto' => $follow['user']["avatar"],
                        'firstName' => $follow['user']["firstName"],
                        'lastName' => $follow['user']["lastName"],
                        'level' => $follow['user']["level"],
                        'mutual' => $this->checkMutual($data['userId'], $data, $tokenId, $follower['id']),
                    ];

                }
                return $followVar;
            }
        }
            return 0;
    }

    public function getFollow($followObject)
    {
        $return = array();
        foreach ($followObject as $follow) {
            $user = array();
            $checkUser = $this->checkUsers(array($follow));
            if ($checkUser > 0) {
                $user['id'] = $follow->getUserId();
                $user['createAt'] = $follow->getCreatedAt();
                $return[] = $user;
            }

        }
        return $return;
    }

    /** Method to get following users by Id
     * @param $data
     * @param $tokenId
     * @return array|int
     */
    public function fetchFollowing($data, $tokenId)
    {
        $user = $this->queryBuilder->findOneBy(
            [
                '_id' => $data['userId']
            ],
            [

            ]
        );

        if (count($user) >0) {
            if ($user->getFollowing()) {
                $followings = (array) $this->getFollow($user->getFollowing());
                $followVar = array();
                if (!empty($followings)) {
                    if (isset($data['limit']) && isset($data['skip'])) {
                      $followings = array_slice($followings, $data['skip'], $data['limit']);
                    }
                    foreach ($followings as $following) {
                        $follow = $this->buildUser($following["id"]);

                        $message = [
                            'userId' => $following['id'],
                            'userPhoto' => $follow['user']["avatar"],
                            'firstName' => $follow['user']["firstName"],
                            'lastName' => $follow['user']["lastName"],
                            'level' => $follow['user']["level"],
                            'mutual' => $this->checkMutual($data['userId'], $followings, $tokenId, $following['id']),
                        ];
                        $followVar[] = $message;
                    }
                }
                return $followVar;
            }
            return 0;
        }
        return false;
    }

    public function unFollowUser($followerId, $userId)
    {
        $users = $this->queryBuilder->createQueryBuilder(User::class)
           ->field("_id")
            ->equals(new \MongoId($followerId))
            ->getQuery()
            ->getSingleResult();

        $follower = $this->queryBuilder->createQueryBuilder(User::class)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        
        if (count($users->getFollowing())>0) {
            $following = array();
            foreach ($users->getFollowing() as $user) {
                $following[] = $user->getId();
            }
            $check = in_array($userId, $following);

            if ($check == true) {
                $follower->deleteFollower($followerId);
                $users->deleteFollowing($userId);
                $this->setReturnObject($users);
                $this->setSecondaryObject($follower);
                return true;
            }
        }
        return 'nofollow';
    }

    /** Method to get works than i liked
     * @param $data
     * @return array|bool|int
     */
    public function fetchLikes($data)
    {
        $user = $this->queryBuilder->getRepository(User::class)->findOneBy(
            [
                '_id' => new \MongoId($data['userId'])
            ],
            [

            ]
        );

        if (count($user) === 0) {
            return false;
        }

        if (count($user->getLikes()) > 0) {
            $userLiked = array();
            foreach ($user->getLikes() as $like) {
                $userLiked[] = $like;
            }

            if (isset($data['limit']) && isset($data['skip'])) {
                $likes = array_slice($userLiked, $data['skip'], $data['limit']);
            }
            $likeVar = [];
            foreach ($likes as $like) {
                $worksRepo = $this->queryBuilder->getRepository(Work::class);
                $work = $worksRepo->findOneBy(
                    [
                        '_id' => new \MongoId($like->getWorkId()),
                        "status" => "enabled"
                    ]
                );
                if (count($work) > 0) {
                    $likeVar[] = $this->buildWork($work);
                }
            }
            return $likeVar;
        }
        return 0;
    }

    public function insertFollow($Ids, User $userFollowing)
    {
        $following = $this->getFollow($userFollowing->getFollowing());
        if (!empty($following)) {
            $data = array_column($following, 'id');
            $check = in_array($Ids['followId'], $data);
            if ($check) {
                return false;
            }
        }
        $following = new Follow();
        $following->setUserId($Ids['followId']);
        $following->setCreatedAt($this->getMongoDate());
        $userFollowing->setFollowing($following);
        $this->setReturnObject($userFollowing);
        return true;
    }


    public function insertFollowers($Ids, User $userFollowing)
    {
        $following = new Follow();
        $following->setUserId($Ids['userId']);
        $following->setCreatedAt($this->getMongoDate());
        $userFollowing->setFollowers($following);
        $this->setReturnObject($userFollowing);
        return true;
    }

    public function checkFollow($id, $fid)
    {
        $user = $this->queryBuilder->findOneBy(
            [
                '_id' => new \MongoId($id)
            ]
        );
        if (count($user) === 0) {
            $this->setMessage('User Id Not Found');
            return false;
        }
        $following = $this->getFollow($user->getFollowing());
        if (!empty($following)) {
            $data = array_column($following, 'id');
            $check = in_array($fid, $data);
            if ($check) {
                return 'Unfollow';

            }
            return 'Follow';
        }
        return 'Follow';
    }

    public function portfolio($data)
    {
        $works = $this->queryBuilder->createQueryBuilder(Work::class)
            ->field('userId')
            ->equals(new \MongoId($data['userId']))
            ->field('sendPortfolio')
            ->equals(true)
            ->field('status')->equals('enabled')
            ->getQuery()
            ->execute();

        $totalWork = count($works);
        $works = $works->limit($data['limit'])->skip($data['skip']);
        $workService = new WorkService();
        $portfolio = array();
        $list = array();
        if (count($works) > 0) {
            foreach ($works as $work) {
                $tst['workId'] = $work->getId();
                $workService->getWorkById($this->queryBuilder, $tst);
                $list[] = $workService->getMessage();
            }
            $portfolio['work'] = $list;
        }
        $portfolio['totalWork'] = $totalWork;
        return $portfolio;
    }

    /** Method to check if the user exist
     * @param $usersInfo an object that have the user's information.
     * @return int
     */
    public function checkUsers($usersInfo) {
        $userRepo = $this->entityManager->createQueryBuilder(User::class);
        $this->setRepository($userRepo);
        $users = array();

        foreach ($usersInfo as $userInfo) {
            if (!empty($this->buildUser($userInfo->getId()))) {
                $users[] = $this->buildUser($userInfo->getId());
            }

        }
        return count($users);
    }

    /** Method to check if the works of likes are not disable or flagged
     * @param $likesInfo an object that have the like's information.
     * @return int
     */
    public function checkLike($likesInfo) {
        $like = array();

        foreach ($likesInfo as $likeInfo) {
            $works = $this->queryBuilder->createQueryBuilder(Work::class)
                ->field('_id')
                ->equals(new \MongoId($likeInfo->getWorkId()))
                ->field('status')->equals('enabled')
                ->getQuery()
                ->execute();

            if (count($works) > 0) {
                foreach ($works as $work) {
                    $like[] = $work;
                }
            }
        }
        return count($like);
    }
    
}
