<?php

namespace App\Repository\Group;

use App\Entities\ClassRoom;
use App\Entities\User;

class ClassRoomRepository extends GroupRepository
{
    /**
     * Method to build a lab
     * @param $data
     * @return mixed
     */
    public function buildClass($data)
    {
        $class = new ClassRoom();
        $class->setUserId(new \MongoId($data['userId']));
        $class->setDescription($data['description']);
        $class->setTitle(ucfirst($data['title']));
        $class->setEmailEducator($data['educatorEmail']);
        $class->setEmailPro($data['proEmail']);
        $class->setSchoolId($data['schoolId']);
        $class->setUploadAt($this->getMongoDate());
        $class->setStatus('pending');
        $class->setTotalMembers(1);

        if ($data['type'] == 'classroom') {
            $class->setStatus('approved');
        }

        return $class;
    }

    public function updateClass($data)
    {
        $classRepo = $this->getRepository()->getRepository(ClassRoom::Class);
        $class = $classRepo->findOneBy(
            [
                '_id' => new \MongoId($data['classId'])
            ]
        );
        if (count($class) >0) {
            $class->setDescription($data['description']);
            $class->setTitle($data['title']);
            $class->setCoverImage($data['coverImage']);
            $this->setMessage($class);
            return true;
        }
        return false;
    }

    public function getClassIdByUser($userId)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);

        $user = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        if (count($user) > 0) {
            $var = $user->getClassRoom();

            return $var;
        }
        return false;
    }
    
    public function join($data)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);
        $class['id'] = $data['classId'];
        $class['uploaded_at'] = $this->getMongoDate();
        $queryBuilder
            ->updateOne()
            ->field('classroom')->push($class)
            ->field('_id')->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->execute();
        return true;
    }

}