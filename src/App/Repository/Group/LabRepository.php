<?php

namespace App\Repository\Group;

use App\Entities\ClassRoom;
use App\Entities\Lab;
use App\Entities\Task;
use App\Entities\User;
use App\Entities\Work;
use App\Services\Work\WorkService;

class LabRepository extends GroupRepository
{
    /**
     * Method to build a lab
     * @param $data
     * @return mixed
     */
    public function buildLab($data)
    {
        $lab = new Lab();
        $lab->setUserId(new \MongoId($data['userId']));
        $lab->setDescription($data['description']);
        $lab->setTitle($data['title']);
        $lab->setEmailEducator($data['educatorEmail']);
        $lab->setEmailPro($data['proEmail']);

        if (!is_null($data['schoolId'])) {
            $lab->setSchoolId($data['schoolId']);
        }
        $lab->setUploadAt($this->getMongoDate());
        $lab->setStatus('pending');
        $lab->setTotalMembers(1);

        if ($data['type'] == 'classroom') {
            $lab->setStatus('approved');
        }
        return $lab;
    }

    public function updateLab($data)
    {

        $labRepo = $this->getRepository()->getRepository(ClassRoom::Class);
        if ($data['type'] == 'lab') {
            $labRepo = $this->getRepository()->getRepository(Lab::Class);
        }
        $lab = $labRepo->findOneBy(
            [
                '_id' => new \MongoId($data['groupId'])
            ]
        );
        if (count($lab) >0) {
            $lab->setDescription($data['description']);
            $this->setMessage($lab);
            return true;
        }
        return false;
    }

    public function getLabsIdByUser($userId)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);

        $user = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        if (count($user) > 0) {
            $var = $user->getLabs();
            return $var;
        }
        return false;
    }

    public function updateCount($id, $type)
    {
        $queryBuilder = $this->queryBuilder->createQueryBuilder(Lab::class);

        if ($type === 'classroom') {
            $queryBuilder = $this->queryBuilder->createQueryBuilder(ClassRoom::class);
        }
        $queryBuilder
            ->updateOne()
            ->field('totalMembers')->inc(1)
            ->field('_id')->equals(new \MongoId($id))
            ->getQuery()
            ->execute();
        return true;
    }


    public function join($data)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);

        $groups = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->getSingleResult();

        $labs = array();
        $class = array();
        if (!empty($groups->getLabs())) {
            $labs = $groups->getLabs();
        }
        if (!empty($groups->getClassRoom())) {
            $class = $groups->getClassRoom();
        }
        $myGroups = array_merge($labs, $class);

        if (!in_array($data['groupId'], array_column($myGroups, "id"))) {
            $lab['id'] = $data['groupId'];
            $lab['uploaded_at'] = $this->getMongoDate();
            $queryBuilder
                ->updateOne()
                ->field('lab')->push($lab)
                ->field('_id')->equals(new \MongoId($data['userId']))
                ->getQuery()
                ->execute();
            return true;
        }
        return false;
    }

    /** Method to build a task
     * @param $data
     * @return Task object
     */
    public function buildTask($data)
    {
        $task = new Task();
        $task->setUserId($data['userId']);
        $task->setTitle($data['title']);
        $task->setDueDate($this->getMongoDateByDate($data['dueDate']));
        $task->setDescription($data['description']);
        return $task;
    }

    /** Method to get a lab by Id
     * @param $data
     * @return mixed
     */
    public function getLabById($labId)
    {
        return $this->getRepository()
            ->field("_id")
            ->equals(new \MongoId($labId))
            ->getQuery()
            ->getSingleResult();
    }

    public function buildGetTask($task)
    {
        $future['id'] = $task->getId();
        $future['title'] = $task->getTitle();
        $future['description'] = $task->getDescription();
        $future['due'] = $task->getDueDate();
        $user = $this->buildUser($task->getUserId());
        return array_merge($future, $user);
    }

    public function getTurnedIn($task, $userId)
    {
        if (!empty($task->getTurnedIn())) {
            if (in_array($userId, array_column($task->getTurnedIn(), 'userId'))) {
                return true;
            }
            return false;
        }
        return false;
    }

    /** Method to get tasks by Object Lab
     * @param Lab $lab
     * @return array
     */
    public function getTask(Lab $lab, $userId)
    {
        $tasks = array();
        $this->setRepository($this->getRepository()->CreateQueryBuilder(User::class));

        $now = new \DateTime('now');
        $dateNow = strtotime($now->format('Y-m-d'));
        foreach ($lab->getTasks() as $task) {
            $duoDate = strtotime($task->getDueDate()->format('Y-m-d'));
            if (($dateNow >= $duoDate)) {
                $past = $this->buildGetTask($task);
                $past['turnedIn'] = $this->getTurnedIn($task, $userId);
                $past['hasSubmission'] = !empty($task->getTurnedIn()) ? true : false;
                $tasks['past'][] = $past;
            }

            if ($dateNow <= $duoDate) {
                $future = $this->buildGetTask($task);
                $future['turnedIn'] = $this->getTurnedIn($task, $userId);
                $future['hasSubmission'] = !empty($task->getTurnedIn()) ? true : false;
                $tasks['future'][] = $future;
            }
        }
        if (!empty($tasks['future'])) {
            usort($tasks['future'], function ($a, $b) {
                return $a['due']->getTimestamp() - $b['due']->getTimestamp();
            });
        }

        if (!empty($tasks['past'])) {
            usort($tasks['past'], function ($a, $b) {
                return $a['due']->getTimestamp() - $b['due']->getTimestamp();
            });
        }

        return $tasks;
    }

    public function turnIn($lab, $data)
    {
        $tasks = $lab->getTasks();
        $now = new \DateTime('now');
        $dateNow = strtotime($now->format('Y-m-d'));
        foreach ($tasks as $task) {
            $due = $task->getDueDate()->getTimestamp();
            if ($data['taskId'] === $task->getId()) {
                if ($dateNow > $due) {
                    $this->setError('Task Expired');
                    return false;
                }
                if ($task->getTurnedIn()) {
                    if (in_array($data['userId'], array_column($task->getTurnedIn(), 'userId'))) {
                        $this->setError('Already Turned this Task in');
                        return false;
                    }
                }
                $workRepo = $this->getRepository()->getRepository(Work::Class);
                $work = $workRepo->findOneBy(
                    [
                        '_id' => new \MongoId($data['workId'])
                    ]
                );
                $this->getRepository()->persist($work);
                $this->getRepository()->flush();
                $task->setTurnedIn(
                    [
                        'workId' => $data['workId'],
                        'userId' => $data['userId'],
                        'date' => $this->getMongoDate()
                    ]
                );
            }

        }
        $this->setMessage($lab);
        return true;
    }

    /**
     * Method will return the information of the task's submissions.
     * @param Lab $lab    An Object of lab used to get information to search
     *                    submissions.
     * @param array $data An array used to check if the taskId inside the lab is
     *                    the same task that we need to get the submission.
     * @return array      An array with the user information and
     *                    works information to display the submission.
     */
    public function seeSubmissions(Lab $lab, array $data)
    {
        $workService = new WorkService();
        $repository = $this->getRepository();
        $this->setRepository(
            $this->getRepository()->createQueryBuilder(User::class)
        );
        $tasks = $lab->getTasks();
        $returnArray = array();
        foreach ($tasks as $task) {
            if ($task->getId() === $data['taskId'] && count($task->getTurnedIn()) > 0) {
                foreach ($task->getTurnedIn() as $submission) {
                    $workService->getWorkById($repository, $submission);
                    $work['work'] = $workService->getMessage();
                    $user = $this->buildUser($submission['userId']);
                    $array = array_merge($user, $work);
                    $array['submitted'] = date('Y-d-m H:i:s', $submission['date']->sec);
                    $returnArray[] = $array;
                }
            }
        }

        return $returnArray;
    }

}