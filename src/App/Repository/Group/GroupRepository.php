<?php

namespace App\Repository\Group;

use App\Entities\ClassRoom;
use App\Entities\Comments;
use App\Entities\Lab;
use App\Entities\Like;
use App\Entities\Post;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\CommentLikeRepository;
use App\Repository\LikeRepository;
use App\Repository\Repository;
use Doctrine\ODM\MongoDB\Cursor;

class GroupRepository extends Repository
{

    private $commentsById;

    /**
     * @return mixed
     */
    public function getCommentsById()
    {
        return $this->commentsById;
    }

    /**
     * @param mixed $commentsById
     */
    public function setCommentsById($commentsById)
    {
        $this->commentsById = $commentsById;
    }

    /** Method to build a sidebarMenu group only
     * @param $data
     * @return string
     */
    public function buildSideBar($data)
    {
        $buildedLab = '';
        $lab = $this->getRepository()
            ->field("_id")
            ->equals(new \MongoId($data))
            ->getQuery()
            ->getSingleResult();

        if (count($lab)>0) {
            $buildedLab['title'] = $lab->getTitle();
            $buildedLab['code'] = $lab->getId();
            $buildedLab['description'] = $lab->getDescription();
            $buildedLab['uploaded_at'] = $lab->getUploadAt();
            $buildedLab['coverImage'] = $lab->getCoverImage();
            $buildedLab['userId'] = $lab->getUserId();
            $buildedLab['totalMembers'] = $lab->getTotalMembers();
        }
        return $buildedLab;
    }
    
    

    /**
     * Method to build a new post object
     * @param $data
     * @return Post data
     */
    public function buildPost($data)
    {
        $post = new Post();
        $post->setUserId($data['userId']);
        $post->setType($data['type']);
        $post->setPublication($data['publication']);
        $post->setGroupId($data['groupId']);
        $post->setTotalComment(0);
        $post->setTotalLike(0);
        $post->setUploadAt($this->getMongoDate());

        return $post;
    }

    /**
     * Method to create a feed builder
     * @param $id
     * @return mixed
     */
    public function buildFeed($id)
    {

        $group = $this->getRepository()
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();

        if (count($group)>0) {
            if (!empty($group->getPosts())) {
                return $group->getPosts();
            }
            return false;
        }
        return false;
    }

    public function fetchUserById($userBuilder, $id)
    {
        return $userBuilder
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Method to build the classroom and lab feeds
     * @param $posts
     * @return array
     */
    public function feedBuilder($posts)
    {
        $groupPosts = '';
        $commentRepository = $this->getRepository()->createQueryBuilder(Comments::class);
        $likeRepository = new LikeRepository($this->getRepository()->createQueryBuilder(Post::class));
        $this->setRepository($this->getRepository()->createQueryBuilder(User::class));

        foreach ($posts as $post) {
            $data['productId'] = $post->getId();
            $data['userId'] = $post->getUserId();
            $like = $likeRepository->findLike($data);

            $liked = count($like) >0 ? true : false;

            $groupPost['postId'] = $post->getId();
            $groupPost['publication'] = $post->getPublication();
            $groupPost['totalLike'] = count($post->getLike());
            $groupPost['totalComment'] =  $this->getTotalComments($commentRepository, $post->getId());
            $groupPost['upload_at'] = $post->getUploadAt()->format('Y/m/d H:i:s');
            $groupPost['liked'] = $liked;
            $user = $this->buildUser($post->getUserId());
            $groupPost['user'] = $user['user'];
            $groupPosts[] = $groupPost;
        }
        usort($groupPosts, function ($a, $b) {
            return strtotime($a['upload_at']) - strtotime((string)$b['upload_at']);
        });

        return $groupPosts;
    }

    public function countCommentsById($id)
    {
        $qty = $this->getRepository()->createQueryBuilder(Comments::class)
            ->field('userId')
            ->equals(new \MongoId($id))
            ->getQuery()
            ->execute();
        $counter = count($qty);
        $objComments = array();
        if (count($qty) > 0) {
            $buildComment = $this->sortComment($qty);
            foreach ($buildComment as $comment) {
                if ($id == $comment['userId']) {
                    $objComments[] = $comment;
                }
            }
        }
        $this->setCommentsById($objComments);
        return $counter;
    }

    public function countWorksByUserId($id)
    {
        $group = $this->getRepository()->createQueryBuilder(Work::class)
            ->field('userId')
            ->equals(new \MongoId($id))
            ->getQuery()
            ->execute();

        return count($group);
    }

    public function dateLastComment($userId)
    {
        $group = $this->getRepository()
            ->sort('works.comments.created_at', 'desc')
            ->getQuery()
            ->execute();
        $date = array();
        foreach ($group as $work) {
            foreach ($this->sortCommentByDate($work->getComment()) as $comment) {
                if ($userId == $comment['id']) {
                    $date[] = $comment['date'];
                }
            }
        }
        return $date;
    }

    /** Method to check group id
     * @param $query
     * @param $id
     * @return mixed
     */
    public function checkClassCode($query, $id)
    {
        return $query
            ->getQuery()
            ->execute();
    }


    /** Method to build a roosters by users list
     * @param $userList
     * @return array
     */
    public function buildRooster($userList)
    {
        $rooster = array();
        if (count($userList)>0) {
            foreach ($userList as $user) {
                if (isset($user['user'])) {
                    $roost['user'] = $user['user'];
                    $roost['comments'] = $this->countCommentsById($user['user']['id']);
                    $roost['uploads'] = $this->countWorksByUserId($user['user']['id']);
                    $rooster['results'][] = $roost;
                }
            }
        }
        $rooster['totalPages'] = $userList['totalPages'];
        $rooster['totalResults'] = (integer)$userList['totalMembers'];
        return $rooster;
    }

    /**
     * Method to get all labs and classrooms
     * @return array
     */
    public function getLabAndClassAdmin()
    {
        $repo = $this->getRepository();
        $labsBuilder = $repo->createQueryBuilder(Lab::class);
        $classBuilder = $repo->createQueryBuilder(ClassRoom::class);
        $var = array();
        $labs = $labsBuilder
            ->getQuery()
            ->execute();
        if (count($labs) > 0) {
            foreach ($labs as $lab) {
                $var['lab'][] = ['id' => $lab->getId()];
            }
        }
        $classes = $classBuilder
            ->getQuery()
            ->execute();
        if (count($classes) > 0) {
            foreach ($classes as $class) {
                $var['classRoom'][] = ['id' => $class->getId()];
            }
        }
        return $var;
    }

    public function getLabsAndClassById($userId)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);

        $user = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        if (count($user) > 0) {
            $var['lab'] = $user->getLabs();
            $var['classRoom'] = $user->getClassRoom();
            return $var;
        }
        return false;
    }

    public function findLab($labId)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(Lab::class);
        return $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($labId))
            ->field("status")
            ->equals('approved')
            ->getQuery()
            ->getSingleResult();
    }


    /**
     * This method will return an array of labs where their ids are on labsIds
     * @param array $labIds
     * @param null $orderBy
     * @return mixed
     */
    public function findRangeLab(array $labIds, $orderBy = null )
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(Lab::class);
        $query =  $queryBuilder
            ->field("_id")
            ->in($labIds)
            ->field("status")
            ->equals('approved')
            ->sort('title', 'asc')
            ->getQuery()
            ->execute();

        return $query;
    }


    /**
     * This method will return an array of labs where their ids are on labsIds
     * @param array $labIds
     * @param null $orderBy
     * @return mixed
     */
    public function findRangeClassRoom(array $classroomIds, $orderBy = null )
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(ClassRoom::class);
        $query =  $queryBuilder
            ->field("_id")
            ->in($classroomIds)
            ->field("status")
            ->equals('approved');

        if (!is_null($orderBy)) {
            $query = $query->sort('title','asc');
        }
        return $query->getQuery()->execute();
    }


    /**
     * Method to find a classRoom by id
     * @param $classRoomId
     * @return mixed
     */
    public function findClassRoom($classRoomId)
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder(ClassRoom::class);
        return $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($classRoomId))
            ->field("status")
            ->equals('approved')
            ->getQuery()
            ->getSingleResult();
    }

    public function countUserOnLabOrClass($id)
    {
        $counter = 0;
        $queryBuilder = $this->getRepository()->createQueryBuilder(User::class);
        $users = $queryBuilder
            ->getQuery()
            ->execute();
        foreach ($users as $user) {
            $labs = $user->getLabs();
            if ($labs) {
                foreach ($labs as $idx => $lab) {
                    if ($lab['id'] == $id) {
                        $counter += 1;
                    }
                }
            }

        }
        return $counter;
    }

    public function buildLabOrClass($group, $type = 'lab')
    {
        $repo = $this->getRepository();
        $data = array();
        if (count($group)>0) {
            if (empty($group->getUserId())) {
                $edu = $this->findUserByEmail(
                    $this->getRepository()->createQueryBuilder(User::class),
                    $group->getEmailEducator()
                );

                if (count($edu)>0) {
                    $id = $edu->getId();
                }
            } else {
                $id = $group->getUserId();
            }
            $data['title'] = $group->getTitle();
            $data['id'] = $group->getId();
            $data['description'] = $group->getDescription();
            $data['coverImage'] = $group->getCoverImage();
            $data['type'] = ucfirst(strtolower($type));
            $data['countUsers'] = $this->countUserOnLabOrClass($group->getId());
            $this->setRepository($repo->createQueryBuilder(User::class));
            $data['user'] = $this->buildUser($id)['user'];
            $data['owner'] = $this->buildUser($id)['user'];
            $data['createdAt'] = $group->getUploadAt();
            $data['status'] = $group->getStatus();
        }
        $this->setRepository($repo);
        return $data;
    }


    /**
     * Generic method to getById
     * @param $postId
     * @return mixed
     */
    public function getComment($postId)
    {
        return $this->getRepository()
            ->field("_id")
            ->equals(new \MongoId($postId))
            ->getQuery()
            ->getSingleResult();
    }

    public function getPosts($groupId)
    {
        return $this->getRepository()
            ->field('groupId')
            ->equals(new \MongoId($groupId))
            ->getQuery()
            ->execute();
    }

    public function updateCover($data)
    {
        $group = $this->getRepository()->getRepository(ClassRoom::class);
        if ($data['type'] == 'lab') {
            $group = $this->getRepository()->getRepository(Lab::class);
        }

        $groups = $group->findOneBy(
            [
                '_id' => new \MongoId($data['labId']),
            ]
        );

        if (count($groups) > 0) {
            $groups->setCoverImage($data['url']);
            $this->getRepository()->persist($groups);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function deleteGroup($data)
    {
        $group = $this->getRepository()->getRepository(ClassRoom::class);
        if ($data['type'] == 'lab') {
            $group = $this->getRepository()->getRepository(Lab::class);
        }

        $groups = $group->findOneBy(
            [
                '_id' => new \MongoId($data['groupId']),
                'userId' => new \MongoId($data['userId'])
            ]
        );

        if (count($groups) > 0) {
            $groups->setStatus('disabled');
            $this->getRepository()->persist($groups);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    public function addUserOnGroup($data)
    {
        $group = $this->getRepository()->getRepository(ClassRoom::class);
        if ($data['type'] == 'lab') {
            $group = $this->getRepository()->getRepository(Lab::class);
        }

        $groups = $group->findOneBy(
            [
                '_id' => new \MongoId($data['groupId'])
            ]
        );

        $result = false;
        if (count($groups) > 0) {
            if ($data['level'] === 'Student Level 3') {
                if ($groups->getUserId() !== $data['userId']) {
                    $result =  'notOwner';
                }
            }
            foreach ($data['email'] as $mail) {
                $user = $this->findUserByEmail($this->getRepository()->createQueryBuilder(User::class), $mail);
                if (count($user) > 0) {
                    if ($data['type'] == 'lab') {
                        if (@!in_array($data['groupId'], array_column($user->getLabs(), 'id'))) {
                            $user->setLabs([
                                'id' => $data['groupId'],
                                'uploaded_at' => $this->getMongoDate()
                            ]);
                            $groups->setTotalMembers($groups->getTotalMembers()+1);
                            $this->getRepository()->persist($groups);
                            $this->getRepository()->flush();
                            $this->getRepository()->persist($user);
                            $this->getRepository()->flush();
                            $result = true;
                        } else {
                            $result = 'alreadyIn';
                        }
                    } else {
                        if(is_array($user->getClassroom())) {
                            if (!in_array($data['groupId'], array_column($user->getClassroom(), 'id'))) {
                                $this->setClassRoom($user, $groups, $data["groupId"]);
                                $result = true;
                            } else {
                                $result = 'alreadyIn';
                            }
                        }
                        else {
                            if(is_null($user->getClassroom())) {
                                $this->setClassRoom($user, $groups, $data["groupId"]);
                                $result = true;
                            } else {
                                $result = 'alreadyIn';
                            }
                        }
                    }

                }
            }
        }
        return $result;
    }

    /** Method to increment a new member on totalMembers when a new user was add
     * @param $id
     * @return mixed
     */
    public function incrementNewMember($id)
    {
        return $this->queryBuilder
            ->updateOne()
            ->field('totalMembers')->inc(1)
            ->field('_id')
            ->equals(new \MongoId($id))
            ->getQuery()
            ->execute();
    }

    /** Method to remove a lab or class post
     * @param $id
     * @return mixed
     */
    public function removeAPost($id)
    {
        return $this->queryBuilder
            ->remove()
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->execute();
    }

    public function removeUserRoster($data)
    {
        $user = $this->getRepository()->getRepository(User::class)->findOneBy(
            [
                '_id' => new \MongoId($data['userId']),
            ]
        );
        if ($data['type'] == 'lab') {
            if (in_array($data['groupId'], array_column($user->getLabs(), 'id'))) {
                $user->deleteLab($data['groupId']);
                $this->getRepository()->persist($user);
                $this->getRepository()->flush();
                return true;
            }
            return false;

        } else {
            if (in_array($data['groupId'], array_column($user->getClassRoom(), 'id'))) {
                $user->deleteClassroom($data['groupId']);
                $this->getRepository()->persist($user);
                $this->getRepository()->flush();
                return true;
            }
            return false;
        }
    }

    /**
     * This method will check if exist a task inside a lab or classroom
     * if to exist, will be removed and returned a group object to be persisted
     *
     * @param $taskId
     *
     * @return mixed
     */
    public function removeTask($taskId)
    {
       $group = $this->taskFind($taskId);

        if (count($group)>0) {
            $group->removeATask($taskId);
            return $group;
        }
        return false;
    }

    /** Method to find a task
     * @param $taskId
     * @return mixed
     */
    public function taskFind($taskId)
    {
        return $this->getRepository()
            ->field("tasks._id")
            ->in(array(new\MongoId($taskId)))
            ->getQuery()
            ->getSingleResult();
    }

    /** Method to decrease total members on lab or class
     * @param $labOrClassDocument
     * @return object
     */
    public function decreaseTotalMember($labOrClassDocument)
    {
        return $labOrClassDocument
            ->setTotalMembers($labOrClassDocument->getTotalMembers()-1);
    }

    /**
     *  Method to set a classroom to an user.
     * @param User $user
     * @param Classroom $classRoom
     * @param $groupId
     */
    public function setClassRoom(User $user, ClassRoom $classRoom, $groupId)
    {
        $user->setClassRoom([
            'id' => $groupId,
            'uploaded_at' => $this->getMongoDate()
        ]);
        $classRoom->setTotalMembers($classRoom->getTotalMembers() + 1);
        $this->getRepository()->persist($classRoom);
        $this->getRepository()->flush();
        $this->getRepository()->persist($user);
        $this->getRepository()->flush();
    }
}