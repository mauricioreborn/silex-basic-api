<?php

namespace App\Repository;

use App\Entities\CommentLike;
use App\Entities\Comments;
use App\Entities\User;
use App\Entities\Work;
use App\Entities\LiveSession;
use \Doctrine\ODM\MongoDB\DocumentRepository;
use Sokil\Mongo\Exception;

class CommunityRepository extends Repository
{
    private $entityManager;
    protected $message;
    protected $status;


    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager);
    }

    public function getRepo($class)
    {
        return $this->entityManager->getRepository($class);
    }

    public function getUser($userId)
    {
        $user = $this->entityManager->createQueryBuilder(User::class)
            ->field('_id')
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        if (count($user) > 0) {
            return [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'profile' => $user->getProfile(),
                'level' => $user->getLevel()->getAlias(),
                'avatar' => $user->getUserPhoto(),
            ];
        }
        return '';
    }

    public function buildWorkType(Work $work, $type, $label)
    {
        return
            [
                'id' => $work->getId(),
                'thumbnail' => $this->getFile($work->getThumbnail()),
                'mainFile' => $this->getFile($work->getMainFile()),
                'totalLikes' => count($work->getLikes()),
                'totalViews' => $work->getViews(),
                'totalComments' => count($work->getComment()),
                'title' => $work->getTitle(),
                'path' => $this->getFile($work->getAttach()),
                'upload_at' => $work->getUploadAt(),
                'workLevel' => $work->getLevel()->getAlias(),
                'description' => $work->getDescription(),
                'group' => $work->getGroup(),
                'highlighted' => !empty($work->getHighLight()) ? $work->getHighLight() : false,
                'type' => $type,
                'label' => $label,
                'availableReview' => $work->getAvailable(),
                'user' => $this->getUser($work->getUserId())
            ];
    }

    public function getRecentActivity($data)
    {
        $userRepo = $this->getRepo(User::Class);
        $user = $userRepo->findOneBy(
            [
                '_id' => $data['userId']
            ]
        );
        if (count($user) === 0) {
            return false;
        }
        $workIds = array();
        if (count($user->getFollowing())>0) {
            foreach ($user->getFollowing() as $follow) {
                $work = $this->workIterator($follow->getUserId());
                $activity = $work->getLastActivity();
                   $workIds[] = [
                       'id' => $work->getId(),
                       'date' => $activity->getDate()->format('Y/m/d H:i:s'),
                       'label' => $activity->getType()."  b  " . $this->fetchName($activity->getUserId()),
                       'type' => $activity->getIcon()
                   ];

            }

            $array = $workIds;
            usort($array, function ($a, $b) {
                return strtotime($b['date']) - strtotime((string)$a['date']);
            });
            $sorted = [];
            foreach ($array as $val) {
                $compare = array_column($sorted, 'id');
                if (!in_array($val['id'], $compare)) {
                    $sorted[] = [
                        'id' => $val['id'],
                        'date' => $val['date'],
                        'label' => $val['label'],
                        'type' => $val['type']
                    ];
                }
            }
            $recentActivity = [];
            foreach ($sorted as $recent) {
                  $workRepo = $this->getRepo(Work::Class);
                 $fetchWork = $workRepo->findOneBy(
                     [
                        '_id' => $recent['id']
                     ]
                 );
                 $recentActivity[] =
                     $this->buildWorkType($fetchWork, $recent['type'], $recent['label']);

            }
            return $recentActivity;
        }

        return 0;
    }

    public function workIterator($id)
    {
        $repo = $this->entityManager->createQueryBuilder(Work::class);
        return $repo
            ->field('userId')->equals(new \MongoId($id))
            ->field('status')->equals("enabled")
            ->sort('lastActivity.date', 'desc')
            ->getQuery()
            ->getSingleResult();
    }

    public function commentIterator($id, $workId)
    {
        $repo = $this->entityManager->createQueryBuilder(Comments::class);
        return $repo
            ->field('userId')->equals(new \MongoId($id))
            ->field('productId')->equals(new \MongoId($workId))
            ->field('status')->equals('enabled')
            ->sort('created_at', 'desc')
            ->getQuery()
            ->getSingleResult();
    }

    public function commentInterest($id)
    {
        $repo = $this->entityManager->createQueryBuilder(Work::class);
        return $repo
            ->field('userId')->equals(new \MongoId($id))
            ->field('status')->equals("enabled")
            ->sort('works.comments.likes', 'desc')
            ->limit(3)
            ->getQuery()
            ->execute();
    }

    public function likeIterator($id)
    {
        $repo = $this->entityManager->createQueryBuilder(Work::class);
        return $repo
            ->field('userId')->equals(new \MongoId($id))
            ->field('status')->equals("enabled")
            ->sort('works.likes.createdAt', 'desc')
            ->getQuery()
            ->getSingleResult();
    }

    public function fetchName($id)
    {
        $userRepo = $this->getRepo(User::Class);
        $user = $userRepo->findOneBy(
            [
                '_id' => new \MongoId($id)
            ]
        );
        if (count($user)>0) {
            return $user->getFirstName().' '.$user->getLastName();
        }
        return 'user not found';
    }

    public function getActualComment($id)
    {
        $repo = $this->entityManager->createQueryBuilder(Comments::class);
        $comment = $repo
            ->field('_id')
            ->equals(new \MongoId($id))
            ->field('status')->equals('enabled')
            ->sort('created_at','desc')
            ->getQuery()
            ->getSingleResult();
        return $comment->getComment();
    }

    public function getActualWorkName($id)
    {
        $repo = $this->entityManager->createQueryBuilder(Work::class);
        $comment = $repo
            ->field('_id')
            ->equals(new \MongoId($id))
            ->field('status')->equals("enabled")
            ->getQuery()
            ->getSingleResult();
        return $comment->getTitle();
    }

    public function sortComment($comments)
    {
        $commented = array();
        foreach ($comments as $comment) {
            $data['created_at'] = $comment->getCreateAt()->format('Y/m/d H:i:s');
            $data['comment'] = $comment->getComment();
            $data['userId'] = $comment->getUserId();
            $data['commentId'] = $comment->getId();
            $data['like'] = $comment->getLike();
            $commented[] = $data;
        }

        return $commented;
    }

    public function filterCommentsById($comment)
    {
                $data['id'] = $comment->getId();
                $data['created_at'] = $comment->getCreateAt()->format('Y/m/d H:i:s');
                $data['comment'] = $comment->getComment();
                $data['userId'] = $comment->getUserId();
                $data['workName'] = $this->getActualWorkName($comment->getProductId());
                $data['workId'] = $comment->getProductId();


        return $data;

    }

    /**
     * Method to get interesting Comments
     * @return array
     */
    public function getInterestingComments()
    {

        $moreLikeComments = $this->entityManager->createQueryBuilder(CommentLike::class)
            ->group(array('commentId' => 1), array(
                'total' => 0,
                'commentId' => 'commentId',
                'userId' => 'userId'
            ),
             1,
                array (
                   'limit' => 3,
                    'skip' => 0
                )
            )
            ->reduce('function (obj, prev) {
           prev.total++;
           prev.commentId = obj.commentId;
           prev.userId = obj.userId;
           }')
            ->getQuery()
            ->execute();

        $works = [];

        $moreLikeComments =  $moreLikeComments->getCommandResult()['retval'];
        $moreLikeComments = $this->checkInterestingCommentsFlag($moreLikeComments);
        $interestComment = [];

        if (!empty($moreLikeComments)) {

            usort($moreLikeComments, function ($a, $b) {
                return $b['total'] - $a['total'];
            });

            $moreLikeComments = array_slice($moreLikeComments, 0, 3);

            foreach ($moreLikeComments as $likeComment) {

                $comment = $this->entityManager->createQueryBuilder(Comments::class)
                    ->field("_id")
                    ->equals(new \MongoId($likeComment['commentId']))
                    ->field('type')
                    ->equals('work')
                    ->getQuery()
                    ->getSingleResult();

                if (count($comment) > 0) {
                    $works[] = $this->filterCommentsById($comment);
                }

            }
            foreach ($works as $work) {
                $user = $this->entityManager->createQueryBuilder(User::class)
                    ->field('_id')
                    ->equals(new \MongoId($work['userId']))
                    ->getQuery()
                    ->getSingleResult();

                $interestComment[] =
                    [
                        'comment' => $work['comment'],
                        'commentId' => $work['id'],
                        'createAt' => $work['created_at'],
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                        'userPhoto' => $user->getUserPhoto(),
                        'userId' => $user->getId(),
                        'workName' => $work['workName'],
                        'workId' => $work['workId']
                    ];
            }
        }
        return ['interesting' => $interestComment];
    }

    /**
     * Method will check if the interesting comments were flag
     * @param $interestingComments
     * @return array
     */
    public function checkInterestingCommentsFlag($interestingComments)
    {
        $notFlaggedComment = [];

        foreach ($interestingComments as $interestingComment) {
            $comment = $this->entityManager->createQueryBuilder(Comments::class)
                ->field("_id")
                ->equals(new \MongoId($interestingComment['commentId']))
                ->field('type')
                ->equals('work')
                ->field('flagged')
                ->equals(false)
                ->getQuery()
                ->getSingleResult();

            if (count($comment) > 0) {
                $notFlaggedComment[] = $interestingComment;
            }
        }

        return $notFlaggedComment;
    }

    public function checkEnrolled($uid, $enrolled)
    {
        if (count($enrolled) >0 || !empty($enrolled)) {
            if (in_array($uid, $enrolled)) {
                return true;
            }
        }
        return false;
    }

    public function getLiveSessionHighlight()
    {
        $repo = $this->entityManager->createQueryBuilder(LiveSession::class);
        return $repo
            ->field('highLight')->equals(true)
            ->field('status')->equals('approved')
            ->sort('created_at', 'desc')
            ->limit(1)
            ->getQuery()
            ->getSingleResult();
    }

    public function getWorksHighlight($limit)
    {
        $repo = $this->entityManager->createQueryBuilder(Work::class);
        return $repo
            ->field('highLight')->equals(true)
            ->field('status')->equals("enabled")
            ->sort('upload_at', 'desc')
            ->limit($limit)
            ->getQuery()
            ->execute();
    }

    public function checkOccurringNow($session)
    {
        $now = new \DateTime('now');
        $dateNow = $now->format('m-d');
        $dateThen = $session->getDate()->format('m-d');
        $HrInicio = $session->getStartTime();
        $HrAtual = date('h:i A', strtotime('now'));
        if ($dateNow == $dateThen) {
            if ($HrAtual >= $HrInicio) {
                return true;
            }
        }
        return false;
    }

    public function calcLimit($level)
    {
        $exception = [
            'recruiter',
            'pro',
            'admin',
            'educator',
            'moderator'
        ];
        if (in_array($level, $exception)) {
            return (int)4;
        }
        return (int)3;
    }

    public function timeIso($session) {
        if (!empty($session->getUtcOffset())) {
            $tz = $session->getUtcOffset();
        } else {
            $tz = '+0000';
        }
        $timeIso['date'] = strstr($session->getDate()->format(\DateTime::ISO8601), '+', true).$tz;

        if(!empty($session->getHighLightDate())) {
            $timeIso['highlightDate'] = strstr($session->getHighLightDate()->format(\DateTime::ISO8601), '+', true).$tz;
            return $timeIso;
        }
        $timeIso['highlightDate'] = strstr($session->getCreatedAt()->format(\DateTime::ISO8601), '+', true).$tz;

        return $timeIso;
    }

    public function getHighlights($uid, $level)
    {
        $workLimit = $this->calcLimit($level);
        $highlights = [];
        $session = $this->getLiveSessionHighlight();
        if (count($session) > 0) {
            $workLimit = $this->calcLimit($level)+1;
            $timeIso = $this->timeIso($session);
            $ls = [
                'sessionId' => $session->getId(),
                'name' => $session->getName(),
                'description' => $session->getDescription(),
                'type' => $session->getType(),
                'date' => $session->getDate(),
                'timestamp' => $session->getDate()->getTimestamp(),
                'isoDate' => $timeIso['date'],
                'startTime' => $session->getStartTime(),
                'speaker' => $this->fetchName($session->getSpeaker()),
                'thumbnail' => $session->getThumbnail(),
                'enrolled' => $this->checkEnrolled($uid, $session->getUsers()),
                'occurringNow' => $this->checkOccurringNow($session),
                'highlighted' => !empty($session->getHighLight()) ? $session->getHighLight() : false,
                'highlightedDate' => !empty($session->getHighLightDate()) ? $session->getHighLightDate() : $session->getCreatedAt(),
                'highlightedDateIso' => $timeIso['highlightDate'],
            ];
            $highlights['liveSession'] = $ls;

        }
        $works = $this->getWorksHighlight($workLimit);
        if (count($works) > 0) {
            $wrk = [];
            foreach ($works as $work) {
                $wr = $this->buildWork($work);
                $wr['user'] = $this->getUser($work->getUserId());
                $wrk[] = $wr;
                $highlights['works'] = $wrk;
            }
        }
        return $highlights;
    }

}
