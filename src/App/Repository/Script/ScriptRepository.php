<?php

namespace App\Repository\Script;

use App\Entities\ClassRoom;
use App\Entities\Lab;
use App\Entities\LiveSession;
use App\Entities\Task;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\Group\GroupRepository;
use App\Services\Work\WorkService;

class ScriptRepository extends GroupRepository
{

    /**
     * Method to get all 
     * @return mixed
     */
    public function getAllLabs()
    {
        $labEntity = $this->getRepository()->getRepository(Lab::class);
        return $labEntity->findAll();
    }

    /**
     * Method to get all
     * @return mixed
     */
    public function getAllClassrooms()
    {
        $labEntity = $this->getRepository()->getRepository(ClassRoom::class);
        return $labEntity->findAll();
    }

    /**
     * Method to get all
     * @return mixed
     */
    public function getAllWorks()
    {
        $workEntity = $this->getRepository()->getRepository(Work::class);
        return $workEntity->findAll();
    }

    /**
     * Method to get all
     * @return mixed
     */
    public function getAllLiveSessions()
    {
        $liveSessionkEntity = $this->getRepository()->getRepository(LiveSession::class);
        return $liveSessionkEntity->findAll();
    }
}