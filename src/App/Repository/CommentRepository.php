<?php

namespace App\Repository;

use App\Entities\CommentLike;
use App\Entities\User;
use App\Entities\Work;
use Doctrine\ODM\EntityManager;
use Sokil\Mongo\Exception;
use Doctrine\ODM\MongoDB\Cursor;
use MongoId;

class CommentRepository extends LikeRepository
{
    protected $queryBuilder;

    public function __construct($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
        parent::__construct($queryBuilder);
    }
    

    public function addComment($data)
    {
        $comment = $this->buildAComment($data);
        $work = $this->queryBuilder->getRepository(Work::class)->find($data['workId']);
        if (count($work)>0) {
                $work->setComment($comment);
                $work->setPopular($work->getPopular()+1);
                return $work;
        }

        throw new \Exception('work not found');
    }

    

    public function addLikeOnComment($data)
    {
        $work = $this->queryBuilder
            ->field('_id')->equals(new MongoId($data['productId']))
            ->getQuery()
            ->getSingleResult();
        if (count($work) > 0) {
            $comments = $work->getComment();
            foreach ($comments as $comment) {
                if ($comment->getId() == $data['commentId']) {
                    $comment->setLike($comment->getLike()+1);
                    break;
                }
            }
            $work->updateAllComment($comments);
            return $work;
        }
        return false;
    }

    public function addLike($data)
    {

        $commentLike = new CommentLike();
        $commentLike->setCreatedAt($this->getMongoDate());
        $commentLike->setUserId(new MongoId($data['userId']));
        $commentLike->setProductId(new MongoId($data['productId']));
        $commentLike->setCommentId(new MongoId($data['commentId']));
        return $commentLike;
    }

    public function removeLike($data)
    {

        return $this->queryBuilder
            ->remove()
            ->field('productId')
            ->equals(new MongoId($data['productId']))
            ->field('commentId')
            ->equals(new MongoId($data['commentId']))
            ->field('userId')
            ->equals(new MongoId($data['userId']))
            ->getQuery()
            ->execute();
    }

    public function removeLikeOnWork($data)
    {
        return $this->queryBuilder->updateOne()
            ->field('comments.like')->inc(-1)
            ->field('_id')->equals($data['workId'])
            ->getQuery()
            ->execute();
    }

    /**
     * Method to build an array with comments of the comment collection
     * @param Cursor $commentQuery A cursor with comments objects to cas
     *                             to array
     * @param string $userId       Id of logged user
     * @return array               An array with comments information
     */
    public function buildAGetComment(Cursor $commentQuery, string $userId)
    {
        $entity = $this->queryBuilder;
        $this->setRepository($entity->createQueryBuilder(User::class));
        $commentArray = [];
        foreach ($commentQuery as $comment) {
            $this->setRepository($entity->createQueryBuilder(User::class));
            $user = $this->buildUser($comment->getUserId());
            $this->setRepository($entity->createQueryBuilder(CommentLike::class));

            $liked = $this->searchLikeOnCommentLike($comment->getId());
            $userLiked = false;

            if (count($liked)>0) {
                $userLiked = true;
            }

            $data['commentId'] = $comment->getId();
            if ($comment->getOwner() != null) {
                $data['originalCommentId'] = $comment->getOwner();
            }
            $data['comment'] = $comment->getComment();
            $data['type'] = $comment->getType();
            $data['alreadyFlagged'] = $comment->checkIFlagged($userId);
            $data['productId'] = $comment->getProductId();
            $data['totalLikes'] = count($liked);
            $data['created_at'] = $comment->getCreateAt()->format(\DateTime::ISO8601);
            $data['liked'] = $userLiked;
            $data = array_merge($data, $user);
            $commentArray[] = $data;
        }
        return $commentArray;
    }


    /** Method to increment more on on comments ssssssssss
     * @param $id
     * @return bool
     */
    public function incrementComment($id)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('comment')
            ->inc(1)
            ->field("_id")
            ->equals(new MongoId($id))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to delete comments by productId(workId,postId)
     * @param $productId
     * @return bool
     */
    public function deleteCommentsByProductId($productId)
    {
        $this->queryBuilder
            ->update()
            ->field('status')
            ->set('disabled')
            ->field('productId')
            ->equals(new MongoId($productId))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to get CommentIds by productId
     * @param $productId
     * @return array|int
     */
    public function getIdCommentsByProductId($productId)
    {
        $comments =  $this->queryBuilder
            ->field('productId')
            ->equals(new MongoId($productId))
            ->getQuery()
            ->execute();



        if (count($comments) >0) {
            $commentIds = array();
            foreach ($comments as $comment) {
                $commentIds[] = $comment->getId();
            }
            return $commentIds;
        }
        return false;
    }

    /** Method to delete comments by productId(workId,postId)
     * @param $commentId
     * @return bool
     */
    public function deleteReplyCommentsByCommentId($commentId)
    {
        $this->queryBuilder
            ->update()
            ->field('status')
            ->set('disabled')
            ->field('commentId')
            ->equals(new MongoId($commentId))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to set flagged field how false on a Comment
     * @param $data
     */
    public function removeFlag($data)
    {
        $this->queryBuilder
            ->updateOne()
            ->field("flagged")
            ->set(false)
            ->field("_id")
            ->equals(new MongoId($data['productId']))
            ->getQuery()
            ->execute();
    }

    public function checkWorkComment($data)
    {
        $workCount = $this->queryBuilder
            ->field("productId")
            ->equals(new MongoId($data['id']))
            ->field("userId")
            ->equals(new MongoId($data['userId']))
            ->getQuery()
            ->execute();
        return count($workCount);
    }

    /** Method to unset all flags on a comment
     * @param string $commentId id of the comment on database
     * @return null
     */
    public function resetFlag(string $commentId)
    {
        $this->getRepository()
            ->updateMany()
            ->field('flag')->unsetField()->exists(true)
            ->field('_id')
            ->equals(new MongoId($commentId))
            ->getQuery()
            ->execute();
    }
}