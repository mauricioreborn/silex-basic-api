<?php

namespace App\Repository;

use App\Entities\Flag;
use App\Entities\FlagContent;
use \MongoId;

class FlagRepository extends Repository
{
    /**
     * Method to check if a product was flagged
     * @param string $productId if of the work or comment
     * @return Flag Object 
     */
    public function getFlag(string $productId)
    {
        return $this->getRepository()
            ->field('productId')
            ->equals(new MongoId($productId))
            ->field('status')
            ->notEqual(false)
            ->getQuery()
            ->execute();
    }

    /**
     * Method to create a flag content object
     * @param string $userId  id of who want to flag
     * @return A FlagContent object with date userId and status to set on work
     * or comment.
     */
    public function createFlagContent(string $userId)
    {
        $flagContent = new FlagContent();
        $flagContent->setCreatedAt($this->getMongoDate());
        $flagContent->setUserId(new MongoId($userId));
        $flagContent->setStatus(true);
        return $flagContent;
    }
}