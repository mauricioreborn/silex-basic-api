<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 18/04/17
 * Time: 11:34
 */

namespace App\Repository;

use App\Entities\User;

class ForgotPasswordRepository extends Repository
{

    private $entityManager;
    private $repository;
    protected $message;
    protected $status;


    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(User::class);
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




    public function getRepository()
    {
        return $this->repository;
    }

    public function validateToken($userId, $token, $expiration)
    {
        $user = $this->repository->findOneBy(
            [
                '_id' => new \MongoId($userId)
            ]
        );
        if (!$expiration >= time()) {
            return 'expired';
        }
        if (count($user) === 0) {
            return 'cu';
        }
        if ($user->getTokenPass()) {
            if ((string)$user->getTokenPass() === (string)$token) {
                return ['message' => 'Token Valid', 'email' => $user->getEmail()];
            }
            return false;
        }
    }

    public function getUser($email)
    {
        $fetchToken = $this->repository->findOneBy(
            [
                'email' => $email,
            ]
        );
        return $fetchToken;
    }

    public function clearToken($user)
    {
        $user->setTokenPass('');
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function createToken($user, $newToken)
    {
        $user->setTokenPass($newToken);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function updatePassword($id, $password)
    {
        $fetchUser = $this->repository->findOneBy(
            [
                '_id' => $id,
            ]
        );
        $fetchUser->setPassword(md5($password));
        $this->entityManager->persist($fetchUser);
        $this->entityManager->flush();
        return $fetchUser->getPassword();
    }



}