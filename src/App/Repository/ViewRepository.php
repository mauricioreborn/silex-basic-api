<?php

namespace App\Repository;

use App\Entities\View;
use App\Entities\Work;

class ViewRepository extends Repository
{
    public function verifyIfWorkWasViewed($data)
    {
        return $this->getRepository()
            ->field('userId')
            ->equals(new \MongoId($data['userId']))
            ->field('workId')
            ->equals(new \MongoId($data['workId']))
            ->getQuery()
            ->getSingleResult();
    }

    public function addViewOnWork($data)
    {
         return $this->getRepository()
             ->updateOne()
            ->field('_id')
            ->equals(new \MongoId($data['workId']))
            ->field('views')->inc(1)
             ->field('popular')->inc(1)
            ->getQuery()
            ->execute();
    }

    public function addView($data)
    {
        $view = new View();
        $view->setUserId($data['userId']);
        $view->setWorkId($data['workId']);
        return $view;
    }
}