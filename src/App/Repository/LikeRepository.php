<?php

namespace App\Repository;

use App\Entities\Like;

class LikeRepository extends Repository
{
    public function addLike($data)
    {
        $like = new Like();
        $like->setUserId($data['userId']);
        $like->setWorkId($data['workId']);
        $like->setCreatedAt($this->getMongoDate());
        $work = $this->getRepository()
                ->field('_id')->equals(new \MongoId($data['workId']))
            ->getQuery()
                ->getSingleResult();
        if (count($work)>0) {
            $work->setLikes($like);
            $work->setPopular($work->getPopular()+1);
            return $work;
        }
        return false;
    }

    public function searchLikeByUserId($data)
    {
        
        return $this->getRepository()
            ->field('likes.userId')
            ->equals(new \MongoId($data['userId']))
            ->field('likes.workId')
            ->equals(new \MongoId($data['workId']))
            ->getQuery()
            ->getSingleResult();
    }


    public function searchLikeByUserIdOnPost($data)
    {
        return $this->getRepository()
            ->field('likes.userId')
            ->equals(new \MongoId($data['userId']))
            ->field('likes.productId')
            ->equals(new \MongoId($data['productId']))
            ->getQuery()
            ->getSingleResult();
    }

    /** Method to find a user on commentLike entity
     * @param $commentId
     * @return mixed
     */
    public function searchLikeOnCommentLike($commentId)
    {
        return $this->getRepository()
            ->field('commentId')
            ->equals(new \MongoId($commentId))
            ->getQuery()
            ->execute();
    }



    
    public function deleteWorkLike($data)
    {
        $work = $this->getRepository()
            ->field('_id')->equals(new \MongoId($data['workId']))->getQuery()
            ->getSingleResult();
        if (count($work)>0) {
            $work->deleteLike($data['userId']);
            $work->setPopular($work->getPopular() - 1);
        }
        return $work;
    }


    public function deletePostLike($data)
    {
        $work = $this->getRepository()
            ->field('_id')->equals(new \MongoId($data['productId']))->getQuery()
            ->getSingleResult();
        if (count($work)>0) {
            $work->deleteLike($data['userId']);
            $work->setPopular($work->getPopular() - 1);
        }
        return $work;
    }

    public function buildLike($data)
    {
        $like = new Like();
        if (isset($data['productId'])) {
            $like->setProductId(new \MongoId($data['productId']));
        }

        if (isset($data['workId'])) {
            $like->setWorkId(new \MongoId($data['workId']));
        }
        $like->setUserId(new \MongoId($data['userId']));
        $like->setCreatedAt($this->getMongoDate());
        return $like;
    }

}