<?php

namespace App\Repository;


use App\Entities\LiveSession;
use App\Entities\User;
use App\Services\Work\Refine\Month;

class EventRepository extends Repository
{
    protected $message;
    protected $status;

    public function fetchName($id)
    {
        $userRepo = $this->getRepository()->getRepository(User::Class);
        $user = $userRepo->findOneBy(
            [
                '_id' => new \MongoId($id)
            ]
        );

        if (count($user)>0) {

            return $user->getFirstName() . ' ' . $user->getLastName();
        }
    }

    public function findByRange($queryBuilder, $data, $bol)
    {
        $dt = new \DateTime(date('Y-m-d', strtotime($data['startDate'])), new \DateTimeZone('UTC'));
        $fdt = new \DateTime(date('Y-m-d', strtotime($data['endDate'])), new \DateTimeZone('UTC'));
        $fdt->modify("+1 day");
        $ts = $dt->getTimestamp();
        $firstDayOfMonth = new \MongoDate($ts);
        $ts2 = $fdt->getTimestamp();

        $lastDayOfMonth = new \MongoDate($ts2);

        return $queryBuilder->field('date')->range($firstDayOfMonth, $lastDayOfMonth)->field('approved')->equals($bol);
    }


    public function findByMonth($queryBuilder, $data, $bol)
    {
        $day =  new \DateTime(date('Y-m-1', strtotime($data['endDate'])), new \DateTimeZone('UTC'));
        $day = $day->format("Y-m-d");

        $dt = new \DateTime(date('Y-m-1', strtotime($data['startDate'])), new \DateTimeZone('UTC'));

        $fdt = new \DateTime(date('Y-m-t', strtotime($data['endDate'])), new \DateTimeZone('UTC'));

        if ($day >= 30) {
            $fdt = new \DateTime(date('Y-m-1', strtotime($data['startDate'])), new \DateTimeZone('UTC'));
            $fdt->modify("+1 month");
        }

        $ts = $dt->getTimestamp();
        $firstDayOfMonth = new \MongoDate($ts);
        $ts2 = $fdt->getTimestamp();

        $lastDayOfMonth = new \MongoDate($ts2);

        return $queryBuilder->field('date')->range($firstDayOfMonth, $lastDayOfMonth)->field('approved')->equals($bol)
            ->field('status')->equals('approved');
    }

    public function timeIso(LiveSession $session)
    {
        if (!empty($session->getUtcOffset())) {
            $tz = $session->getUtcOffset();
        } else {
            $tz = '+0000';
        }

        $dates['isoDate'] = strstr($session->getDate()->format(\DateTime::ISO8601), '+', true) . $tz;

        if (!is_null($session->getDeadLineDate())) {
            $dates['deadlineDate'] = strstr($session->getDeadLineDate()->format(\DateTime::ISO8601), '+', true) . $tz;
        }

        return $dates;
    }

    public function buildEvents(LiveSession $session)
    {
        $dates = $this->timeIso($session);
        return [
            'sessionId' => $session->getId(),
            'name' => $session->getName(),
            'description' => $session->getDescription(),
            'type' => $session->getType(),
            'date' => $session->getDate(),
            'timestamp' => $session->getDate()->getTimestamp(),
            'isoDate' => $dates['isoDate'],
            'startTime' => $session->getStartTime(),
            'speaker' => $this->fetchName($session->getSpeaker()),
            'speakerId' => $session->getSpeaker(),
            'moderatorId' => $session->getModerator(),
            'thumbnail' => $session->getThumbnail(),
            'occurringNow' => $this->checkOccurringNow($session),
            'approved' => $session->getApproved(),
            'status' => $session->getStatus(),
            'works' => $session->getWorkQueue(),
            'highlighted' => !empty($session->getHighLight()) ? $session->getHighLight() : false,
            'deadlineStatus' => !is_null($session->getDeadLineStatus()) ? $session->getDeadLineStatus() : false,
            'deadlineDate' => isset($dates['deadlineDate']) ? $dates['deadlineDate'] : false,
            'deadlineTime' => !is_null($session->getDeadLineTime()) ? $session->getDeadLineTime() : false
        ];
    }

    public function buildCalendar(LiveSession $session)
    {
        $data['date'] = $session->getDate()->format('Y-m-d');
        $calendar[] = $data['date'];
        return $calendar;
    }

    public function checkOccurringNow($session)
    {
        if ($session->getStatus() == 'ended') {
            return false;
        }
        $dates = $this->timeIso($session);
        $now = time();
        $sessionStart = (new \DateTime($dates['isoDate']))->getTimestamp();
        $delta = $now - $sessionStart;
        $anticipation = -15*60; // 15 minutes before, count as started so people can wait in the room
        $maxDuration = 120*60; // 2 hours later, it should count as finished
        return $delta >= $anticipation && $delta <= $maxDuration;
    }

    public function checkEnrolled($uid, $enrolled)
    {

        if (!empty($enrolled)) {
            if (in_array($uid, $enrolled)) {
                return true;
            }
        }
        return false;
    }

    public function checkOccurring($sessions, $uid)
    {
        $oevents = array();
        $oevents['total'] = count($sessions);
        foreach ($sessions as $session) {
            $now = new \DateTime('now');
            $dateNow = $now->format('m-d');
            $dateThen = $session->getDate()->format('m-d');
            $HrInicio = $session->getStartTime();
            $HrAtual = date('h:i A', strtotime('now'));
            if ($dateNow == $dateThen) {
                if ($HrAtual >= $HrInicio) {
                    $newArray = $this->buildEvents($session);
                    $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
                    $newArray['enrollCounter'] = count($session->getUsers());
                    $newArray['occurringNow'] = true;
                    $oevents['oevents'][] = $newArray;
                }
            }
        }
        return $oevents;
    }

    /** Method to get EventList
     * @param $data[startDate,endDate]
     * @param $uid
     * @return array
     */
    public function getEventList($data, $uid)
    {
        $repo = $this->queryBuilder->createQueryBuilder(LiveSession::class);
        $events = $this->findByRange($repo, $data, true)
            ->field('status')->in(array('approved', 'ended'))
            ->getQuery()->execute()->skip($data['skip'])->limit($data['limit']);

        $listEvents = array();
        $nevents = array();
        $calendar = array();

        foreach ($events as $session) {
            $newArray = $this->buildEvents($session);
            $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
            $newArray['enrollCounter'] = count($session->getUsers());
            $nevents[] = $newArray;
        }

        $eventsOfMonth = $this->findByMonth($repo, $data, true)->getQuery()->execute();

        foreach ($eventsOfMonth as $calenderEvent) {
            $data['date'] = $calenderEvent->getDate()->format('Y-m-d');
            $calendar[] = $data['date'];
        }

        $checkEvents['oevents'] = isset($checkEvents['oevents']) ? $checkEvents['oevents'] : [];
        $listEvents['sessions'] = $this->filterEvents($nevents, array_column($checkEvents['oevents'], 'sessionId'));
        $listEvents['totalResults'] = count($events);
        $listEvents['calendar'] = array_unique($calendar);


        return $listEvents;
    }

    public function filterEvents($nevents, $ids)
    {
        if (count($nevents) >0) {
            foreach ($nevents as $idx => $event) {
                if (in_array($event['sessionId'], $ids)) {
                    unset($nevents[$idx]);
                }
            }
        }
        return $nevents;
    }

    /** Method to getEventEnrolledListByRangeDates
     * @param $data[startDate,endDate]
     * @param $uid
     * @return array
     */
    public function getEventEnrolledList($data, $uid)
    {
        $repo = $this->getRepository()->createQueryBuilder(LiveSession::class);
        $events = $this->findByRange($repo, $data, true)
          ->field('status')->equals('approved')
          ->getQuery()->execute();
        $listEvents = array();
        $nevents = array();
        $oevents = array();
        $calendar = array();
        foreach ($events as $session) {
            if ($this->checkEnrolled($uid, $session->getUsers())) {
                $now = new \DateTime('now');
                $dateNow = $now->format('m-d');
                $dateThen = $session->getDate()->format('m-d');
                $HrInicio = $session->getStartTime();
                $HrAtual = date('h:i A', strtotime('now'));
                if ($dateNow == $dateThen) {
                    if ($HrAtual >= $HrInicio) {
                        $newArray = $this->buildEvents($session);
                        $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
                        $newArray['enrollCounter'] = count($session->getUsers());
                        $nevents[] = $newArray;
                    } else {
                        $newArray = $this->buildEvents($session);
                        $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
                        $newArray['enrollCounter'] = count($session->getUsers());
                        $nevents[] = $newArray;
                    }

                } else {
                    $newArray = $this->buildEvents($session);
                    $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
                    $newArray['enrollCounter'] = count($session->getUsers());
                    $nevents[] = $newArray;
                }
            }
            $newArray = $this->buildCalendar($session);
            $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
            $newArray['enrollCounter'] = count($session->getUsers());


        }

        $events = $this->findByMonth($repo, $data, true)->getQuery()->execute();
        foreach ($events as $event) {
            if ($this->checkEnrolled($uid, $event->getUsers())) {
                $calendar[] = $event->getDate()->format('Y-m-d');
            }
        }
        $listEvents['sessions'] = $nevents;
        $listEvents['occurringNow'] = $oevents;
        $listEvents['calendar'] = $calendar;
        return $listEvents;
    }

    public function setEnroll($eid, $uid)
    {
        $sessionRepo  = $this->getRepository()->getRepository(LiveSession::Class);
        $session = $sessionRepo->findOneBy(
            [
                '_id' => new \MongoId($eid)
            ]
        );
        if (count($session) === 0) {
            return 0;
        }
        if (count($session->getUsers()) || !empty($session->getUsers())) {
            if (in_array($uid, $session->getUsers())) {
                return false;
            }
        }
        $session->setUsers($uid);
        $this->getRepository()->persist($session);
        $this->getRepository()->flush();
        return true;
    }

    public function setUnEnroll($eid, $uid)
    {
        $sessionRepo  = $this->getRepository()->getRepository(LiveSession::Class);
        $session = $sessionRepo->findOneBy(
            [
                '_id' => new \MongoId($eid)
            ]
        );

        if (count($session) === 0) {
            return 0;
        }

        if (in_array($uid, $session->getUsers())) {
            $session->deleteEnroll($uid);
            $this->getRepository()->persist($session);
            $this->getRepository()->flush();
            return true;
        }
        return false;
    }

    /** Method to remove an work of a events using an user ID
     * @param $eventId
     * @param $userId
     * @return string
     */
    public function setUnEnrollWork($eventId, $userId)
    {
        $liveSessionRepository = $this->getRepository()->getRepository(LiveSession::Class);
        $liveSession = $liveSessionRepository->findOneBy(
            [
                '_id' => new \MongoId($eventId)
            ]
        );

        $message = "This user does not have works to remove";

        if(!empty($liveSession->getWorkQueue()) || $liveSession->getWorkQueue() != null) {
            foreach($liveSession->getWorkQueue() as $liveSessionWork) {
                if($liveSessionWork["userId"] == $userId || $liveSessionWork["ownerWorkId"] == $userId) {
                    $liveSession->deleteUserWorkQueue($userId);
                    $this->getRepository()->persist($liveSession);
                    $this->getRepository()->flush();
                    $message = "Work(s) removed";
                }
            }
        } else {
            $message = "This event has not works.";
        }

        return $message;
    }

    public function buildEnrolledUsers($session)
    {
        $listUser = array();
        if (count($session) >0) {
            foreach ($session as $userId) {
                $sessionRepo  = $this->getRepository()->getRepository(User::Class);
                $user = $sessionRepo->findOneBy(
                    [
                        '_id' => new \MongoId($userId)
                    ]
                );
                $listUser[] =
                    [
                        'id' => $user->getId(),
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                        'userPhoto' => $user->getUserPhoto(),
                    ];
            }
        }
        return $listUser;
    }

    public function getEventDetails($eid, $uid)
    {
        $sessionRepo  = $this->getRepository()->getRepository(LiveSession::Class);
        $session = $sessionRepo->findOneBy(
            [
                '_id' => new \MongoId($eid)
            ]
        );
        if (count($session) === 0) {
            return 0;
        }
        $newArray = $this->buildEvents($session);
        $newArray['enrolled'] = $this->checkEnrolled($uid, $session->getUsers());
        $newArray['enrollCounter'] = count($session->getUsers());
        $newArray['enrolledUsers'] = $this->buildEnrolledUsers($session->getUsers());
        return $newArray;
    }

    /**
     * Method to set the deadline of an event
     * @param array $deadLineInfo
     * @return string
     */
    public function setDeadLine(array $deadLineInfo)
    {
        $liveSessionRepository = $this->getRepository()->getRepository(LiveSession::Class);
        $liveSession = $liveSessionRepository->findOneBy([
            '_id' => new \MongoId($deadLineInfo['eventId'])
        ]);
        unset($deadLineInfo['eventId']);
        $message = "This event was not found.";

        if (count($liveSession) > 0) {
            $liveSession->setDeadLineStatus($deadLineInfo['deadlineStatus']);
            if (isset($deadLineInfo['deadlineDate'])) {
                $liveSession->setDeadLineDate($deadLineInfo['deadlineDate']);
                $liveSession->setDeadLineTime($deadLineInfo['deadlineTime']);
            }
            $this->getRepository()->persist($liveSession);
            $this->getRepository()->flush();
            $message = "DeadLine was changed";
        }

        return $message;
    }

}
