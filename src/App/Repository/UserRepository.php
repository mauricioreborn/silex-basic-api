<?php

namespace App\Repository;

use App\Entities\Notification;
use App\Entities\User;
use App\Entities\UserLike;
use App\Entities\Work;
use App\Services\UserFactory;

class UserRepository extends Repository
{

    /**
     * @return array|string
     */
    public function getUsers()
    {
        $users =  $this->queryBuilder
            ->getQuery()
            ->execute();
        $userWithNameAndId ='';
        foreach ($users as $user) {
            $data['name'] = $user->getFirstName();
            $data['email'] = $user->getEmail();
            $data['lastName'] = $user->getLastName();
            $data['avatar'] = $user->getUserPhoto();
            $data['id'] = $user->getId();
            $data['level'] =  $user->getLevel()->getAlias();
            $userWithNameAndId[] = $data;
        }
        return $userWithNameAndId;
    }

    /* Expects an array of user ids; returns an array of arrays with [user id, name] */
    public function resolveUserNames($ids)
    {
        if (!is_array($ids)) {
            $ids = array($ids);
        }

        $cursor = $this->queryBuilder->field('_id')->in($ids)->getQuery()->execute();
        $users = array();
        foreach ($cursor as $user) {
              $users[] = array(
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'id' => $user->getId(),
              );
        }
        return $users;
    }

    /**
     * Method to get a user by email.
     * @param string $userEmail A string that is the email of the user to be
     *                          returned.
     * @return mixed            An object of user.
     */
    public function getUserByEmail(string $userEmail)
    {
        $userQueryBuilder= $this->getRepository()->createQueryBuilder(
            User::class
        );

        $user = $userQueryBuilder
            ->field('email')
            ->equals($userEmail)
            ->getQuery()
            ->getSingleResult();

        return $user;
    }

    /**
     * Method to search users by name and last name
     * @param $searchData
     * @return array
     */
    public function searchUsers($searchData)
    {
        $userFactory = new UserFactory($this->getRepository());
        $users = $userFactory->searchAllFilters($searchData);
        $usersFound = [];

        if (count($users) > 0) {
            foreach ($users as $user) {
                $usersFound[] = $this->buildUserByObject($user);
            }
        }

        return $usersFound;
    }

    public function addLike($data)
    {
        $userLike = new UserLike();
        $userLike->setWorkId(new \MongoId($data['workId']));
        $userLike->setCreatedAt($this->getMongoDate());

        $user = $this->queryBuilder->field('_id')
            ->equals(new \MongoId($data['userId']))->getQuery()->getSingleResult();
        if (count($user)>0) {
            $user->setLikes($userLike);
            return $user;
        }
        return false;
    }

    /**
     * Method to update workActivity
     * @param $userId
     */
    public function updateActivity($userId)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('lastActivity')->set($this->getMongoDate())
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
    }

    /** Method to remove a like
     * @param $data
     * @return bool
     */
    public function removeLike($data)
    {
       $user = $this->queryBuilder
            ->field('likes.owner')
            ->equals(new \MongoId($data['userId']))
            ->field('likes.workId')
            ->equals(new \MongoId($data['workId']))
            ->getQuery()
            ->getSingleResult();

        if (count($user)>0) {
            $user->removeLike($data['workId']);
            return $user;
        }
        return false;
    }


    /**
     * Method to remove a like on a post
     * @param $data
     * @return bool
     */
    public function removePostLike($data)
    {
        $user = $this->queryBuilder
            ->field('likes.userId')
            ->equals(new \MongoId($data['userId']))
            ->field('likes.productId')
            ->equals(new \MongoId($data['productId']))
            ->getQuery()
            ->getSingleResult();

        if (count($user)>0) {
            $user->removeLike($data['workId']);
            return $user;
        }
        return false;
    }


    /** Method to save a notification
     * @param  $notification
     * @param $id
     * @return User object
     */
    public function setNotification(Notification $notification, $id)
    {
        $user = $this->queryBuilder
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();

        $user->setNotifications($notification);
        return $user;
    }

    /** Method to get notificationBy userId
     * @param $notificationObject
     * @return array
     */
    public function getNotification($notificationObject)
    {
        $notifications = array();
        $notRead = '';
        $unseen = false;
        $data= array();

        if (count($notificationObject)>0) {
            foreach ($notificationObject as $notification) {
                $responsibleUser = '';
                if ($notification->getType() != 'flag') {
                    $responsibleUser = $this->getUserName($notification->getResponsibleUser());
                }
                    $data['message'] = $responsibleUser." ". $notification->getMessage();
                    $data['productName'] = $this->getWorkNameById($notification->getProductId());
                    $data['createdAt'] = $notification->getCreatedAt();

                $internUnseen = false;
                if ($notification->getStatus() == 'unread') {
                        $notRead++;
                        $internUnseen = true;
                }
                $data['status'] = $notification->getStatus();
                $data['type'] = $notification->getType();
                $data['action'] = $notification->getAction();
                $data['productId'] = $notification->getProductId();
                $data['id'] = $notification->getId();
                $data['unseen'] = $internUnseen;
                $data['responsibleUserId'] = $notification->getResponsibleUser();
                $notifications[] = $data;
            }
        }
        return $notifications;
    }

    /** Method to set a notification how read
     * @param $data[userId,notificationId]
     * @return bool
     */
    public function setReadNotification($data)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('status')
            ->set('read')
            ->field('_id')
            ->equals(new \MongoId($data['notificationId']))
            ->getQuery()
            ->execute();

        return  true;
    }

    /** Method to get a notification by Id
     * @param $id
     * @return mixed
     */
    public function getNotificationById($id)
    {
        return $this->queryBuilder
            ->field('_id')
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();
    }

    /** Method to a UserName of an User by your Id
     * @param $userId
     * @return string
     */
    public function getUserName($userId)
    {
        $queryBuilder = $this->queryBuilder->createQueryBuilder(User::class);
        $user = $queryBuilder
            ->field('_id')
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();
        return $user->getFirstName()." ".$user->getLastName();
    }

    /** desperate method to get workNameById
     * @param $workId
     * @return mixed
     */
    public function getWorkNameById($workId)
    {
        $queryBuilder = $this->queryBuilder->createQueryBuilder(Work::class);
        $work = $queryBuilder
            ->field('_id')
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->getSingleResult();
        if (count($work)>0) {
            return $work->getTitle();
        }
        return '';
    }

    /**Method to update more one after an work upload
     * @param $userId
     */
    public function updateUserTotalWorkCount($userId)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('totalWork')
            ->inc(1)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
    }

    /**Method to update more one after an new user comment
     * @param $userId
     */
    public function updateUserTotalCommentCount($userId)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('totalComment')
            ->inc(1)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
    }

    /** Method to set totalComment as zero when a work was reviewed
     * @param $userId
     */
    public function removeAllTotalComment($userId)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('totalComment')
            ->set(0)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
    }

    /** Method to set totalWork as zero when a work was reviewed
     * @param $userId
     */
    public function removeAllTotalWork($userId)
    {
        $this->queryBuilder
            ->updateOne()
            ->field('totalWork')
            ->set(0)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
    }

    /** Method to set a custom notification how read.
     * @param $data
     * @return boolean
     */
    public function setNoteHowRed($data)
    {
        $this->getRepository()
            ->updateOne()
            ->field("notes")
            ->push($data['notificationId'])
            ->field("_id")
            ->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to set review notification how ready
     * @param $userId
     * @return bool
     */
    public function setAlreadyNotified($userId)
    {
        $this->getRepository()
            ->updateOne()
            ->field("alreadyNotified")
            ->set(true)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to verify if an user already see a review notification
     * @param $userId
     * @return bool
     */
    public function verifyAlreadyNotification($userId)
    {
        $user = $this->getRepository()
            ->field("alreadyNotified")
            ->equals(false)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();

        if (count($user)>0) {
            return true;
        }
        return false;
    }

    public function verifyFirstReview($userId)
    {
        $user = $this->getRepository()
            ->field("firstReview")
            ->equals(true)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();

        if (count($user)>0) {
            return true;
        }
        return false;
    }


    /** Method to update firstReview
     * @param $userId
     * @param $status
     * @return bool
     */
    public function updateFirstReview($userId, $status)
    {
        $this->getRepository()
            ->updateOne()
            ->field("firstReview")
            ->set($status)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
        return true;
    }

    /** Method to return if an user already join on a lab or classroom
     * @param $userId
     * @param $groupId
     * @return mixed
     */
    public function verifyAlreadyJoin($userId, $groupId)
    {
        $result = $this->queryBuilder
            ->addOr($this->queryBuilder->expr()->field('classroom.id') ->in(array($groupId)))
            ->addOr($this->queryBuilder->expr()->field('lab.id') ->in(array($groupId)))
            ->field("_id")
            ->equals(new \MongoId($userId))
        ->getQuery()
        ->getSingleResult();

        if ($result) {
            return $result->getClassRoom();
        }

        return [];
    }
    
    /** Method to return the users that are members of a class
     * @param $classroomId
     * @return mixed
     */
    public function getUserByClassId($classroomId)
    {
        return $this->queryBuilder
            ->addOr($this->queryBuilder->expr()->field('classroom.id') ->in(array($classroomId)))
            ->getQuery()
            ->execute();
    }

    /** Method to increment more one on totalCoWork
     * @param $userId
     * @return bool
     */
    public function incrementCoWork($userId)
    {
        $this->getRepository()
            ->updateOne()
            ->field("totalCoWork")
            ->inc(1)
            ->field("_id")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();
        return true;
    }
}
