<?php

namespace App\Repository;

use App\Entities\Notes;

class NotesRepository extends Repository
{

    /**
     * Method to build a Note entity
     * @param $data[message,userId,title,userId,group]
     * @return Notes
     */
    public function notesBuild($data)
    {
        $notes = new Notes();
        $notes->setStatus(true);
        $notes->setMessage($data['message']);
        $notes->setTitle($data['title']);
        $notes->setUserId(new \MongoId($data['userId']));
        $notes->setCreatedAt($this->getMongoDate());
        foreach ($data['group'] as $group) {
            $notes->setGroup($group);
        }
        return $notes;
    }

    public function getAllNotes()
    {
        return $this->queryBuilder
            ->field("status")
            ->equals(true)
            ->sort("createdAt", "desc")
            ->getQuery()
            ->execute();
    }

    /** Method to get a announcement by Id
     * @param $id
     * @return mixed
     */
    public function getNoteById($id)
    {
        return $this->queryBuilder
            ->field("status")
            ->equals(true)
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();
    }

    /** Method to build a note with a NoteEntity
     * @param Notes $note
     * @return mixed
     */
    public function getNote(Notes $note)
    {
        $data['message'] = $note->getMessage();
        $data['title'] = $note->getTitle();
        $data['createdAt'] = $note->getCreatedAt()->format("Y-m-d:h:m:s");
        $data['id'] = $note->getId();
        $data['group'] = $note->getGroup();
        return $data;
    }

    public function noteRemove($noteId)
    {
        $this->queryBuilder
            ->remove()
            ->field("_id")
            ->equals(new \mongoId($noteId))
            ->getQuery()
            ->execute();
    }
}