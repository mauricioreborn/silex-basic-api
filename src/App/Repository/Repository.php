<?php

namespace App\Repository;

use App\Entities\Comment;
use App\Entities\Comments;
use App\Entities\ReplyComment;
use App\Entities\User;
use App\Entities\Work;

abstract class Repository
{

    protected $queryBuilder;
    private $status;
    private $message;
    private $error;

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error[] = $error;
    }


    public function __construct($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;

    }

    public function getMongoDate($date = null)
    {
        $date = $date == null ? "Y-m-d H:i:s" : $date;
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        return $this->today;
    }

    /** Method to build a mongodate by date formated MM/DD/YYYY as string
     * @param $date
     * @return \MongoDate
     */
    public function getMongoDateByDate($date)
    {
        $newDate = date('Y-m-d', strtotime($date));
        $dt = new \DateTime($newDate, new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        return new \MongoDate($ts);
    }
    
    public function getRepository()
    {
        return $this->queryBuilder;
    }

    /**
     * Method to get path and type of the file entity.
     * @param $file
     * @return array|string
     */
    public function getFile($file)
    {
        $files ='';
        foreach ($file as $data) {
            $inc['path'] = $data->getPath();
            $inc['type'] = $data->getType();
            $inc['name'] = '';
            if (!empty($data->getName())) {
                $inc['name'] = $data->getName();
            }
            $files[] = $inc;
        }
        return $files;
    }

    public function sortComment($comments)
    {
        $commented = '';
        foreach ($comments as $comment) {
            $data['created_at'] = $comment->getCreateAt()->format('Y/m/d H:i:s');
            $data['comment'] = $comment->getComment();
            $data['userId'] = $comment->getUserId();
            $data['commentId'] = $comment->getId();
            $commented[] = $data;
        }
        usort($commented, function ($a, $b) {
            return strtotime($a['created_at']) - strtotime((string)$b['created_at']);
        });
        return $commented;
    }

    public function buildUserByObject(User $user)
    {
        $userBuilded['firstName'] = $user->getFirstName();
        $userBuilded['id'] = $user->getId();
        $userBuilded['email'] = $user->getEmail();
        $userBuilded['lastName'] = $user->getLastName();
        $userBuilded['level'] = ucfirst($user->getLevel()->getAlias());
        $userBuilded['avatar'] = $user->getUserPhoto();
        $userBuilded['upload_at'] = $user->getCreatedAt();
        $userBuilded['lastActivity'] = $user->getLastActivity();
        $userBuilded['community'] = $user->getCommunity();
        $userBuilded['status'] = $user->getStatus();
        $userBuilded['phone'] = '';
        switch ($userBuilded['level']) {
            case 'Student Level 1':
                $acl = 'studentLevel1';
                break;
            case 'Student Level 2':
                $acl = 'studentLevel2';
                break;
            case 'Student Level 3':
                $acl = 'studentLevel3';
                break;
            default:
                $acl = $userBuilded['level'];
        }
        $userBuilded['acl'] = $acl;
        if (!empty($user->getPhone())) {
            $userBuilded['phone'] = $user->getPhone();
        }
        $userBuilded['birthDate'] = '';
        if (!empty($user->getBirthDate())) {
            $userBuilded['birthDate'] = $user->getBirthDate();
        }
        $userBuilded['approval'] = $user->getState();
        $userBuild['user'] = $userBuilded;
        return $userBuild;
    }

    /**
     * This method will receive a work object and return a work as array
     * @param Work $work An object of Entity\Work
     * @return array     An array of the object Work
     */
    public function buildWork(Work $work)
    {
        $timeZone = !empty($work->getUtcOffset()) ?
            $work->getUtcOffset() :
            '+0000';
        $timeIso = !empty($work->getHighLightDate()) ?
                    strstr($work->getHighLightDate()->format(\DateTime::ISO8601), '+', true) . $timeZone :
                    strstr($work->getUploadAt()->format(\DateTime::ISO8601), '+', true) . $timeZone;


        return [
                'id' => $work->getId(),
                'thumbnail' => $this->getFile($work->getThumbnail()),
                'mainFile' => $this->getFile($work->getMainFile()),
                'totalLikes' => count($work->getLikes()),
                'totalViews' => empty($work->getViews()) ?
                    0 :
                    $work->getViews(),
                'totalComments' =>  $this->getTotalComments(
                    $this->queryBuilder->createQueryBuilder(Comments::class),
                    $work->getId()
                ),
                'title' => $work->getTitle(),
                'path' => $this->getFile($work->getAttach()),
                'upload_at' => $work->getUploadAt(),
                'workLevel' => $work->getLevel()->getAlias(),
                'description' => $work->getDescription(),
                'reviewed' => $work->getReviewed(),
                'group' => $work->getGroup(),
                'availableReview' => $work->getAvailable(),
                'highlighted' => !empty($work->getHighLight()) ?
                    $work->getHighLight() :
                    false,
                'highlightedDate' => !empty($work->getHighLightDate()) ?
                    $work->getHighLightDate() :
                    $work->getUploadAt(),
                'highlightedDateIso' => $timeIso,
                'labId' => !is_null($work->getLabId()) ?
                    $work->getLabId() :
                    false,
                'isPrivate' => !is_null($work->getIsPrivate()) ?
                    $work->getIsPrivate() :
                    false
             ];
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function existWork($workId)
    {
        $work = $this->getRepository()
            ->field("_id")
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->getSingleResult();

        if (count($work)> 0) {
            return true;
        }
        return false;
    }

    /**
     * Method to construct a necessary new comments object to save on database
     * @param array $commentParams an array with params to construct a object
     * @return Comments Comments object
     */
    public function buildAComment($commentParams)
    {
        $comment = new Comments();
        $comment->setCreateAt($this->getMongoDate());
        $comment->setComment($commentParams['comment']);
        $comment->setUserId($commentParams['userId']);
        $comment->setType($commentParams['type']);
        $comment->setProductId($commentParams['postId']);
        $comment->setFlagged(false);
        $comment->setStatus($commentParams['status']);
        return $comment;
    }

    public function buildAReplyComment($data)
    {
        $comment = new ReplyComment();
        $comment->setCreateAt($this->getMongoDate());
        $comment->setFlagged(false);
        $comment->setComment($data['comment']);
        $comment->setType('work');
        $comment->setStatus(true);
        $comment->setUserId(new \MongoId($data['userId']));
        $comment->setOwner(new \MongoId($data['commentId']));
        return $comment;
    }

    public function setRepository($entityManager)
    {
        $this->queryBuilder = $entityManager;
    }

    /**
     * Method to build a user
     * @param $id
     * @return array
     */
    public function buildUser($id)
    {
        $userBuild = array();

        $user = $this->findAnUser($id);
        if (count($user)>0) {
            $userBuilded['firstName'] = $user->getFirstName();
            $userBuilded['id'] = $user->getId();
            $userBuilded['lastName'] = $user->getLastName();
            $userBuilded['level'] = ucfirst($user->getLevel()->getAlias());
            $userBuilded['avatar'] = $user->getUserPhoto();
            $userBuilded['upload_at'] = $user->getCreatedAt();
            $userBuilded['lastActivity'] = $user->getLastActivity();
            $userBuilded['community'] = $user->getCommunity();
            $userBuilded['status'] = $user->getStatus();
            $userBuilded['approval'] = $user->getState();
            $userBuild['user'] = $userBuilded;
        }
        return $userBuild;
    }

    public function buildCoWorker($id)
    {
        $userBuilded = array();
        $user = $this->findAnUser($id);
        if (count($user)> 0) {
            $userBuilded['firstName'] = $user->getFirstName();
            $userBuilded['lastName'] = $user->getLastName();
            $userBuilded['id'] = $user->getId();
        }
        return $userBuilded;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findAnUser($id)
    {
        return $this->queryBuilder
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();
    }

    public function findUserByEmail($queryBuilder, $email)
    {
        return $queryBuilder
            ->field("email")
            ->equals($email)
            ->getQuery()
            ->getSingleResult();
    }

    public function getTotalComments($commentRepository, $id)
    {
        $qty = $commentRepository
            ->field('productId')
            ->equals(new \MongoId($id))
            ->field('status')
            ->equals('enabled')
            ->getQuery()
            ->execute();
        return count($qty);
    }

    /** Find a Like
     * @param $userId
     * @return mixed
     */
    public function findLike($data)
    {
        $var =  $this->queryBuilder
            ->field('likes.userId')
            ->equals(new \MongoId($data['userId']))
            ->field('_id')
            ->equals(new \MongoId($data['productId']))
            ->getQuery()
            ->getSingleResult();
        return $var;
    }


    /** Method to cast an id to mongoId
     * @param array $objects
     * @return array
     */
    public function castMongoId(array $objects)
    {
        $mongoIds = array();
        foreach ($objects as $object) {
            $mongoIds[] = new \MongoId($object['id']);

        }
        return $mongoIds;
    }
}