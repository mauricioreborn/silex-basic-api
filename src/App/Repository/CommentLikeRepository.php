<?php

namespace App\Repository;

use App\Entities\CommentLike;

class CommentLikeRepository extends CommentRepository
{


    public function verifyCommentLike($data)
    {
        $commentLike = $this->queryBuilder->createQueryBuilder(CommentLike::class);
        $like = $commentLike
            ->field('userId')
            ->equals(new \MongoId($data['userId']))
            ->field('commentId')
            ->equals(new \MongoId($data['commentId']))
            ->getQuery()
            ->getSingleResult();

        if (count($like)> 0) {
            return true;
        }
        return false;
    }

    public function countLikes($commentId)
    {
        return $this->queryBuilder->createQueryBuilder(CommentLike::class)
            ->field("commentId")
            ->equals(new \MongoId($commentId))
            ->getQuery()
            ->execute();
    }
}