<?php

namespace App\Repository;

use App\Entities\User;
use App\Entities\ClassRoom;

class ClassRoomRepository extends Repository
{
    
    public function getClassRoomById($classCode)
    {
        return $this->getRepository()
            ->field('_id')
            ->equals(new \MongoId($classCode))
            ->getQuery()
            ->execute();
    }

    /**
     * Method to get created classrooms.
     * @param array $queryData  An array that contains the limit of classrooms
     *                          and how many classrooms should be skipped.
     * @return mixed            Return an array of classrooms object.
     */
    public function getClassrooms(array $queryData)
    {
        $classQueryBuilder= $this->getRepository()->createQueryBuilder(
            ClassRoom::class
        );

        $classrooms = $classQueryBuilder
            ->limit($queryData['limit'])
            ->skip($queryData['skip'])
            ->getQuery()
            ->execute();

        return $classrooms;
    }

    public function join($data)
    {
        $queryBuilder = $this->queryBuilder->createQueryBuilder(User::class);

        $groups = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($data['userId']))
            ->getQuery()
            ->getSingleResult();

        $labs = array();
        $class = array();
        if (!empty($groups->getLabs())) {
            $labs = $groups->getLabs();
        }
        if (!empty($groups->getClassRoom())) {
            $class = $groups->getClassRoom();
        }
        $myGroups = array_merge($labs, $class);

        if (!in_array($data['groupId'], array_column($myGroups, "id"))) {
            $classroom['id'] = $data['groupId'];
            $classroom['uploaded_at'] = $this->getMongoDate();
            $queryBuilder
                ->updateOne()
                ->field('classroom')->push($classroom)
                ->field('classCode')->set($data['groupId'])
                ->field('_id')->equals(new \MongoId($data['userId']))
                ->getQuery()
                ->execute();
            return true;
        }
        return false;
    }
}