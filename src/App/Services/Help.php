<?php

namespace App\Services;

use App\Entities\User;
use App\Services\Auth\TokenGenerator;
use App\Services\Auth\TokenBuilder;
use Doctrine\ORM\EntityManager;

class Help
{
    private $app;
    private $entity;
    public function __construct()
    {
        $app = require __DIR__ . '/../../../resources/config/bootstrap.php';
        $this->app =  $app;
        $this->entity = $app['mongodbodm.dm'];
    }

    public function getTokenWithValidUser()
    {
        $token = new TokenGenerator(new TokenBuilder());
        $repository = $this->queryBuilder->getRepository(User::class);
        $user = $repository->findOneBy(['email' => 'api@acme.com.br']);
        $token->setContext(['user_id' => $user->getId()]);
        return $token->getToken();
    }
}