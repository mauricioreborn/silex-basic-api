<?php

namespace App\Services\ForgotPassword;

use App\EmailTemplate\ForgotPassword;
use App\Entities\User;
use App\Repository\ForgotPasswordRepository;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;
use App\Services\Email\EmailBuilder;
use App\Services\TokenService;

class ForgotPasswordService
{
    protected $message;
    protected $status;
    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function validateForgotToken($em, $userId, $token, $expiration)
    {
        $validateToken = new ForgotPasswordRepository($em);
        $valid = $validateToken->validateToken($userId, $token, $expiration);

        if ($valid === false) {
            $this->setMessage('Token not Valid');
            $this->setStatus(400);
            return false;
        }
        if ($valid === 'expired') {
            $this->setMessage('Token Expired, Reset Again');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($valid);
        $this->setStatus(200);
        return true;
    }

    public function forgotPassword($em, $email, $mailer)
    {
        $validateToken = new ForgotPasswordRepository($em);
        $fetchToken = $validateToken->getUser($email);
        if (! $fetchToken) {
            $this->setMessage('User Not found');
            $this->setStatus(400);
            return false;
        }

        if (!empty($fetchToken->tokenPass)) {
            $validateToken->clearToken($fetchToken);
        }

        $tokenService = new TokenService();
        $token = $tokenService->tokenBuild($fetchToken);

        $validateToken->createToken($fetchToken, $token);

        if ($this->sendEmail($email, $token, $mailer)) {
            $this->setMessage('E-mail with Token Sent');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Error Sending email');
        $this->setStatus(400);
        return false;
    }

    public function updatePassword($em, $id, $password)
    {
        $update = new ForgotPasswordRepository($em);
        $newpass = $update->updatePassword($id, $password);
        if ($newpass === md5($password)) {
            $this->setMessage('Password Changed');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Password Not Changed');
        $this->setStatus(400);
        return false;
    }

    public function sendEmail($email, $Token, $mailer)
    {
        $templateBuilder = new ForgotPassword();

        $emailBuilder = new EmailBuilder($mailer);
        $emailBuilder->setTo($email);
        $emailBuilder->setSubject('ACME Forgot Password Link');
        $emailBuilder->setFrom('info@theacmenetwork.org');
        $emailBuilder->setContent(
            $templateBuilder->getTemplate($Token)
        );
        if ($emailBuilder->sendEmail()) {
            return true;
        }
        return false;
    }

}