<?php

namespace App\Services\Work;

use App\Entities\CommentLike;
use App\Entities\Work;
use App\Repository\CommentLikeRepository;
use App\Repository\CommentRepository;
use App\Services\ServiceInterface;

class CommentService implements ServiceInterface
{
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to verify if an user already liked this comment
     * @param $queryBuilder
     * @param $data
     * @return bool
     */
    public function verifyLike($queryBuilder, $data)
    {
        $commentRepository = new CommentLikeRepository($queryBuilder);
        if ($commentRepository->verifyCommentLike($data)) {
            $this->setMessage('the user Already liked of this comment');
            $this->setStatus(400);
            return true;
        }
        $this->setMessage('the user do not liked this comment or This comment does not belong to work');
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to add a new like on comment
     * @param $entity
     * @param $data
     * @return bool
     */
    public function addLikeOnComment($queryBuilder, $data)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        $work = $commentRepository->addLikeOnComment($data);
        if ($work) {
            $this->setMessage('like has been add to the comment');
            $this->setStatus(200);
            return $work;
        }

        $this->setMessage('like has not been add to the comment');
        $this->setStatus(400);
        return false;
    }

    public function addLike($entity, $data)
    {
        $commentRepository = new CommentRepository($entity->createQueryBuilder(CommentLike::class));
        $commentLike = $commentRepository->addLike($data);
        $entity->persist($commentLike);
        $entity->flush();
        $this->setMessage('like has been add to the comment');
        $this->setStatus(200);
        return true;
    }

    /** Method to remove an like on commentLike document
     * @param $queryBuilder
     * @param $data
     */
    public function removeLike($queryBuilder, $data)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        $commentRepository->removeLike($data);
        $this->setMessage('like has been removed');
        $this->setStatus(200);
        return true;
    }

    /** Remove one inc on work.comments.like
     * @param $queryBuilder
     * @param $data
     */
    public function removeLikeOnComment($queryBuilder, $data)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        $commentRepository->removeLike($data);
        $this->setMessage('like has been removed');
        $this->setStatus(200);
        return true;
    }

    public function checkWorkComment($queryBuilder, $data)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        return $commentRepository->checkWorkComment($data);
    }

}