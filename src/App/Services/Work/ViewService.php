<?php

namespace App\Services\Work;

use App\Entities\Work;
use App\Repository\ViewRepository;
use App\Services\ServiceInterface;

class ViewService implements ServiceInterface
{
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function userViewedWork($data, $repository)
    {
        $viewRepository = new ViewRepository($repository);
        $views = $viewRepository->verifyIfWorkWasViewed($data);
        if (count($views) > 0) {
            $this->setMessage('The user already viewed this work');
            $this->setStatus(400);
            return true;
        }
        return false;
    }

    public function addViewOnWork($data, $viewRepository)
    {
        $viewRepositorys = new ViewRepository($viewRepository->createQueryBuilder(Work::class));
        $work = $viewRepositorys->addViewOnWork($data);
        if ($work) {
            $this->setMessage('more one view has been add on work');
            $this->setStatus(200);
            return $work;
        }
        $this->setMessage('work not found');
        $this->setStatus(400);
        return false;
    }

    public function addView($data, $entity)
    {
        $viewRepository = new ViewRepository($entity->getRepository(Work::class));
         $view = $viewRepository->addView($data);
        $entity->persist($view);
        $entity->flush();
            $this->setMessage('view has been add');
            $this->setStatus(200);
            return true;
    }
}