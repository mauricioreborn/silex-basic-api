<?php

namespace App\Services\Work;

use App\Entities\CommentLike;
use App\Entities\Comments;
use App\Entities\Post;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\LikeRepository;
use App\Repository\UserRepository;
use App\Repository\WorkRepository;
use App\Services\ServiceInterface;

class LikeService implements ServiceInterface
{

    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function addLike($entity, $data)
    {
        $repository = $entity->createQueryBuilder(Work::class);
        $workRepository = new LikeRepository($repository);
        $work = $workRepository->addLike($data);
        if ($work) {
            $entity->persist($work);
            $entity->flush();
            $this->setMessage('like has been add');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('work not found');
        $this->setStatus(400);
        return false;
    }

    public function addLikeOnUser($entity, $data)
    {
        $userRepository = new UserRepository($entity->createQueryBuilder(User::class));
        $user = $userRepository->addLike($data);
        if ($user) {
            $entity->persist($user);
            $entity->flush();
            $this->setMessage('like has been add');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('user not found');
        $this->setStatus(400);
        return false;
    }

    public function deleteLike($entity, $data)
    {
        $repository = $entity->createQueryBuilder(Work::class);
        $workRepository = new LikeRepository($repository);
        $work = $workRepository->deleteWorkLike($data);
        if ($work) {
            $entity->persist($work);
            $entity->flush();
            $this->setMessage('like has been removed');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('work not found');
        $this->setStatus(400);
        return false;
    }

    public function deleteLikeOnPost($entity, $data)
    {
        $repository = $entity->createQueryBuilder(Post::class);
        $workRepository = new LikeRepository($repository);
        $work = $workRepository->deletePostLike($data);
        if ($work) {
            $entity->persist($work);
            $entity->flush();
            $this->setMessage('like has been removed');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('post not found');
        $this->setStatus(400);
        return false;
    }
    
}