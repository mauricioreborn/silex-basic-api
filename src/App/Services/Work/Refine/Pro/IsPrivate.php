<?php
namespace App\Services\Work\Refine\Pro;

use Doctrine\ODM\MongoDB\Query\Builder;

class IsPrivate
{
    /**
     * Search works that by a isPrivate value.
     * @param Builder $queryBuilder Used to construct DB Query.
     * @param bool $value           The value used to search the works by
     *                              isPrivate field.
     * @return Builder              The querybuilder to search works with an
     *                              specific value.
     */
    public function startSearch(Builder $queryBuilder, bool $value)
    {
        if ($value == true) {
            return $queryBuilder->field('isPrivate')->equals($value);
        }

        return $queryBuilder
            ->addOr($queryBuilder->expr()->field('isPrivate')->equals($value))
            ->addOr($queryBuilder->expr()->field('isPrivate')->exists(false));
    }
}
