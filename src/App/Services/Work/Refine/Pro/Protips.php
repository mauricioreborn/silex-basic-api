<?php

namespace App\Services\Work\Refine\Pro;

class Protips
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder)
    {
        return $queryBuilder->field('group')->equals('proTip');
    }
}