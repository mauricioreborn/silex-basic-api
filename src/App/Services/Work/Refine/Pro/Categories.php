<?php

namespace App\Services\Work\Refine\Pro;

use App\Repository\WorkRepository;
use Symfony\Component\HttpFoundation\Request;

class Categories implements RefineBy
{

    private $query;
    private $category;

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query[] = $query;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category[] = $category;
    }

    public function animation()
    {
        if ($this->getCategory() == 'animation') {
            $this->setQuery(
                [
                    'works' =>
                    [
                        'category' => 'animation'
                    ]
                ]
            );
        }
    }

    public function startSearch(array $request, $query = null)
    {
        $this->setCategory($request['category']);
        $this->setQuery($query);
        $this->animation();
        return $this->next($request);
    }
    public function next(array $request)
    {
        return $this->getQuery();
    }
}