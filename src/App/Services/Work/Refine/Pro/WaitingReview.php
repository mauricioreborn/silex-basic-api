<?php

namespace App\Services\Work\Refine\Pro;

class WaitingReview
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $value)
    {
        $query =  $queryBuilder->field('available')->equals($value);
        if ($value == false) {
            return $query->field('reviewed')->equals(false);
        }
        return $query;
    }
}