<?php

namespace App\Services\Work\Refine\Pro;

class Week
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder)
    {
        $date = "Y-m-d H:i:s";
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);

        $date = $date == null ? "Y-m-d H:i:s" : $date;
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $dt2 = $dt->modify("-1 week");
        $ts = $dt2->getTimestamp();

        $lastWeek = new \MongoDate($ts);

        return $queryBuilder->field('upload_at')->range($lastWeek, $this->today);
    }
}