<?php

namespace App\Services\Work\Refine\Pro;

class Time implements RefineBy
{

    private $time;
    private $query;

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }/**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query[] = $query;
    }

    public function today()
    {
        if ($this->getTime() == 'today') {
            $this->setQuery(
                [
                    '$orderby' =>
                        [
                            'works.upload_at' => -1
                        ]
                ]
            );
        }
    }


    public function week()
    {
        if ($this->getTime() == 'week') {
            $exampleDate = new \MongoDate();
            $this->today = new \DateTime(DATE_ISO8601, $exampleDate->sec);
            $date2 = $this->today->modify('-7 day');
            $date2 = $date2->format(DATE_ISO8601, $exampleDate->sec);

            $this->setQuery(
                [
                    'work.upload_at' =>
                        [
                            '$gte' => $date2,
                            '$lt' => $this->today
                        ]
                ]
            );
        }
    }

    public function month()
    {
        if ($this->getTime() == 'month') {
            $exampleDate = new \MongoDate();
            $this->today = new \DateTime(DATE_ISO8601, $exampleDate->sec);
            $date2 = $this->today->modify('-30 day');
            $date2 = $date2->format(DATE_ISO8601, $exampleDate->sec);

            $this->setQuery(
                [
                    'work.upload_at' =>
                        [
                            '$gte' => $date2,
                            '$lt' => $this->today
                        ]
                ]
            );
        }
    }


    public function startSearch(array $request, $query = null)
    {
        $this->setTime($request['time']);
        $this->setQuery($query);
        $this->today();
        $this->week();
        $this->month();
        return $this->next($request);
    }
    public function next(array $request)
    {
        $categories = new Categories();
        return $categories->startSearch($request, $this->getQuery());
    }
}