<?php

namespace App\Services\Work\Refine\Pro;

class Search
{
    private $type;

    public function __construct()
    {
        $this->setType('selectMeta');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $searchWork)
    {
        return $queryBuilder
            ->addOr($queryBuilder->expr()->field('title') ->equals(new \MongoRegex('/.*'.$searchWork.'.*/i')))
            ->addOr($queryBuilder->expr()->field('description')->equals(new \MongoRegex('/.*'.$searchWork.'.*/i')));
    }
}
