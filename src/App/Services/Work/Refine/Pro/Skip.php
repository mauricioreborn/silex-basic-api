<?php

namespace App\Services\Work\Refine\Pro;

class Skip
{

    private $type;

    public function __construct()
    {
        $this->setType('skip');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $skip)
    {
        return $queryBuilder
            ->field('status')->equals('enabled')
            ->getQuery()->execute()->skip($skip);
    }
}