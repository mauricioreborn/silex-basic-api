<?php


namespace App\Services\Work\Refine\Pro;

use Symfony\Component\HttpFoundation\Request;

class Draw implements RefineBy
{
    private $query;
    private $drawFilter;

    /**
     * @return mixed
     */
    public function getDrawFilter()
    {
        return $this->drawFilter;
    }

    /**
     * @param mixed $drawFilter
     */
    public function setDrawFilter($drawFilter)
    {
        $this->drawFilter = $drawFilter;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query[] = $query;
    }

    public function popular()
    {
        if ($this->getDrawFilter() == 'popular') {
            $this->setQuery(
                [
                    'work'=>
                        [
                        'popular'=>'seila'
                        ]
                ]
            );
        }
    }

    public function recent()
    {
        if ($this->getDrawFilter() == 'recent') {
            $this->setQuery(
                [
                    'work.comments' =>
                        [
                            '$sum'=> '$qty'
                        ],
                    '$orderby' =>
                        [
                        'upload_at' => -1
                        ]
                ]
            );
        }
    }



    public function needFeedBack()
    {
        if ($this->getDrawFilter() == 'feedback') {
            $this->setQuery(
                [
                    'works' =>
                        [
                            '$sum' => 'comments'
                        ],

                    '$orderby' =>
                        [
                            'works' =>
                                [
                                    'comments' => 1
                                ]
                        ]
                ]);
        }
    }
    
    
    
    public function startSearch(array $request, $query = null)
    {

        $this->setDrawFilter($request['draw']);
        $this->setQuery(['works'=>['$exists'=>true]]);
        $this->popular();
        $this->recent();
        $this->needFeedBack();
        return $this->next($request);
    }
    public function next(array $request)
    {
        $time = new Time();
        return $time->startSearch($request, $this->getQuery());
    }
}