<?php

namespace App\Services\Work\Refine\Pro;

class Month
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder)
    {
        $dt = new \DateTime(date('Y-m-d'));

        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        $dt2 = $dt->modify('-30 days');
        $ts2 = $dt2->getTimestamp();
        $lastWeek = new \MongoDate($ts2);

        return $queryBuilder->field('upload_at')->range($lastWeek, $this->today);
    }
}