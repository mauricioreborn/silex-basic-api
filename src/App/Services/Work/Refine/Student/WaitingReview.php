<?php

namespace App\Services\Work\Refine\Student;

class WaitingReview
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $value)
    {
        return $queryBuilder->field('available')->equals(false);
    }
}