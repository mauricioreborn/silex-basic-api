<?php

namespace App\Services\Work\Refine\Student;

class Feedback
{
    private $type;

    public function __construct()
    {
        $this->setType('aggregate');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder)
    {
        return $queryBuilder->sort('comment', 'asc');
    }
}