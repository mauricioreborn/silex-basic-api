<?php

namespace App\Services\Work\Refine\Student;

class Limit
{

    private $type;

    public function __construct()
    {
        $this->setType('skip');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $limit)
    {
        return $queryBuilder->limit($limit);
    }
}