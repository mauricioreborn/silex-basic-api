<?php

namespace App\Services\Work;

use App\Entities\Work;
use App\Services\Work\Refine\Pro\WaitingReview;
use App\Services\Work\Refine\Pro\IsPrivate;
use App\Services\Work\Refine\Student\Limit;
use App\Services\Work\Refine\Pro\Feedback as ProFeedback;
use App\Services\Work\Refine\Student\Feedback as StudentFeedback;
use App\Services\Work\Refine\Student\Search;
use App\Services\Work\Refine\Student\Skip;
use App\Services\Work\Refine\Student\User;

class RefineByFactory
{
    private $queryBuilder;
    private $path;
    private $request;
    private $skip;


    public function __construct($entity, $type)
    {
        $this->path = "App\\Services\\Work\\Refine\\Student\\";

        if ($type == 'pro' || $type == 'recruter'|| $type == 'recruiter') {
            $this->path = "App\\Services\\Work\\Refine\\Pro\\";
        }
        $this->queryBuilder = $entity->createQueryBuilder(Work::class);
    }

    public function startSearch(array $request)
    {
        $this->skip = $request['skip'];
        unset($request['skip']);

        $this->request = $request;
        
        $this->feedback();
        $this->search();
        $this->limit();
        $this->uniqUser();
        $this->waitingForReview();
        $this->isPrivate();
        
        foreach ($this->request as $data => $key) {
            $key = (string) ucfirst($key);
            $class = $this->path.$key;
            if (class_exists($class)) {
                $filter = new $class();
                $this->queryBuilder = $filter->startSearch($this->queryBuilder);
            }
        }
        $this->skip();
        return $this->queryBuilder;
    }

    /*
     * verify if exist a skip of data
     */
    protected function skip()
    {
            $class = $this->path.'Skip';
            $skip = new $class();
            $this->queryBuilder = $skip->startSearch($this->queryBuilder, $this->skip);
    }

    /*
     * verify if exist a limit of data
     */
    protected function limit()
    {
        if (isset($this->request['limit'])) {
            $limit = new Limit();
            $this->queryBuilder = $limit->startSearch($this->queryBuilder, $this->request['limit']);
            unset($this->request['limit']);
        }
    }

    /*
     * get works by a specific user
     */
    protected function uniqUser()
    {
        if (isset($this->request['user'])) {
            $user = new User();
            $this->queryBuilder = $user->startSearch($this->queryBuilder, $this->request['user']);
            unset($this->request['user']);
        }
    }

    public function search()
    {
        if (isset($this->request['search'])) {
            $search = new Search();
            $this->queryBuilder = $search->startSearch($this->queryBuilder, $this->request['search']);
            unset($this->request['search']);
        }
    }

    /**
     * Method to filter by feedback of pro or student
     */
    public function feedback()
    {
        if (isset($this->request['browse'])) {
            if ($this->request['browse'] === 'feedback') {
                if ($this->request['myLevel'] == 'pro') {
                    $feedback = new ProFeedback();
                    $this->queryBuilder = $feedback->startSearch(
                        $this->queryBuilder,
                        $this->request['browse']
                    );
                } else {
                    $feedback = new StudentFeedback();
                    $this->queryBuilder = $feedback->startSearch(
                        $this->queryBuilder,
                        $this->request['browse']
                    );
                }
                unset($this->request['browse']);
            }
        }
        unset($this->request['myLevel']);
    }

    /**
     * This method is used to construct the query builder that will search works
     * by a specific value in "isPrivate" field.
     */
    public function isPrivate()
    {
        $isPrivate = new IsPrivate();

        if (isset($this->request['isPrivate'])) {
            $this->queryBuilder = $isPrivate->startSearch(
                $this->queryBuilder,
                $this->request['isPrivate']
            );
            unset($this->request['isPrivate']);
        }
    }
    
    public function waitingForReview()
    {
        $waitingForReview = new WaitingReview();
        $this->queryBuilder = $waitingForReview->startSearch($this->queryBuilder, $this->request['waitingReview']);
        unset($this->request['waitingReview']);
    }

}