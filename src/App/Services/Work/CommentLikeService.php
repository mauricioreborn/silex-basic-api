<?php

namespace App\Services\Work;

use App\Repository\CommentLikeRepository;
use App\Services\ServiceInterface;

class CommentLikeService extends CommentService implements ServiceInterface
{
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function verifyCommentLike($queryBuilder, $data)
    {
        $commentLikeRepository = new CommentLikeRepository($queryBuilder);
        if (count($commentLikeRepository->verifyCommentLike($data))>0) {
            $this->setMessage('user already like this comment');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage("user don't like this comment");
        $this->setStatus(400);
        return false;
    }
}