<?php

namespace App\Services\Work;

use App\Entities\FlagContent;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\LiveSessionRepository;
use App\Repository\WorkRepository;
use App\Services\LiveSession\LiveSessionService;
use App\Services\Service;
use App\Services\ServiceInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use \MongoId;

class WorkService extends Service implements ServiceInterface
{

    protected $message;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    protected $status;

    public function getWorkById($entityManager, $data)
    {
        $workRepository = new WorkRepository($entityManager);
        $work = $workRepository->getWorkById(
            $data
        );
        if (count($work) == 0) {
            $this->setMessage('work not found');
            $this->setStatus(400);
            return false;
        }

        $this->setMessage($work);
        $this->setStatus(200);
        return $work;
    }

    public function getWorkVersion($entityManager, $data)
    {
        $workRepository = new WorkRepository($entityManager);
        $work = $workRepository->getWorkVersion(
            $data
        );
        if (count($work) == 0) {
            $this->setMessage('work not found');
            $this->setStatus(400);
            return false;
        }

        $this->setMessage($work);
        $this->setStatus(200);
        return $work;
    }

    /**
     * This method is used to create a work.
     * @param DocumentManager $entityManager Object used to execute database
     *                                       operation.
     * @param array $workInfo                An array with the information to
     *                                       create a work.
     */
    public function setWork(DocumentManager $entityManager, array $workInfo)
    {
        $workRepository = new WorkRepository($entityManager->createQueryBuilder(Work::class));
        $work = $workRepository->setWork($workInfo);


        if (!empty($workInfo['coWorkers'])) {
            $userRepository = new \App\Repository\UserRepository($entityManager->createQueryBuilder(User::class));
            foreach ($workInfo['coWorkers'] as $coWork) {
                $userRepository->incrementCoWork($coWork['id']);
            }
        }

        $entityManager->persist($work);
        $entityManager->flush();
        $workInfo['postId'] = $work->getId();
        $workInfo['type'] = 'new work';
        $workInfo['icon'] = 'mode_edit';
        $this->updateWorActivity($entityManager, $workInfo);
        $userRepository = new \App\Repository\UserRepository($entityManager->createQueryBuilder(User::class));
        $userRepository->updateUserTotalWorkCount($workInfo['userId']);
        $this->setMessage($work->getId());
        $this->setStatus(200);
    }

    public function checkLikes($em, $type, $userId, $id)
    {
        $workRepository = new WorkRepository($em);
        if ($workRepository->checkLikes($type, $userId, $id)) {
            $this->setMessage(true);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(false);
        $this->setStatus(200);
        return false;
    }

    public function deleteWork($em, $data)
    {
        $workRepository = new WorkRepository($em);
        $workRepository->deleteWork($data) ;
        $this->setMessage($workRepository->getMessage());
        $this->setStatus($workRepository->getStatus());
    }

    public function fetchCommunity($em, $id)
    {
        $workRepository = new WorkRepository($em);
        $workRepository->fetchCommunity($id);
        $this->setMessage($workRepository->getMessage());
        $this->setStatus($workRepository->getStatus());
    }

    public function fetchCategories($em, $id)
    {
        $workRepository = new WorkRepository($em);
        $workRepository->fetchCategories($id);
        $this->setMessage($workRepository->getMessage());
        $this->setStatus($workRepository->getStatus());
    }

    /** Method to verify if work has reviewed
     * @param $queryBuilder
     * @param $data
     * @return bool
     */
    public function workHasReviewed($queryBuilder, $workId)
    {
        $workRepository = new WorkRepository($queryBuilder);
        if (count($workRepository->workHasReviewed($workId))>0) {
            $this->setMessage('This work has already been reviewed');
            $this->setStatus(213);
            return true;
        }
        $this->setMessage('This work not has reviewed');
        $this->setStatus(213);
        return false;
    }

    /** Method to update a work
     * @param $entity
     * @param $data
     * @return bool
     */
    public function editWork($entity, $data)
    {
        $workRepository = new WorkRepository($entity->createQueryBuilder(Work::class));
        $work = $workRepository->getWork($data);
        if (count($work) >0) {
            $work->removeImagesToEdit();
            $entity->persist($work);
            $entity->flush();
            $editedWork = $workRepository->buildAWork($work, $data);
            $entity->persist($editedWork);
            $entity->flush();

            $data['postId'] = $data['workId'];
            $data['type'] = 'edited a work';
            $data['icon'] = 'mode_edit';
            $this->updateWorActivity($entity, $data);
            $this->setMessage($work->getId());
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('work not updated');
        $this->setStatus(401);
    }

    public function userWorksByName($queryBuilder, $data)
    {
        $workRepository = new WorkRepository($queryBuilder);
        $works = $workRepository->userWorksByName($data);
        $this->setMessage($works);
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to add or remove a work of the session
     * @param DocumentManager $em
     * @param array $data
     * @return bool
     */
    public function submitSessionWork(DocumentManager $em, array $data)
    {
        $workRepository = new WorkRepository($em);
        $liveSessionRepository = new LiveSessionRepository($em);
        $liveSession = $liveSessionRepository->getSession($data);
        if (count($liveSession->getId())) {
            if ($data['action'] == 'remove') {
                $workRepository->removeLiveSessionWork($liveSession, $data);
                $this->setMessage($workRepository->getMessage());
                $this->setStatus(200);
                return true;
            }
            $workRepository->submitSessionWork($liveSession, $data);
            $this->setMessage($workRepository->getMessage());
            $this->setStatus(200);
            return true;
        }
    }

    public function submittedList($em, $data)
    {
        $workRepository = new WorkRepository($em);
        $submit = $workRepository->submittedList($data);
        $this->setMessage($submit);
        $this->setStatus(200);
        return true;
    }

    public function selectSubmittedWork($em, $data)
    {
        $workRepository = new WorkRepository($em);
        $selected = $workRepository->selectSubmittedWork($data);
        if ($selected === true) {
            $data['url'] = getenv('LIVESESSION_URL').'submit/works';
            $data['content'] = 'application/json';
            $liveService = new LiveSessionService($em);
            $response = json_decode($liveService->postLiveSession($data));
            $this->setMessage('Works Selected');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Work Not Found');
        $this->setStatus(400);
        return false;
    }

    /** Method to set a new work Version
     * @param $entityManager
     * @param $work
     */
    public function setWorkVersion($entityManager, $work)
    {
        $workRepository = new WorkRepository($entityManager->createQueryBuilder(Work::class));
        $work = $workRepository->setWork($work);
        $entityManager->persist($work);
        $entityManager->flush();
        $this->setMessage('a new work version has been saved');
        $this->setStatus(200);
    }

    /** Method to update more on value on total comment after an user comment a work
     * @param $userQueryBuilder
     * @param $userId
     */
    public function updateTotalWorkComment($userQueryBuilder, $userId)
    {
        $userRepository = new \App\Repository\UserRepository($userQueryBuilder);
        $userRepository->updateUserTotalCommentCount($userId);
    }

    /** Method to fild a work by workId
     * @param $workBuilder
     * @param $workId
     * @return mixed
     */
    public function findWork($workBuilder, string $workId)
    {
        $workRepository = new WorkRepository($workBuilder);
        $work = $workRepository->findAnUser($workId);

        if (count($work) == 0) {
            $this->setMessage('work not found');
            $this->setStatus(204);
            return false;
        }
        $this->setMessage('work found');
        $this->setStatus(204);
        return $work;
    }


    /**
     * Method to set a new flag inside work
     * @param DocumentManager $entityManager EntityManager to persist data
     * @param FlagContent $flagContent       Entity Content builded to persist
     * @param string $workId                 Work id on database to reference
     *                                       on flag
     * @return bool                          True is success and false is work
     *                                       not found
     */
    public function setFlag(DocumentManager $entityManager, FlagContent $flagContent, string $workId)
    {
        $workQueryBuilder = $entityManager->createQueryBuilder(Work::class);
        $work = $workQueryBuilder
            ->field("_id")
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->getSingleResult();

        if (count($work) > 0) {
            $work->setFlag($flagContent);
            $work->setFlagged(true);
            $entityManager->persist($work);
            $entityManager->flush();
            $this->setMessage('work has been flagged');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('work not found');
        $this->setStatus(401);
        return false;
    }

    /** method to see if exist an work on available
     * @param $workQueryBuilder
     * @param $userId
     * @return bool
     */
    public function workOnReview($workQueryBuilder, $userId)
    {
        $workRepository = new WorkRepository($workQueryBuilder);
        $work = $workRepository->avaliableWork($userId);
        if (count($work)>0) {
            return true;
        }
        return false;
    }

    /** Method to check if a work can sent to portfolio. by her id
     * @param $workQueryBuilder - queryBuilder with entity work
     * @param int $workId - workId
     * @return bool
     */
    public function sendToPortfolio($workQueryBuilder, $workId)
    {
        $workRepository = new WorkRepository($workQueryBuilder);
        $work = $workRepository->findAnUser($workId);
        if (count($work)>0) {
            if (count($work->getCriterias()) >= 12) {
                $this->setMessage('Work may be submitted to portfolio');
                $this->setStatus(200);
                return true;
            }
            $this->setMessage('Work cannot be submitted to portfolio');
            $this->setStatus(200);
            return false;
        }

        $this->setMessage('Work not found');
        $this->setStatus(401);
        return false;
    }

    /** Method to get Co works by an userId
     * @param $entityManager
     * @param $data[skip,limit,userId]
     * @return null
     */
    public function getMyCoWorks($entityManager, $data)
    {
        $workRepository = new WorkRepository($entityManager->createQueryBuilder(Work::class));
        $works = $workRepository->getMyCoWorks($data);
        $workRepository->setRepository($entityManager);
        $coWorks = array();
        $workData = array();
        if (count($works) >0) {
            foreach ($works as $work) {
                $workData[] = $workRepository->buildWork($work);
            }
            $coWorks['works'] = $workData;
            $coWorks['total'] = count($works);

        }
        $this->setMessage($coWorks);
        $this->setStatus(200);
    }

    /**
     * Method to build a list of works on a range of ids
     * @param $entityManager
     * @param array $filters   An array to filters works using userId,eventId,limit and skip
     * @param array $workIds
     * @return bool
     */
    public function getWorkByRange(DocumentManager $entityManager, array $filters, array $workInfo)
    {
        $workRepository = new WorkRepository($entityManager);
        $works = $workRepository->getWorkByRangeOfIds($filters, $workInfo);

        if (count($works)>0) {
            $this->setMessage($works);
            $this->setStatus(200);
            return $works;
        }
        $this->setMessage(['message' => 'work not found']);
        $this->status(400);
    }

    /**
     * Method to check if the user already flagged this work.
     * @param DocumentManager $entityManager
     * @param array $data
     *        string userId  user identify
     *        string productId work identify
     * @return bool true as already flagged false is not
     */
    public function checkWasFlag(DocumentManager $entityManager, array $data)
    {
        $workQuery = $entityManager->createQueryBuilder(Work::class);

        $work = $this->findWork($workQuery, $data['productId']);
        if (count($work) > 0) {
            if (count($work->getFlag()) > 0 ) {
                foreach ($work->getFlag() as $flags) {
                    if ($flags->getuserId() == new MongoId($data['userId'])) {
                        $this->setMessage('user already flagged this work');
                        $this->setStatus(200);
                        return true;
                    }
                }
            }
            $this->setMessage('user not flagged this work');
            $this->setStatus(200);
            return false;
        }
        $this->setMessage('work not found');
        $this->setStatus(400);
        return false;
    }
}
