<?php
namespace App\Services\Community;

use App\Entities\Work;
use App\Repository\CommunityRepository;
use App\Services\Service;
use App\Services\ServiceInterface;

class CommunityService extends Service implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getRecentActivity($entityManager, $data)
    {
        $communityRepository = new CommunityRepository($entityManager);
        $recent = $communityRepository->getRecentActivity($data);
        if (count($recent) == 0) {
            $this->setMessage(['message' => 'No recent Works Found']);
            $this->setStatus(204);
            return false;
        }
        $paginated = array();
        if ($recent !== 0) {
            $paginated = $this->paginateArray($recent, $data['limit'], $data['skip']);
        }
        $this->setMessage($paginated);
        $this->setStatus(200);
        return true;
    }



    public function getInterestingComments($entityManager)
    {
        $communityRepository = new CommunityRepository($entityManager);
        $recent = $communityRepository->getInterestingComments();
        if (count($recent) == 0) {
            $this->setMessage(['message' => 'No interesting comments found']);
            $this->setStatus(204);
            return false;
        }
        $this->setMessage($recent);
        $this->setStatus(200);
        return true;
    }

    /**Method get activities highLights
     * @param $entityManager
     * @param $userId
     * @param $level
     * @return bool
     */
    public function getHighlights($entityManager, $userId, $level)
    {
        $communityRepository = new CommunityRepository($entityManager);
        $highlights = $communityRepository->getHighlights($userId, $level);
        if (count($highlights) == 0) {
            $this->setMessage(['message' =>'No Work Highlights Found']);
            $this->setStatus(204);
            return false;
        }
        $this->setMessage($highlights);
        $this->setStatus(200);
        return true;
    }
}