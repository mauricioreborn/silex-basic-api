<?php

namespace App\Services\Comment;

use App\Entities\Comment;
use App\Entities\Comments;
use App\Entities\FlagContent;
use App\Entities\User;
use App\Entities\Work;
use App\Repository\CommentRepository;
use App\Services\ServiceInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use \MongoId;

class CommentService implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /** Method to get Comment e ReplyComment
     * @param $queryBuilder = Comment or ReplyComment Entity
     * @param $entity = Need to be changed is only to can possible filter an user.
     * @param $data
     * @return bool
     */
    public function getComment($queryBuilder, $entity, $data)
    {
        $userId = $data['userId'];
        unset($data['userId']);
        $commentFactory = new CommentFactory($queryBuilder);
        $queryBuilderFiltered = $commentFactory->startSearch($data)
            ->getQuery()->execute();
        if (count($queryBuilder)>0) {
            $commentRepository = new CommentRepository($entity);
            $comments = $commentRepository->buildAGetComment(
                $queryBuilderFiltered,
                $userId
            );
            $this->setMessage($comments);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'comment not found']);
        $this->setStatus(213);
        return false;
    }

    /** Method to get Comment e ReplyComment
     * @param $queryBuilder = Comment or ReplyComment Entity
     * @param $entity = Need to be changed is only to can possible filter an user.
     * @param $data
     * @return bool
     */
    public function getUserComment($queryBuilder, $entity, $data)
    {
        $commentFactory = new CommentFactory($queryBuilder);
        $queryBuilderFiltered = $commentFactory->startSearch($data)
            ->field("userId")
            ->equals(new \MongoId($data['userId']))
            ->field('flagged')
            ->equals(false)
            ->field('status')
            ->notIn(['disabled'])
            ->getQuery()->execute();
        if (count($queryBuilderFiltered)>0) {
            $interestComment = array();
            foreach ($queryBuilderFiltered as $work) {
                $user = $entity->createQueryBuilder(User::class)
                    ->field('_id')
                    ->equals(new \MongoId($work->getUserId()))
                    ->getQuery()
                    ->getSingleResult();

                $workName = $entity->createQueryBuilder(Work::class)
                    ->field('_id')
                    ->equals(new \MongoId($work->getProductId()))
                    ->getQuery()
                    ->getSingleResult();

                $interestComment[] =
                    [
                        'comment' => $work->getComment(),
                        'commentId' => $work->getId(),
                        'createAt' => $work->getCreateAt()->format(\DateTime::ISO8601),
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                        'userPhoto' => $user->getUserPhoto(),
                        'userId' => $user->getId(),
                        'workName' => $workName->getTitle(),
                        'workId' => $work->getProductId()
                    ];
            }
            $this->setMessage($interestComment);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'comment not found']);
        $this->setStatus(213);
        return false;
    }

    /** Method to delete all comments by workId/postId
     * @param $queryBuilder
     * @param $productId
     * @return bool
     */
    public function deleteCommentsByProductId($queryBuilder, $productId)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        $commentRepository->deleteCommentsByProductId($productId);
        $this->setMessage('comments has been disabled');
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to getCommentsId By productId
     * @param $queryBuilder
     * @param $productId
     * @return Array
     */
    public function getIdCommentsByProductId($queryBuilder, $productId)
    {
        $commentRepository = new CommentRepository($queryBuilder);
        return $commentRepository->getIdCommentsByProductId($productId);
    }

    /** Method to delete all reply comments by commentId
     * @param $queryBuilder
     * @param $commentsId
     */
    public function deleteReplyCommentsByCommentId($queryBuilder, $commentsId)
    {
        
        $commentRepository = new CommentRepository($queryBuilder);
        foreach ($commentsId as $id) {
            $commentRepository->deleteReplyCommentsByCommentId($id);
        }
        $this->setMessage('reply comments has been disabled');
        $this->setStatus(200);
    }

    /**
     * Method to set a new flag inside work
     * @param DocumentManager $entityManager object to conect on mongo database
     * @param FlagContent $flagContent an object to set inside a comment
     * @param string $commentId comment id to insert flag content
     * @return bool false as comment not found and true as comment flagged
     */
    public function setFlag(
        DocumentManager $entityManager,
        FlagContent $flagContent,
        string $commentId
    ) {
        $commentBuilder = $entityManager->createQueryBuilder(Comments::class);
        $comment = $commentBuilder
            ->field("_id")
            ->equals(new MongoId($commentId))
            ->getQuery()
            ->getSingleResult();

        if (count($comment) > 0 ) {
            $comment->setFlagged(true);
            $comment->setFlag($flagContent);
            $entityManager->persist($comment);
            $entityManager->flush();
            $this->setMessage('Comment has been flagged');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('comment not found');
        $this->setStatus(401);
        return false;
    }


    /** Method to remove a comment by id
     * @param $commentBuilder
     * @param $commentId
     * @return mixed
     */
    public function deleteCommentById($commentBuilder, $commentId)
    {
         $commentBuilder
            ->remove()
            ->field("_id")
            ->equals(new \MongoId($commentId))
            ->getQuery()
            ->execute();

        $this->setMessage(['message' => 'comment has been removed']);
        $this->setStatus(200);
    }

    /** Method to remove a comment by userId
     * @param $commentBuilder
     * @param $userId
     * @return mixed
     */
    public function deleteCommentByUserId($commentBuilder, $userId)
    {
        $commentBuilder
            ->remove()
            ->field("userId")
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->execute();

        $this->setMessage(['message' => 'comment has been removed']);
        $this->setStatus(200);
    }

    /** Method to remove a comment by userId
     * @param $commentBuilder
     * @param $userId
     * @return mixed
     */
    public function deleteCommentByWorkId($commentBuilder, $workId)
    {
        $commentBuilder
            ->remove()
            ->field("productId")
            ->equals(new \MongoId($workId))
            ->getQuery()
            ->execute();

        $this->setMessage(['message' => 'comment has been removed']);
        $this->setStatus(200);
    }

    /**
     * Method to check if the user already flagged this comment.
     * @param DocumentManager $entityManager
     * @param array $data this array contain an string userId and a
     *                    string commentId.
     * @return bool false as there is an error and true as success
     */
    public function checkWasFlag(DocumentManager $entityManager, array $data)
    {
        $commentRepository = new CommentRepository(
            $entityManager->createQueryBuilder(Comments::class)
        );

        $comment = $commentRepository->findAnUser($data['productId']);
        if (count($comment) > 0) {
                foreach ($comment->getFlag() as $flags) {
                    if ($flags->getuserId() == new MongoId($data['userId'])) {
                        $this->setMessage('user already flagged this comment');
                        $this->setStatus(200);
                        return true;
                    }
                }
            }
            $this->setMessage('user not flagged this comment');
            $this->setStatus(200);
            return false;
        $this->setMessage('comment not found');
        $this->setStatus(400);
        return false;
    }
}

