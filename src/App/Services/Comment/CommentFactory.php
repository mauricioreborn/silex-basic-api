<?php

namespace App\Services\Comment;

use App\Services\ServiceInterface;

class CommentFactory implements ServiceInterface
{

    protected $message;
    protected $status;
    private $queryBuilder;
    protected $request;
    public $path;


    public function __construct($queryBuilder)
    {
        $this->path = "App\\Services\\Comment\\Refine\\";
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function startSearch($data)
    {
        $this->request = $data;
        foreach ($this->request as $data => $key) {
            $data = (string) ucfirst($data);
            $class = $this->path.$data;
            if (class_exists($class)) {
                $filter = new $class();
                $this->queryBuilder = $filter->startSearch($this->queryBuilder, $key);
            }
        }
        return $this->queryBuilder;
    }
}