<?php


namespace  App\Services\Comment\Refine;

class OrderBy
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $filter)
    {
        return $queryBuilder->sort($filter, 'asc');
    }
}