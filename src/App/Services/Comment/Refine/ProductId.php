<?php

namespace  App\Services\Comment\Refine;

class ProductId
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $productId)
    {
        return $queryBuilder->field('productId')->equals(new \MongoId($productId));
    }
}