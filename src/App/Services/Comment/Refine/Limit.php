<?php

namespace  App\Services\Comment\Refine;

class Limit
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $limit)
    {
        return $queryBuilder->limit($limit);
    }
}