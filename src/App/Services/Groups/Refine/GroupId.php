<?php

namespace App\Services\Groups\Refine;

class GroupId
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $groupId)
    {
        return $queryBuilder->addOr($queryBuilder->expr()->field('lab.id')->equals($groupId))
            ->addOr($queryBuilder->expr()->field('classroom.id')->equals($groupId));
    }
}