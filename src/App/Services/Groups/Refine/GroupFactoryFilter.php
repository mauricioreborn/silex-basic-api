<?php

namespace App\Services\Groups\Refine;


class GroupFactoryFilter
{
    private $queryBuilder;
    private $request;


    public function __construct($queryBuilder)
    {
        $this->path = "App\\Services\\Groups\\Refine\\";
        $this->queryBuilder = $queryBuilder;
    }

    /** Method to start the filters build
     * @param array $request
     * @return mixed
     */
    public function startSearch(array $request)
    {
        $this->request = $request;

        foreach ($this->request as $name => $value) {
            if ($value !== null) {
                $className = (string)ucfirst($name);
                $class = $this->path . $className;
                if (class_exists($class)) {
                    $filter = new $class();
                    $this->queryBuilder = $filter->startSearch($this->queryBuilder, $value);
                }
            }
        }
        return $this->queryBuilder;
    }
}