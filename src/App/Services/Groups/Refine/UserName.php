<?php

namespace App\Services\Groups\Refine;

class UserName
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $firstName)
    {
        return $queryBuilder
            ->addOr($queryBuilder->expr()->field('firstName')->equals(new \MongoRegex('/.*'.$firstName.'.*/i')))
            ->addOr($queryBuilder->expr()->field('lastName')->equals(new \MongoRegex('/.*'.$firstName.'.*/i')));
    }
}