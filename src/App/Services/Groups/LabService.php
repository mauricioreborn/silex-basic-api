<?php

namespace App\Services\Groups;

use App\Entities\User;
use App\Repository\Group\GroupRepository;
use App\Repository\Group\LabRepository;
use App\Repository\UserRepository;
use App\Services\Service;
use App\Services\ServiceInterface;

class LabService extends Service implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to insert a new lab
     * @param $data
     * @param $entityManager
     * @return boolean
     */
    public function addLab($data, $entityManager)
    {
        $labRepository = new LabRepository($entityManager);
        $lab = $labRepository->buildLab($data);
        $entityManager->persist($lab);
        $entityManager->flush();
        $data['groupId'] = $lab->getId();
        $data['userId'] = $lab->getUserId();
        $labRepository->join($data);

        $this->setMessage(['message' => 'lab has been add']);
        $this->setStatus(200);
        return $lab->getId();
    }

    /**
     * Method to insert a new lab
     * @param $data
     * @param $entityManager
     * @return boolean
     */
    public function updateLab($data, $entityManager)
    {
        $labRepository = new LabRepository($entityManager);
        $lab = $labRepository->updateLab($data);
        if ($lab) {
            $entityManager->persist($labRepository->getMessage());
            $entityManager->flush();
            $this->setMessage(['message' => 'lab has been updated']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'lab not found']);
        $this->setStatus(400);
        return true;
    }

    /**
     * This method get all labs created.
     * @param array $dataSend              An array with information to paginate
     *                                     and sort the labs.
     * @param LabRepository $labRepository An object used to access
     *                                     labRepository method to get labs
     *                                     information.
     * @return bool
     */
    public function getLabAdmin(array $dataSend, LabRepository $labRepository)
    {
        $labsId = $labRepository->getLabAndClassAdmin(
            $dataSend['userId']
        )['lab'];
        $labs = array();
        if (count($labsId) > 0) {
            foreach ($labsId as $labId) {
                $lab = $labRepository->findLab($labId['id']);
                if (count($lab)  > 0) {
                    $data = $labRepository->buildLabOrClass($lab);
                    $labs[] = $data;
                }
            }
        }

        if (!is_null($dataSend['sort'])) {
            $labs = $this->sortAlphabetically($labs, $dataSend['sort']);
        }

        $paginated = $this->paginateArray(
            $labs,
            $dataSend['limit'],
            $dataSend['skip']
        );

        if (count($labs) > 0) {
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage([
            'message' =>"Looks like you didn't joined in any Lab"
        ]);
        $this->setStatus(400);
        return false;
    }

    /**
     * This method will sort labs by title in alphabetical order ascending or
     * descending
     * @param array $labs   An array with labs to be sorted
     * @param string $sort  A string that will indicate if the order should be
     *                      ASC or DESC
     * @return array        An array with the labs sorted alphabetically
     */
    public function sortAlphabetically(array $labs, string $sort)
    {
        $sortedAlphabetical = [];

        foreach ($labs as $title) {
            $sortByTitle[] = $title['title'];
        }

        if (strtoupper($sort) == 'ASC') {
            natcasesort($sortByTitle);
        } else {
            rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);
        }

        foreach ($sortByTitle as $title) {
            foreach ($labs as $lab) {
                if ($lab['title'] == $title) {
                    $sortedAlphabetical[] = $lab;
                }
            }
        }

        return $sortedAlphabetical;
    }

    public function getLab($dataSend, $entityManager)
    {
        $labRepository = new LabRepository($entityManager);
        $labsId = $labRepository->getLabsIdByUser($dataSend['userId']);
        $labs = array();
        if ($labsId) {
            foreach ($labsId as $labId) {
                $lab = $labRepository->findLab($labId['id']);
                if (count($lab)  > 0) {
                    $data = $labRepository->buildLabOrClass($lab);
                    $labs[] = $data;
                }
            }
        }
        if (count($labs)>0) {
            $paginated = $this->paginateArray($labs, $dataSend['limit'], $dataSend['skip']);
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' =>"Looks like you didn't joined in any Lab"]);
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to join on lab
     * @param $data
     * @param $entityManager
     * @return bool
     */
    public function joinLab($data, $entityManager)
    {
        $labRepository = new LabRepository($entityManager);
        $labRepository->join($data);
        $this->setMessage(['message' =>'the user has been add on lab']);
        $this->setStatus(200);
        return true;
    }

    /** Method to add a new task on lab
     * @param $entityManager
     * @param $data
     */
    public function createTask($entityManager, $data)
    {
        $labRepository = new LabRepository($entityManager);
        $task = $labRepository->buildTask($data);
        $lab = $labRepository->findLab($data['labId']);
        if (count($lab)> 0) {
            $lab->setTasks($task);
            $entityManager->persist($lab);
            $entityManager->flush();
            $this->setMessage(['message' => 'task has been add']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'lab not found']);
        $this->setStatus(400);
        return false;
    }

    /** Method to get all tasks on lab
     * @param $entityManager
     * @param $data
     */
    public function getTasks($entityManager, $data)
    {
        $labRepository = new LabRepository($entityManager);
        $lab = $labRepository->findLab($data['labId']);
        $tasks = $labRepository->getTask($lab, $data['userId']);
        if (count($tasks)>0) {
            $this->setMessage($tasks);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => "Looks like you don't have create any tasks"]);
        $this->setStatus(204);
        return true;
    }

    public function turnIn($em, $data)
    {
        $labRepository = new LabRepository($em);
        $lab = $labRepository->findLab($data['labId']);
        $turnIn = $labRepository->turnIn($lab, $data);
        if ($turnIn === true) {
            $em->persist($labRepository->getMessage());
            $em->flush();
            $this->setMessage(['message' => 'Work Turned In']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => $labRepository->getError()]);
        $this->setStatus(400);
        return true;
    }

    public function seeSubmissions($em, $data)
    {
        $labRepository = new LabRepository($em);
        $lab = $labRepository->findLab($data['labId']);
        $turnIn = $labRepository->seeSubmissions($lab, $data);
        $this->setMessage(['submissions' => $turnIn]);
        $this->setStatus(200);
        return true;
    }
}