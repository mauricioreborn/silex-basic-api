<?php

namespace App\Services\Groups;

use App\Entities\User;
use App\Repository\Group\ClassRoomRepository;
use App\Repository\Group\GroupRepository;
use App\Services\Service;
use App\Services\ServiceInterface;

class ClassRoomService extends Service implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to insert a new lab
     * @param $data
     * @param $entityManager
     * @return boolean
     */
    public function addClass($data, $entityManager)
    {
        $classRoomRepository = new ClassRoomRepository($entityManager);
        $classRoom = $classRoomRepository->buildClass($data);
        $entityManager->persist($classRoom);
        $entityManager->flush();
        $data['classId'] = $classRoom->getId();
        $data['userId'] = $classRoom->getUserId();
        $classRoomRepository->join($data);
        $this->setMessage(['message' =>'Class has been add']);
        $this->setStatus(200);
        return $classRoom->getId();
    }

    /**
     * This method will return all classrooms created
     * @param array $paginate                          An array with information
     *                                                 to paginate the
     *                                                 classrooms.
     * @param ClassRoomRepository $classRoomRepository Object used to access
     *                                                 ClassRoomRepository
     *                                                 methods.
     * @return bool
     */
    public function getClassAdmin(
        array $paginate,
        ClassRoomRepository $classRoomRepository
    ) {
        $classId = $classRoomRepository->getLabAndClassAdmin(
            $paginate['userId']
        )['classRoom'];

        $classes = [];
        if (count($classId)>0) {
            foreach ($classId as $cId) {
                $class = $classRoomRepository->findClassRoom($cId['id']);
                if (count($class)  > 0) {
                    $data = $classRoomRepository->buildLabOrClass(
                        $class,
                        'classRoom'
                    );
                    $classes[] = $data;
                }
            }
        }

        if (!is_null($paginate['sort'])) {
            $classes = $this->sortAlphabetically($classes, $paginate['sort']);
        }

        $paginated = $this->paginateArray(
            $classes,
            $paginate['limit'],
            $paginate['skip']
        );

        if (count($classes) > 0) {
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage(['message' => "Looks like you didn't joined in any Classroom"]);
        $this->setStatus(400);
        return false;
    }

    /**
     * This method will sort classrooms by title in alphabetical order ascending
     * or descending
     * @param array $classrooms   An array with classrooms
     * @param string $sort        A string that can be "ASC" or "Desc" that will
     *                            be used to order the classrooms.
     * @return array              An array with the classrooms sorted
     *                            alphabetically
     */
    public function sortAlphabetically(array $classrooms, string $sort)
    {
        $sortedAlphabetical = [];

        foreach ($classrooms as $title) {
            $sortByTitle[] = $title['title'];
        }

        if (strtoupper($sort) == 'ASC') {
            natcasesort($sortByTitle);
        } else {
            rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);
        }

        foreach ($sortByTitle as $title) {
            foreach ($classrooms as $classroom) {
                if ($classroom['title'] == $title) {
                    $sortedAlphabetical[] = $classroom;
                }
            }
        }

        return $sortedAlphabetical;
    }

    public function getClass($dataSend, $entityManager)
    {
        $classRoomRepository = new ClassRoomRepository($entityManager);
        $classId = $classRoomRepository->getClassIdByUser($dataSend['userId']);

        $classes = [];
        if (count($classId)>0) {
            foreach ($classId as $cId) {
                $class  = $classRoomRepository->findClassRoom($cId['id']);
                if (count($class)  > 0) {
                    $data = $classRoomRepository->buildLabOrClass($class, 'classRoom');
                    $classes[] = $data;
                }
            }
        }
        $paginated = $this->paginateArray($classes, $dataSend['limit'], $dataSend['skip']);
        if (count($classes) > 0) {
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage(['message' => "Looks like you didn't joined in any Classroom"]);
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to join on lab
     * @param $data
     * @param $entityManager
     * @return bool
     */
    public function joinClass($data, $entityManager)
    {
        $ClassRoomRepository = new ClassRoomRepository($entityManager);
        $ClassRoomRepository->join($data);
        $this->setMessage(['message' => 'the user has been add on class']);
        $this->setStatus(200);
        return true;
    }

    public function updateClass($data, $entityManager)
    {
        $labRepository = new ClassRoomRepository($entityManager);
        $lab = $labRepository->updateClass($data);
        if ($lab) {
            $entityManager->persist($labRepository->getMessage());
            $entityManager->flush();
            $this->setMessage(['message' => 'Classroom has been updated']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'Classroom not found']);
        $this->setStatus(400);
        return true;
    }
}