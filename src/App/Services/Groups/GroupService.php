<?php

namespace App\Services\Groups;

use App\Entities\ClassRoom;
use App\Entities\Lab;
use App\Entities\Post;
use App\Entities\ReplyComment;
use App\Entities\User;
use App\Repository\ClassRoomRepository;
use App\Repository\CommentRepository;
use App\Repository\Group\LabRepository;
use App\Repository\LikeRepository;
use App\Repository\UserRepository;
use App\Services\Groups\Refine\GroupFactoryFilter;
use App\Services\Service;
use App\Services\ServiceInterface;
use App\Repository\Group\GroupRepository;
use App\Services\BackOffice\BOService;
use Doctrine\ODM\MongoDB\DocumentManager;

class GroupService extends Service implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to build a sideBar on lab or classroom
     * @param $queryBuilder
     * @param $id
     * @return string
     */
    public function buildSideBar($queryBuilder, $id)
    {
        $groupRepository = new GroupRepository($queryBuilder);
        $sideBar = $groupRepository->buildSideBar($id);
        if ($sideBar) {
            $this->setMessage($sideBar);
            $this->setStatus(200);
            return $sideBar;
        }
        $this->setMessage(['message' => 'group not found']);
        $this->setStatus(400);
    }

    /**
     * Method will get all member of this classroom
     * @param $userQueryBuilder
     * @param $classroomId
     * @return int
     */
    public function getTotalMembers($userQueryBuilder, $classroomId)
    {
        $BOService = new BOService();
        $classMembers = $BOService->getMembersClassRoom($userQueryBuilder, array('id' => $classroomId));

        if ($classMembers == false) {
            return 0;
        }

        return count($classMembers);
    }

    /**
     * Method to build a user grid
     * @param $queryBuilder
     * @param $id
     *
     * @return mixed
     */
    public function buildUser($queryBuilder, $id)
    {

        $groupRepository = new GroupRepository($queryBuilder);
        $user = $groupRepository->buildUser($id);

        if ($user) {
            $this->setMessage($user);
            $this->setStatus(200);
            return $user;
        }
        $this->setMessage(['message' => 'user not found']);
        $this->setStatus(400);
    }

    /**
     * Method to build a post and add on documment
     * @param $queryBuilder
     * @param $data
     */
    public function addPost($entityManager, $data)
    {
        $groupRepository = new GroupRepository($entityManager);
        $post  = $groupRepository->buildPost($data);
        $entityManager->persist($post);
        $entityManager->flush();
        $labClass = '';
        if ($post->getType() === 'lab') {
            $labClassRepo = $entityManager->getRepository(Lab::Class);
            $labClass = $labClassRepo->findOneBy(
                [
                    '_id' => new \MongoId($post->getGroupId())
                ]
            );
        }
        if ($post->getType() === 'classroom') {
            $labClassRepo = $entityManager->getRepository(ClassRoom::Class);
            $labClass = $labClassRepo->findOneBy(
                [
                    '_id' => new \MongoId($post->getGroupId())
                ]
            );
        }
        $labClass->setPosts($post->getId());
        $entityManager->persist($labClass);
        $entityManager->flush();
        $this->setMessage('post has been added');
        $this->setStatus(200);
        return $this;
    }

    /**
     * Method to build the feeds on lab or classroom
     * @param $queryBuilder
     * @param $id
     * @return mixed
     */
    public function buildFeed($queryBuilder, $id)
    {
        $groupRepository = new GroupRepository($queryBuilder);
        $check = $groupRepository->buildFeed($id);
        if ($check === false) {
            return false;
        }
        $post  = array_reverse($groupRepository->buildFeed($id));
        return $post;
    }

    /**
     * Method to return all stream grid
     * @param $em
     * @param $groupId
     * @return bool
     */
    public function feedBuilder($em, $groupId)
    {
        $groupRepository = new GroupRepository($em->createQueryBuilder(Post::class));
        $posts = $groupRepository->getPosts($groupId);
        if (count($posts)>0) {
            $groupRepository->setRepository($em);
            $this->setMessage($groupRepository->feedBuilder($posts));
            $this->setStatus(200);
            return true;
        }
        $this->setMessage([]);
        $this->setStatus(200);
        return false;
    }

    public function checkClassCode($em, $data)
    {
        $repo = $em->createQueryBuilder(User::class);
        $groupRepository = new GroupRepository($repo);
        $groupFilterFactory = new GroupFactoryFilter($repo);
        $queryFiltered = $groupFilterFactory->startSearch($data);
        $users = $queryFiltered;

        $total = count($users)/$data['limit'] <1 ? 1 :ceil(count($users)/$data['limit']);

        $userBuilded = '';
        if ($total > 0) {
            foreach ($users as $user) {
                $user = $groupRepository->buildUser($user->getId());
                $userBuilded[] = $user;
                $userBuilded['totalPages'] = $total;
                $userBuilded['totalMembers'] = count($users);
            }
            $this->setMessage($userBuilded);
            $this->setStatus(200);
            return $userBuilded;
        }
        $this->setMessage(['message'=>'Group or User Not found']);
        $this->setStatus(400);
        return false;
    }

    public function buildRooster($user, $workBuilder)
    {
        $groupRepository = new GroupRepository($workBuilder);
        $post  = $groupRepository->buildRooster($user);
        return $post;
    }


    /**
     * Method to get All labs and classroom
     * @param array $paginate                  An array with the information to
     *                                         paginate the return.
     * @param GroupRepository $groupRepository Object used to access
     *                                         groupRepository methods.
     * @return bool
     */
    public function getLabAndClassAdmin(
        array $paginate,
        GroupRepository $groupRepository
    ) {
        $labAndClassroomId = $groupRepository->getLabAndClassAdmin();
        $data = [];
        if ($labAndClassroomId) {
            $labs = [];
            if (!empty($labAndClassroomId['lab'])) {
                foreach ($labAndClassroomId['lab'] as $labId) {
                    $lab = $groupRepository->findLab($labId['id']);
                    if (count($lab)  > 0) {
                        $data = $groupRepository->buildLabOrClass($lab, 'lab');
                        $labs[] = $data;
                    }
                }
            }

            $classRoom = [];
            if (!empty($labAndClassroomId['classRoom'])) {
                foreach ($labAndClassroomId['classRoom'] as $labId) {
                    $class = $groupRepository->findClassRoom($labId['id']);
                    if (count($class) > 0) {
                        $data = $groupRepository->buildLabOrClass(
                            $class,
                            'classroom'
                        );
                        $classRoom[] = $data;
                    }
                }
            }
            $data = array_merge($labs, $classRoom);
        }

        if (!is_null($paginate['sort'])) {
            $data = $this->sortAlphabetically($data, $paginate['sort']);
        }

        $paginated = $this->paginateArray(
            $data,
            $paginate['limit'],
            $paginate['skip']
        );

        if (count($data)>0) {
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage([
            "message"=> "Looks like you didn't joined in any Classroom or Lab"
        ]);
        $this->setStatus(400);
        return false;
    }

    /**
     * This method will sort classrooms and labs by title in alphabetical order
     * ascending or descending
     * @param array $groups An array with classrooms and lab
     * @param string $sort  A string that can be "ASC" or "Desc" that will be
     *                      used to order the groups.
     * @return array        An array with the classrooms and labs sorted
     *                      alphabetically
     */
    public function sortAlphabetically(array $groups, string $sort)
    {
        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($groups as $title) {
            $sortByTitle[] = $title['title'];
        }

        if (strtoupper($sort) == 'ASC') {
            natcasesort($sortByTitle);
        } else {
            rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);
        }


        foreach ($sortByTitle as $title) {
            foreach ($groups as $group) {
                if ($group['title'] == $title) {
                    $sortedAlphabetical[] = $group;
                }
            }
        }

        return $sortedAlphabetical;
    }

    /**
     *  Method to get total labs and clasrooms
     * @param $dataSend
     * @param $entityManager
     * @return bool
     */
    public function getLabAndClass($dataSend, $entityManager)
    {
        $groupRepository = new GroupRepository($entityManager);
        $labAndClassroomId = $groupRepository->getLabsAndClassById($dataSend['userId']);
        $data = array();
        if ($labAndClassroomId) {
            $labs = array();

            if (!empty($labAndClassroomId['lab'])) {
               $labsIds = $groupRepository->castMongoId($labAndClassroomId['lab']);
                $rangeLabs = $groupRepository->findRangeLab($labsIds, 'title');

                foreach ($rangeLabs as $lab) {
                    $data = $groupRepository->buildLabOrClass($lab, 'lab');
                    $labs[] = $data;
                }

            }

            $classRoom = array();
            if (!empty($labAndClassroomId['classRoom'])) {
                $ClassRoomIds = $groupRepository->castMongoId($labAndClassroomId['classRoom']);
                $rangeClassroom = $groupRepository->findRangeClassRoom($ClassRoomIds, 'title');

                foreach ($rangeClassroom as $classroom) {
                    $data = $groupRepository->buildLabOrClass($classroom, 'classroom');
                    $classRoom[] = $data;
                }

            }

            $data =  array_merge($labs,$classRoom);
        }
        $paginated = $this->paginateArray($data, $dataSend['limit'], $dataSend['skip']);
        if (count($paginated)>0) {
            $this->setMessage($paginated);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage(["message"=> "Looks like you didn't joined in any Classroom or Lab"]);
        $this->setStatus(400);
        return false;
    }

    

    

    /** Method to join on lab or classroom
     * @param $entityManager
     * @param $data
     * @return bool
     */
    public function joinGroup($entityManager, $data)
    {
        $labRepository = new LabRepository($entityManager->createQueryBuilder(Lab::class));
        $classRoomRepository = new ClassRoomRepository($entityManager->createQueryBuilder(ClassRoom::class));
        
        if (count($labRepository->getLabById($data['groupId']))>0) {
            if ($data['level'] >= 3) {
                $labRepository->setRepository($entityManager);
                $join = $labRepository->join($data);
                if ($join === false) {
                    $this->setMessage(['message' => 'You already entered this group']);
                    $this->setStatus(200);
                    return true;
                }
                $labRepository->updateCount($data['groupId'], 'lab');
                $this->setMessage(['message' => 'code has been accepted']);
                $this->setStatus(200);
                return true;
            }
            $this->setMessage(['message' => 'only level 3 members can join on a lab']);
            $this->setStatus(200);
            return true;
        }

        if (count($classRoomRepository->getClassRoomById($data['groupId']))>0) {
            $classRoomRepository->setRepository($entityManager);
            $labRepository->setRepository($entityManager);
            $join = $classRoomRepository->join($data);
            if ($join === false) {
                $this->setMessage(['message' => 'You already entered this group']);
                $this->setStatus(200);
                return true;
            }
            $labRepository->updateCount($data['groupId'], 'classroom');
            
            $this->setMessage(['message' => 'code has been accepted']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'code not found']);
        $this->setStatus(213);
        return false;
    }

    /** Method to insert a new like on posts
     * @param $entityManager
     * @param $data
     * @return bool
     */
    public function addLikeOnPost($entityManager, $data)
    {

        $groupRepository = new GroupRepository($entityManager->createQueryBuilder(Post::class));
        $likeRepository =  new LikeRepository($entityManager);
        $like = $likeRepository->buildLike($data);
        $post = $groupRepository->getComment($data['productId']);
        if (count($post)>0) {
            $post->setLike($like);
            $entityManager->persist($post);
            $entityManager->flush();
            $this->setMessage(['message' => 'like has been add']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'post not found']);
        $this->setStatus(213);
        return false;
    }


    /** Method to verify if user already like a comment
     * @param $queryBuilder
     * @param $data
     */
    public function findLike($queryBuilder, $data)
    {
        $likeRepository  = new LikeRepository($queryBuilder);
        $likes = $likeRepository->findLike($data);
        if (count($likes) >0 ) {
            $this->setMessage('the user alright likes this post');
            $this->setStatus(400);
            return true;
        }
        $this->setMessage('the user alright likes this post');
        $this->setStatus(400);
        return false;
    }


    public function updateCover($em, $data)
    {
        $groupRepo = new GroupRepository($em);
        $group = $groupRepo->updateCover($data);
        if ($group === true) {
            $this->setMessage('Group Cover Updated');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Group Not Found');
        $this->setStatus(400);
        return false;
    }

    public function deleteGroup($em, $data)
    {
        $groupRepo = new GroupRepository($em);
        $group = $groupRepo->deleteGroup($data);
        if ($group === true) {
            $this->setMessage('Group Deleted');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Group Not Found or User Not Allowed');
        $this->setStatus(400);
        return false;
    }

    public function addUserOnGroup($em, $data)
    {
        $groupRepo = new GroupRepository($em);
        $group = $groupRepo->addUserOnGroup($data);

        if ($group === true) {
            $this->setMessage('User Added in group');
            $this->setStatus(200);
            return true;
        }
        if ($group === 'notOwner') {
            $this->setMessage('You Are Not The Owner of This Group');
            $this->setStatus(400);
            return true;
        }
        if ($group === 'alreadyIn') {
            $this->setMessage('User Already part of group');
            $this->setStatus(400);
            return true;
        }
        $this->setMessage('Group Not Found or User Not Allowed');
        $this->setStatus(400);
        return false;
    }
    
    public function incrementNewMember($queryBuilder, $id)
    {
        $groupRepository = new GroupRepository($queryBuilder);
        $groupRepository->incrementNewMember($id);
    }

    /**
     * Remove a post
     * @param $postBuilder
     * @param $id
     */
    public function removeAPost($postBuilder, $id)
    {
        $groupRepo = new GroupRepository($postBuilder);
        $groupRepo->removeAPost($id);
        $this->setMessage(['message' => 'post has been removed']);
        $this->setStatus(200);
    }

    /** Method to remove an user of the group
     * @param $em
     * @param $data
     * @return bool
     */
    public function removeUserRoster($em, $data)
    {
        $groupRepo = new GroupRepository($em);

        $group = $groupRepo->findClassRoom($data['groupId']);
        if ($data['type'] == 'lab') {
           $group =  $groupRepo->findLab($data['groupId']);
        }
        $groupRepo = new GroupRepository($em);
        $groupRepo->decreaseTotalMember($group);
        $em->persist($group);
        $em->flush($group);
        $response = $groupRepo->removeUserRoster($data);
        if ($response == true) {
            $this->setMessage(['message' => 'User has been removed from group']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'User not found inside group']);
        $this->setStatus(400);
        
        
        return true;
    }

    /** Method to remove an embedded  task by their id
     * @param $entityManager
     * @param $taskId
     * @return bool
     */
    public function removeTask($entityManager, $taskId)
    {
        $groupRepo = new GroupRepository($entityManager->createQueryBuilder(Lab::class));
        $group = $groupRepo->removeTask($taskId);
        if ($group) {
            $entityManager->persist($group);
            $entityManager->flush();
            $this->setMessage(['message' => 'task has been removed']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'task not found']);
        $this->setStatus(200);
        return false;

    }

    /** Method to edit a task
     * @param $entityManager
     * @param $data[userId,taskId,title,description,duoDate]
     * @return bool
     */
    public function editTask($entityManager, $data)
    {
        $groupRepo = new GroupRepository($entityManager->createQueryBuilder(Lab::class));
        $lab = $groupRepo->taskFind($data['taskId']);
        if (count($lab)>0) {
            $labRepository = new LabRepository($entityManager);
            $newLab = $labRepository->buildTask($data);
            $newLab->setId(new \MongoId($data['taskId']));
            $group = $groupRepo->removeTask($data['taskId']);
            $entityManager->persist($group);
            $entityManager->flush();
            $lab->setTasks($newLab);
            $entityManager->persist($lab);
            $entityManager->flush();
            $this->setMessage(['message' => 'task has been changed']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'task not found']);
        $this->setStatus(401);
        return false;
        
    }
}
