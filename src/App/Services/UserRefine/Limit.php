<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 08/12/17
 * Time: 19:15
 */

namespace App\Services\UserRefine;


class Limit
{

    private $type;

    public function __construct()
    {
        $this->setType('limit');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $limit)
    {
        return $queryBuilder
            ->limit($limit);
    }
}