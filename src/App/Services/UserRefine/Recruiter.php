<?php

namespace App\Services\UserRefine;

class Recruiter
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $type)
    {
        if (filter_var($type['name'], FILTER_VALIDATE_EMAIL)) {
            return $queryBuilder
                ->field('email')->equals($type['name'])
                ->field('level.alias')->equals('recruiter');

        } else {
            return $queryBuilder
                ->addOr($queryBuilder->expr()->field('firstName')->equals(new \MongoRegex('/.*'.$type['name'].'.*/i')))
                ->addOr($queryBuilder->expr()->field('lastName')->equals(new \MongoRegex('/.*'.$type['name'].'.*/i')))
                ->field('level.alias')->equals('recruiter');
        }
    }
}