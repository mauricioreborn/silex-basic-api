<?php

namespace App\Services\UserRefine;

class ClassCode
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $classCode)
    {
        return $queryBuilder
            ->addOr($queryBuilder->expr()->field("classroom.id")->in(array($classCode)))
            ->addOr($queryBuilder->expr()->field("lab.id")->in(array($classCode)));
    }
}