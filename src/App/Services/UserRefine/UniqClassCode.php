<?php

namespace App\Services\UserRefine;

class UniqClassCode
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $id)
    {
        return $queryBuilder->field("classroom.id")->in(array($id));
    }
}