<?php

namespace App\Services\UserRefine;

class Name
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $name)
    {
        if (is_array($name)) {
            if (filter_var($name['name'], FILTER_VALIDATE_EMAIL)) {
                return $queryBuilder
                    ->field('email')->equals($name['name']);
            } else {
                return $queryBuilder
                    ->addOr($queryBuilder->expr()
                    ->field('firstName')
                    ->equals(new \MongoRegex('/.*' . $name['name'] . '.*/i')))
                    ->addOr($queryBuilder->expr()
                    ->field('lastName')
                    ->equals(new \MongoRegex('/.*' . $name['name'] . '.*/i')));
            }
        } else {
            return $queryBuilder
                ->addOr($queryBuilder->expr()->field('firstName')->equals(new \MongoRegex('/.*' . $name. '.*/i')))
                ->addOr($queryBuilder->expr()->field('lastName')->equals(new \MongoRegex('/.*' . $name. '.*/i')));
        }

    }
}