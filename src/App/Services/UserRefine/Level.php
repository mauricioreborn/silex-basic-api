<?php

namespace App\Services\UserRefine;

class Level
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $type)
    {
        if ($type = 'all') {
            return $queryBuilder;
        }
        return $queryBuilder->field('level.alias')->equals($type);
    }
}