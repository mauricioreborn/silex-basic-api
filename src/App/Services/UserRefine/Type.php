<?php

namespace App\Services\UserRefine;

use Doctrine\MongoDB\Query\Builder;

class Type
{
    /**
     * Add a type or kind of user profile that will be search in the data base
     * @param $queryBuilder
     * @param $type
     * @return mixed
     */
    public function startSearch(Builder $queryBuilder, string $type)
    {
        if ($type === 'all') {
            return $queryBuilder;
        }

        return $queryBuilder->where(
            "function ($type) 
            {
                if ('$type' != 'organization' && '$type' != 'admin') {
                    return this.profile == '$type';
                    }
                return this.level.alias == '$type';
            }"
        );
    }
}
