<?php

namespace App\Services\UserRefine;

class SortAsc
{
    private $type;

    public function __construct()
    {
        $this->setType('find');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $type)
    {
        
        if ($type == 'name') {
            return $queryBuilder->sort('firstName','asc');
        }

        return $queryBuilder->sort($type,'asc');
    }
}