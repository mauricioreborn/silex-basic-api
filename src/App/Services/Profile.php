<?php

namespace App\Services;

use Doctrine\ODM\MongoDB\DocumentRepository;

class Profile implements ServiceInterface
{

    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function getProfiles($dm)
    {
            $qb = $dm->createQueryBuilder('App\Entities\Profile')
            ->eagerCursor(true);
        $query = $qb->getQuery();
        $cursor = $query->execute();

        $communities  = array();

        foreach ($cursor as $value) {
            $community['label'] = $value->getLabel();
            $community['name'] = strtolower($value->getName());
            $community['description'] = $value->getDescription();
            $community['order'] = $value->getOrder();
            $communities[] = $community;
        }

        return $communities;
    }
}