<?php

namespace App\Services\Events;

use App\Entities\LiveSession;
use App\Entities\Work;
use App\Repository\CommunityRepository;
use App\Repository\EventRepository;
use App\Repository\LiveSessionRepository;
use App\Repository\WorkRepository;
use App\Services\ServiceInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoId;

class EventService implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /** Method to get all eventList by range date
     * @param $entityManager
     * @param $data[startDate,endDate]
     * @param $ui
     * @return bool
     */
    public function getEventList($entityManager, $data, $ui)
    {
        $eventRepository = new EventRepository($entityManager);
        $events = $eventRepository->getEventList($data, $ui);
        if (count($events) == 0) {
            $this->setMessage(['message' => "Looks like you aren't Enrolled in any Events"]);
            $this->setStatus(204);
            return false;
        }
        $this->setMessage($events);
        $this->setStatus(200);
        return true;
    }

    /** Method to get enrolledList
     * @param $em
     * @param $data[startDate,endDate]
     * @param $uid
     * @return bool
     */
    public function getEventEnrolledList($em, $data, $uid)
    {
        $eventRepository = new EventRepository($em);
        $events = $eventRepository->getEventEnrolledList($data, $uid);
        if (count($events) == 0) {
            $this->setMessage('No recent Events Found');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($events);
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to add users to an event
     * @param $entityManager
     * @param $eventId
     * @param $userIds
     * @return bool
     */
    public function setEnroll($entityManager, $eventId, $userIds)
    {
        $eventRepository = new EventRepository($entityManager);
        $usersEnrolled = 0;
        
        foreach ($userIds as $userId) {
            $events = $eventRepository->setEnroll($eventId, $userId);
            if (count($events) == 0) {
                $this->setMessage('Event not valid');
                $this->setStatus(400);
                return false;
            }
            if ($events) {
                $usersEnrolled ++;
            }
        }

        if ($usersEnrolled == 0) {
            $this->setMessage('Users already enrolled');
            $this->setStatus(403);
            return false;
        }

        $this->setMessage($usersEnrolled . " Users were enrolled");
        $this->setStatus(200);
        return true;
    }


    public function setUnEnroll($em, $eid, $uid)
    {
        $eventRepository = new EventRepository($em);
        $events = $eventRepository->setUnEnroll($eid, $uid);
        if (count($events) == 0) {
            $this->setMessage('Event not valid');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage('User Disenrolled');
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to remove works related to an user.
     * @param $entityManager
     * @param $eventId
     * @param $userId
     * @return string
     */
    public function setUnEnrollWork(DocumentManager $entityManager, string $eventId, string $userId)
    {
        $eventRepository = new EventRepository($entityManager);
        $deleteWorkResult = $eventRepository->setUnEnrollWork($eventId, $userId);

        return $deleteWorkResult;
    }

    /**
     * Method to set the event's deadLine
     * @param $entityManager
     * @param $deadLineInfo
     * @return mixed
     */
    public function setDeadLine(DocumentManager $entityManager, array $deadLineInfo)
    {
        $eventRepository = new EventRepository($entityManager);
        $deadLineResult = $eventRepository->setDeadLine($deadLineInfo);
        $this->setMessage($deadLineResult);
        $this->setStatus(200);
        
        return $deadLineResult;
    }

    /**
     * Method to get event details by eventId and userId
     * @param $em
     * @param $eventId
     * @param $userId
     * @return bool
     */
    public function getEventDetails($em, $eventId, $userId)
    {
        $eventRepository = new EventRepository($em);
        $events = $eventRepository->getEventDetails($eventId, $userId);
        if (count($events) == 0) {
            $this->setMessage('Event not valid');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($events);
        $this->setStatus(200);
        return $events;
    }


    /**
     * Method to sort submitted works
     * @param DocumentManager $documentManager
     * @param array $data must contain workId,sessionId and an order number
     * @return bool
     */
    public function sessionWorksOrder(DocumentManager $documentManager, array $data)
    {
        $liveSessionEntity = $documentManager->createQueryBuilder(LiveSession::class);
        $liveSessionRepository = new LiveSessionRepository($liveSessionEntity);
        $liveSession = $liveSessionRepository->findAnUser($data['eventId']);
        if (count($liveSession) > 0 ) {
            $worksQueue = $liveSession->getWorkQueue();
            if (count($worksQueue) > 0 ) {
                foreach ($worksQueue as $work) {
                    if ($work['workId'] == $data['workId']) {
                        $liveSession->updateWorkQueue($data);
                        $documentManager->persist($liveSession);
                        $documentManager->flush();

                        $this->setMessage('work has been sorted');
                        $this->setStatus(200);
                        return true;
                    }
                }
                $this->setMessage('work not found');
                $this->setStatus(200);
                return false;
            }
        }
        $this->setMessage('Session not found');
        $this->setStatus(301);
        return false;
    }

    /**
     * Method to get works of enrolled users.
     * @param DocumentManager $entityManager
     * @param array $data
     * @return bool
     */
    public function getWorksOfEnrolledUsers(
        DocumentManager $entityManager,
        array $data
    ) {
        $work = [];
        $workRepository = new WorkRepository($entityManager);

        if (ucfirst($data['level']) == 'Admin') {
          $works = $workRepository->getAllWorks($data['limit']);

          foreach ($works as $workObject) {
            $work[] = $workRepository->buildWork($workObject);
          }

          $this->setMessage($work);
          $this->setStatus(200);
          return true;
        }

        $sessionRepository = new LiveSessionRepository($entityManager->createQueryBuilder(LiveSession::class));
        $session = $sessionRepository->findAnUser($data['sessionId']);

        if (count($session->getId()) > 0 ) {
            if (count($session->getUsers()) > 0 ) {
                $mongoIdUsers = [];
                foreach ($session->getUsers() as $userId) {
                    $mongoIdUsers[] = new MongoId($userId);
                }

                $works = $workRepository->getWorksByUserId(
                    $data['userId'],
                    $data['limit']
                );

                foreach ($works as $workObject) {
                    $work[] = $workRepository->buildWork($workObject);
                }
                $this->setMessage($work);
                $this->setStatus(200);
                return true;
            }
            $this->setMessage('users not found');
            $this->setStatus(200);
            return false;
        }
        $this->setMessage('session not found');
        $this->setStatus(400);
        return false;
    }
}
