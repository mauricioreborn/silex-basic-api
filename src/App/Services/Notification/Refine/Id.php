<?php

namespace App\Services\Notification\Refine;

class Id
{
    private $type;

    public function __construct()
    {
        $this->setType('skip');
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function startSearch($queryBuilder, $id)
    {
        return $queryBuilder->field("_id")->equals(new \MongoId($id));
    }
}