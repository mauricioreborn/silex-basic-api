<?php

namespace App\Services\Notification;

use App\Entities\Notification;
use App\Services\Notification\Refine\Skip;

class NotificationFactory
{
    private $queryBuilder;
    private $path;
    private $request;
    private $limitSkip = array();



    public function __construct($entity)
    {
        $this->path = "App\\Services\\Notification\\Refine\\";
        $this->queryBuilder = $entity;
    }

    public function startSearch(array $request)
    {
        $this->request = $request;
        $this->limitSkip['skip'] = $this->request['skip'];
        $this->limitSkip['limit'] = $this->request['limit'];
        unset($this->request['skip']);
        unset($this->request['limit']);
        foreach ($this->request as $name => $value) {
            if ($value !== null) {
                $className = (string)ucfirst($name);
                $class = $this->path . $className;
                if (class_exists($class)) {
                    $filter = new $class();
                    $this->queryBuilder = $filter->startSearch($this->queryBuilder, $value);
                }
            }
        }
        $skip = new Skip();
        return  $skip->startSearch($this->queryBuilder, $this->limitSkip);
    }
}