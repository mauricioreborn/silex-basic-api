<?php

/**
 * Check Community File Doc Comment
 *
 * PHP version 7
 *
 * @category Check Community
 * @package  MyPackage
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */

namespace App\Services\Commom;

use App\Controller;
use App\Entities\Community;
use Silex\Application;

/**
 * Class to Check Community
 *
 * @category Class
 * @package  MyPackage
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class CheckCommunity extends Controller
{

    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function checkCommunity($data,$repository)
    {
        $findCommunity = $repository->findBy(
            [
                'name' => ucfirst($data),
            ]
        );

        if (count($findCommunity) === 0) {
            return false;
        }
        return true;
    }
}
