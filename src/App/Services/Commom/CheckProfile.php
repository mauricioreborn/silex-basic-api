<?php

/**
 * CheckProfile File Doc Comment
 *
 * PHP version 7
 *
 * @category UploadPhoto
 * @package  MyPackage
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */

namespace App\Services\Commom;

use App\Controller;
use App\Entities\Profile;
use Silex\Application;
use App\Services\Service;

/**
 * Class to Upload Photo
 *
 * @category Class
 * @package  Profile
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class CheckProfile extends Service
{

    public function checkProfile($data, $repository)
    {
        $findProfile = $repository->findBy(
            [
                'name' => ucfirst($data),
            ]
        );
        
        if ($findProfile) {
                return true;
        }
        return false;
    }
}
