<?php

/**
 * UploadPhoto File Doc Comment
 *
 * PHP version 7
 *
 * @category UploadPhoto
 * @package  MyPackage
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */

namespace App\Services\Commom;

use App\Services\ServiceInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use \Silex\Application;

/**
 * Class to Upload Photo
 *
 * @category Class
 * @package  MyPackage
 * @author   Display Name <wsantos@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class UploadPhoto implements ServiceInterface
{
    private $fileName;
    private $targetDir;
    private $userId;
    private $application;
    private $aws;
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
    
    public function __construct(Application $app, $userId, $preFolder = 'users')
    {
        $this->userId = $userId;
        $this->application = $app;
        $this->targetDir = 'uploads/'.$preFolder.'/'.$userId.'/';
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName[] = $fileName;
    }

    public function upload(UploadedFile $file)
    {
            //'Bucket' => getenv('S3_BUCKET'),
            $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();

            $s3 = $this->application['aws']->createS3()
                ->putObject([
                    'Key' => $this->targetDir.$fileName,
                    'Bucket' => 'acmeconnect-data-dm',
                    'ContentType' => $file->getClientMimeType(),
                    'Body' => file_get_contents($file),
                    'ACL' => 'public-read'
                ]);

            return $s3['ObjectURL'];
    }

    /**
     * @return string
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

    /**
     * @param string $targetDir
     */
    public function setTargetDir($targetDir)
    {
        $this->targetDir = $targetDir;
    }

}
