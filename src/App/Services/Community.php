<?php


namespace App\Services;


class Community implements ServiceInterface
{

    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function getCommunities($repository)
    {
        $datas = $repository->findAll();
        $communities = array();

        foreach ($datas as $data) {
            $community['name'] = $data->getName();
            $community['description'] = $data->getDescription();
            $community['order'] = $data->getOrder();
            $communities[] = $community;
        }

        return $communities;
    }
}