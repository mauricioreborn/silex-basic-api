<?php

namespace App\Services\Setting;

use App\Entities\Comment;
use App\Entities\Comments;
use App\Entities\Flag;
use App\Entities\Work;
use App\Repository\CommentRepository;
use App\Repository\Setting\FlagRepository;
use App\Repository\Setting\SettingRepository;
use App\Repository\UserRepository;
use App\Repository\WorkRepository;
use App\Services\FlagService;
use App\Services\ServiceInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use \MongoId;

class SettingService implements ServiceInterface
{

    protected $message;
    protected $status;
    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to change user account information
     * @param $queryBuilder
     * @param $data
     */
    public function changeAccount($queryBuilder, $data)
    {
        $settingRepository = new SettingRepository($queryBuilder);
        $settingRepository->changeAccount($data);
        $this->setMessage('Account information has been updated');
        $this->setStatus(200);
    }


    /** Method to verify if is a valid password
     * @param $queryBuilder
     * @param $data
     * @return boolean
     */
    public function validatePassword($queryBuilder, $data)
    {
        $settingRepository = new SettingRepository($queryBuilder);
        $user = $settingRepository->verifyUserPassword($data);
        if ($user === true) {
            $this->setMessage('password is valid');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('the current password is invalid');
        $this->setStatus(213);
        return false;
    }

    /** Method to change the current password
     * @param $queryBuilder
     * @param $data
     * @return bool
     */
    public function changePassword($queryBuilder, $data)
    {
        $settingRepository = new SettingRepository($queryBuilder);
        $settingRepository->changePassword($data);
        $this->setMessage('password has been changed');
        $this->setStatus(200);
        return true;
    }

    /** Method to get user data
     * @param User $queryBuilder
     * @param $data
     * @return boolean
     */
    public function getUserData($queryBuilder, $data)
    {
        $userRepository = new SettingRepository($queryBuilder);
        $user = $userRepository->getUserInformation($data['userId']);
        if ($user) {
            $this->setMessage($userRepository->getMessage());
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('user not found');
        $this->setStatus(401);
        return false;
    }

    /**
     * @param $userQueryBuilder
     * @param $data
     */
    public function changeNotification($userQueryBuilder, $data)
    {
        $settingRepository = new SettingRepository($userQueryBuilder);
        $settingRepository->changeNotification($data);
        $this->setMessage('notification has been updated');
        $this->setStatus(200);
    }

    /**Method to set a flag on FlagDocument
     * @param $entityManager
     * @param $data[userId,productId,type]
     * @return boolean
     */
    public function setFlag($entityManager, $data)
    {
        $flagRepository = new FlagRepository($entityManager);
        $flag = $flagRepository->flagBuild($data);
        $entityManager->persist($flag);
        $entityManager->flush();
        $this->setMessage('flag has been add');
        $this->setStatus(200);
        return $flag->getId();
    }

    public function getFlags($entityManager)
    {
        $flagRepository = new FlagRepository($entityManager);
        $flags = $flagRepository->getFlags();
        if ($flags) {
            $this->setMessage($flags);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'no flags to show']);
        $this->setStatus(401);
        return true;
    }

    /**
     * This method will verify the parameter type and remove
     * the flag of the inside product if the user was flagged.
     * @param DocumentManager $entityManager
     * @param array $flagRequest an array with request data to remove
     *                           the flag received from params
     *                           how type as string.
     * @return bool              true for flag removed and false
     *                           to flag not found
     */
    public function removeFlag(
        DocumentManager $entityManager,
        array $flagRequest) {
        $repository = new CommentRepository(
            $entityManager->createQueryBuilder(Comments::class)
        );

        if ($flagRequest['type'] == 'work') {
            $repository = new WorkRepository(
                $entityManager->createQueryBuilder(Work::class)
            );
        }

        $product = $repository->findAnUser($flagRequest['productId']);

        if (count($product) > 0) {
            $product->setFlagged(false);
            if (count($product->getFlag()) > 0) {
                $product->removeUserFlag($flagRequest['userId']);
                $entityManager->persist($product);
                $entityManager->flush();
            }
            $flagRepository = new FlagRepository(
                $entityManager->createQueryBuilder(Flag::class)
            );

            if (ucfirst($flagRequest['level']) == 'Admin') {
                if (count($product->getFlag()) > 0) {
                    $repository->resetFlag($flagRequest['productId']);
                }

                $flagRepository->disableAllFlags($flagRequest['productId']);
                $this->setMessage(['message' => 'Flag has been removed']);

            } else {
                $flagRepository->disableFlag($flagRequest);
                $this->setMessage(['message' => 'Your flag has been removed']);
            }
            $this->setStatus(200);
            return true;
        }
        $this->setMessage([
            'message' => "flag not found"
        ]);
        $this->setStatus(400);
        return false;
    }
}

