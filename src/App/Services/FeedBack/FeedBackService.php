<?php

namespace App\Services\FeedBack;

use App\Entities\User;
use App\Entities\Work;
use App\Repository\CommunityRepository;
use App\Repository\FeedBackRepository;
use App\Repository\UserRepository;
use App\Services\ServiceInterface;

class FeedBackService implements ServiceInterface
{

    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getFeedBack($entityManager, $data)
    {
        $feedRepo = new FeedBackRepository($entityManager);
        $feed = $feedRepo->getFeedBack($data);
        if (count($feed) == 0) {
            $this->setMessage('No Works Found');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($feed);
        $this->setStatus(200);
        return true;
    }
    
    /** Method to save a feedback inside a work
     * @param $em
     * @param $workId
     * @param $criteria
     * @param $msg
     * @param $userId
     * @return bool
     */
    public function saveFeedBack($em, $workId, $criteria, $msg, $userId, $owner)
    {
        $feedRepo = new FeedBackRepository($em);

        $fb = $feedRepo->saveFeedBack($workId, $criteria, $msg, $userId);
        if ($fb === true) {
            $userRepository = new UserRepository($em->createQueryBuilder(User::class));
            //$userRepository->removeAllTotalWork($owner);
            $userRepository->removeAllTotalComment($owner);
            $this->setMessage(['message' => 'FeedBack Saved']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'Work or User Not Found']);
        $this->setStatus(204);
        return true;
    }

}