<?php

namespace App\Services;

use App\Entities\Post;
use App\Entities\ReplyComment;
use App\Entities\Work;
use App\Repository\CommentRepository;
use App\Repository\Group\GroupRepository;
use App\Repository\WorkRepository;
use Silex\Application;

abstract class Service
{
    private $app;

    /**
     * @return Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param Application $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }

    /**
     * Method to paginate an array
     * @param $array
     * @param $limit
     * @param $skip
     * @return array
     */
    public function paginateArray($array, $limit, $skip)
    {
        $result = array();
        if ($limit == 0) {
            return $array;
        }
        if (count($array) > 0) {
            $result = array_slice($array, (int)$skip, (int)$limit);
        }
        return $result;
    }


    /** Generic method to add a new ReplyComment
     * @param $entityManager
     * @param $data
     */
    public function postReplyComment($entityManager, $data)
    {
        $groupRepository = new GroupRepository($entityManager->createQueryBuilder(ReplyComment::class));
        $comment = $groupRepository->buildAReplyComment($data);
        $entityManager->persist($comment);
        $entityManager->flush();
        $this->setMessage('comment has been add');
        $this->setStatus(200);
    }

    /** Generic Method to add a new Comment
     * @param $entityManager
     * @param $data
     * @return bool
     */
    public function postComment($queryBuilder, $entityManager, $data)
    {
        $groupRepository = new GroupRepository($queryBuilder);
        $post = $groupRepository->getComment($data['postId']);
        if (count($post)>0) {
            $newComment = $groupRepository->buildAComment($data);
            $entityManager->persist($newComment);
            $entityManager->flush();
            $commentRepository = new CommentRepository($queryBuilder);
            $commentRepository->incrementComment($data['postId']);
            $this->setMessage('comment has been add');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'post not found']);
        $this->setStatus(400);
        return false;
    }

    /** Method to update workActivity
     * @param $entity
     * @param $data['postId',userId,type]
     */
    public function updateWorActivity($entity, $data)
    {
        $workRepository = new WorkRepository($entity->createQueryBuilder(Work::class));
        $activity = $workRepository->buildNewActivity($data);
        $work = $workRepository->updateWorkActivity($data['postId'], $activity);
        $entity->persist($work);
        $entity->flush();
    }
}