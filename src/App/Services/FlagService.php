<?php

/**
 * Entity Flag
 *
 * PHP version 7
 *
 * @category Service
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Services;


use App\Entities\Flag;
use App\Entities\FlagContent;
use App\Repository\FlagRepository;
use Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\Query\Builder;

/**
 * Entity Flag
 *
 * PHP version 7
 *
 * @category Service
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
class FlagService implements ServiceInterface
{
    /**
     * Variable to set information about the methods
     *
     * @var
     */
    protected $message;

    /**
     * Method to set status about the methods 200,400 etc
     *
     * @var
     */
    protected $status;

    /**
     * Method to get messages of the return methods
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Method to set information about all methods
     *
     * @param mixed $message A message about the method
     *
     * @return null
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Method to return status about all method
     *
     * @return integer Http status responses
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Method to set status about method information
     *
     * @param integer $status Http status response
     *
     * @return null
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to check if a comment or work was flagged inside
     * of flag document.
     *
     * @param Cursor $flagBuilder The flag QueryBuilder object
     * @param string $productId   The comment or work string mongo identifier.
     *
     * @return bool               True as flagged and false as not flagged
     */
    public function checkFlag(Cursor $flagBuilder, string $productId)
    {
        $flagRepository = new FlagRepository($flagBuilder);
        $flags = $flagRepository->getFlag($productId);
        if (count($flags) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method to create a flag content.
     *
     * @param FlagContent $flagEntity flagEntity to create a one
     * @param string      $userId     UserId logged
     *
     * @return Builder                A object to save on database
     */
    public function createFlagContent(Builder $flagEntity, string $userId)
    {
        $flagRepository = new FlagRepository($flagEntity);
        return $flagRepository->createFlagContent($userId);
    }
}