<?php

namespace App\Services;

use App\Entities\Work;
use App\Repository\ProfileRepository;
use App\Repository\UserRepository;
use App\Validators\UserValidator;
use Doctrine\ODM\MongoDB\DocumentManager;
use Sokil\Mongo\Exception;
use App\Entities\User;

class UserService implements ServiceInterface
{

    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function deleteUser($entityManager, $userId)
    {

        $userRepository = $entityManager->createQueryBuilder(User::class);
        $workRepository = $entityManager->createQueryBuilder(Work::class);
        
        try {
            $userRepository
                ->remove()
                ->field("_id")
                ->equals(new \MongoId($userId))
                ->getQuery()
                ->execute();

            $workRepository
                ->remove()
                ->field("userId")
                ->equals(new \MongoId($userId))
                ->getQuery()
                ->execute();

            $this->setMessage(['message' => 'user has been removed']);
            $this->setStatus(200);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function validUser($entityManager, $userIds)
    {
        $repository = $entityManager->getRepository(User::class);

        $user = $repository->findOneBy(['_id' => $userIds]);

        if (count($user) === 0) {
            return false;

        }
        return true;
    }

    public function userFollow($entityManager, $userIds)
    {

        if (!$this->insertFollow($entityManager, $userIds)) {
                return false;
        }

            return true;
    }

    public function insertFollow($entityManager, $userIds)
    {
        $profileRepository = new ProfileRepository($entityManager);
        $userFollowing = $entityManager->getRepository(User::class)->findOneBy(['_id' => $userIds['userId']]);
        $user = $profileRepository->insertFollow($userIds, $userFollowing);
        if ($user === false) {
            $this->setMessage('User Already Followed');
            $this->setStatus(400);
            return false;
        }
        $entityManager->persist($profileRepository->getReturnObject());
        $entityManager->flush();
        $this->setMessage('following has been saved');
        $this->setStatus(200);
        return true;
    }

    public function insertFollower($entityManager, $userIds)
    {
        $profileRepository = new ProfileRepository($entityManager);
        $userFollowing = $entityManager->getRepository(User::class)->findOneBy(['_id' => $userIds['followId']]);
        $user = $profileRepository->insertFollowers($userIds, $userFollowing);
        if ($user === false) {
            $this->setMessage('User Already Followed');
            $this->setStatus(400);
            return false;
        }
        $entityManager->persist($profileRepository->getReturnObject());
        $entityManager->flush();
        $this->setMessage('follower has been saved');
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to getUsers by profile and name.
     * @param DocumentManager $entity
     * @param array $data   An array that has type of profile and name to be search and the uniqclasscode.
     * @return bool
     */
    public function getUsers(DocumentManager $entity, array $data)
    {
        $userFactory = new UserFactory($entity);
        if ($data['profile'] == 'student' && empty($data['uniqClassCode'])) {
            $queryRefined = $userFactory->getUsers($data);
        } else {
            $queryRefined = $userFactory->startSearch($data);
        }
        $userRepository = new UserRepository($queryRefined);
        $users = $userRepository->getUsers();

        if (count($users)>0) {
            $this->setMessage($users);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('user not found');
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to search users by first name and last name
     * @param $entityManager
     * @param $searchData
     * @return bool
     */
    public function searchUsers($entityManager, $searchData)
    {
        $userRepository = new UserRepository($entityManager);
        $users = $userRepository->searchUsers($searchData);

        if (count($users)) {
            $this->setMessage($users);
            $this->setStatus(200);
            return true;
        }

        $this->setMessage('users not found');
        $this->setStatus(400);
        return false;
    }

    public function validateId($id)
    {
        $userValidator = new UserValidator();
        return $userValidator->validateMongoId($id);
    }

    public function updateActivity($queryBuilder, $userId)
    {
        $user = new UserRepository($queryBuilder);
        $user->updateActivity($userId);
    }

    public function removeLikes($entityManager, $data)
    {
        $userRepository = new UserRepository($entityManager->createQueryBuilder(User::class));
        $user = $userRepository->removeLike($data);
        if ($user) {
            $entityManager->persist($user);
            $entityManager->flush();
            $this->setMessage('liked has been removed');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('like not found');
        $this->setStatus(224);
    }

    /** Metodo to remove a like on post
     * @param $entityManager
     * @param $data
     * @return bool
     */
    public function removePostLike($entityManager, $data)
    {
        $userRepository = new UserRepository($entityManager->createQueryBuilder(User::class));
        $user = $userRepository->removePostLike($data);
        if ($user) {
            $entityManager->persist($user);
            $entityManager->flush();
            $this->setMessage('liked has been removed');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('like not found');
        $this->setStatus(224);
    }

    public function canSendToReview($userQueryBuilder, $data)
    {
        $userRepository = new UserRepository($userQueryBuilder);
        $user = $userRepository->findAnUser($data['userId']);

        if ($user->getTotalComment() >= 15 && $user->getTotalWork() >= 5) {
            $this->setMessage(true);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(false);
        $this->setStatus(201);
        return false;
    }

    /** Method to remove totalComments or totalWorks
     * @param $userQueryBuilder
     * @param $userId
     * @return bool
     */
    public function removeTotalCommentsAndWorks($userQueryBuilder, $userId)
    {
        $userRepository = new UserRepository($userQueryBuilder);
        if ($userRepository->verifyFirstReview($userId)) {
            //$userRepository->removeAllTotalWork($userId);
            $userRepository->removeAllTotalComment($userId);
            return true;
        }
        $userRepository->removeAllTotalComment($userId);
    }

    /** Method to verify if an user already see a notification
     * @param $userQueryBuilder
     * @param $userId
     * @return bool
     */
    public function verifyAlreadyNotification($userQueryBuilder, $userId)
    {
        $userRepository = new UserRepository($userQueryBuilder);
        return $userRepository->verifyAlreadyNotification($userId);
    }


    /** Method to verify if an user already join on a lab or class
     * @param $userQueryBuilder
     * @param $data
     * @return bool
     */
    public function verifyAlreadyJoin($userQueryBuilder, $data)
    {
        $userRepository = new UserRepository($userQueryBuilder);
        $user =  $userRepository->verifyAlreadyJoin($data['userId'], $data['groupId']);
        if (count($user)>0) {
            $this->setMessage(['message' => 'user already joined on this lab or classroom']);
            $this->setStatus(401);
            return true;
        }
        $this->setMessage(['message' => 'user not found']);
        $this->setStatus(200);
        return false;
    }
}