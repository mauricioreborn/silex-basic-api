<?php

namespace App\Services\Profiles;

use App\Entities\User;
use App\Entities\Work;
use App\Repository\ProfileRepository;
use App\Services\Work\WorkService;
use App\Services\ServiceInterface;

class ProfileService implements ServiceInterface
{
    protected $message;
    protected $status;
    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**Get sidebar
     * @param $entityManager
     * @param $data[userId,level]
     */
    public function getSidebar($entityManager, $data)
    {
        $user = $entityManager->getRepository(User::class)->findOneBy(
            [
                '_id' => $data['userId']
            ]
        );

        $repo = new ProfileRepository($entityManager);
        $workService = new WorkService();

        isset($data['skip']) ? $data['skip'] : $data['skip'] = 0;
        $workService->getMyCoWorks($entityManager, $data);
        !empty($workService->getMessage()) ? $collaborations = $workService->getMessage() : $collaborations['total'] = 0;

        $sidebar = $repo->getSidebar($data['userId'], $user, $collaborations['total']);
        $this->setMessage($sidebar);
        $this->setStatus(200);
    }

    /** Method to get Profile works
     * @param $entityManager
     * @param $data[userId,level]
     * @return boolean
     */
    public function getProfileWorks($entityManager, $data)
    {
        $repo = new ProfileRepository($entityManager->CreateQueryBuilder(User::class));
        $user = $repo->findAnUser($data['userId']);
        $repo->setRepository($entityManager);
        if (count($user)>0) {
            if ($user->getLevel()->getAlias() == 'educator') {
                $sidebar = $repo->getEducatorWorks($user->getLikes(), $data);
                $this->setMessage($sidebar);
                $this->setStatus(200);
                return true;
            }
        }
        $sidebar = $repo->getProfileWorks($data);
        if ($sidebar === 0) {
            $this->setMessage(['message' => "Looks like you didn't publish any work"]);
            $this->setStatus(200);
        }
        $this->setMessage($sidebar);
        $this->setStatus(200);
    }

    /** Method to get followers
     * @param $entityManager
     * @param $data[userId,limit,skip]
     * @param $tokenId
     * @return bool
     */
    public function fetchFollowers($entityManager, $data, $tokenId)
    {
        $followersRepo = new ProfileRepository($entityManager->getRepository(User::class));
        $followers = $followersRepo->fetchFollowers($data, $tokenId);
        if ($followers === false) {
            $this->setMessage('UserId not Valid');
            $this->setStatus(400);
            return false;
        }
        if ($followers === 0) {
            $this->setMessage(['message' => "Looks like you don't have any followers"]);
            $this->setStatus(200);
        }
        $this->setMessage(['followers' => $followers]);
        $this->setStatus(200);
    }

    /** Method to get following by userId
     * @param $entityManager
     * @param $data[userId,limit,skip]
     * @param $tokenId
     * @return bool
     */
    public function fetchFollowing($entityManager, $data, $tokenId)
    {
        $followingRepo = new ProfileRepository($entityManager->getRepository(User::class));
        $following = $followingRepo->fetchFollowing($data, $tokenId);

        if ($following === false) {
            $this->setMessage(['message' => 'UserId not Valid']);
            $this->setStatus(400);
            return false;
        }
        if ($following === 0) {
            $this->setMessage(['message' => "Looks like you didn't follow any user"]);
            $this->setStatus(200);
        }
        $this->setMessage(['following' => $following]);
        $this->setStatus(200);
    }

    public function unFollow($entityManager, $followerId, $userId)
    {
        $followingRepo = new ProfileRepository($entityManager);
        $following = $followingRepo->unFollowUser($followerId, $userId);
        if ($following === 'nofollow') {
            $this->setMessage(['message' => 'Not following']);
            $this->setStatus(200);
            return false;
        }
        $entityManager->persist($followingRepo->getReturnObject());
        $entityManager->flush();
        $entityManager->persist($followingRepo->getSecondaryObject());
        $entityManager->flush();
        $this->setMessage([
            'error' => false,
            'message' => 'user unfollowed'
            ]);
        $this->setStatus(200);
    }

    /** Method to get works that i liked
     * @param $entityManager
     * @param $data[userId,limit,skip]
     * @return bool
     */
    public function fetchLikes($entityManager, $data)
    {
        $likeRepo = new ProfileRepository($entityManager);
        $likedWorks = $likeRepo->fetchLikes($data);
        

        if ($likedWorks === false) {
            $this->setMessage(['message' => 'UserId not Valid']);
            $this->setStatus(400);
            return false;
        }
        if ($likedWorks === 0) {
            $this->setMessage(['message' => 'User has no likes']);
            $this->setStatus(200);
        }
        $this->setMessage($likedWorks);
        $this->setStatus(200);
    }

    public function checkFollow($em, $userId, $followId)
    {
        $checkRepo = new ProfileRepository($em->getRepository(User::class));
        $check = $checkRepo->checkFollow($userId, $followId);
        if ($check === false) {
            $this->setMessage($check);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($check);
        $this->setStatus(200);
        return true;
    }

    /** Method to get works on  portfolio by Id
     * @param $entityManager
     * @param $data[userId,skip,limit]
     * @return bool
     */
    public function portfolio($entityManager, $data)
    {
        $likeRepo = new ProfileRepository($entityManager);
        $portFolio = $likeRepo->portfolio($data);


        if (empty($portFolio)) {
            $this->setMessage(['message' => "Looks like you don't have works to show on portfolio"]);
            $this->setStatus(204);
            return false;
        }
        $this->setMessage($portFolio);
        $this->setStatus(200);
    }
}
