<?php

namespace App\Services;

use App\Entities\User;
use App\Services\UserRefine\Limit;
use App\Services\UserRefine\Skip;

class UserFactory
{
    private $queryBuilder;
    private $path;
    private $request;
    private $skip;


    public function __construct($entity)
    {
        $this->path = "App\\Services\\UserRefine\\";
        $this->queryBuilder = $entity->createQueryBuilder(User::class);
    }

    public function startSearch(array $request)
    {
        $this->request = $request;

        $class = $this->path . (string) ucfirst($request['profile']);
        @$uniq = @$request['uniqClassCode'];
        if (empty($uniq) || $uniq == null || $uniq == 'null') {
            $uniq = '212311teste';
        }
        if (\MongoId::isValid($uniq)) {
            $class = $this->path . (string)('ClassCode');
            $filter = new $class();
            $this->queryBuilder = $filter->startSearch($this->queryBuilder, $request['uniqClassCode']);
        } else {
            if (class_exists($class)) {
                $filter = new $class();
                $this->queryBuilder = $filter->startSearch($this->queryBuilder, $request);
            }
        }
        return $this->queryBuilder;
    }

    /*
     * verify if exist a skip of data
     */
    protected function skip()
    {
            $skip = new Skip();
            $this->queryBuilder = $skip->startSearch($this->queryBuilder, $this->skip);
    }

    /*
     * verify if exist a limit of data
     */
    protected function limit()
    {
        if (isset($this->request['limit'])) {
            $limit = new Limit();
            $this->queryBuilder = $limit->startSearch($this->queryBuilder, $this->request['limit']);
            unset($this->request['limit']);
        }
    }

    /*
     * get works by a specific user
     */
    protected function uniqUser()
    {
        if (isset($this->request['user'])) {
            $user = new User();
            $this->queryBuilder = $user->startSearch($this->queryBuilder, $this->request['user']);
            unset($this->request['user']);
        }
    }

    protected function level()
    {
        $class = $this->path . (string) ucfirst($this->request['profile']);
        if (class_exists($class)) {
            $filter = new $class();
            $this->queryBuilder = $filter->startSearch($this->queryBuilder, $this->request['profile']);
        }
    }

    public function getUsers(array $request)
    {
        $this->request = $request;
        foreach ($request as $key => $value) {
            $class = $this->path . (string) ucfirst($key);
            if (class_exists($class)) {
                $filterClass = new $class();
               $this->queryBuilder =  $filterClass->startSearch($this->queryBuilder, $value);
            }
            return $this->queryBuilder;
        }
    }


    /**
     *  Method to verify all filters and make a dinamic query
     * @param array $request
     * @return mixed
     */
    public function searchAllFilters(array $request)
    {
        $this->skip = $request['skip'];
        unset($request['skip']);

        $this->request = $request;
        $this->limit();
        foreach ($this->request as $data => $key) {
            $data = (string) ucfirst($data);
            $class = $this->path.$data;

            if (class_exists($class)) {
                $filter = new $class();

                $this->queryBuilder = $filter->startSearch($this->queryBuilder, $key);
            }
        }

        $this->skip();

        return $this->queryBuilder;
    }
}