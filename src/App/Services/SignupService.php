<?php

namespace App\Services;

use App\Entities\Level;
use App\Entities\Organization;
use App\Entities\Work;
use App\Repository\ClassRoomRepository;
use \Silex\Application;
use App\Entities\User;
use App\Validators\Commom;
use App\Services\Commom\UploadPhoto;
use App\Repository\UserRepository;
use App\Validators\Email;

class SignupService implements ServiceInterface
{

    protected $message;

    protected $status;

    protected $notification;

    private $app;

    // Removed typing to refactor
    public function __construct($app)
    {
        $this->notification = [
            ['alias' => 'comment_work', 'text' => 'Someone like or comment my work', 'status' => true],
            ['alias' => 'work_review', 'text' => 'Receive work Review','status' => true],
            ['alias' => 'level_up', 'text' => 'Level up','status' => true],
            ['alias' => 'co_work', 'text' => "I'm added as a work Collaborator", 'status' => true ],
            ['alias' => 'follow', 'text' => "I'm followed by someone new",'status' => true],
            ['alias' => 'message', 'text' => "I'm sent a suport message",'status' => true],
            ['alias' => 'acme', 'text' => "News about ACME product and feature updates",'status' => true]
        ];

        $this->app = $app;
    }

    public function getMongoDate($date = null)
    {
        $date = $date == null ? "Y-m-d H:i:s" : $date;
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        return $this->today;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function registerStudent($data, $entityManager)
    {
        $commonValidator = new Commom($this->app);
        $validator = $commonValidator->validateRegisterUser($data, $entityManager);

        if ($validator === true) {
            $emailValidator = new Email();
            if ($emailValidator->existEmail($entityManager->getRepository(User::class), $data['email'])) {
                return [
                    'error' => true,
                    'message' => 'User already exists',
                    'status' => 409
                ];
            }

            $user = new User();
            $user->setEmail($data['email']);
            $user->setPassword(md5($data['password']));
            $user->setAlreadyNotified(false);
            $user->setAssociation($data['association']);
            $user->setFirstReview(true);
            if ($data['association'] === 'classroom') {
                $user->setClassCode($data['classroomCode']);
                $user->setClassRoom([
                    'id' => $data['classroomCode'],
                    'uploaded_at' => $this->getMongoDate()
                    ]);
            }
            $user->setBirthDate($data['birthDate']);
            $user->setFirstName(ucfirst($data['firstName']));
            $user->setLastName(ucfirst($data['lastName']));
            foreach ($data['community'] as $com) {
                $user->setCommunity($com);
            }
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setAgreement($data['agreement']);
            $user->setGender($data['gender']);
            if (!empty($data['phone'])) {
                $user->setPhone($data['phone']);
            }
            $user->setProfile($data['profile']);
            $user->setAssociation($data['association']);
            $level = new Level();
            $level->setDate($this->getMongoDate());

            $type = strpos($data['profile'], 'student');

            $level->setAlias('Student Level 1');
            $level->setNumber(1);

            if ($type === false) {
                $level->setAlias($data['profile']);
                $level->setNumber(4);
            }

            $user->setLevel($level);
            $user->setStatus(false);
            $user->getCreatedAt();
            if (!empty($data['userPhoto'])) {
                $user->setUserPhoto($data['userPhoto']);
            }

            foreach ($this->notification as $notification) {
                $user->setNotificationSettings($notification);
            }

            $entityManager->persist($user);
            $entityManager->flush();

            return [
                'id' => $user->getId(),
                'error' => false,
                'message' => 'User has been created successfully',
                'status' => 200
            ];


        }

        return [
            'error' => true,
            'message' => $validator,
            'status' => 400
        ];
    }

    public function registerProfessional($data, $entityManager)
    {
        $commonValidator = new Commom($this->app);
        $validator = $commonValidator->validateRegisterProRec($data, $entityManager);
        if ($validator === true) {
            $emailValidator = new Email();
            if ($emailValidator->existEmail($entityManager->getRepository(User::class), $data['email'])) {
                return [
                    'error' => true,
                    'message' => 'User already exists',
                    'status' => 409
                ];
            }

            $user = new User();
            $user->setEmail($data['email']);
            $user->setPassword(md5($data['password']));
            $user->setProfile($data['profile']);
            foreach ($data['community'] as $com) {
                $user->setCommunity($com);
            }


            if ($data['profile'] == 'professional') {
                $level = new Level();
                $level->setDate($this->getMongoDate());
                $level->setAlias('pro');
                $level->setNumber(5);
                $user->setLevel($level);
            } else {
                $level = new Level();
                $level->setDate($this->getMongoDate());
                $level->setAlias(strtolower($data['profile']));
                $level->setNumber(6);
                $user->setLevel($level);
                $user->setBirthDate($data['birthDate']);
            }
            $user->setFirstName($data['firstName']);
            if (!empty($data['lastName'])) {
                $user->setLastName($data['lastName']);
            }
            $user->setAgreement($data['agreement']);
            $user->setGender($data['gender']);
            if (!empty($data['phone'])) {
                $user->setPhone($data['phone']);
            }
            if (!empty($data['proTitle'])) {
                $user->setTitle($data['proTitle']);
            }
            if (!empty($data['proCompany'])) {
                $user->setCompany($data['proCompany']);
            }
            $user->setStatus(false);
            $user->getCreatedAt();
            if (!empty($data['userPhoto'])) {
                $user->setUserPhoto($data['userPhoto']);
            }
            $entityManager->persist($user);
            $entityManager->flush();


            return [
                'id' => $user->getId(),
                'error' => false,
                'message' => 'User has been created successfully',
                'status' => 200
            ];


        }

        return [
            'error' => true,
            'message' => $validator,
            'status' => 400
        ];
    }

    public function resendEmail($entityManager, $email)
    {
        $repository = $entityManager->getRepository(User::class);
        $user = $repository->findOneBy(
            [
                'email' => $email
            ]
        );
        if (count($user) === 0) {
            $this->setMessage('this email was not found in the system');
            $this->setStatus(400);
            return false;
        }
        $this->setMessage('Confirmation Email Sent');
        $this->setStatus(200);
        return $user;
    }

    public function confirmEmail($em, $id)
    {
        $repository = $em->getRepository(User::class);
        $findUser = $repository->findOneBy(
            [
                '_id' => $id
            ]
        );
        if (count($findUser) > 0) {
            $findUser->setStatus(true);
            $findUser->setState('approved');
            $em->persist($findUser);
            $em->flush();
            $this->setMessage('User Has Been Activated');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('User not found');
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to verify if is a valid classRoomCode
     * @param ObjectId $classCode
     * @return void
     */
    public function verifyClassCode($classCode)
    {
        $classRoomRepository = new ClassRoomRepository($this->app);
        if (count($classRoomRepository->getClassRoomById($classCode))>0) {
            $this->setMessage('is a valid classCode');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('not is a valid classCode');
        $this->setStatus(400);
        return false;
    }

    public function uploadAvatar($userPhoto)
    {
        if (!empty($userPhoto)) {
            $photoUpload = new UploadPhoto($this->app, 'avatars');
            $photo = $photoUpload->upload($userPhoto);
            $this->setMessage(['message' => $photo]);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'Error Photo']);
        $this->setStatus(400);
        return false;
    }

    /**
     * Method to get information of existent classroom, returning the classroom
     * and the teacher of this classroom.
     * @param array $queryData                         An array with the limit
     *                                                 and skip to get the
     *                                                 classroom data.
     * @param ClassRoomRepository $classroomRepository Object used to get
     *                                                 classroom information.
     * @param UserRepository $userRepository           Object used to get user
     *                                                 information.
     * @return array                                   An array with the
     *                                                 classroom found and their
     *                                                 teacher.
     */
    public function classroomsInformation(
        array $queryData,
        ClassRoomRepository $classroomRepository,
        UserRepository $userRepository
    ) {
        $classrooms = $classroomRepository->getClassrooms($queryData);

        if (count($classrooms) > 0) {
            foreach ($classrooms as $classroom) {
                $teacher = $userRepository->getUserByEmail(
                    $classroom->getEmailEducator()
                );

                $classroomInfo = [
                    'classroom' => [
                        'id' => $classroom->getId(),
                        'title' => $classroom->getTitle(),
                        'description' => $classroom->getDescription(),
                        'totalMembers' => $classroom->getTotalMembers(),
                        'cover' => $classroom->getCoverImage()
                    ],
                    'teacher' => [
                        'id' => $teacher->getId(),
                        'firstName' => $teacher->getFirstName(),
                        'lastName' => $teacher->getLastName(),
                        'email' => $teacher->getEmail(),
                        'profileImage' => $teacher->getUserPhoto()
                    ]
                ];

                $classroomsInfo[] = $classroomInfo;
            }
            $this->setStatus(200);
            return $classroomsInfo;
        }

        $this->setMessage('There is not Classrooms');
        $this->setStatus(400);

    }

    public function registerOrganization($data, $entityManager)
    {
        $repository = $entityManager->getRepository(Organization::class);
        $findOrg = $repository->
        findBy(
            [
                'organizationName' => $data['organizationName']
            ]
        );
        if (count($findOrg) != 0) {
            $this->setMessage('Organization already exists');
            $this->setStatus(400);
            return false;
        }

        $org = new Organization();
        $org->setEmail($data['contactEmail']);
        $org->setFirstName($data['contactFirstName']);
        $org->setLastName($data['contactLastName']);
        $org->setTitle($data['contactTitle']);
        $org->setOrganizationName($data['organizationName']);
        $org->setNumbersOfSchool($data['numberOfSchools']);
        $org->setNumbersOfClassroom($data['numberOfClassrooms']);
        $org->setPhone($data['phone']);
        $org->setEducationalStage($data['educationalStage']);
        $org->setDuration($data['duration']);
        $org->setInvolvement($data['involvement']);
        $org->setInterestInvolvement($data['interestInvolvement']);
        $org->setInterestedCommunity($data['interestedCommunity']);
        $org->setFoundOut($data['foundOut']);
        $org->setApproval('pending');
        if (!empty($data['comments'])) {
            $org->setComments($data['comments']);
        }
        $org->setStatus(false);
        $entityManager->persist($org);
        $entityManager->flush();

        $this->setMessage('Organization Created');
        return true;
    }

    /** Method to build a level
     * @param $data
     * @return Level
     */
    public function buildALevel($data)
    {
        $level = new Level();
        $level->setDate($this->getMongoDate());
        $level->setAlias($data['alias']);
        $level->setNumber($data['number']);
        return $level;
    }
}
