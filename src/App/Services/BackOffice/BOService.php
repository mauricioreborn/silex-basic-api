<?php

namespace App\Services\BackOffice;

use App\Entities\LiveSession;
use App\Entities\User;
use App\Repository\BackOffice\BORepository;
use App\Repository\UserRepository;
use App\Repository\LiveSessionRepository;
use App\Services\ServiceInterface;
use App\Services\UserFactory;
use App\Services\Work\WorkService;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoRegex;
use MongoId;

class BOService implements ServiceInterface
{

    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getEventListBackOffice($entityManager, $data)
    {
        $eventRepository = new BORepository($entityManager);
        $events = $eventRepository->getEventListBackOffice()['sessions'];
        if (count($events) == 0) {
            $this->setMessage(['sessions' => []]);
            $this->setStatus(200);
            return true;
        }

        $paginated = array_slice($events, (int)$data['skip'], (int)$data['limit']);
        $totalPages['totalPages'] = count($events)/$data['limit'] < 1 ? 1 :ceil(count($events)/$data['limit']);
        $totalResults['totalResults'] = count($events);
        $this->setMessage(
            [
                'sessions' => $paginated,
                'totalResults' => $totalResults['totalResults'],
                'totalPages' => $totalPages['totalPages']
            ]
        );
        $this->setStatus(200);
        return true;
    }

    public function updateSession($entityManager, $data)
    {
        $eventRepository = new BORepository($entityManager->getRepository(LiveSession::class));
        $events = $eventRepository->updateSession($data);
        if ($events == false) {
            $this->setMessage(['message' => 'Session Not Valid']);
            $this->setStatus(400);
            return false;
        }
        $entityManager->persist($events);
        $entityManager->flush();
        $this->setMessage(['message' => 'Session '.$data['approved']]);
        $this->setStatus(200);
        return true;
    }

    public function excludeSession($entityManager, $data)
    {
        $eventRepository = new BORepository($entityManager->getRepository(LiveSession::class));
        $events = $eventRepository->excludeSession($data);
        if ($events == false) {
            $this->setMessage(['message' => 'Session Not Valid']);
            $this->setStatus(400);
            return false;
        }
        $entityManager->persist($events);
        $entityManager->flush();
        $this->setMessage(['message' => 'Session removed']);
        $this->setStatus(200);
        return true;
    }

    public function getModeratorList($entityManager)
    {
        $BORepo = new BORepository($entityManager);
        $mods = $BORepo->getModerators();
        if ($mods == false) {
            $this->setMessage(['message' => 'No Moderators Found']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => $mods]);
        $this->setStatus(200);
        return true;
    }

    public function getListOrganization($entityManager, $data)
    {
        if ($data['status'] === 'all') {
            $data['status'] = ['deleted' => false];
        } else {
            $data['status'] = ['deleted' => false, 'approval' => $data['status']];
        }
        
        $BORepo = new BORepository($entityManager);

        $orgs = $BORepo->getListOrganization($data);

        if ($orgs == false) {
            $this->setMessage(['message' => 'No Organizations Registered']);
            $this->setStatus(400);
            return false;
        }
        $paginated = array_slice($orgs, (int)$data['skip'], (int)$data['limit']);
        $orgArray['orgs'] = $paginated;
        $orgArray['pagination']= [
            'totalPages' => count($orgs)/$data['limit'] <1 ? 1 :ceil(count($orgs)/$data['limit']),
            'totalResults' => count($orgs)
            ];
        $this->setMessage(['message' => $orgArray['orgs'],$orgArray['pagination']]);
        $this->setStatus(200);
        return true;
    }

    public function getListOrganizationSchools($entityManager, array $data)
    {
        if ($data['organizationId'] === 'all') {
            $data['param'] = ['status' => 'active'];
        } else {
            $data['param'] = [
                'status' => 'active',
                'organizationId' => new MongoId($data['organizationId']),
            ];
        }

        $BORepo = new BORepository($entityManager);
        $orgs = $BORepo->getListOrganizationSchools($data);
        
        if ($orgs == false) {
            $this->setMessage(['message' => 'No Schools Registered']);
            $this->setStatus(400);
            return false;
        }

        $paginated = array_slice($orgs, (int)$data['skip'], (int)$data['limit']);
        
        $array = [
            'totalPages' => count($orgs)/$data['limit'] <1 ? 1 :ceil(count($orgs)/$data['limit']),
            'totalResults' => count($orgs)
        ];
        $this->setMessage(['message' => $paginated, $array]);
        $this->setStatus(200);
        return true;
    }

    public function getListOrganizationSchoolClassrooms($entityManager, array $data)
    {
        if ($data['schoolId'] === 'all') {
            $data['param'] = ['status' => 'approved'];
        } else {
            $data['param'] = [
                'status' => 'approved',
                'schoolId' => new \MongoId($data['schoolId']),
                'search' => $data['search']
            ];
        }
        $BORepo = new BORepository($entityManager);
        $orgs = $BORepo->getListOrganizationSchoolClassrooms($data);
        if ($orgs == false) {
            $this->setMessage(['message' => 'No Classrooms Registered']);
            $this->setStatus(400);
            return false;
        }
        $paginated = array_slice($orgs, (int) $data['skip'], (int)$data['limit']);
        $this->setMessage(['message' => $paginated, [
            'totalPages' =>count($orgs)/$data['limit'] <1 ? 1 :ceil(count($orgs)/$data['limit']),
            'totalResults' => count($orgs)
        ]
        ]);
        $this->setStatus(200);
        return true;
    }

    /**
     * Method to get the classroom's information by id
     * @param $entintyManager
     * @param $data this param will have the id of the classroom.
     * @return array|string
     */
    public function getClassRoomById($entityManager, $data)
    {
        $BORopository = new BORepository($entityManager);
        $classInformation = $BORopository->getClassroomById($data);
        
        if($classInformation == false) {
            $this->setMessage(['message' => 'Classroom was not found']);
            $this->setStatus(400);
            return false;
        }

        return $classInformation;
    }
    
    /**
     * Method to get the members of this class ID
     * @param $userQueryBuilder
     * @param $data this param will have classroom information.
     * @return array|string
     */
    public function getMembersClassRoom($userQueryBuilder, $data)
    {
        $userRepository = new UserRepository($userQueryBuilder);
        $users = $userRepository->getUserByClassId($data['id']);

        if(count($users) > 0) {
            $usersInformation = array();
            foreach ($users as $user) {
                $usersInformation[] = $userRepository->buildUserByObject($user);
            }
            return $usersInformation;
        }
        return false;

    }

    public function organizationApproveDeny($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $orgs = $BORepo->organizationApproveDeny($data);
        if ($orgs == false) {
            $this->setMessage(['message' => 'OrgId not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage($BORepo->getMessage());
        $this->setStatus(200);
        return true;
    }

    public function organizationDetails($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $orgs = $BORepo->organizationDetails($data);
        if ($orgs == false) {
            $this->setMessage(['message' => 'OrgId not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => $orgs]);
        $this->setStatus(200);
        return true;
    }

    public function organizationSchoolDetails($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $orgs = $BORepo->organizationSchoolDetails($data);
        if ($orgs == false) {
            $this->setMessage(['message' => 'SchoolId not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => $orgs]);
        $this->setStatus(200);
        return true;
    }

    public function organizationSchoolNew($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $BORepo->organizationSchoolNew($data);
        return true;
    }

    public function organizationClassroomNew($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $classId = $BORepo->organizationClassroomNew($data);
        $this->setMessage($BORepo->getMessage());
        $this->setStatus(200);
        return $classId;
    }


    public function listLabsApproval($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->listLabsApproval();
        if ($labs === true) {
            $labss = $BORepo->getMessage();
            $labArray['labs'] = array_slice($labss, (int)$data['skip'], (int)$data['limit']);
            $events['totalPages'] = count($labss)/$data['limit'] <1 ? 1 :ceil(count($labss)/$data['limit']);
            $events['totalResults'] = count($labss);
            $labArray['pagination'] = $events;
            $this->setMessage($labArray);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('No Labs Pending Approval');
        $this->setStatus(204);
        return false;
    }

    public function approveOrDenyLab($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->approveOrDenyLab($data);
        if ($labs === true) {
            $this->setMessage('Lab '.$data['approval']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Lab Not Found');
        $this->setStatus(400);
        return false;
    }

    public function listMessages($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->listMessagesUser($data);
        if ($labs === true) {
            $this->setMessage($BORepo->getMessage());
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('No Messages Found');
        $this->setStatus(200);
        return false;
    }

    public function readMessage($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->readMessage($data);
        if ($labs === true) {
            $this->setMessage($BORepo->getMessage());
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('No Messages Found');
        $this->setStatus(204);
        return false;
    }

    public function listProsApproval($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->listUsers($data);

        if ($labs === true) {
            $users['users'] = $BORepo->getMessage()['users'];
            $arr['totalPages'] = $BORepo->getMessage()['total']/$data['limit'] <= 1 ? 1 : ceil($BORepo->getMessage()['total']/$data['limit']);
            $arr['totalResults'] = $BORepo->getMessage()['total'];
            $users['pagination'] = $arr;
            $this->setMessage($users);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('No Pros Pending Approval');
        $this->setStatus(200);
        return false;
    }


    public function approveOrDenyPro($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $pro = $BORepo->approveOrDenyPro($data);
        if ($pro === true) {
            $this->setMessage($BORepo->getMessage());
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Pro Not Found');
        $this->setStatus(400);
        return false;
    }



    public function proDetails($entityManager, $data)
    {
        $BORepo = new BORepository($entityManager);
        $pro = $BORepo->proDetails($data);
        if ($pro == false) {
            $this->setMessage(['message' => 'Pro not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => $pro]);
        $this->setStatus(200);
        return true;
    }

    public function saveChat($em, $data)
    {
        $sessionRepo = new BORepository($em);
        if (!empty($data['userId'])) {
            $data['chatId'] = $sessionRepo->fetchChatId($data['userId']);
        }
        if ($data['chatId'] === 'new') {
            $chat = $sessionRepo->buildNewChat($data);
            $em->persist($chat);
            $em->flush();
            $data['chatId'] = $chat->getId();
        }
        $msg = $sessionRepo->saveNewMessage($data);
        if ($msg === false) {
            $this->setMessage(['message' => 'Chat Id invalid']);
            $this->setStatus(200);
            return false;
        }
        $em->persist($msg);
        $em->flush();
        $this->setMessage(['message' => 'Message Saved', 'chatId' => $msg->getChatId()]);
        $this->setStatus(200);
        return true;
    }

    public function deleteOrganization($em, $data)
    {
        $BORepo = new BORepository($em);
        $org = $BORepo->deleteOrganization($data);
        if ($org == false) {
            $this->setMessage(['message' => 'Org Id not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => 'Organization and Children Deleted']);
        $this->setStatus(200);
        return true;
    }


    public function deleteSchool($em, $data)
    {
        $BORepo = new BORepository($em);
        $org = $BORepo->deleteSchool($data);
        if ($org == false) {
            $this->setMessage(['message' => 'Org Id not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => 'School and Children Deleted']);
        $this->setStatus(200);
        return true;
    }


    public function deleteClassroom($em, $data)
    {
        $BORepo = new BORepository($em);
        $org = $BORepo->deleteClassroom($data);
        if ($org == false) {
            $this->setMessage(['message' => 'classId not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => 'Classroom Deleted']);
        $this->setStatus(200);
        return true;
    }

    public function updateClassUserId($em, $labArray)
    {
        $BORepo = new BORepository($em);
        $org = $BORepo->updateClassUserId($labArray);
        if ($org == false) {
            $this->setMessage(['message' => 'classId not Valid']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => 'Classroom updated']);
        $this->setStatus(200);
        return true;
    }

    /** Method to verify if pro,edu,org exist
     * @param $entityManager
     * @param $data
     * @return mixed
     */
    public function verifyUserExist($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $boRepository->verifyUsersExist($data);
        $this->setMessage($boRepository->getMessage());
        return $boRepository->getMessage();
    }

    public function dashboardData($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $boRepository->dashboardData($data);
        $this->setMessage($boRepository->getMessage());
        return true;
    }


    public function dataMiningEvents($entityManager)
    {
        $boRepository = new BORepository($entityManager);
        $boRepository->dataMiningEvents();
        $this->setMessage($boRepository->getMessage());
        return true;
    }

    public function updateOrganization($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->updateOrganization($data);
        if ($response === true) {
            $this->setMessage(['message' => 'Organization Updated']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'Organization Not Found']);
        $this->setStatus(400);
        return false;
    }

    public function updateSchool($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->updateSchool($data);
        if ($response === true) {
            $this->setMessage(['message' => 'School Updated']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'School Not Found']);
        $this->setStatus(400);
        return false;
    }

    public function updateClassroom($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $boRepository->updateClassroom($data);
        $this->setMessage(['message' => 'ClassRoom Updated']);
        $this->setStatus(200);
        return true;
    }

    public function userDetails($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->userDetails($data);
        if ($response === true) {
            $this->setMessage(['message' => $boRepository->getMessage()]);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'User Not Found']);
        $this->setStatus(400);
        return false;
    }

    public function updateUser($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->updateUser($data);
        if ($response === true) {
            $this->setMessage(['message' => 'User Updated']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'User Not Found']);
        $this->setStatus(400);
        return false;
    }

    public function filterOrganizationByName($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->filterOrganizationByName($data);
        if ($response === true) {
            $this->setMessage(['message' => $boRepository->getMessage()]);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'No Organizations Found']);
        $this->setStatus(400);
        return false;
    }

    public function setHighLight($entityManager, $data)
    {
        $boRepository = new BORepository($entityManager);
        $response = $boRepository->setHighLight($data);
        if ($response === true) {
            $this->setMessage(['message' => $boRepository->getMessage()]);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'ID not found']);
        $this->setStatus(400);
        return false;
    }

    /**
     * Method used to return a list of banned users
     * @param DocumentManager $entityManager
     * @param array $data   This array should have the data to build the query. skip, limit, page, type, name
     *                      and orderby.
     * @return bool
     */
    public function bannedUsers(DocumentManager $entityManager, array $data)
    {
        $BORepo = new BORepository($entityManager);
        $labs = $BORepo->bannedUsers($data);

        if ($labs === true) {
            $labss = $BORepo->getMessage();
            $labArray['users'] = array_slice($labss, (int)$data['skip'], (int)$data['limit']);
            $events['totalPages'] = count($labss)/$data['limit'] <1 ? 1 :ceil(count($labss)/$data['limit']);
            $events['totalResults'] = count($labss);
            $labArray['pagination'] = $events;
            $this->setMessage($labArray);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('No Labs Pending Approval');
        $this->setStatus(400);
        return false;
    }

    public function unBanUser($em, $data)
    {
        $sessionRepo = new BORepository($em);
        $sessionRepo->unBanUser($data);
        $this->setMessage(['message' => 'User Ban Removed']);
        $this->setStatus(200);
        return true;
    }
}
