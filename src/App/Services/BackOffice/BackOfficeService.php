<?php
namespace App\Services\BackOffice;


use App\Services\BackOffice\Refine\Search;
use App\Services\BackOffice\Refine\SearchControl;
use App\Services\ServiceInterface;

class BackOfficeService implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to search something on BackOffice
     * @param $data
     * @param $entityManager
     */
    public function searchOnBackOffice($data, $entityManager)
    {
        $searchService = new SearchControl($data, $entityManager);
        $queryBuilder = $searchService->startSearch();
        $this->setMessage($queryBuilder);
        $this->setStatus(200);

    }
}