<?php

namespace App\Services;

use App\Entities\Notification;
use App\Entities\User;
use App\Repository\UserRepository;
use App\Services\Notification\NotificationFactory;

class NotificationService extends NotificationMethods implements ServiceInterface
{
    private $message;
    private $status;


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /** Method to save a notification
     * @param $entity
     * @param  $notification
     * @param $id
     * @return boolean
     */
    public function save($entity, Notification $notification)
    {
        $entity->persist($notification);
        $entity->flush();
        return true;
    }

    public function getNotification($entity, $data)
    {
        $notificationFactory = new NotificationFactory($entity->createQueryBuilder(Notification::class));
        $notifications = $notificationFactory->startSearch($data);

        $userRepository = new UserRepository($entity);
        $notification =  $userRepository->getNotification($notifications);

        $this->setMessage($notification);
        
        if (count($notification)>0) {
            $total = count($entity->createQueryBuilder(Notification::class)
                ->field("userId")
                ->equals(new \MongoId($data['userId']))
                ->field("status")
                ->equals("unread")
                ->getQuery()->execute());
            $this->setMessage($notification);
            $this->setStatus(200);
            return $notification;
        }
        $this->setMessage('there is not notification');
        $this->setStatus(200);
    }

    /** Method to set a notification how read
     * @param $queryBuilder
     * @param $data(notificationId,userId)
     * @return bool
     */
    public function setReadNotification($queryBuilder, $data)
    {
        $userRepository = new UserRepository($queryBuilder);
        if (count($userRepository->getNotificationById($data['notificationId'])) >0) {
            $userRepository->setReadNotification($data);
            $this->setMessage('done');
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('Notification not found');
        $this->setStatus(401);
        return true;
    }

    /** Method to set that an user already receive a submit-review-notification
     * @param $userQueryBuilder
     * @param $userId
     * @return bool
     */
   

}