<?php

namespace App\Services\LiveSession;

use App\Entities\LiveSession;
use App\Entities\User;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use App\Repository\LiveSessionRepository;
use App\Services\ServiceInterface;
use App\Services\Work\WorkService;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Entities\DrawoverOperation;
use MongoId;

class LiveSessionService implements ServiceInterface
{
    protected $message;
    protected $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function syncLvApiWorkList(LiveSession $lvs, $data)
    {
        $workArray = $lvs->getWorkQueue();
        if (!empty($workArray)) {
            $idArray = array_column($workArray, 'workId');
            $dataSend = array(
                'url' => getenv('LIVESESSION_URL').'submit/works',
                'createdSessionId' => $lvs->getId(),
                'workId' => $idArray,
                'userId' => $data['userId'],
                'content' => 'application/json'
            );
            $this->postLiveSession($dataSend);
        }
        return true;
    }

    public function generateSessionToken($em, $data)
    {
        $sessionRepository = new LiveSessionRepository($em);
        $sessions = $sessionRepository->generateSessionToken($data);
        if ($sessions === true) {
            $session = $sessionRepository->getMessage()['session'];

            $enrolledList = $session->getUsers();
            $enrolledList[] = $session->getSpeaker();
            $enrolledList[] = $session->getModerator();

            if (!in_array($data['userId'], $enrolledList)) {
                $evRepo = new EventRepository($em);
                $evRepo->setEnroll($data['sessionId'], $data['userId']);
            }

            if ($sessionRepository->checkBan($data) == true) {
                $this->setMessage(['message' => 'You have Been Banned From This LiveSession']);
                $this->setStatus(400);
                return false;
            }

            if ($session->getStatus() == 'ended') {
                $this->setMessage(['message' => 'This LiveSession is Over']);
                $this->setStatus(400);
                return false;
            }

            $role = $sessionRepository->getMessage()['role'];
            $dataSend = array(
                'url' => getenv('LIVESESSION_URL').'create/token',
                'createdSessionId' => $data['sessionId'],
                'speaker' => $session->getSpeaker(),
                'date' => $session->getDate()->getTimestamp(),
                'startTime' => $session->getStartTime(),
                'endTime' => $session->getEndTime(),
                'type' => $session->getType(),
                'userId' => $data['userId'],
                'role' => $role,
                'content' => 'application/json'
            );
            $output = $this->postLiveSession($dataSend);
            $this->syncLvApiWorkList($session, $data);
            $response = json_decode($output, true);
            $sessionDataArray = $session->toArray();
            foreach ($sessionDataArray as $key => $value) {
                $response[$key] = $value;
            }
            $userRepository = new UserRepository($em->createQueryBuilder(User::CLASS));
            $response['speaker'] = $userRepository->resolveUserNames($response['speaker']);
            $response['moderator'] = $userRepository->resolveUserNames($response['moderator']);
            $response['users'] = $userRepository->resolveUserNames($response['users']);
            $response['role'] = $role;
            $response['chat'] = $this->getChatHistory($em, [
                'sessionId' => $data['sessionId'],
                'limit' => 0,
                'skip' => 0
            ])['history'];
            $response['drawover'] = $this->getDrawoverHistory($em, [
                'sessionId' => $data['sessionId'],
                'limit' => 0,
                'skip' => 0
            ])['history'];
            $this->setMessage($response);
            $this->setStatus(200);
            return true;
        }
        if ($sessions === 0) {
            $this->setMessage(['message' => 'live sessions not found']);
            $this->setStatus(400);
            return false;
        }
        $this->setMessage(['message' => 'User Not Enrolled']);
        $this->setStatus(400);
        return false;
    }


    public function getSessions($queryBuilder)
    {
        $sessionRepository = new LiveSessionRepository($queryBuilder);
        $sessions = $sessionRepository->getSessions();
        if (count($sessions) > 0) {
            $this->setMessage($sessions);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage('live sessions not found');
        $this->setStatus(400);
        return false;
    }
    /* @param mixed $entityManage
     * @param mixed $data
     * @return $mixed
     */
    public function getSession($entityManager, $data)
    {
        $sessionRepository = new LiveSessionRepository($entityManager);
        $sessionInformation = $sessionRepository->getSession($data);
        if(count($sessionInformation) > 0) {
            return $sessionInformation;
        }
        $this->setMessage('live session was not found');
        $this->setStatus(400);
        return false;
    }

    public function postLiveSession($data)
    {
        $content = isset($data['content']) ? $data['content'] : 'application/json';
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-Type: '.$content,
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($opts);
        return file_get_contents($data['url'], false, $context);
    }

    public function getLiveSession($data)
    {
        $url = $data['url'].http_build_query($data['params']);
        return file_get_contents($url);
    }

    public function createSession($entityManager, $data)
    {
        if (isset($data['originalSessionId'])) {
            $message = 'session was duplicated';
        } else {
            $message = 'session has been created';
        }
        $sessionRepository = new LiveSessionRepository($entityManager);
        $liveSession = $sessionRepository->createSession($data);
        $entityManager->persist($liveSession);
        $entityManager->flush();
        $this->setMessage($message);
        $this->setStatus(200);
    }

    /**
     * Method send the session information to update session cover.
     * @param array $sessionInfo                       An array with session
     *                                                 unique identifier of
     *                                                 database and url of cover
     * @param LiveSessionRepository $sessionRepository An Object used to access
     *                                                 LiveSessionRepository
     *                                                 methods.
     * @return bool                                    A boolean the inform if
     *                                                 the cover was updated
     *                                                 (true) or not (false)
     */
    public function uploadCover(
        array $sessionInfo,
        LiveSessionRepository $sessionRepository
    ) {
        $session = $sessionRepository->uploadCover($sessionInfo);

        if ($session == true) {
            $this->setMessage('Cover updated!');
            $this->setStatus(200);
            return true;
        }

        $this->setMessage('Cover was not updated');
        $this->setStatus(400);
        return false;
    }

    public function raiseHand($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'raise/hand';
        $data['content'] = 'application/json';
        $response = json_decode($this->postLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function raiseHandList($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'raise/hand?';
        $data['params'] = [
            'sessionId' => $data['sessionId']
        ];
        $data['content'] = 'application/json';
        $response = json_decode($this->getLiveSession($data));
        $this->setMessage($response->list);
        $this->setStatus($response->status);
    }

    public function delLiveSession($data)
    {
        $opts = array('http' =>
            array(
                'method'  => 'DELETE',
                'header'  => 'Content-Type: '.$data['content'],
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );
        $url = $data['url'].http_build_query($data['params']);
        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
    }

    public function putLiveSession($data)
    {
        $opts = array('http' =>
            array(
                'method'  => 'PUT',
                'header'  => 'Content-Type: '.$data['content'],
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($opts);
        return file_get_contents($data['url'], false, $context);
    }

    public function delRaiseYourHand($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'raise/hand?';
        $data['params'] = [
            'sessionId' => $data['sessionId'],
            'userId' => $data['userId']
        ];
        $data['content'] = 'application/json';
        $response = json_decode($this->delLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function promoteSpeaker($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'promote/speaker';
        $data['content'] = 'application/json';
        $response = json_decode($this->putLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function unPromoteSpeaker($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'return/speaker';
        $data['content'] = 'application/json';
        $response = json_decode($this->putLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function startRecording($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'start/archive';
        $data['content'] = 'application/json';
        $response = json_decode($this->postLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function stopRecording($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'stop/archive';
        $data['content'] = 'application/json';
        $response = json_decode($this->postLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function getRecording($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'get/archive';
        $data['content'] = 'application/json';
        $response = json_decode($this->postLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function promoteWork($em, $data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'promote/work';
        $data['content'] = 'application/json';
        $response = json_decode($this->putLiveSession($data));
        $workService = new WorkService();
        $workService->getWorkById($em, $data);
        $promotedWork = $workService->getMessage();
        $this->setMessage(['message' => $response->message, 'work' => $promotedWork]);
        $this->setStatus($response->status);
    }

    public function unPromoteWork($data)
    {
        $data['url'] = getenv('LIVESESSION_URL').'return/work';
        $data['content'] = 'application/json';
        $response = json_decode($this->putLiveSession($data));
        $this->setMessage($response->message);
        $this->setStatus($response->status);
    }

    public function workList($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $sessionRepo->workList($data);
        $this->setStatus(200);
        $this->setMessage($sessionRepo->getMessage());
        return true;
    }

    public function saveChat($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $chat = $sessionRepo->saveChat($data);
        $em->persist($chat);
        $em->flush();
        $data['url'] = getenv('LIVESESSION_URL').'save/chat';
        $data['id'] = $chat->getId();
        $this->postLiveSession($data);
        $this->setMessage(['message' => 'Message Saved']);
        $this->setStatus(200);
    }

    public function flagChat($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $data['url'] = getenv('LIVESESSION_URL').'flag/chat';
        $this->postLiveSession($data);
        $chat = $sessionRepo->flagChat($data);
        $em->persist($chat);
        $em->flush();
        $this->setMessage(['message' => 'Message Saved']);
        $this->setStatus(200);
    }

    public function getChatHistory($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $chat = $sessionRepo->getChatHistory($data);
        $this->setStatus(200);
        return $chat;
    }

    public function searchUserSendMessage($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $list = $sessionRepo->searchUserSendMessage($data);
        if ($list === true) {
            $this->setStatus(200);
            $this->setMessage(['list' => $sessionRepo->getMessage()]);
            return true;
        }

        $this->setMessage(['message' => 'No Users Found']);
        $this->setStatus(200);
        return false;
    }

    /**
     * The folloing method executes draw actions,
     * (As the method names is really explicative) such as
     * pull and push. Those operations are described in the `operation` index
     * given by the parameter $data.
     *
     * @param DocumentManager $em object to execute database operation
     * @param array $data array with the params needed (originId as string,
     * operation as string, tag as string or array).
     * $return $this|null
     */
    public function saveDrawoverOperation(DocumentManager $em, array $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $liveSessionUrl = getenv('LIVESESSION_URL') . 'save/drawover';

        if (is_array($data['tag'])) {
            $lines = $em->getRepository(DrawoverOperation::class)->findBy([
                'originId' => new MongoId($data['originId']),
                'operation' => $data['operation']
            ]);

            foreach ($lines as $currentLine) {
                $em->remove($currentLine);
                $em->flush();
            }

            foreach ($data['tag'] as $currentTag) {
                $line['url'] = $liveSessionUrl;
                $line['originId'] = $data['originId'];
                $line['operation'] = $data['operation'];
                $line['tag'] = $currentTag;

                $drawoverOp = $sessionRepo->saveDrawoverOperation($line);
                $this->postLiveSession($line);

                $em->persist($drawoverOp);
                $em->flush();
            }

            $this->setMessage([
                'message' => 'Operation hide all',
                'updatedTags' => $data['tag']
            ]);

            $this->setStatus(200);

            return $this;
        }

        $data['url'] = $liveSessionUrl;
        $drawoverOp = $sessionRepo->saveDrawoverOperation($data);
        $this->postLiveSession($data);
        $em->persist($drawoverOp);
        $em->flush();

        $this->setMessage(['message' => 'Operation Saved', 'id' => $drawoverOp->getId()]);
        $this->setStatus(200);
    }

    public function getDrawoverHistory($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $drawover = $sessionRepo->getDrawoverHistory($data);
        $this->setStatus(200);
        return $drawover;
    }

    public function banUser($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $sessionRepo->banUser($data);
        $this->setMessage(['message' => 'User Banned']);
        $this->setStatus(200);
        return true;
    }

    public function terminateSession($em, $data)
    {
        $sessionRepo = new LiveSessionRepository($em);
        $response = $sessionRepo->terminateSession($data);
        if ($response == true) {
            $data['url'] = getenv('LIVESESSION_URL').'terminate/session';
            $this->postLiveSession($data);
            $this->setMessage(['message' => 'LiveSession Ended']);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'LiveSession Not found']);
        $this->setStatus(400);
        return false;
    }
}
