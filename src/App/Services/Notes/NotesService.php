<?php

namespace App\Services\Notes;

use App\Entities\Notes;
use App\Entities\User;
use App\Repository\NotesRepository;
use App\Repository\UserRepository;
use App\Services\ServiceInterface;

class NotesService implements ServiceInterface
{
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Method to add a new notes
     * @param $entityManager
     * @param $data
     * @return bool
     */
    public function addNotes($entityManager, $data)
    {
        $notesRepository = new NotesRepository($entityManager);
        $newNote = $notesRepository->notesBuild($data);
        $entityManager->persist($newNote);
        $entityManager->flush();
        $this->setMessage(['message' => 'note has been add']);
        $this->setStatus(200);
        return true;
    }

    /** Method to get all notes
     * @param $entityManager
     * @param $data
     * @return array
     */
    public function getNotes($entityManager, $data)
    {
        $notesRepository = new NotesRepository($entityManager->createQueryBuilder(Notes::class));
        $notes = $notesRepository->getAllNotes();
        $userRepository = new UserRepository($entityManager->createQueryBuilder(User::class));
        $user = $userRepository->findAnUser($data['userId']);

        $arrayNotes = array();
        if (count($notes)>0) {
            foreach ($notes as $note) {
                if (!$user->getNotes() || !in_array($note->getId(), $user->getNotes())) {
                    if (in_array($user->getLevel()->getAlias(), $note->getGroup())) {
                        $arrayNotes[] = $notesRepository->getNote($note);
                        $user->setNotes($note->getId());
                    }
                }
            }
            $entityManager->persist($user);
            $entityManager->flush();
        }
        return $arrayNotes;
    }

    /** Method to set a Note how Read
     * @param $entityManager
     * @param $data
     */
    public function setNoteHowRead($entityManager, $data)
    {
        $notesRepository = new UserRepository($entityManager->createQueryBuilder(User::class));
        $notesRepository->setNoteHowRed($data);
        $this->setMessage(['message' => 'notes has been saved']);
        $this->setStatus(200);
    }

    /** Method to remove a announcement by Id
     * @param $entityManager
     * @param $data[noteId]
     */
    public function noteRemove($entityManager, $data)
    {
        $notesRepository = new NotesRepository($entityManager->createQueryBuilder(Notes::class));
        $notesRepository->noteRemove($data['noteId']);
        $this->setMessage(['message' => 'announcement has been removed']);
        $this->setStatus(200);
    }

    /** Method to get all announcements
     * @param $noteQueryBuilder
     * @return bool
     */
    public function getAllNotes($noteQueryBuilder)
    {
        $notesRepository = new NotesRepository($noteQueryBuilder);
        $notes = $notesRepository->getAllNotes();
        $noteArray = array();
        if (count($notes)>0) {
            foreach ($notes as $note) {
                $noteArray[] = $notesRepository->getNote($note);
            }
            $this->setMessage($noteArray);
            $this->setStatus(200);
            return true;
        }
        $this->setMessage(['message' => 'announcements not found']);
        $this->setStatus(401);
    }

    /** Method to return all announcements that an user reade
     * @param $noteQueryBuilder
     * @param $arrayId
     * @return array
     */
    public function getNoteById($noteQueryBuilder, $arrayId)
    {
        $notesRepository = new NotesRepository($noteQueryBuilder);
        $notes = array();

        if (count($arrayId)>0) {
            foreach ($arrayId as $item) {
                $note = $notesRepository->getNoteById($item);
                $notes[] = $notesRepository->getNote($note);
            }
        }
        return $notes;
    }

}
