<?php

namespace App\Services;

use App\Entities\User;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;

class TokenService implements ServiceInterface
{

    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /** Method to build a token
     * @param User $user
     * @return mixed
     */
    public function tokenBuild(User $user, $org = null)
    {
        $levels = $user->getLevel();

        $userLevel = $levels->getAlias();
        $tokenGenerator = new TokenGenerator(new TokenBuilder());
        switch ($userLevel) {
            case 'Student Level 1':
                $level = 'studentLevel1';
                break;
            case 'Student Level 2':
                $level = 'studentLevel2';
                break;
            case 'Student Level 3':
                $level = 'studentLevel3';
                break;
            default:
                $level = $userLevel;
        }

        $classrooms = array();


        if (!is_null($user->getClassRoom())) {
            if (count($user->getClassRoom() > 0)) {
                foreach ($user->getClassRoom() as $classroom) {
                    $classrooms[] = $classroom['id'];
                }
            }
        }
        $tokenGenerator->setContext(
            [
                'name' => $user->getFirstName(),
                'profile' => $user->getProfile(),
                'community' => $user->getCommunity(),
                'user_id' => $user->getId(),
                'classCode' => $classrooms,
                'level' => $userLevel,
                'acl' => $level,
                'levelNumber' => $levels->getNumber(),
                'orgId' => !empty($org) ? $org : ''
            ]
        );

        return $tokenGenerator->getToken();
    }
}