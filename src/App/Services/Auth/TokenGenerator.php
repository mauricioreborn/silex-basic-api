<?php

/**
 * TokenGenerator File Doc Comment
 *
 * PHP version 7
 *
 * @category TokenGenerator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Services\Auth;

use Dotenv\Dotenv;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;

/**
 * Class to Generate a token
 *
 * @category Class
 * @package  MyPackage
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class TokenGenerator
{
    private $configuration;
    private $userId;
    private $context;
    private $expiration;

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param TokenBuilder $config Token build instance to configuration.
     *
     * @return void
     */
    public function __construct(TokenBuilder $config)
    {
        $this->configuration = $config;
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @return integer
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * Method the set the expiration of token.
     *
     * @param int $expiration time of the token expiration
     *
     * @return void
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }
    /**
     * Method the set the user id  token.
     *
     * @param string $userId id of the user
     *
     * @return void
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Method to set the user id
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Method the set the expiration of token.
     *
     * @param array $context params to token
     *
     * @return void
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * Method to get other token options
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Method to get the token
     *
     * @return string
     */
    public function getToken()
    {
        $signer = new Sha256();

        return $this->configuration->setIssuer(getenv('APP_URL'))
            ->setAudience(getenv('APP_URL'))
            ->setId($this->getUserId(), true)
            ->setSubject('student')
            ->setExpiration(time() + $this->getExpiration())
            ->setIssuedAt(time('d-m-Y'))
            ->set('context', $this->getContext())
            ->sign($signer, 'testing')
            ->toString();
    }

    /**
     * Method to get the array token
     *
     * @param string $token token generated
     *
     * @return array $token
     */
    public function readToken($token)
    {
        $token = (new Parser())->parse((string) $token);
        return $token;
    }
}
