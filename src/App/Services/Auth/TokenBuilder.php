<?php

/**
 * TokenBuilder File Doc Comment
 *
 * PHP version 7
 *
 * @category TokenBuilder
 * @package  MyPackage
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */


namespace App\Services\Auth;

use Lcobucci\JWT\Builder;

/**
 * Class to Build a token
 *
 * @category Class
 * @package  MyPackage
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class TokenBuilder extends Builder
{

    /**
     * Method toString token build
     *
     * @return string
     */
    public function toString()
    {
        $token = parent::getToken();

        ob_start();

        echo $token;

        $stringToken = ob_get_contents();

        ob_end_clean();

        return $stringToken;
    }
}
