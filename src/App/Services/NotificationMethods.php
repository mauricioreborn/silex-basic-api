<?php

namespace App\Services;

abstract class NotificationMethods
{
    private $notificationClass;

    /** construct to build a notificationClass
     * NotificationService constructor.
     * @param $notificationClass
     */
    public function __construct($notificationClass)
    {
        $this->notificationClass = $notificationClass;
    }

    /** get a notification message when something is created
     * @return mixed
     */
    public function create($data)
    {
        return $this->getNotificationClass()->create($data);
    }

    /** get a notification message when something is updated
     * @return mixed
     */
    public function update($data)
    {
        return $this->getNotificationClass()->update($data);
    }

    /** get a notification message when something is removed
     * @return mixed
     */
    public function delete($data)
    {
        return $this->getNotificationClass()->delete($data);
    }

    /**
     * @return mixed
     */
    public function getNotificationClass()
    {
        return $this->notificationClass;
    }

    /**
     * @param mixed $notificationClass
     */
    public function setNotificationClass($notificationClass)
    {
        $this->notificationClass = $notificationClass;
    }

    /** Method to send a notification when an user can send a work to review
     * @param $data
     * @return mixed
     */
    public function allowToReview($data)
    {
        return $this->getNotificationClass()->allowToReview($data);
    }

}