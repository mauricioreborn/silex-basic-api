<?php

namespace App\Validators\Profile;

use App\Repository\ProfileRepository;
use App\Validators\UserValidator;
use App\Validators\Validator;

class ProfileValidator extends Validator
{

    public function validateUserId($userId, $em)
    {
        if (empty($userId)) {
            return false;
        }

        $profileRepo = new UserValidator();
        if ($profileRepo->validateMongoId($userId)) {
            return $profileRepo->checkUser($em, $userId);
        }
    }

    public function validateFollowId($userId, $em)
    {
        if (empty($userId)) {
            return false;
        }

        $profileRepo = new UserValidator();
        if ($profileRepo->validateMongoId($userId)) {
            return $profileRepo->checkUser($em, $userId);
        }
    }

    public function validateProfile($request, $em)
    {
        if (!empty($request['userId'])) {
            if (!$this->validateUserId($request['userId'], $em)) {
                $this->setErrorMessage('invalid user Id');
                $this->setStatus(400);
                return false;
            }
        }
        if (!empty($request['followId'])) {
            if (!$this->validateFollowId($request['followId'], $em)) {
                $this->setErrorMessage('invalid follow Id');
                $this->setStatus(400);
                return false;
            }
        }
        return true;
    }

}