<?php

/**
 * User Validator File Doc Comment
 *
 * PHP version 7
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Validators;

use \Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class to generic validation of  request users
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class LoginValidator
{

    private $app;

    /**
     * Should receive an instance of aplication
     *
     * @param Application $app instance of all application
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Processes to validate if is a valid email.
     *
     * @param string $email email to make the validation
     *
     * @return boolean
     */
    public function validEmail($email)
    {
        $errors = $this->app['validator']->validate($email, new Assert\Email());

        if (count($errors) > 0) {
            return false;
        }

        if ($email == '') {
            return false;
        }

        return true;
    }

    /**
     * Processe to validate if is a valid password.
     *
     * @param string $password password to make the validation
     *
     * @return boolean
     */
    public function validPassword($password)
    {
        if ($password == '') {
            return false;
        }
        $errors = $this->app['validator']
            ->validate($password, new Assert\Length(array('min' => 5)));
        if (count($errors) > 0) {
            return false;
        }
        return true;
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param string $data password and email  to make the validation
     *
     * @return boolean
     */
    public function validateLogin($data)
    {
        if (!$this->validEmail($data['email'])) {
            return 'This is not a valid email';
        }

        if (!$this->validPassword($data['password'])) {
            return 'The password is not valid';
        }

        return true;
    }
}
