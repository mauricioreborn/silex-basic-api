<?php
/**
 * Created by PhpStorm.
 * User: viking
 * Date: 11/03/17
 * Time: 14:22
 */

namespace App\Validators;


use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class Work
{
    private $message;
    private $status;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    protected $errorMessage = array();

    private $maxsize =  [
        'avi' => '200M',
        'mpeg' => '200M',
        'png' => '2M',
        'jpg' => '2M',
        'gif' => '10M',
        'pdf' => '200M'
    ];

    public function validateAddWorkExtension(Application $app, $extension, $type)
    {
        $photoTypes = [
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ];
        // need to be defined which files are allowed#
        if ($extension == null) {
            return false;
        }

        if (in_array($type, $photoTypes)) {
            $validatePhoto = new Commom($app);
            $msg = $validatePhoto->validPhoto($extension);
            $this->setMessage(['message' => $validatePhoto->getMessage()]);
            $this->setStatus(413);
            return $msg;
        }

        $errors = $app['validator']->validate($extension, new Assert\File([
            'maxSize' => '2000M',
            'mimeTypes' => [
                'video/*',
                'image/*',
                'application/*',
            ],
            'mimeTypesMessage' => 'Please upload a valid Video AVI, MPEG, MP4 or Image PNG, JPG, JPEG, GIF',
            'maxSizeMessage' => 'Please Send a file with size less than 2MB'
        ]));
        if (count($errors) > 0) {
            $this->setMessage(['message' => $errors[0]->getMessage()]);
            $this->setStatus(413);
            return false;
        }
        return true;
    }

    public function validateVideoExtension(Application $app, $extension)
    {
        // need to be defined which files are allowed#
        if ($extension == null) {
            return false;
        }
        $errors = $app['validator']->validate($extension, new Assert\File([
            'maxSize' => '2000M',
            'mimeTypes' => [
                'video/*',
                'image/*',
                'application/*',
            ],
            'mimeTypesMessage' => 'Please upload a valid Video AVI, MPEG, MP4',
            'maxSizeMessage' => 'Please Send a picture with size less than 2MB'
        ]));
        if (count($errors) > 0) {
            return false;
        }
        return true;
    }

    /**
     * This method will validate the inputs to create an work.
     * @param Application $app An object of Application to use the validate
     *                         method.
     * @param array $workInfo  An array with the information of the work to be
     *                         validated.
     * @return bool            Return a boolean to inform if the work's inputs
     *                         are valid (true) or invalid (false).
     */
    public function validateWorkInputs(Application $app, array $workInfo)
    {
        $validator = new Assert\Collection([
            'workId' => new Assert\Optional(new Assert\Length(['min' => 24])),
            'title' => new Assert\Length(['min' => 3]),
            'category' => new Assert\Length(['min' => 2]),
            'description' => new Assert\Collection([
                new Assert\Length(['min' => 4]),
                new Assert\Optional(new Assert\Length(['min' => 4])),
            ]),
            'level' => new Assert\Length(['min' => 3]),
            'group' => new Assert\Length(['min' => 2]),
            'mainFile' => new Assert\Collection([
                'path' => new Assert\Url(['protocols' => [
                    'http',
                    'https'
                ]]),
                'type' => new Assert\Length(['min' => 2]),
                'name' => new Assert\Length(['min' => 1])
            ]),
            'thumbnail' => new Assert\Collection([
                'path' => new Assert\Url(['protocols' => [
                    'http',
                    'https'
                ]]),
                'type' => new Assert\Length(['min' => 2]),
                'name' => new Assert\Length(['min' => 1])
            ]),
            'coWorkers' => new Assert\Optional(),
            'attachments' => new Assert\Optional(),
            'availableReview' => new Assert\Optional(),
            'sendPortfolio' => new Assert\Optional(),
            'utcOffset' => new Assert\Length(['min' => '2']),
            'labId' => new Assert\Optional(new Assert\Length(['min' => 24])),
            'isPrivate' => new Assert\Optional(
                new Assert\Type(['type' => 'bool'])
            ),
            'userId' => new Assert\Length(['min' => 24]),
            'studentLevel' => new Assert\Optional(
                new  Assert\Length(['min' => 2])
            ),
            'levelNumber' => new Assert\Length(['min' => 1]),
            'type' => new Assert\Length(['min' => 2])
        ]);

        $errors = $app['validator']->validate($workInfo, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage(
                    $error->getPropertyPath() .
                    $error->getMessage()
                );
            }
            $this->setStatus(400);
            return false;
        }
        return true;
    }

    public function getMaxSizeByType($type)
    {
        return $this->maxsize[$type];
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage[] = $errorMessage;
    }


}