<?php

namespace App\Validators;

use \Silex\Application;
use App\Entities\Community;
use App\Services\Commom\CheckCommunity;
use App\Services\Commom\CheckProfile;
use Symfony\Component\Validator\Constraints as Assert;

class OrganizationValidator
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function validOrganizationContactFirstName($name)
    {
        if ($name == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($name, new Assert\Length(array('max' => 80)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationContactLastName($name)
    {
        if ($name == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($name, new Assert\Length(array('max' => 80)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationContactTitle($title)
    {
        if ($title == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($title, new Assert\Length(array('max' => 20)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationName($name)
    {
        if ($name == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($name, new Assert\Length(array('max' => 80)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationSchoolNumber($number)
    {
        $number = (int)$number;

        if ($number == '') {
            return false;
        }
        $length = $this->app['validator']->validate($number, new Assert\Length(array('max' => 10)));

        if (count($length)>0) {
            return false;
        }

        $errors = $this->app['validator']->validate($number, new Assert\Type('integer'));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationSchoolClassroomNumber($number)
    {
        $number = (int)$number;

        if ($number == '') {
            return false;
        }
        $length = $this->app['validator']->validate($number, new Assert\Length(array('max' => 10)));

        if (count($length)>0) {
            return false;
        }

        $errors = $this->app['validator']->validate($number, new Assert\Type('integer'));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationEducationalStage($stage)
    {
        if ($stage == '') {
            return false;
        }

        $errors = $this->app['validator']->validate(
            strtolower($stage),
            new Assert\Choice(
                array('k12district', 'middle_school','high_school','college')
            )
        );

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationDuration($time)
    {
        if ($time == '') {
            return false;
        }

        $errors = $this->app['validator']->validate(strtolower($time), new Assert\Choice(array('2', '4')));

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationInvolved($choice)
    {
        $choice = (boolean)$choice;

        if ($choice == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($choice, new Assert\Type('boolean'));
        //$errors = $this->app['validator']->validate(strtolower($choice), new Assert\Choice(array('true', 'false')));

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationInterestInvolvement($choice)
    {
        $choice = (boolean)$choice;
        if ($choice == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($choice, new Assert\Type('boolean'));
        //$errors = $this->app['validator']->validate(strtolower($choice), new Assert\Choice(array('true', 'false')));

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationIntestedCommunity($community, $repository)
    {
        $communities = new CheckCommunity($this->app);
        return $communities->checkCommunity($community, $repository);
    }


    public function validOrganizationFoundAbout($comments)
    {
        if ($comments == '') {
            return false;
        }

        $errors = $this->app['validator']->validate($comments, new Assert\Length(array('max' => 100)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validOrganizationComments($comments)
    {
        if ($comments === null) {
            return true;
        }

        $errors = $this->app['validator']->validate($comments, new Assert\Length(array('max' => 400)));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validateRegisterOrganization($data, $entityManager)
    {
        $loginValidator = new LoginValidator($this->app);
        $commom = new Commom($this->app);

        if (!$loginValidator->validEmail($data['contactEmail'])) {
            return 'invalid email';
        }

        if (!$this->validOrganizationContactFirstName($data['contactFirstName'])) {
            return 'invalid first name';
        }

        if (!$this->validOrganizationContactLastName($data['contactLastName'])) {
            return 'invalid last name';
        }

        if (!$this->validOrganizationContactTitle($data['contactTitle'])) {
            return 'invalid title';
        }

        if (!$commom->validPhoneChars($data['phone'])) {
            return 'invalid phone';
        }

        if (!$this->validOrganizationName($data['organizationName'])) {
            return 'invalid organization name';
        }

        if (!$this->validOrganizationSchoolNumber($data['numberOfSchools'])) {
            return 'invalid number of schools';
        }

        if (!$this->validOrganizationSchoolClassroomNumber($data['numberOfClassrooms'])) {
            return 'invalid number of classrooms';
        }

        if (!$this->validOrganizationEducationalStage($data['educationalStage'])) {
            return 'invalid educational stage';
        }

        if (!$this->validOrganizationDuration($data['duration'])) {
            return 'invalid duration';
        }

        if (!$this->validOrganizationInvolved($data['involvement'])) {
            return 'invalid involvement';
        }

        if (!$this->validOrganizationInterestInvolvement($data['interestInvolvement'])) {
            return 'invalid interest';
        }

        foreach ($data['interestedCommunity'] as $community) {
            if (!$this->validOrganizationIntestedCommunity(
                $community,
                $entityManager->getRepository(Community::class)
            )
                )
            {
                return 'invalid Community';
            }
        }


        if (!$this->validOrganizationFoundAbout($data['foundOut'])) {
            return 'invalid found out';
        }
//        if($data['comments']) {
//            if (!$this->validOrganizationComments($data['comments'])) {
//                return 'invalid comments';
//            }
//        }

        return true;
    }
}
