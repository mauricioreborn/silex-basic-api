<?php

namespace App\Validators\BackOffice;

use App\Entities\User;
use App\Validators\Validator;
use Symfony\Component\Validator\Constraints as Assert;

class Moderator extends Validator
{

    public function checkModerator($entityManager, $userId)
    {
        $mod = $entityManager->getRepository(User::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($userId['moderatorId'])
                ]
            );

        if (count($mod) > 0) {
            $level = $mod->getLevel()->getAlias();
            if ((strtolower($level) === 'moderator') || (strtolower($level) === 'admin')) {
                return true;
            }
            $this->setErrorMessage('User is not a Moderator');
            $this->setStatus(400);
            return false;
        }
        $this->setErrorMessage('User not found');
        $this->setStatus(400);
        return false;
    }

}
