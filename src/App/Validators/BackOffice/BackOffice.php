<?php

namespace App\Validators\BackOffice;

use App\Entities\Schools;
use App\Entities\User;
use App\Validators\Validator;
use Symfony\Component\Validator\Constraints as Assert;

class BackOffice extends Validator
{

    public function checkSchools($entityManager, $schoolName)
    {
        $school = $entityManager->getRepository(Schools::class)
            ->findOneBy(
                [
                    'name' => $schoolName,
                    'status' => 'active'
                ]
            );
        if (count($school) > 0) {
            return false;
        }
        return true;
    }

    public function checkSchoolsById($entityManager, $schoolId)
    {
        $school = $entityManager->getRepository(Schools::class)
            ->findOneBy(
                [
                    '_id' => new \MongoId($schoolId),
                ]
            );
        if (count($school) > 0) {
            $this->setMessage($school->getOrganizationId());
            return true;
        }
        return false;
    }

}