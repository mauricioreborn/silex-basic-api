<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraints as Assert;
use App\Services\UserService;

class UserValidator extends Validator
{

    private $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function checkUser($entityManager, $userId)
    {
        $valid = $this->userService->validUser($entityManager, $userId);

        if (!$valid) {
            return false;
        }

        return true;
    }

    public function follow($entityManager, $userId)
    {
        return $this->userService->userFollow($entityManager, $userId);
    }

    public function followUser($entityManager, $userId)
    {
        foreach ($userId as $id) {
            if (!$this->checkUser($entityManager, $id)) {
                $this->setErrorMessage('User Not found');
                $this->setStatus(false);
            }
            $this->setStatus(true);
        }
        return $this->getStatus();
    }
}

