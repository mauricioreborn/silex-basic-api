<?php
/**
 * Created by PhpStorm.
 * User: viking
 * Date: 08/03/17
 * Time: 19:59
 */

namespace App\Validators;


class Email
{


    public function existEmail($repository, $email)
    {
        if (count($repository->findBy(['email' => $email])) == 0) {
            return false;
        }
        return true;
    }
}