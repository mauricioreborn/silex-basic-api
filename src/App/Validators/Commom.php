<?php

namespace App\Validators;

use App\Entities\Community;
use \Silex\Application;
use App\Validators\LoginValidator;
use App\Services\Commom\CheckCommunity;
use App\Services\Commom\CheckProfile;
use App\Entities\Profile;
use App\Entities\User;
use Symfony\Component\Validator\Constraints as Assert;

class Commom
{

    private $message;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    public function validProfile($profile, $entity)
    {
        $profiles = new CheckProfile($this->app);
        return $profiles->checkProfile($profile, $entity);
    }

    public function validCommunity($community, $repository)
    {
        $communities = new CheckCommunity($this->app);
        return $communities->checkCommunity($community, $repository);
    }

    public function validPhoto($photo)
    {
        $errors = $this->app['validator']->validate($photo, new Assert\File(array(
            'maxSize' => '2M',
            'mimeTypes' => array(
                'image/png',
                'image/jpg',
                'image/jpeg',
            ),
            'mimeTypesMessage' => 'Please upload a valid Photo png, jpg, jpeg',
            'maxSizeMessage' => 'Please Send a file with size less than 2MB'
        )));
        if (count($errors)>0) {
            $this->setMessage($errors[0]->getMessage());
            return false;
        }
        return true;
    }

    public function validNameLengthOptional($name)
    {

        $errors = $this->app['validator']->validate($name, new Assert\Length(array('max' => 50)));
        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validNameLength($name)
    {
        if ($name == '') {
            return false;
        }
        $errors = $this->app['validator']->validate($name, new Assert\Length(array('max' => 50)));
        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validGender($gender)
    {

        $errors = $this->app['validator']->validate(strtolower($gender), new Assert\Choice(array('m', 'f')));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validBirthdayFormat($birthDay)
    {
        if ($birthDay == '') {
            return false;
        }

        $myDateTime = \DateTime::createFromFormat('m/d/Y', $birthDay);
        if ($myDateTime->format('m/d/Y') !== $birthDay) {
            return false;
        }

        $errors = $this->app['validator']->validate(
            $myDateTime,
            new Assert\DateTime(
                array('format' => 'm-d-Y')
            )
        );
        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validPhoneNumber($phone)
    {

        $errors = $this->app['validator']->validate($phone, new Assert\Type('integer'));


        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validPhoneChars($phone)
    {

        $errors = $this->app['validator']->validate(
            $phone,
            new Assert\Length(
                array('min' => 10, 'max' => 10)
            )
        );

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validateAgreement($check)
    {
        $check = (bool)$check;

        $errors = $this->app['validator']->validate($check, new Assert\Type('boolean'));

        if (count($errors)>0) {
            return false;
        }
        return true;
    }

    public function validateRegisterProRec($data, $entityManager)
    {
        $loginValidator = new LoginValidator($this->app);

        if (!$loginValidator->validEmail($data['email'])) {
            return 'invalid email';
        }

        if (!$loginValidator->validPassword($data['password'])) {
            return 'invalid password';
        }

        if (!$this->validProfile($data['profile'], $entityManager->getRepository(Profile::class))) {
            return 'invalid profile';
        }

        foreach ($data['community'] as $community) {
            if (!$this->validCommunity($community, $entityManager->getRepository(Community::class))) {
                return 'invalid Community';
            }
        }

        if (!$this->validateAgreement($data['agreement'])) {
            return 'invalid agreement';
        }

        if (!$this->validGender($data['gender'])) {
            return 'invalid gender';
        }

        if (!$this->validNameLength($data['firstName'])) {
            return 'invalid firstName';
        }

        if (!empty($data['lastName'])) {
            if (!$this->validNameLengthOptional($data['lastName'])) {
                return 'invalid lastName';
            }
        }

        if (!empty($data['phone'])) {
            if (!$this->validPhoneChars($data['phone'])) {
                return 'invalid phone';
            }
        }
        if (!empty($data['proTitle'])) {
            if (!$this->validNameLengthOptional($data['proTitle'])) {
                return 'invalid Professional Title';
            }
        }
        if (!empty($data['proCompany'])) {
            if (!$this->validNameLengthOptional($data['proCompany'])) {
                return 'invalid Company';
            }
        }
        return true;
    }


    public function validateRegisterUser($data, $entityManager)
    {
        $loginValidator = new LoginValidator($this->app);

        if (!$loginValidator->validEmail($data['email'])) {
            return 'invalid email';
        }

        if (!$loginValidator->validPassword($data['password'])) {
            return 'invalid password';
        }

        if (!$this->validProfile($data['profile'], $entityManager->getRepository(Profile::class))) {
            return 'invalid profile';
        }

        if (!$this->validateAgreement($data['agreement'])) {
            return 'invalid agreement';
        }

        if (!$this->validBirthdayFormat($data['birthDate'])) {
            return 'invalid birthDate';
        }

        if (!$this->validGender($data['gender'])) {
            return 'invalid gender';
        }

        if (!$this->validNameLength($data['firstName'])) {
            return 'invalid firstName';
        }
        if (!empty($data['phone'])) {
            if (!$this->validPhoneChars($data['phone'])) {
                return 'invalid phone';
            }
        }
        foreach ($data['community'] as $community) {
            if (!$this->validCommunity($community, $entityManager->getRepository(Community::class))) {
                return 'invalid Community';
            }
        }

        return true;
    }
}
