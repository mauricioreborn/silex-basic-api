<?php

/**
 * Token Validator Doc Comment
 *
 * PHP version 7
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace App\Validators;

use Lcobucci\JWT\ValidationData;

/**
 * Token Validator Doc Comment
 *
 * PHP version 7
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
class Token
{
    private $token ;

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param string $token to make validated.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Method to validate the token
     *
     * @return boolean
     */
    public function validateToken()
    {
        $data = new ValidationData();
        return $this->token->validate($data);
    }

    /**
     * Method to validate if the token has been expired
     *
     * @return boolean
     */
    public function tokenHasExpired()
    {
        return $this->token->getClaim('exp') >= time() ? true : false;
    }
}
