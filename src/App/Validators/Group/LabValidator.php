<?php

namespace App\Validators\Group;

use App\Validators\Validator;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class LabValidator extends Validator
{
    public function validateLabInput($data, $app)
    {
        $validator = new Assert\Collection([
            'title' => new Assert\Length(['min' => 5]),
            'description' => new Assert\Length(['min' => 2]),
            'userId' => new Assert\Length(['min' => 24]),
            'educatorEmail' => new Assert\Length(['min' => 1]),
            'schoolId' => new Assert\Length(['min' => 1]),
            'proEmail' => new Assert\Length(['min' => 1]),
            'type' => new Assert\Length(['min' => 1]),
        ]);

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getMessage());
            }
            return false;
        }
        return true;
    }


    public function validatePostInput($data, $app)
    {
        $validator = new Assert\Collection([
            'groupId' => new Assert\Length(['min' => 5]),
            'publication' => new Assert\Length(['min' => 4]),
            'type' => new Assert\Length(['min' => 1]),
        ]);

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getPropertyPath().$error->getMessage());
            }
            return false;
        }
        return true;
    }


    public function validateTask($data, $app)
    {
        $validator = new Assert\Collection([
            'labId' => new Assert\Length(['min' => 24]),
            'title' => new Assert\Length(['min' => 4]),
            'description' => new Assert\Length(['min' => 1]),
            'dueDate' => new Assert\Length(['min' => 1]),
        ]);

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getPropertyPath().$error->getMessage());
            }
            return false;
        }
        return true;
    }

    /**
     * This method will validate data to before try to add a work to a task of a
     * lab.
     * @param array $turnInInfo An array with the information necessary to add
     *                          a work to lab's task.
     * @param Application $app  Object of Application that will be used to
     *                          validate the $turnInInfo's data
     * @return bool             Return a boolean to indicate the if the data is
     *                          valid(true) or not(false).
     */
    public function validateTurnin(array $turnInInfo, Application $app)
    {
        $validator = new Assert\Collection([
            'labId' => new Assert\Length(['min' => 24]),
            'taskId' => new Assert\Length(['min' => 24]),
            'userId' => new Assert\Length(['min' => 24]),
            'workId' => new Assert\Length(['min' => 24]),
        ]);

        $errors = $app['validator']->validate($turnInInfo, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage(
                    $error->getPropertyPath().
                    $error->getMessage()
                );
            }
            $this->setStatus(400);
            return false;
        }
        return true;
    }
}