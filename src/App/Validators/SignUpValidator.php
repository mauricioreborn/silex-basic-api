<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraints as Assert;

class SignUpValidator extends Validator
{
    public function validateGetClassQuery($data, $app)
    {
        $validator = new Assert\Collection([
            'limit' => new Assert\NotNull(),
            'skip' => new Assert\NotNull()
        ]);

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage(
                    $error->getPropertyPath() .
                    $error->getMessage()
                );
            }
            return false;
        }
        return true;
    }
}