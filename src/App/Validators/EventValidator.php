<?php

namespace App\Validators;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class EventValidator extends Validator
{
    /**
     * Method to validate if all parameters are valid
     * @param Application $app
     * @param array $data
     * @return bool|mixed
     */
    public function validateGetEnrolledUsersWork(Application $app, array $data)
    {
        $request = [
            'sessionId' => $this->validateMongoId($data['sessionId']),
            'limit' => $this->checkValue($data['limit'])
        ];

        $validator = new Assert\Collection(array(
            'sessionId' => new Assert\Length(array('min' => 5)),
            'limit' => new Assert\Length(array('min' => 1)),
        ));

        $errors = $app['validator']->validate($request, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getMessage());
            }
            return $this->getErrorMessage();
        }
        return true;
    }
}