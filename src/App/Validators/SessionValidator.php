<?php

namespace App\Validators;

use App\Validators\Validator;
use Symfony\Component\Validator\Constraints as Assert;

class SessionValidator extends Validator
{
    public function requiredFields($app, $data)
    {
        $required = [
            'title' => $this->checkValue($data['title']),
            'description' => $this->checkValue($data['description']),
            'type' => $this->checkValue($data['type']),
            'startTime' => $this->checkValue($data['startTime']),
            'endTime' => $this->checkValue($data['endTime']),
            'thumbnail' => $this->checkValue($data['thumbnail']),
            'speaker' => $this->checkValue($data['speaker'])

        ];

        $validator = new Assert\Collection(array(
            'title' => new Assert\Length(array('min' => 5)),
            'description' => new Assert\Length(array('min' => 8)),
            'type' => new Assert\Length(array('min' => 5)),
            'startTime' => new Assert\Length(array('min' => 1)),
            'endTime' => new Assert\Length(array('min' => 2)),
            'thumbnail' => new Assert\Url(),
            'speaker' => new Assert\Length(array('min' => 5)),
        ));

        $errors = $app['validator']->validate($required, $validator);
        if (count($errors) > 0) {
            $this->buildError($errors);
            return false;
        }
        return true;
    }


    /**
     * Method to validate if all fields are correct
     * @param $app
     * @param $data
     * @return bool
     */
    public function validateSubmitWork($app, $data)
    {
        $required = [
            'workId' => $this->validateMongoId($data['workId']),
            'userId' => $this->validateMongoId($data['userId']),
            'workOwnerId' => $this->validateMongoId($data['workOwnerId']),
            'SessionId' => $this->validateMongoId($data['sessionId']),
            'type' => $this->checkValue($data['type']),
            'action' => $this->checkValue($data['action']),
        ];

        $validator = new Assert\Collection(array(
            'workId' => new Assert\Length(array('min' => 5)),
            'userId' => new Assert\Length(array('min' => 8)),
            'workOwnerId' => new Assert\Length(array('min' => 5)),
            'SessionId' => new Assert\Length(array('min' => 5)),
            'action' => new Assert\Choice(array('remove','add')),
            'type' => new Assert\Choice(array('Student Level 3','admin','Student Level 2','Student Level 1'))
        ));

        $errors = $app['validator']->validate($required, $validator);
        if (count($errors) > 0) {
            $this->buildError($errors);
            return false;
        }
        return true;
    }
}