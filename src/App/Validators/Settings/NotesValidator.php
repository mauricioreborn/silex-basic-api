<?php

namespace App\Validators\Settings;

use App\Validators\Validator;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class NotesValidator extends Validator
{
    public function validateNotesInputs(Application $app, $data)
    {
        $validator = new Assert\Collection(array(
            'title' => new Assert\Length(array('min' => 5)),
            'message' => new Assert\Length(array('min' => 10)),
            'userId' => new Assert\Length(array('min' => 10)),
            'group' => new Assert\Collection(array(
            )),
        ));

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getPropertyPath().$error->getMessage());
            }
            return $this->getErrorMessage();
        }
        return true;
    }

    /** Method to validate data to check a notification how readied
     * @param Application $app
     * @param $data
     * @return bool|mixed
     */
    public function readNotification(Application $app, $data)
    {
        $validator = new Assert\Collection(array(
            'notificationId' => new Assert\Length(array('min' => 5)),
            'type' => new Assert\Length(array('min' => 3)),
        ));

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getPropertyPath().$error->getMessage());
            }
            return false;
        }
        return true;
    }
}