<?php

namespace App\Validators;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use MongoId;

abstract class Validator
{
    protected $errorMessage = array();
    protected $status;
    protected $message;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage[] = $errorMessage;
    }

    public function checkValue($value)
    {
        if (isset($value)) {
            return $value;
        }
        return 0;
    }

    public function buildError($errors)
    {
        foreach ($errors as $error) {
            $this->setErrorMessage($error->getPropertyPath().' '.$error->getMessage());
            $this->setStatus(400);
        }
        return false;
    }

    /**
     * Validator to verify if is a valid mongoId
     * @param string $id
     * @return bool
     */
    public function validateMongoId(string $id)
    {
        if (MongoId::isValid($id)) {
            return $id;
        }
        return 0;
    }
}