<?php

namespace App\Validators\Work;

use App\Repository\CommentRepository;
use App\Validators\Validator;

class CommentValidator extends Validator
{
    /**
     * Method to verify if is a valid work id
     * @param $workId
     * @param $workRepository
     * @return boolean
     */
    public function validateWorkId($workId, $workRepository)
    {
        if (empty($workId)) {
            return false;
        }

        $commentRepository = new CommentRepository($workRepository);
        $result = $commentRepository->getCommentByWorkId($workId);

        if (count($result)>0) {
            return true;
        }
        return false;
    }
}