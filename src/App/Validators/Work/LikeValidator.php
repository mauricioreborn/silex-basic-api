<?php

namespace App\Validators\Work;

use App\Repository\LikeRepository;
use App\Validators\Validator;

class LikeValidator extends Validator
{

    public function userLikedWork($repository, $data)
    {
        $likeRepository = new LikeRepository($repository);
        $user = $likeRepository->searchLikeByUserId($data);
        if (count($user) > 0) {
            return true;
        }
        return false;
    }

    public function userLikedWorkOnPost($repository, $data)
    {
        $likeRepository = new LikeRepository($repository);
        $user = $likeRepository->searchLikeByUserIdOnPost($data);
        if (count($user) > 0) {
            return true;
        }
        return false;
    }
}