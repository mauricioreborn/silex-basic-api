<?php

namespace App\Validators\LiveSession;

use App\Validators\Validator;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class SessionValidator extends Validator
{
    public function requiredFields(Application $app, $data)
    {
        $validator = new Assert\Collection(array(
            'title' => new Assert\Length(array('min' => 5)),
            'type' => new Assert\Length(array('min' => 5)),
            'date' => new Assert\Length(array('min' => 10)),
            'startTime' => new Assert\Length(array('min' => 1)),
            'endTime' => new Assert\Length(array('min' => 5)),
        ));

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage($error->getPropertyPath().$error->getMessage());
            }
            return false;
        }
        return true;
    }

    /**
     * This method should validate the parameters to update a cover to a session
     * @param Application $app  An object of Silex application.
     * @param array $data       An array with the parameters received to be
     *                          validated
     * @return bool             A boolean that informs if the parameters are
     *                          valid (true) or not (false).
     */
    public function uploadCoverValidator(Application $app, array $data)
    {
        $validator = new Assert\Collection(
            [
                'sessionId' => new Assert\Length(
                    [
                        'min' => 24
                    ]
                ),
                'coverImage' => new Assert\File(
                    [
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/*'
                        ]
                    ]
                )
            ]
        );

        $errors = $app['validator']->validate($data, $validator);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->setErrorMessage(
                    $error->getPropertyPath() .
                    $error->getMessage()
                );
            }
            return false;
        }

        return true;
    }
}