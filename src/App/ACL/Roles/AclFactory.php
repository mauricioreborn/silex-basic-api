<?php

namespace App\ACL\Roles;

use App\ACL\Modules\Classroom;
use App\ACL\Modules\Comment;
use App\ACL\Modules\RecentActivity;
use App\ACL\Modules\Events;
use App\ACL\Modules\FeedBack;
use App\ACL\Modules\Group;
use App\ACL\Modules\Lab;
use App\ACL\Modules\LiveSession;
use App\ACL\Modules\NoToken;
use App\ACL\Modules\Profiles;
use App\ACL\Modules\Settings;
use App\ACL\Modules\User;
use App\ACL\Modules\Work;
use App\ACL\Modules\Script;

class AclFactory implements IRoles
{

    private $path =  "App\\ACL\\Modules\\";

    public function checkPermissions($method, $level)
    {
        $array = array_reverse(explode('\\', $method[0]));
        $class = $array[0];

        $this->path = $this->path.ucfirst($level)."\\";
        $methodCheck = $method[1];
        return $this->accessModules($methodCheck, $level, $class);
    }

    public function accessModules($methodCheck, $level, $class)
    {
        /*
         * Instanciate only the allowed modules for this role
         */
        $permissions = array();
        $class = $this->path.$class;
        if (class_exists($class)) {
            $check = new $class();
        } else {
            $check = new NoToken();
        }
        $permissions = array_merge($check->getPermissions($level), $permissions);
        return (in_array($methodCheck, $permissions));
    }

}