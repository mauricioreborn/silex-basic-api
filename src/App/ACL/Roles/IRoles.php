<?php

namespace App\ACL\Roles;

interface IRoles
{
    public function checkPermissions($method, $level);
    public function accessModules($methodCheck, $level, $class);
}