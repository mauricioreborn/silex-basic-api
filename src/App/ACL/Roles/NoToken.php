<?php

namespace App\ACL\Roles;

use App\ACL\Modules\NoToken as Exception;

class NoToken implements IRoles
{

    public function checkPermissions($method, $level)
    {
        $methodCheck = $method[1];
        return $this->accessModules($methodCheck, $level);
    }

    public function accessModules($methodCheck, $level)
    {
        /*
         * Instanciate only the allowed modules for this role
         */
        $noToken = new Exception();
        $permissions = $noToken->getPermissions($level);
        return (in_array($methodCheck, $permissions));
    }

}