<?php

namespace App\ACL;

use App\ACL\Roles\AclFactory;
use App\Services\Auth\TokenBuilder;
use App\Services\Auth\TokenGenerator;

class AccessControlList
{

    private $method;
    private $tokenData;
    private $path;
    private $validate;

    public function __construct($token, $method)
    {
        $data = array();
        if ($token !== "noToken") {
            $tokenGenerator = new TokenGenerator(new TokenBuilder());
            $token = $tokenGenerator->readToken($token);
            $data['userId'] = $token->getClaim('context')->user_id;
            $data['level'] = $token->getClaim('context')->level;
            $data['name'] = $token->getClaim('context')->name;
            $data['profile'] = $token->getClaim('context')->profile;
            $data['community'] = $token->getClaim('context')->community;
            $data['classCode'] = $token->getClaim('context')->classCode;
        }
        if ($token == "noToken") {
            $data['level'] = 'noToken';
        }
        $this->tokenData = $data;
        $this->method = $method;
        $this->path = "App\\ACL\\Roles\\";
    }

    public function validatePermission()
    {
        if (is_array($this->method)) {
            $lastIdx  = count($this->method) - 1;
            $this->method = 'none:'.$this->method[$lastIdx];
        }
        $level = $this->tokenData['level'];
        if ($level === 'Student Level 1') {
            $level = 'studentLevel1';
        }
        if ($level === 'Student Level 2') {
            $level = 'studentLevel2';
        }
        if ($level === 'Student Level 3') {
            $level = 'studentLevel3';
        }

        $role = new AclFactory();
        $this->validate = $role->checkPermissions(explode(":", $this->method), $level);
        return $this->validate;
    }

}