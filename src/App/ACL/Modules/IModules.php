<?php

namespace App\ACL\Modules;

interface IModules
{
    public function getMethods();
    public function getExceptions();
    public function getPermissions();
}

