<?php

namespace App\ACL\Modules\Recruiter;

use App\ACL\Modules\IModules;

class BMessages implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\BackOffice\BMessages::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}