<?php
/**
 * Created by PhpStorm.
 * User: doismundos
 * Date: 16/06/17
 * Time: 13:19
 */

namespace App\ACL\Modules;


use App\Controllers\Login;

class NoToken implements IModules
{

    public function getMethods()
    {

        return [
            'login',
            'passwordForgot',
            'resetPassword',
            'updatePassword',
            'signUp',
            'verifyClassCode',
            'verifyEmail',
            'getProfilesAndCommunities',
            'sendAvatar',
            'signupOrganization',
            'getSwaggerResponse',
            'emailConfirm',
            'resendConfirmEmail',
            'netWorkCheck',
            'getUserComments',
            'getClassInformation',
        ];
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}