<?php

namespace App\ACL\Modules\Admin;

use App\ACL\Modules\IModules;


class BackOffice implements IModules
{
    /**
     * Method to get all existed route methods on backOffice controller
     * @return array with all backOffice controller methods
     */
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\BackOffice\BackOffice::class);
    }

    /**
     * Method with locked methods for this type user
     * @return array a list with blocked methods for this type user
     */
    public function getExceptions()
    {
        return [];
    }

    /**
     * Method to check if the user can access this route.
     * @return array with allowed methods
     */
    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}