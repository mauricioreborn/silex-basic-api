<?php

namespace App\ACL\Modules\Admin;

use App\ACL\Modules\IModules;

class User implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\User::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}