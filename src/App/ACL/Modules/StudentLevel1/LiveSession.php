<?php

namespace App\ACL\Modules\StudentLevel1;

use App\ACL\Modules\IModules;

class LiveSession implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\LiveSession\LiveSession::class);
    }

    public function getExceptions()
    {
            return [
                'createSession',
                'raiseYourHandList',
                'delRaiseYourHand',
                'promoteSpeaker',
                'unPromoteSpeaker',
                'starArchive',
                'stopArchive',
                'getArchive',
                'promoteWork',
                'unPromoteWork',
            ];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}