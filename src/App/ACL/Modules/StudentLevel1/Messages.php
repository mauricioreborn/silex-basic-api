<?php

namespace App\ACL\Modules\StudentLevel1;

use App\ACL\Modules\IModules;

class Messages implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Messages\Messages::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}