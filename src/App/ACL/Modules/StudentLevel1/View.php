<?php

namespace App\ACL\Modules\StudentLevel1;

use App\ACL\Modules\IModules;
use App\Controllers\Work\View as Controller;

class View  implements IModules
{

    public function getMethods()
    {
        return get_class_methods(Controller::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}