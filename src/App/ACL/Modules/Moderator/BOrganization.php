<?php

namespace App\ACL\Modules\Moderator;

use App\ACL\Modules\IModules;

class BOrganization implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\BackOffice\BOrganization::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}