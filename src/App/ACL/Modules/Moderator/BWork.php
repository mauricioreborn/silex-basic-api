<?php

namespace App\ACL\Modules\Moderator;


use App\ACL\Modules\IModules;

class BWork implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\BackOffice\BWork::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}