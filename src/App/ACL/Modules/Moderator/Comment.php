<?php

namespace App\ACL\Modules\Moderator;

use App\ACL\Modules\IModules;

class Comment implements IModules
{
    public function getMethods()
    {
        $array1 = get_class_methods(\App\Controllers\Comment\Comment::class);
        $array2 = get_class_methods(\App\Controllers\Work\Comment::class);
        return array_merge($array1, $array2);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}