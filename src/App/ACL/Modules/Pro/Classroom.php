<?php

namespace App\ACL\Modules\Pro;

use App\ACL\Modules\IModules;

class Classroom implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Groups\Classroom::class);
    }

    public function getExceptions()
    {
        return [
            'classRoom',
            'updateClass'
        ];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}