<?php

namespace App\ACL\Modules\Pro;

use App\ACL\Modules\IModules;

class Settings implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Settings\Settings::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}