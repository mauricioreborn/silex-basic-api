<?php

namespace App\ACL\Modules\Pro;

use App\ACL\Modules\IModules;

class Lab implements IModules
{
    /*
     * Allowed Access to Specific Access
     */
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Groups\Lab::class);
    }

    /*
     * Forbidden access to the specific types
     */
    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}