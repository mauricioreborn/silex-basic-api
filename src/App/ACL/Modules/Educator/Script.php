<?php

namespace App\ACL\Modules\Educator;

use App\ACL\Modules\IModules;

class Profiles implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Scripts\Script::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}