<?php

namespace App\ACL\Modules\StudentLevel2;

use App\ACL\Modules\IModules;

class RecentActivity implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Community\RecentActivity::class);
    }

    public function getExceptions()
    {
        return [];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}