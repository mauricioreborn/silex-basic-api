<?php

namespace App\ACL\Modules\StudentLevel2;

use App\ACL\Modules\IModules;

class Group implements IModules
{
    public function getMethods()
    {
        return get_class_methods(\App\Controllers\Groups\Group::class);
    }

    public function getExceptions()
    {
        return [
            'uploadCover'
        ];
    }

    public function getPermissions()
    {
        return array_diff($this->getMethods(), $this->getExceptions());
    }
}