<?php

namespace App\Notification;

class NotificationQuerys extends Notification
{

    /** Generic Method to get userId by a QueryBuilder
     * @param $queryBuilder
     * @param $id
     * @return mixed
     */
    public function getUserId($queryBuilder, $id)
    {

        $data = $queryBuilder
            ->field("userId")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();

        if (count($data)>0) {
            return $data->getUserId();
        }
        return false;
    }

    /**
     * Generic Method to get Id
     * @param $queryBuilder
     * @param $id
     * @return mixed
     */
    public function getId($queryBuilder, $id)
    {
        $data = $queryBuilder->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();

        if (count($data)>0) {
            return $data->getId();
        }
        return false;
    }

    /** Method to get userId by another primaryKey
     * @param $queryBuilder
     * @param $id
     * @return bool
     */
    public function getUserIdOwnerId($queryBuilder, $id)
    {
        $data = $queryBuilder
            ->field("_id")
            ->equals(new \MongoId($id))
            ->getQuery()
            ->getSingleResult();

        if (count($data)>0) {
            return $data->getUserId();
        }
        return false;
    }

    /** Method to get an user by commentId and userId
     * @param $queryBuilder
     * @param $commentId
     * @param $userId
     * @return bool
     */
    public function getUserByCommentId($queryBuilder, $commentId, $userId)
    {
        $data = $queryBuilder
            ->field("commentId")
            ->equals(new \MongoId($commentId))
            ->field('userId')
            ->equals(new \MongoId($userId))
            ->getQuery()
            ->getSingleResult();

        if (count($data)>0) {
            return $data->getUserId();
        }
        return false;
    }




}