<?php

namespace App\Notification;

class Follows extends Notification implements NotificationInterface
{
    private $type = 'follow';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'followed you',
                $data['followId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'edit your own comment on your work',
                $data['followId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'unfollow you',
                $data['followId'],
                $data['owner'],
                'create'
            )
        );
    }
}