<?php

namespace App\Notification;

class Flag extends Notification implements NotificationInterface
{
    private $type = 'flag';

    public function create($data)
    {
        $message =  'your '.$data['type']. ' has been flagged and will be analyzed by one of our Admins.';
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                $message,
                $data['productId'],
                $data['owner'],
                'create'
            )
        );
    }
    
    public function update($data)
    {
    }
    
    public function delete($data)
    {
        // TODO: Implement delete() method.
    }
    

}