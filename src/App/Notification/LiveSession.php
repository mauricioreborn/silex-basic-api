<?php
namespace  App\Notification;

class LiveSession extends Notification implements NotificationInterface
{
    public function create($data)
    {
        return $this->notificationBuild('there is a new comment at work');
    }

    public function update($data)
    {
        return $this->notificationBuild('this comment has been updated');
    }

    public function delete($data)
    {
        return $this->notificationBuild('somebody deleted your comment');
    }
}