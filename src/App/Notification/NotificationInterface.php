<?php

namespace  App\Notification;

interface NotificationInterface
{
    public function create($data);
    public function update($data);
    public function delete($data);
    
    
}