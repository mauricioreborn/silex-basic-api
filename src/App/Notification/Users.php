<?php

namespace App\Notification;

class Users extends Notification implements NotificationInterface
{
    private $type = 'submit-review';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'there is a new work to review',
                $data['workId'],
                $data['userId'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'there is a new work to review',
                $data['workId'],
                'update'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a work was removed',
                $data['workId'],
                'delete'
            )
        );
    }

    public function allowToReview($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'You are now willing to submit a work to review',
                $data['userId'],
                $data['userId'],
                'delete'
            )
        );
    }

}