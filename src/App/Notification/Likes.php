<?php

namespace App\Notification;

class Likes extends Notification implements NotificationInterface
{
    private $type = 'like';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'you have a new like on your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a comment was edited on your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a comment was removed on your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

}