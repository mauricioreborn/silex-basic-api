<?php

namespace App\Notification;

class ReplyComments extends Notification implements NotificationInterface
{

    private $type = 'comment-reply';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'replied your comment',
                $data['postId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild('a reply comment was edited');
    }

    public function delete($data)
    {
        return $this->notificationBuild('somebody deleted your comment reply');
    }
}