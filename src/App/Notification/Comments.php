<?php
namespace  App\Notification;

class Comments extends Notification implements NotificationInterface
{
    private $type = 'Comments';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'You have a new comment on your work',
                $data['postId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a comment was edited',
                $data['productId'],
                'create'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'somebody deleted your comment',
                $data['productId'],
                'create'
            )
        );
    }


}