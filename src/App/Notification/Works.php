<?php

namespace  App\Notification;

class Works extends Notification implements NotificationInterface
{
    private $type = 'work';
    
    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'there is a new work to review',
                $data['workId'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'there is a new work to review',
                $data['workId'],
                'update'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a work was removed',
                $data['workId'],
                'delete'
            )
        );
    }
    
    
}