<?php

namespace App\Notification\Work;

use App\Notification\Notification;
use App\Notification\NotificationInterface;

class Likes extends Notification implements NotificationInterface
{
    private $type = 'like-work';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'liked your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a comment was edited on your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'a comment was removed on your work',
                $data['workId'],
                $data['owner'],
                'create'
            )
        );
    }

}