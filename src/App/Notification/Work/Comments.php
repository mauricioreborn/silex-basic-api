<?php
namespace  App\Notification\Work;

use App\Notification\Notification;
use App\Notification\NotificationInterface;

class Comments extends Notification implements NotificationInterface
{
    private $type = 'comment-work';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'commented on your work',
                $data['postId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'Edited your own comment',
                $data['productId'],
                'create'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'somebody deleted your comment',
                $data['productId'],
                'create'
            )
        );
    }


}