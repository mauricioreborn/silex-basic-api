<?php
namespace  App\Notification;

class Reviews extends Notification implements NotificationInterface
{
    private $type = 'review';

    public function create($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'reviewed your work',
                $data['postId'],
                $data['owner'],
                'create'
            )
        );
    }

    public function update($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'reviewed your work',
                $data['postId'],
                $data['owner'],
                'update'
            )
        );
    }

    public function delete($data)
    {
        return $this->notificationBuild(
            $this->arrayBuild(
                $data['userId'],
                $this->type,
                'reviewed your work',
                $data['postId'],
                $data['owner'],
                'delete'
            )
        );
    }
}