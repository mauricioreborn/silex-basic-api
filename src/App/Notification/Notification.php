<?php

namespace  App\Notification;

use App\Entities\Notification as NotificationEntity;
use Silex\Application;

abstract class Notification
{
    
    /** Build a notification
     * @param $data (userId,type,message,productId)
     * @return NotificationEntity
     */
    public function notificationBuild($data)
    {
        $notification = new NotificationEntity();
        $notification->setStatus('unread');
        $notification->setUserId(new \MongoId($data['userId']));
        $notification->setResponsibleUser(new \MongoId($data['responsibleUser']));
        $notification->setMessage($data['message']);
        $notification->setType($data['type']);
        $notification->setAction($data['action']);
        $notification->setProductId(new \MongoId($data['productId']));
        $notification->setCreatedAt($this->getMongoDate());
        return $notification;
    }


    public function getMongoDate($date = null)
    {
        $date = $date == null ? "Y-m-d H:i:s" : $date;
        $dt = new \DateTime(date($date), new \DateTimeZone('UTC'));
        $ts = $dt->getTimestamp();
        $this->today = new \MongoDate($ts);
        return $this->today;
    }

    /** Method to build a array with necessary parameters to generate a notification
     * @param $id  The user that make a action
     * @param $type type of notification (work,comment,livessesion,replyComment,etc).
     * @param $message notification Message.
     * @param $action Action (edit,delete,create)
     * @param $owner The user that will receive a notification.
     * @param $productId productId (userId,WorkId,postId,CommentId,etc)
     * @return mixed
     */
    public function arrayBuild($id, $type, $message, $productId, $owner, $action)
    {
        $data['type'] = $type;
        $data['userId'] = $owner;
        $data['responsibleUser'] = $id;
        $data['productId'] = $productId;
        $data['message'] = $message;
        $data['action'] = $action;
        return $data;
    }

}