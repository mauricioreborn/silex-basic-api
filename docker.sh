#!/bin/bash

if test "$1" = 'run'; then
    docker run -d -v $3:/data/db -v $2:/var/www/html -p 80:80 -p 27017:27017 $4
fi

