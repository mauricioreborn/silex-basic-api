#!/bin/bash

git checkout test
git pull origin test

git checkout engineering
git pull origin engineering
git merge test
git push origin engineering

git checkout law
git pull origin law
git merge test
git push origin law

git checkout prod
git pull origin prod
git merge test
git push origin prod

git checkout master
git pull origin master
git merge test
git push origin master
