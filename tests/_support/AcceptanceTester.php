<?php

use Codeception\Scenario;
use Codeception\Actor;
use App\Controllers\Login;
use Symfony\Component\HttpFoundation\Request;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */
    public function __construct(Scenario $scenario)
    {
        parent::__construct($scenario);
        $this->haveHttpHeader('X-ENVIRONMENT', 'testing');
    }


    public function setAdminToken() {
        $request = [
            'email' => 'proedu9@acme.com.br',
            'password' => '123456'
        ];
        
        $this->sendPOST('/login', $request);
        $response = json_decode($this->grabResponse());

        $this->haveHttpHeader('token', $response->token);
    }

    public function setStudentToken() {
        $request = [
            'email' => 'api@acme.com.br',
            'password' => '123456'
        ];

        $this->sendPOST('/login', $request);
        $response = json_decode($this->grabResponse());

        $this->haveHttpHeader('token', $response->token);
    }

    /**
     *  Method to log in acme with pro type user and set token on header
     */
    public function setProToken()
    {
        $request = [
            'email' => 'proedu4@acme.com.br',
            'password' => '123456'
        ];

        $this->sendPOST('/login', $request);
        $response = json_decode($this->grabResponse());

        $this->haveHttpHeader('token', $response->token);
    }
}
