<?php

namespace Helper;

use AcceptanceTester;
use App\Entities\Schools as EntitySchool;
use MongoId;

class School extends \Codeception\Module
{
    private $schoolId;

    public function createASchool(AcceptanceTester $I, string $organizationId)
    {
        $I->setAdminToken();

        $I->sendPOST('/backoffice/organization/school/new', [
            'schoolName' => 'Touhou',
            'organizationId' =>  $organizationId,
            'numberClassrooms' => 0,
            'numberStudents' => 0
        ]);

        $entityManager = $I->getEntityManager();
        $schoolRepository = $entityManager->getRepository(EntitySchool::class);
        $school = $schoolRepository->findOneBy(['name' => 'Touhou']);
        $this->schoolId[] = $school->getId();
        return ['schoolId' => $school->getId()];
    }

    public function getSchoolIds()
    {
        return $this->schoolId;
    }

    public function removeSchool(AcceptanceTester $I, array $schoolIds)
    {
        $entityManager = $I->getEntityManager();

        $schoolRepository = $entityManager->getRepository(EntitySchool::class);
        foreach ($schoolIds as $schoolId) {
            $school = $schoolRepository->findOneBy(['_id' => new MongoId($schoolId)]);
            $entityManager->remove($school);
            $entityManager->flush();
        }

        return true;
    }
}
