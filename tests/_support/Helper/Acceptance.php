<?php
namespace Helper;

class Acceptance extends \Codeception\Module
{

    public function getApplication() {
        return $this->getModule('Silex')->app;
    }

    public function getEntityManager() {
        return $this->getModule('Silex')->app['mongodbodm.dm'];
    }
}
