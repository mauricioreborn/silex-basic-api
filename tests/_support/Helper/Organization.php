<?php

namespace Helper;

use AcceptanceTester;
use App\Entities\Organization as EntityOrganization;
use MongoId;

class Organization extends \Codeception\Module
{
    private $organizationId;

    public function createAnOrganization(AcceptanceTester $I)
    {
        $I->sendPOST('/organization', [
            'contactFirstName' => 'Jun ya',
            'contactLastName' => 'Ota',
            'contactEmail' => 'zum@acme.com',
            'contactTitle' => 'Mr',
            'organizationName' => 'Project: Touhou',
            'numberOfSchools' => '2',
            'numberOfClassrooms' => '1',
            'phone' => '1234567891',
            'educationalStage' => 'middle_school',
            'duration' => '2',
            'involvement' => '2',
            'interestInvolvement' => '2',
            'interestedCommunity' => ['animation'],
            'foundOut' => 'Google',
        ]);

        $entityManager = $I->getEntityManager();

        $repository = $entityManager->getRepository(EntityOrganization::class);
        $organization = $repository->findOneBy(['email' => 'zum@acme.com']);
        $organization->setApproval('approved');
        $entityManager->flush();
        $this->organizationId[] = $organization->getId();
        return ['organizationId' => $organization->getId()];
    }

    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    public function removeOrganization(AcceptanceTester $I, array $organizationIds)
    {
        $entityManager = $I->getEntityManager();

        $organizationRepository = $entityManager->getRepository(EntityOrganization::class);
        foreach ($organizationIds as $organizationId) {
            $organization = $organizationRepository->findOneBy(['_id' => new MongoId($organizationId)]);
            $entityManager->remove($organization);
            $entityManager->flush();
        }

        return true;
    }
}