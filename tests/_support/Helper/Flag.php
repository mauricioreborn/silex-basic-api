<?php
/**
 * Class to Help creation of tests
 *
 * PHP version 7
 *
 * @category Test
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://acmeconnect.org/
 */
namespace Helper;

use MongoId;
use AcceptanceTester;
use App\Entities\Flag as FlagEntity;



/**
 * Flag Class to Help creation of tests
 *
 * PHP version 7
 *
 * @category Test
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://acmeconnect.org/
 */
class Flag extends \Codeception\Module
{
    /**
     * Method to delete all flags
     *
     * @param AcceptanceTester $I Class to make request and get entityManager
     *
     * @return null
     */
    public function flagDelete(AcceptanceTester $I)
    {
        $flag = $I->getEntityManager()->createQueryBuilder(FlagEntity::class);
        $flag
            ->remove()
            ->getQuery()
            ->execute();
    }

    /**
     * Method to delete all flags
     *
     * @param AcceptanceTester $I      Class to make request
     *
     * @param string           $workId Work id on database
     *
     * @return null
     */
    public function flagWork(AcceptanceTester $I, string $workId)
    {
        $I->sendPOST('/flag/work/' . $workId);
    }

}