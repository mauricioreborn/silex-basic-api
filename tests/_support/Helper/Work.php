<?php

namespace Helper;

use App\Entities\Work as WorkEntity;
use \AcceptanceTester as AcceptanceTester;
use MongoId;

class Work extends \Codeception\Module
{

    public $file = '5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd';
    public $url ='https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/';

    /**
     * Helper to create a work and return the work Id
     * @param AcceptanceTester $I Class used to make request on this method
     * @return string          A string work identifier on database
     */
    public function createAWork(AcceptanceTester $I)
    {
        $workParameters = [
            'title' => 'codeCeption work',
            'description' => [
               'codeception description',
               'test description'
            ],
            'category' => 'animation',
            'level' => 'Student Level 3',
            'group' => 'work',
            'mainFile' => [
                'path' => $this->url.$this->file.'jpg',
                'type' => 'jpg',
                'name' => 'vixi.jpg'
            ],
            'attachments' => [
                0 => [
                    'path' => $this->url.$this->file.'jpg',
                    'type' => 'jpg',
                    'name' => 'my-wallpaper.jpg'
                ]
            ],
            'thumbnail' => [
                'path' => $this->url.$this->file.'jpg',
                'type' => 'jpg',
                'name' => 'my-wallpaper.jpg'
            ],
            'coWorkers' => [],
            'availableReview' => 0,
            'sendPortfolio' => 0,
            'utcOffset' => -0200
        ];

        $I->sendPOST('/upload/work', $workParameters);
        return json_decode($I->grabResponse());
    }

    /**
     * Helper to create a pro tip and return the proTip Id
     * @param AcceptanceTester $I Class used to make request
     * @return string             A string ProTip id on work collection
     */
    public function createAProTip(AcceptanceTester $I)
    {
        $workParameters = [
            'title' => 'Pro tip test work',
            'description' => [
                'codeception description',
            ],
            'category' => 'animation',
            'level' => 'pro',
            'group' => 'proTip',
            'mainFile' => [
                'path' => $this->url.$this->file.'jpg',
                'type' => 'jpg',
                'name' => 'vixi.jpg'
            ],
            'thumbnail' => [
                'path' => $this->url.$this->file.'jpg',
                'type' => 'jpg',
                'name' => 'my-wallpaper.jpg'
            ],
            'coWorkers' => [],
            'availableReview' => 0,
            'sendPortfolio' => 0,
            'utcOffset' => -0200
        ];

        $I->sendPOST('/upload/work', $workParameters);
        return json_decode($I->grabResponse());
    }

    /**
     * Method to delete a work
     * @param AcceptanceTester $I Class used on this method to get entityManager
     * @param string $workId      Work identifier on database
     *                            (field _id on mongodb)
     * @return null
     */
    public function workDelete(AcceptanceTester $I, string $workId = null)
    {
        $workRepository = $I->getEntityManager()->createQueryBuilder(WorkEntity::class);
        $workQuery = $workRepository
            ->remove();

        if ($workId) {
            $workQuery = $workQuery->field('_id')->equals(new MongoId($workId));
        }

        $workQuery->getQuery()->execute();
        $I->getEntityManager()->flush();
    }
}