<?php

namespace Helper;

use AcceptanceTester;
use App\Entities\ClassRoom as EntityClassroom;
use MongoId;

class Classroom extends \Codeception\Module
{
    private $classId;

    public function createAClassroom(AcceptanceTester $I, string $schoolId)
    {
        $I->setAdminToken();

        $I->sendPOST('/backoffice/organization/school/classroom/new', [
            'classroomName' => 'Gensokyo',
            'schoolId' => $schoolId,
            'educatorEmail' => 'proedu1@acme.com.br',
        ]);

        $entityManager = $I->getEntityManager();
        $classroomRepository = $entityManager->getRepository(EntityClassroom::class);
        $classroom = $classroomRepository->findOneBy(['title' => 'Gensokyo']);
        $this->classId[] = $classroom->getId();

        return ['classroomId' => $classroom->getId()];
    }

    public function getClassroomId()
    {
        return $this->classId;
    }

    public function removeClassroom(AcceptanceTester $I, array $classroomIds)
    {
        $entityManager = $I->getEntityManager();
        $classroomRepository = $entityManager->getRepository(EntityClassroom::class);

        foreach ($classroomIds as $classroomId) {
            $classroom = $classroomRepository->findOneBy(['_id' => new MongoId($classroomId)]);
            $entityManager->remove($classroom);
            $entityManager->flush();
        }

        return true;
    }
}
