<?php

namespace Helper;

use AcceptanceTester;
use MongoId;
use App\Entities\User as EntityUser;

class User extends \Codeception\Module
{
    private $usersId;

    public function createAStudent(AcceptanceTester $I, string $classroomId)
    {
        $I->sendPOST('/signup', [
            'profile' => 'student',
            'association' => 'classroom',
            'firstName' => 'Reisen',
            'lastName' => 'Udongein',
            'gender' => 'f',
            'birthDate' => '01/01/1990',
            'email' => $this->getFaker()->email,
            'classroomCode' => $classroomId,
            'password' => '123456',
            'userPhoto' => 'https://s3.amazonaws.com/acmeconnect-data-dm/uploads/users/avatars/2174c83dbc906f3b3bcb4b5351903810.jpg',
            'community' => [
              'Animation',
              'Engineering',
              'Law'
            ],
            'phone' => '2313213213',
            'agreement' => true
        ]);
        $studentInfo = json_decode($I->grabResponse(), true);
        $this->approveUser($I, $studentInfo['id']);
        return $studentInfo['id'];
    }

    public function createAStudentInvalidClassCode(AcceptanceTester $I)
    {
        $I->sendPOST('/signup', [
            'profile' => 'student',
            'association' => 'classroom',
            'firstName' => 'Reimu',
            'lastName' => 'Hakurei',
            'gender' => 'f',
            'birthDate' => '01/01/1990',
            'email' => $this->getFaker()->email,
            'classroomCode' => '123456789123456789123456',
            'password' => '123456',
            'userPhoto' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/590820c0753bd9005006c938/404f333b85e95764e618e15ec57ef7a7.png',
            'community' => [
                'Animation',
                'Engineering',
                'Law'
            ],
            'phone' => '2313213213',
            'agreement' => true
        ]);
        $studentInfo = json_decode($I->grabResponse(), true);
        return $studentInfo['id'];
    }

    public function createAnEducator(AcceptanceTester $I, string $classroomId)
    {
        $I->sendPOST('/signup', [
            'profile' => 'educator',
            'association' => 'classroom',
            'firstName' => 'Sanae',
            'lastName' => 'Kochiya',
            'gender' => 'f',
            'birthDate' => '01/01/1990',
            'email' => $this->getFaker()->email,
            'classroomCode' => $classroomId,
            'password' => '123456',
            'userPhoto' => 'https://s3.amazonaws.com/acmeconnect-data-dm/uploads/users/avatars/9a76d9ee6df95790fdb4d9de809d57be.png',
            'community' => [
                'Animation',
                'Engineering',
                'Law'
            ],
            'phone' => '6546546546',
            'agreement' => true
        ]);
        $educatorInfo = json_decode($I->grabResponse(), true);
        $this->approveUser($I, $educatorInfo['id']);
        return $educatorInfo['id'];
    }

    public function createAProfessional(AcceptanceTester $I)
    {
        $I->sendPOST('/signup', [
            'profile' => 'professional',
            'association' => 'classroom',
            'firstName' => 'Solid',
            'lastName' => 'Snake',
            'gender' => 'm',
            'birthDate' => '01/01/1990',
            'email' => $this->getFaker()->email,
            'classroomCode' => '',
            'password' => '123456',
            'userPhoto' => 'https://s3.amazonaws.com/acmeconnect-data-dm/uploads/users/avatars/b6d7c85b374313297c47d1194d8da4fd.jpg',
            'community' => [
                'Animation',
                'Engineering',
                'Law'
            ],
            'phone' => '1234564654',
            'proTitle' => 'Philantroper',
            'proCompany' => 'Philantropy',
            'agreement' => true
        ]);
        
        $professionalInfo = json_decode($I->grabResponse(), true);
        $this->approveUser($I, $professionalInfo['id']);
        return $professionalInfo['id'];
    }

    public function createARecruiter(AcceptanceTester $I)
    {
        $I->sendPOST('/signup', [
            'profile' => 'recruiter',
            'association' => 'classroom',
            'firstName' => 'Taylor',
            'lastName' => 'Swift',
            'gender' => 'f',
            'birthDate' => '01/01/1990',
            'email' => $this->getFaker()->email,
            'classroomCode' => '',
            'password' => '123456',
            'userPhoto' => 'https://s3.amazonaws.com/acmeconnect-data-dm/uploads/users/avatars/062953dc7e2cd03113bbab00a7d4c9ef.jpg',
            'community' => [
                'Animation',
                'Engineering',
                'Law'
            ],
            'phone' => '2312312312',
            'proTitle' => 'Singer',
            'proCompany' => 'Singer',
            'agreement' => true
        ]);
        $recruiterInfo = json_decode($I->grabResponse(), true);
        $this->approveUser($I, $recruiterInfo['id']);
        return $recruiterInfo['id'];
    }

    public function approveUser(AcceptanceTester $I, string $userId)
    {
        $entityManager = $I->getEntityManager();

        $repository = $entityManager->getRepository(EntityUser::class);
        $user = $repository->findOneBy(['_id' => new MongoId($userId)]);
        $user->setStatus(true);
        $user->setState('approved');
        $entityManager->flush();
        $this->usersId[] = $userId;
        return true;
    }

    public function getUsersId()
    {
        return $this->usersId;
    }

    public function deleteUsers(AcceptanceTester $I, array $userIds)
    {
        $entityManager = $I->getEntityManager();
        $userRepository = $entityManager->getRepository(EntityUser::class);

        foreach ($userIds as $userId) {
            $user = $userRepository->findOneBy(['_id' => new MongoId($userId)]);
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return true;
    }

    public function getFaker()
    {
        $faker = \Faker\Factory::create();
        return $faker;
    }
}
