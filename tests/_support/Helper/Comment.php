<?php

namespace Helper;

use \AcceptanceTester as AcceptanceTester;
use App\Entities\Comments;
use MongoId;

class Comment extends \Codeception\Module
{
    public function createAComment(AcceptanceTester $I, $workId)
    {
        $commentParams = [
            'id' => $workId,
            'comment' => 'some important comment'
        ];
        $I->wantTo('route to  flag a work');
        $I->sendPOST('/work/comment',$commentParams);
        $I->canSeeResponseContains("comment has been add");
    }

    /**
     * @param AcceptanceTester $I
     * @param string $workId  work id identifier  on database
     * @return mixed
     */
    public function getCommentByWorkId(AcceptanceTester $I, string $workId)
    {
        $I->wantTo(' get all work comments');
        $I->sendGET('/comment/productId/'. $workId. '?limit=1');
        $comment =  json_decode($I->grabResponse(), true);
        return $comment[0]['commentId'];
    }

    /**
     * Method to remove all comments on acme-test database
     * @param AcceptanceTester $I
     */
    public function commentDelete(AcceptanceTester $I, string $commentId)
    {
        $workRepository = $I->getEntityManager()
            ->createQueryBuilder(Comments::class);
        $workRepository
            ->remove()
            ->field('_id')
            ->equals(new MongoId($commentId))
            ->getQuery()
            ->execute();
        $I->getEntityManager()->flush();
    }
}