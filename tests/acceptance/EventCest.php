<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Helper\Session;
use App\Entities\LiveSession;
use Helper\Work;
use Helper\User;

class EventCest
{
    private $events;
    private $createdEvents;
    private $hour = '09:00 PM';
    private $date;
    public $request = '/backoffice/list/users?limit=1';
    private $studentId;

    /**
     * Method to check if validators are working
     * @param AcceptanceTester $I Class used to make request
     * @return array              With event data
     */
    public function eventsResquest(AcceptanceTester $I)
    {

        $this->date = date('Y') + 1;
        $I->setAdminToken();
        $I->sendGet($this->request . '&page=1&status=approved&type=admin');
        $speakerId = json_decode($I->grabResponse("message"), true);
        $speakerId = $speakerId['message'][0]['user']['id'];

        $I->sendGet($this->request . '&page=1&status=approved&type=student');
        $studentId = json_decode($I->grabResponse("message"), true);
        $this->studentId = $studentId['message'][0]['user']['id'];

        $files = [
            'file' => [
                'name' => 'teste2.jpg',
                'type' => 'teste2/jpg',
                'size' => filesize(__DIR__ . '/../Files/images/teste2.jpg'),
                'tmp_name' => __DIR__ . '/../Files/images/teste2.jpg',
                'error' => 0,
            ]
        ];

        $em =  $I->getEntityManager();
        $repository =$em->getRepository(LiveSession::class);

        $livesessions = $repository->findAll();

        foreach ($livesessions as $livesession) {
            $em->remove($livesession);
            $em->flush();
        }

        $I->sendPOST('/upload/file', [], [$files]);
        $file = json_decode($I->grabResponse('path'), true);

        return $this->events = [
            [
                'status' => 200,
                'message'=> 'session has been created',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'endTime' => '07:00 PM',
                    'title' => 'First Session',
                    'type' => 'public',
                    'description' => 'description long :)',
                    'speaker' => $speakerId,
                    'participants' => [],
                    'utcOffset' => -0200,
                    'deadlineStatus' => true,
                    'deadlineDate' => $this->date,
                    'deadlineTime' => $this->hour
                ]
            ],
            [
                'status' => 500,
                'message'=> 'Invalid object ID',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'endTime' => '07:00 PM',
                    'title' => 'First Session',
                    'type' => 'public',
                    'description' => 'description long :)',
                    'speaker' => '',
                    'participants' => [$this->studentId],
                    'utcOffset' => -0200
                ]
            ],
            [
                'status' => 400,
                'message'=> '[description] This value is too short. It should have 8 characters or more.',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'endTime' => '07:00 PM',
                    'title' => 'First Session',
                    'type' => 'public',
                    'speaker' => $speakerId,
                    'participants' => [$this->studentId],
                    'utcOffset' => -0200
                ]
            ],
            [
                'status' => 400,
                'message'=> '"[endTime] This value is too short. It should have 2 characters or more."]',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'title' => 'First Session',
                    'type' => 'public',
                    'description' => 'description long :)',
                    'speaker' => $speakerId,
                    'participants' => [$this->studentId],
                    'utcOffset' => -0200
                ]
            ],
            [
                'status' => 200,
                'message'=> 'session has been created',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'endTime' => '07:00 PM',
                    'title' => 'Second Session',
                    'type' => 'private',
                    'description' => 'description long :)',
                    'speaker' => $speakerId,
                    'participants' => [$this->studentId],
                    'utcOffset' => -0200
                ]
            ],
            [
                'status' => 200,
                'message'=> 'session has been created',
                'data' => [
                    'thumbnail' => $file['path'],
                    'date' => $this->date,
                    'startTime' => '07:00 PM',
                    'endTime' => '07:00 PM',
                    'title' => 'third Session',
                    'type' => 'public',
                    'description' => 'description long :)',
                    'speaker' => $speakerId,
                    'participants' => [$this->studentId],
                    'utcOffset' => -0200
                ]
            ]
        ];
    }

    /**
     * @depends eventsResquest
     */
    public function testShouldCreateAnEvent(AcceptanceTester $I)
    {
        $I->setAdminToken();
        $I->sendGet($this->request . '&page=1&status=approved&type=admin');

        $I->wantTo('Create An Event');
        foreach ($this->events as $event) {
            $I->sendPOST('/session', $event['data']);
            $I->canSeeResponseContains($event['message']);
            $I->canSeeResponseCodeIs($event['status']);
        }
    }

    /**
     * @depends testShouldCreateAnEvent
     */
    public function testShouldFetchPendingApprovalEvents(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Show Pending Events');
        $I->sendGet('/backoffice/fetch/sessions', []);
        $I->canSeeResponseContains('sessionId');

        $pendingEvents = json_decode($I->grabResponse(), true);
        $this->createdEvents = $pendingEvents['sessions'];
    }

    /**
     * @depends testShouldCreateAnEvent
     */
    public function setFieldsForEventApproval(AcceptanceTester $I)
    {
        $eventsToBeApproved = [];
        $I->setAdminToken();

        $I->sendGet($this->request . '&page=1&status=approved&type=moderator');
        $moderatorId = json_decode($I->grabResponse("message"), true);
        $moderatorId = $moderatorId['message'][0]['user']['id'];

        foreach ($this->createdEvents as $createdEvent) {
            $createdEvent['approved'] = true;
            $createdEvent['moderatorId'] = $moderatorId;
            $createdEvent['highLight'] = false;
            $createdEvent['date'] = $this->date;
            $eventsToBeApproved[] = $createdEvent;
        }

        unset($this->createdEvents);
        $this->createdEvents = $eventsToBeApproved;
    }

    /**
     * @depends setFieldsForEventApproval
     */
    public function testShouldApproveAnEvent(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Approve An Event');

        foreach ($this->createdEvents as $createdEvent) {
            $I->sendPUT('/backoffice/session/update', $createdEvent);
            $I->canSeeResponseContains('Session 1');
            $I->canSeeResponseCodeIs(200);
        }
    }

    /**
     * @depends testShouldApproveAnEvent
     */
    public function testShouldGetEventsApproved(AcceptanceTester $I)
    {
        $I->setAdminToken();
        $route = '/events/list/' . $this->date . '/' . $this->date ;
        $I->wantTo('I want to see an event that was already approved');
        $I->sendGET($route . '?limit=5&skip=0', []);

        foreach ($this->createdEvents as $createdEvent) {
            $I->canSeeResponseContains('"approved":true');
            $I->canSeeResponseContains($createdEvent['sessionId']);
        }
    }

    public function testShouldSearchUsersByName(AcceptanceTester $I, User $userHelper)
    {
        $studentId = $userHelper->createAStudentInvalidClassCode($I);
        $I->setAdminToken();

        $I->wantTo('I want to search students in add participants search');
        $I->sendGET('/user/name/rei/profile/student', []);
        $I->canSeeResponseContains('Reimu');
        $userHelper->deleteUsers($I, [$studentId]);
    }

    public function shouldAddAWorkOnASession(AcceptanceTester $I, Work $work)
    {
        $I->setAdminToken();
        $workId = $work->createAWork($I);

        foreach ($this->createdEvents as $createdEvent) {
            $submitWorkSessionParameter = [
                'sessionId' => $createdEvent['sessionId'],
                'workId' => $workId,
                'action' => 'add',
                'userId' => $this->studentId
            ];

            $I->wantTo('I wanna add a work on a live session with an admin profile');
            $I->sendPOST('/submit/session/work', $submitWorkSessionParameter);
            $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
            codecept_debug($I->grabResponse());
            $I->seeResponseContains('work has been added');
        }

        $work->workDelete($I, $workId);
    }

    /**
     * @depends shouldAddAWorkOnASession
     */
    public function testShouldSetEventIsDeadLine(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Set the deadline of an event');

        foreach ($this->createdEvents as $createdEvent) {
            $I->sendPOST('/events/deadline/' . $createdEvent['sessionId'], [
                'deadlineStatus' => true,
                'deadlineDate' => $this->date,
                'deadlineTime' => $this->hour
            ]);
            $I->canSeeResponseContains('DeadLine was changed');

            $I->sendGET('/events/list/' . $this->date . '/' . $this->date . '?limit=5&skip=0', []);
            $I->canSeeResponseContains('"deadlineStatus":true');
            $I->canSeeResponseContains($createdEvent['sessionId']);
        }
    }

    /**
     * @depends testShouldSetEventIsDeadLine
     */
    public function testShouldUnsetEventIsDeadLine(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Unset the deadline of an event');
        foreach ($this->createdEvents as $createdEvent) {
            $I->sendPOST('/events/deadline/' . $createdEvent['sessionId'], []);
            $I->canSeeResponseContains('DeadLine was changed');

            $I->sendGET('/events/list/' . $this->date . '/' . $this->date . '?limit=5&skip=0', []);
            $I->canSeeResponseContains('"deadlineStatus":false');
            $I->canSeeResponseContains($createdEvent['sessionId']);
        }
    }

    /**
     * @param AcceptanceTester $I
     * @param Work $work
     * @param Session $session
     */
    public function shouldReturnWorksWhenAStudentEnrollOnASession(
        AcceptanceTester $I,
        Work $work,
        Session $session
    ) {
        $I->setStudentToken();
        $work->createAWork($I);
        $I->setProToken();
        $work->createAProTip($I);
        $I->setAdminToken();
        $session->SessionEnroll($I, $this->createdEvents[0]['sessionId']);

        $I->wantTo('getWorks of the enrolled users');
        $I->sendGET("/event/".$this->createdEvents[0]['sessionId']."/users/works", []);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContains('title');
        $work->workDelete($I);
    }

    /**
     * @param AcceptanceTester $I
     * @param Work $work
     * @param Session $session
     */
    public function shouldModifyASubmittedWorkOrder(AcceptanceTester $I, Work $work, Session $session)
    {
        $I->setAdminToken();
        $workId = $work->createAWork($I);
        foreach ($this->createdEvents as $createdEvent) {
            $submitWorkSessionParameter = [
                'sessionId' => $createdEvent['sessionId'],
                'workId' => $workId,
                'action' => 'add',
                'userId' => $this->studentId
            ];
            $session->submitAWorkOnASession($I, $submitWorkSessionParameter);
        }
        $I->wantTo('I wanna change work order');
        foreach ($this->createdEvents as $createdEvent) {
            $orderParameters = [
                'eventId' => $createdEvent['sessionId'],
                'workId' => $workId,
                'order' => rand(1, 10)
            ];

            $I->sendPUT('/events/works/order', $orderParameters);
            $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
            $I->seeResponseContains('work has been sorted');
        }
    }

    /**
     * @param AcceptanceTester $I
     * @param Session $helper
     * @depends shouldModifyASubmittedWorkOrder
     */
    public function removeEvents(AcceptanceTester $I, Session $helper)
    {
        foreach ($this->createdEvents as $createdEvent) {
            $helper->sessionDelete($I, $createdEvent['sessionId']);
        }
    }
}
