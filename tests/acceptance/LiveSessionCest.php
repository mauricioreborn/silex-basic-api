<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Codeception\Example;

class LiveSessionCest
{

    public function shouldSaveDrawoverLine(AcceptanceTester $I)
    {
        $I->wantTo('Save drawover line');
        $I->setStudentToken();
        $I->sendPOST('/save/drawover', [
            'originId' => '5a6645240be80a05db6f2da2',
            'operation' => 'push',
            'tag' => '<path/>'
        ]);

        $I->seeResponseContains('"Operation Saved"');
        $I->seeResponseContains('"id"');
    }

    public function pathsProvider()
    {
        return [
            [ 'path' => '<path1/>', 'operation' => 'push', 'result' => 'path1' ],
            [ 'path' => '<path2/>', 'operation' => 'pull', 'result' => 'path2' ],
            [ 'path' => [
                '<path1/>',
                '<path2/>',
                '<path3/>',
            ], 'operation' => 'pull', 'result' => 'path2' ],
        ];
    }

    /**
     * @dataProvider pathsProvider
     */
    public function shouldHideAllPathsWhenAnArrayIsGivenAsArgument(AcceptanceTester $I, Example $examples)
    {
        $I->wantTo('Hide drawover lines');
        $I->setStudentToken();
        $I->sendPOST('/save/drawover', [
            'originId' => '5a6645240be80a05db6f2da2',
            'operation' => $examples['operation'],
            'tag' => [
                $examples['path']
            ]
        ]);

        $I->seeResponseContains('"Operation hide all"');
        $I->seeResponseContains($examples['result']);
    }
}
