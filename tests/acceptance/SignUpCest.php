<?php 

namespace App\Tests\Acceptance;

use App\Entities\User;
use AcceptanceTester;

class SignUpCest {

    public function testShouldSignUp(AcceptanceTester $I)
    {
        $I->wantTo('I want to register an user');
        $I->sendPOST('/signup', [
            'token' => '244',
            'name' => 'user one',
            'email' => 'api-acme@acme.com.br',
            'password' => '123456',
            'association' => 'individual',
            'birthDate' => '10/10/1993',
            'community' => ['animation','law'],
            'firstName' => 'test Api',
            'lastName' => 'acceptain',
            'agreement' => 'true',
            'gender' => 'm',
            'phone' => '3224400088',
            'profile' => 'student'
        ]);

        $em =  $I->getEntityManager();
        $repository =$em->getRepository(User::class);

        $user = $repository->findOneBy(['email' => 'api-acme@acme.com.br']);
        
        $em->remove($user);
        $em->flush();

        $I->seeResponseContains('User has been created successfully');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
    }

    public function testShouldVerifyIfIsAnAvailableEmail(AcceptanceTester $I)
    {
        $I->wantTo('I want to verify if is  an available email');
        $emails = [
            [
                'email' => 'api@acme.com.br',
                'result' => 'email unavailable'
            ],
            [
                'email' => 'mauricio.reborn@hotmail.com',
                'result' => 'email available'
            ]
        ];

        foreach ($emails as $email) {
            $I->sendGET(
                '/signup/email/'.$email['email']
            );
            $I->seeResponseContains($email['result']);
        }
    }
}
