<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;

class ApiStructureCest
{

    public function shouldNotAllowRequestWithoutToken(AcceptanceTester $I)
    {
        $I->wantTo('make sure the api rejects request without token');
        $I->sendGET('/');
        $I->seeResponseCodeIs(401);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":"Access Not Allowed Or Invalid Token"}');
    }
}