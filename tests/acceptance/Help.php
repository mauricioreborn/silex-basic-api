<?php

namespace App\Tests\Acceptance;

use App\Entities\User;
use App\Services\Auth\TokenGenerator;
use App\Services\Auth\TokenBuilder;
use Doctrine\ORM\EntityManager;

 class Help
{
    private $app;
    private $entity;
    public function __construct()
    {
        $app = require __DIR__ . '/../resources/config/bootstrap.php';
        $this->app =  $app;
        $this->entity = $app['mongodbodm.dm'];
    }

    public function getTokenWithValidUser()
    {
        $token = new TokenGenerator(new TokenBuilder());
        $user = $this->queryBuilder->getRepository(User::class)->findOneBy(['email' => 'api@acme.com.br'])->getId();
        $token->setContext(['user_id' => $user]);
        return $token->getToken();
    }
}