<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Codeception\Example;
use Helper\Work;

class WorkCest
{

    private $workIds = [];

    public function testShouldUploadAFile(AcceptanceTester $I)
    {
        $I->setStudentToken();

        $I->wantTo('Upload a file');
        $files = [
            'file' => [
                'name' => 'teste2.jpg',
                'type' => 'teste2/jpg',
                'size' => filesize(__DIR__ . '/../Files/images/teste2.jpg'),
                'tmp_name' => __DIR__ . '/../Files/images/teste2.jpg',
                'error' => 0,
            ]
        ];
        $I->sendPOST('/upload/file', [], [$files]);
        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseContains('path');
    }

    private function provider()
    {
        return [
            [
                'status' => 200,
                'data' => [
                    'title' => 'Work one1111',
                    'description' => [
                        'A description cool1111l',
                        'A cool question11111'
                    ],
                    'category' => 'animation',
                    'level' => 'Student Level 3',
                    'group' => 'work',
                    'mainFile' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'thumbnail' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'utcOffset' => -0200,
                    'availableReview' => 0,
                    'sendPortfolio' => 0,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'isPrivate' => 0
                ]
            ],
            [
                'status' => 200,
                'data' => [
                    'title' => 'Work Two22222',
                    'description' => [
                        'A description cool1111l',
                        'A cool question11111'
                    ],
                    'category' => 'animation',
                    'level' => 'Student Level 3',
                    'group' => 'work',
                    'mainFile' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'thumbnail' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'availableReview' => 0,
                    'sendPortfolio' => null,
                    'utcOffset' => -0200,
                    'isPrivate' => 0
                ]
            ],
            [
                'status' => 200,
                'data' => [
                    'title' => 'Tiny Toons :D',
                    'description' => [
                        'A description cool1111l',
                        'A cool question11111'
                    ],
                    'category' => 'animation',
                    'level' => 'Student Level 3',
                    'group' => 'work',
                    'mainFile' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'thumbnail' => [
                        'path' => 'https://acmeconnect-data-dm.s3.amazonaws.com/uploads/users/5a65e6e1533c8900da456a62/6b12f89103bf86d9a0dca388d953f8fd.jpg',
                        'type' => 'jpg',
                        'name' => 'teste2'
                    ],
                    'availableReview' => 0,
                    'utcOffset' => -0200,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'isPrivate' => 1
                ]
            ]
        ];
    }

    /**
     * @dataprovider provider
     */
    public function testShouldAddAWork(AcceptanceTester $I, Example $example)
    {
        $I->setStudentToken();

        $I->wantTo('Add a new work');
        $I->sendPOST('/upload/work', $example['data']);
        if ($example['data']['isPrivate'] == false) {
            $this->workIds[] = str_replace('"', '', $I->grabResponse());
        }
        $I->canSeeResponseCodeIs($example['status']);
    }

    public function testShouldReturnAllWorks(AcceptanceTester $I)
    {
        $I->setStudentToken();
        $I->wantTo('I want to see all Works');
        $I->sendGET('/getwork');

        foreach ($this->workIds as $workId) {
            $I->canSeeResponseContains($workId);
        }
    }

    public function testShouldReturnAWorkById(AcceptanceTester $I)
    {
        $I->setStudentToken();

        $I->wantTo('I want to see a work using its ID');
        $I->sendGET('/work/' . $this->workIds[0]);
        $I->canSeeResponseContains($this->workIds[0]);
    }

    public function testShouldCheckPublicWork(AcceptanceTester $I)
    {
        $I->setStudentToken();

        $I->wantTo('I want to a public work');
        $I->sendGET('/getwork');
        $I->canSeeResponseContainsJson(['isPrivate' => false]);
        $I->cantSeeResponseContainsJson(['isPrivate' => true]);
    }

    /**
     * @depends testShouldAddAWork
     */
    public function testShouldRemoveWorks(AcceptanceTester $I, Work $workHelper)
    {
        $I->setStudentToken();

        $I->wantTo('I want to delete works');
        foreach ($this->workIds as $workId) {
            $I->sendDELETE('/work/' . $workId);
            $I->sendGET('/getwork');
            $I->cantSeeResponseContains($workId);
            $workHelper->workDelete($I, $workId);
        }
    }
}