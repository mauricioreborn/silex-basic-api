<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Helper\Comment;
use Helper\Flag;
use Helper\Work;

class FlagCest
{
    /**
     * Method to test if is possible to flag a work
     * @param AcceptanceTester $I Acceptance to make requests
     * @param Work $workHelper a Helper to create a work
     */
    public function testShouldFlagAWork(
        AcceptanceTester $I,
        Work $workHelper,
        Flag $flagHelper
    ) {
        $I->setStudentToken();
        $workId = $workHelper->createAWork($I);
        $I->setAdminToken();
        $I->wantTo('method to test if is possible to flag a work');
        $I->sendPOST('/flag/work/' . $workId);
        $I->canSeeResponseContains("work has been flagged");
        $I->sendPOST('/flag/work/' . $workId);
        $I->canSeeResponseContains("user already flagged this work");
        $workHelper->workDelete($I, $workId);
        $flagHelper->flagDelete($I);
    }

    /**
     * Method to test if is possible to flag a work and remove the flag.
     * @param AcceptanceTester $I Acceptance to make requests
     * @param Work $workHelper a Helper to create a work
     */
    public function testShouldRemoveAWorkFlag(
        AcceptanceTester $I,
        Work $workHelper,
        Flag $flagHelper
    ) {
        $I->setStudentToken();
        $workId = $workHelper->createAWork($I);
        $I->setAdminToken();

        $I->wantTo('method to test flag a work and remove');

        $I->sendPOST('/flag/work/' . $workId);
        $I->canSeeResponseContains("work has been flagged");
        $I->sendDELETE('/flag/work/' . $workId);
        $I->canSeeResponseContains("Flag has been removed");

        $workHelper->workDelete($I, $workId);
        $flagHelper->flagDelete($I);
    }

    /**
     * Method to test if is possible to flag a comment
     * @param AcceptanceTester $I $I Acceptance to make requests
     * @param Work $workHelper a Helper to create a work
     * @param Comment $commentHelper a Helper to create a comment
     */
    public function testShouldFlagAComment(
        AcceptanceTester $I,
        Work $workHelper,
        Comment $commentHelper,
        Flag $flagHelper
    ) {
        $I->setStudentToken();

        $workId = $workHelper->createAWork($I);
        $commentHelper->createAComment($I, $workId);

        $commentId = $commentHelper->getCommentByWorkId($I, $workId);

        $I->setAdminToken();

        $I->wantTo('test to flag a comment');

        $I->sendPOST('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("Comment has been flagged");
        $I->sendPOST('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("user already flagged this comment");
        $workHelper->workDelete($I, $workId);
        $commentHelper->commentDelete($I, $commentId);
        $flagHelper->flagDelete($I);
    }

    /**
     * Method to test if is possible to flag a comment and remove
     * @param AcceptanceTester $I $I Acceptance to make requests
     * @param Work $workHelper a Helper to create a work
     * @param Comment $commentHelper a Helper to create a comment
     */
    public function testShouldRemoveACommentFlag(
        AcceptanceTester $I,
        Work $workHelper,
        Comment $commentHelper,
        Flag $flagHelper
    ) {
        $I->setStudentToken();
        $workId = $workHelper->createAWork($I);
        $commentHelper->createAComment($I, $workId);
        $commentId = $commentHelper->getCommentByWorkId($I, $workId);
        $I->wantTo('to  flag a comment and after remove ');
        $I->sendPOST('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("Comment has been flagged");
        $I->sendDELETE('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("Your flag has been removed");
        $commentHelper->commentDelete($I, $commentId);

        $I->setAdminToken();
        $workId = $workHelper->createAWork($I);
        $commentHelper->createAComment($I, $workId);
        $commentId = $commentHelper->getCommentByWorkId($I, $workId);
        $I->wantTo('to  flag a comment and after remove ');
        $I->sendPOST('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("Comment has been flagged");
        $I->sendDELETE('/flag/comment/' . $commentId);
        $I->canSeeResponseContains("Flag has been removed");

        $flagHelper->flagDelete($I);
        $commentHelper->commentDelete($I, $commentId);
    }
}