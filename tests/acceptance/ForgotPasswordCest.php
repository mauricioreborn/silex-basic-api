<?php

namespace App\Tests\Acceptance;

use DDesrosiers\SilexAnnotations\Annotations\After;
use App\Tests\Acceptance\SignUpCest as SignUp;
use App\Tests\Acceptance\UserCest as User;
use DDesrosiers\SilexAnnotations\Annotations\Before;

class ForgotPasswordCest
{
    /**
     * @param \AcceptanceTester $I
     *
     * @After(SignUp::testShouldSignUp)
     * @Before(User::testShouldDeleteAnUser)
     */
    public function testShouldSendEmailForgotPassword(\AcceptanceTester $I)
    {
        $I->wantTo('I want to send Email to forgotPassword and trigger email send with token');
        $I->sendPOST(
            '/password/forgot',
            [
                'email' => 'api@acme.com.br'
            ]
        );
//        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContains('E-mail with Token Sent');
    }

}