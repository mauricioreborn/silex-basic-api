<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;

class UserCest
{

    public function testShouldDeleteAnUser(AcceptanceTester $I, $scenario)
    {
        $scenario->incomplete();
        $I->wantTo('I want to delete an user');
        $email = 'api-acme@acme.com.br';
        $I->sendDELETE(
            '/user/delete/'.$email
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContains('User has been removed');
    }
}