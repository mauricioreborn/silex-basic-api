<?php


namespace App\Tests\Acceptance;


class CommunitiesCest
{

    public function communitiesHasBeenReturned(\AcceptanceTester $I)
    {
        $I->wantTo('I want to get all valids profiles and communities');
        $I->sendGET('/signup/data');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContains('student');
    }
}