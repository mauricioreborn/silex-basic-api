<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use App\Entities\Organization;
use Helper\Organization as Helper;

class RequestSearchCest
{
    private $orgId;
    private $search = '?limit=10&page=1&status=pending&type=admin';

    public function _before(AcceptanceTester $I, Helper $organizationHelper)
    {
        $orgId = $organizationHelper->createAnOrganization($I);
        $this->orgId[] = $orgId['organizationId'];

        $I->sendPOST('/organization', [
            'contactFirstName' => 'Shinigame',
            'contactLastName' => 'Sama',
            'contactEmail' => 'shinigame@acme.com',
            'contactTitle' => 'Mr',
            'organizationName' => 'Death City',
            'numberOfSchools' => '2',
            'numberOfClassrooms' => '1',
            'phone' => '1234567891',
            'educationalStage' => 'middle_school',
            'duration' => '2',
            'involvement' => '2',
            'interestInvolvement' => '2',
            'interestedCommunity' => ['animation'],
            'foundOut' => 'Google',
        ]);

        $I->sendPOST('/organization', [
            'contactFirstName' => 'Shinigame',
            'contactLastName' => 'Sama',
            'contactEmail' => 'shinigame@acme.com',
            'contactTitle' => 'Mr',
            'organizationName' => 'Life City',
            'numberOfSchools' => '2',
            'numberOfClassrooms' => '1',
            'phone' => '1234567891',
            'educationalStage' => 'middle_school',
            'duration' => '2',
            'involvement' => '2',
            'interestInvolvement' => '2',
            'interestedCommunity' => ['animation'],
            'foundOut' => 'Google',
        ]);

        $entityManager = $I->getEntityManager();

        $repository = $entityManager->getRepository(Organization::class);
        $organizations = $repository->findBy(['email' => 'shinigame@acme.com']);

        foreach ($organizations as $organization) {
            $this->orgId[] = $organization->getId();
        }
    }

    public function _after(AcceptanceTester $I, Helper $organizationHelper)
    {
        $organizationHelper->removeOrganization($I, $this->orgId);
        $this->orgId = '';
    }

    public function testSearchAllPendingOrganization(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search only pending organization');
        $I->sendGET('/backoffice/list/organizations' . $this->search);
        $I->cantSeeResponseContainsJson(['status' => true]);
        $I->canSeeResponseContainsJson(['totalResults' => 2]);
    }

    public function testSearchPendingOrganizationWithLetterL(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search only pending organizations with letter L');
        $I->sendGET('/backoffice/list/organizations' . $this->search . '&search=l');
        $I->cantSeeResponseContainsJson(['status' => true]);
        $I->canSeeResponseContainsJson(['totalResults' => 1]);
    }
}