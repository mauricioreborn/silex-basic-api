<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use App\Entities\Schools;
use Helper\Organization as OrganizationHelper;
use MongoId;

class SchoolSearchCest
{
    private $orgId;
    private $search = 'limit=10&page=1&status=approved&type=admin';

    public function _before(AcceptanceTester $I, OrganizationHelper $organizationHelper)
    {
        $this->orgId = $organizationHelper->createAnOrganization($I);

        $I->setAdminToken();

        $I->sendPOST('/backoffice/organization/school/new', [
            'schoolName' => 'Gensokyo',
            'organizationId' => $this->orgId['organizationId'],
            'numberClassrooms' => 0,
            'numberStudents' => 0
        ]);

        $I->sendPOST('/backoffice/organization/school/new', [
            'schoolName' => 'Alura',
            'organizationId' => $this->orgId['organizationId'],
            'numberClassrooms' => 0,
            'numberStudents' => 0
        ]);
    }

    public function _after(AcceptanceTester $I, OrganizationHelper $organizationHelper)
    {
        $organizationHelper->removeOrganization($I, $this->orgId);
        $entityManager = $I->getEntityManager();

        $schoolRepository = $entityManager->getRepository(Schools::class);
        $schools = $schoolRepository->findBy(['organizationId' => new MongoId($this->orgId['organizationId'])]);

        foreach ($schools as $school) {
            $entityManager->remove($school);
            $entityManager->flush();
        }
    }

    public function testShouldSearchAllSchools(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search schools');
        $I->sendGET('/backoffice/list/organization/schools?' . $this->search);
        $I->canSeeResponseContainsJson(['totalResults' => 2]);
        $I->canSeeResponseContains('Alura');
        $I->canSeeResponseContains('Gensokyo');
    }

    public function testShouldSearchSchoolsWithLetterG(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search schools with letter G');
        $I->sendGET('/backoffice/list/organization/schools?' . $this->search . '&search=g');
        $I->canSeeResponseContainsJson(['totalResults' => 1]);
        $I->canSeeResponseContains('Gensokyo');
    }
}
