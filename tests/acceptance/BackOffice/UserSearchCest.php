<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Helper\Classroom;
use Helper\Organization;
use Helper\School;
use Helper\User;

class UserSearchCest
{
    private $orgId;
    private $schoolId;
    private $classId;
    private $usersId;
    private $search = 'limit=30&page=1&status=all&type=all';

    public function _before(
        AcceptanceTester $I,
        Organization $orgHelper,
        School $schoolHelper,
        Classroom $classHelper,
        User $userHelper
    ) {
        $this->orgId = $orgHelper->createAnOrganization($I);
        $this->schoolId = $schoolHelper->createASchool($I, $this->orgId['organizationId']);
        $this->classId = $classHelper->createAClassroom($I, $this->schoolId['schoolId']);
        $this->usersId[] = $userHelper->createAStudent($I, $this->classId['classroomId']);
        $this->usersId[] = $userHelper->createAnEducator($I, $this->classId['classroomId']);
        $this->usersId[] = $userHelper->createAProfessional($I);
        $this->usersId[] = $userHelper->createARecruiter($I);
    }

    public function _after(
        AcceptanceTester $I,
        Organization $orgHelper,
        School $schoolHelper,
        Classroom $classHelper,
        User $userHelper
    ) {
        $orgHelper->removeOrganization($I, $this->orgId);
        $schoolHelper->removeSchool($I, $this->schoolId);
        $classHelper->removeClassroom($I, $this->classId);
        $userHelper->deleteUsers($I, $this->usersId);
        $this->usersId = '';
    }

    public function testSearchAllUsers(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search all users in');
        $I->sendGET('/backoffice/list/users?' . $this->search);
        $I->canSeeResponseContainsJson(['level' => 'Pro']);
        $I->canSeeResponseContainsJson(['level' => 'Student Level 3']);
        $I->canSeeResponseContainsJson(['level' => 'Student Level 1']);
        $I->canSeeResponseContainsJson(['level' => 'Recruiter']);
    }

    public function testSearchUserByName(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search an user called Reisen');
        $I->sendGET('/backoffice/list/users?' . $this->search . '&search=reisen');
        $I->canSeeResponseContains('Reisen');
    }

    public function testSearchOnlyAdmins(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search only admins');
        $search = str_replace($this->search, 'type=all', 'type=admin');
        $I->sendGET('/backoffice/list/users?' . $search);
        $I->cantSeeResponseContainsJson(['level' => 'Pro']);
        $I->cantSeeResponseContainsJson(['level' => 'Student Level 3']);
        $I->cantSeeResponseContainsJson(['level' => 'Student Level 1']);
        $I->cantSeeResponseContainsJson(['level' => 'Recruiter']);
        $I->canSeeResponseContainsJson(['level' => 'Admin']);
    }
}
