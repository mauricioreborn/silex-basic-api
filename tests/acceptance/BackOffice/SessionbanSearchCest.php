<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Helper\Session;
use Helper\User;

class SessionbanSearchCest
{
    private $sessionId;
    private $search = 'limit=10&page=1';
    private $userId;
    
    public function _before(AcceptanceTester $I, Session $sessionHelper, User $userHelper)
    {
        $sessionHelper->createASession($I);
        $this->sessionId = $sessionHelper->getFetchSessions($I);
        $this->userId = $userHelper->createAProfessional($I);

        $sessionInformation['eventId'] = $this->sessionId;
        $sessionInformation['userId'][] = '590820c0753bd9005006c930';
        $sessionInformation['userId'][] = $this->userId;
        
        $sessionHelper->addParticipantsInASession($I, $sessionInformation);
        $sessionHelper->banAParticipant($I, $sessionInformation);
    }
    
    public function _after(AcceptanceTester $I, Session $sessionHelper, User $userHelper)
    {
        $sessionHelper->sessionDelete($I, $this->sessionId);
        $sessionHelper->banDelete($I, $this->sessionId);
        $userHelper->deleteUsers($I, [$this->userId]);
    }

    public function testSearchAllBannedUsers(AcceptanceTester $I)
    {
        $I->wantTo('See all users banned of a session');
        $I->sendGET('/backoffice/session/ban/list?' . $this->search);
        $I->canSeeResponseContains('590820c0753bd9005006c930');
    }

    public function testSearchABannedUserByFirstName(AcceptanceTester $I)
    {
        $I->setAdminToken();
        $I->wantTo('Search a banned user by first name');
        $I->sendGET('/backoffice/session/ban/list?' . $this->search . '&search=solid');
        $I->canSeeResponseContains($this->userId);
    }

    public function testSearchABannedUserByLastName(AcceptanceTester $I)
    {
        $I->setAdminToken();
        $I->wantTo('Search a banned user by last name');
        $I->sendGET('/backoffice/session/ban/list?' . $this->search . '&search=snake');
        $I->canSeeResponseContains($this->userId);
    }
}
