<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Codeception\Example;

class OrganizationCest
{

    private function provider()
    {
        return [
            [
                "status" => 200,
                "message" => "invalid email",
                "data" => [
                    'contactEmail' => '',
                ]
            ], 
            [
                "status" => 200,
                "message" => "invalid first name",
                "data" => [
                    'contactFirstName' => '',
                    'contactLastName' => '',
                    'contactEmail' => 'testing@acme.com',
                ]
            ],
            [
                "status" => 200,
                "message" => "invalid last name",
                "data" => [
                    'contactFirstName' => 'John Brain',
                    'contactLastName' => '',
                    'contactEmail' => 'testing@acme.com',
                ]
            ],
            [
                "status" => 200,
                "message" => "invalid title",
                "data" => [
                    'contactFirstName' => 'John Brain',
                    'contactLastName' => 'Mister',
                    'contactEmail' => 'testing@acme.com',
                    'contactTitle' => '',
                ]
            ],
            [
                "status" => 200,
                "message" => "invalid organization name",
                "data" => [
                    'contactFirstName' => 'John Brain',
                    'contactLastName' => 'Mister',
                    'contactEmail' => 'testing@acme.com',
                    'contactTitle' => 'Mr',
                    'organizationName' => '',
                    'phone' => ''
                ]
            ],
            [
                "status" => 200,
                "message" => "invalid number of schools",
                "data" => [
                    'contactFirstName' => 'John Brain',
                    'contactLastName' => 'Mister',
                    'contactEmail' => 'testing@acme.com',
                    'contactTitle' => 'Mr',
                    'organizationName' => 'NASA',
                    'numberOfSchools' => '',
                    'phone' => ''
                ]
            ],
        ];
    }

    /**
     * @dataprovider provider
     */
    public function shouldValidateTheOrganizationEndPoint(AcceptanceTester $I, Example $example)
    {
        $I->wantTo('Search for a organization');
        $I->sendPOST('/organization', $example['data']);
        $I->seeResponseIsJson();
        $I->seeResponseCodeIs($example['status']);
        $I->seeResponseContains('{"message":"' . $example['message'] . '"}');
    }
}
