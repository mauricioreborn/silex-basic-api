<?php

namespace App\Tests\Acceptance\BackOffice;

use AcceptanceTester;
use Codeception\Example;
use App\Entities\Organization;

class SearchCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->sendPOST('/organization', [
            'contactFirstName' => 'Maria',
            'contactLastName' => 'Silva',
            'contactEmail' => 'testing@acme.com',
            'contactTitle' => 'Mrs',
            'organizationName' => 'SPACE X',
            'numberOfSchools' => '2',
            'numberOfClassrooms' => '1',
            'phone' => '1234567891',
            'educationalStage' => 'middle_school',
            'duration' => '2',
            'involvement' => '2',
            'interestInvolvement' => '2',
            'interestedCommunity' => ['animation'],
            'foundOut' => 'Google',
        ]);

        $em = $I->getEntityManager();

        $repository = $em->getRepository(Organization::class);
        $organization = $repository->findOneBy(['email' => 'testing@acme.com']);
        $organization->setApproval('approved');
        $em->flush();
    }

    public function _after(AcceptanceTester $I)
    {
        $em = $I->getEntityManager();

        $repository = $em->getRepository(Organization::class);
        $organization = $repository->findOneBy(['email' => 'testing@acme.com']);

        $em->remove($organization);
        $em->flush();
    }
    
    /**
     * @example { "term": "P" }
     * @example { "term": "A" }
     * @example { "term": "C" }
     * @example { "term": "SPACE X" }
     * @example { "term": "S" }
     */
    public function shouldSearchAnOrganization(AcceptanceTester $I, \Codeception\Example $example)
    {
        $I->wantTo('search for an organization');
        $I->setAdminToken();
        $I->sendGET('/backoffice/list/organizations?search=' . $example['term'] . '&status=approved' );
        $I->seeResponseContains('testing@acme.com');
        $I->seeResponseContainsJson([[ 'totalResults' =>  1]]);
    }
}
