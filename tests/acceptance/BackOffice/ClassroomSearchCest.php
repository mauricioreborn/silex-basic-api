<?php

namespace App\Tests\Acceptance;

use AcceptanceTester;
use Helper\School;
use Helper\Organization;
use MongoId;
use App\Entities\ClassRoom;

class ClassroomSearchCest
{
    private $search = 'limit=10&page=1&status=approved&type=admin';
    private $schoolId;
    private $orgId;

    public function _before(AcceptanceTester $I, School $schoolHelper, Organization $organizationHelper)
    {
        $this->orgId = $organizationHelper->createAnOrganization($I);
        $this->schoolId = $schoolHelper->createASchool($I, $this->orgId['organizationId']);

        $I->setAdminToken();

        $I->sendPOST('/backoffice/organization/school/classroom/new', [
            'classroomName' => 'Gensokyo',
            'schoolId' => $this->schoolId['schoolId'],
            'educatorEmail' => 'proedu1@acme.com.br',
        ]);

        $I->sendPOST('/backoffice/organization/school/classroom/new', [
            'classroomName' => 'Eintein',
            'schoolId' => $this->schoolId['schoolId'],
            'educatorEmail' => 'proedu1@acme.com.br',
        ]);
    }

    public function _after(AcceptanceTester $I, School $schoolHelper, Organization $organizationHelper)
    {
        $organizationHelper->removeOrganization($I, $this->orgId);
        $schoolHelper->removeSchool($I, $this->schoolId);

        $entityManager = $I->getEntityManager();

        $classroomRepository = $entityManager->getRepository(ClassRoom::class);
        $classrooms = $classroomRepository->findBy(['schoolId' => new MongoId($this->schoolId['schoolId'])]);

        foreach ($classrooms as $classroom) {
            $entityManager->remove($classroom);
            $entityManager->flush();
        }
    }

    public function testShouldSearchAllClassroom(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search classrooms');
        $I->sendGET('/backoffice/list/classrooms?' . $this->search);
        $I->canSeeResponseContains('Eintein');
        $I->canSeeResponseContains('Gensokyo');
    }

    public function testShouldSearchClassroomWithLetterT(AcceptanceTester $I)
    {
        $I->setAdminToken();

        $I->wantTo('Search classrooms with letter T');
        $I->sendGET('/backoffice/list/classrooms?' . $this->search . '&search=t');
        $I->canSeeResponseContains('Eintein');
    }
}