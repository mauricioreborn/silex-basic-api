<?php

namespace App\Tests\Acceptance;

use Codeception\Util\HttpCode;
use App\Tests\Acceptance\SignUpCest as SignUp;
use App\Tests\Acceptance\UserCest as User;
use DDesrosiers\SilexAnnotations\Annotations\After;
use DDesrosiers\SilexAnnotations\Annotations\Before;

class LoginCest
{

    /**
     * @Before(SignUp::testShouldSignUp)
     *
     * @After(User::testShouldDeleteAnUser)
     */
    public function shouldGetJWTToken(\AcceptanceTester $I)
    {
        $I->wantTo('I want to login and get a JWT token');
        $I->sendPOST('/login', ['email' => 'api@acme.com.br', 'password' => '123456']);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContains('token');
    }
}
