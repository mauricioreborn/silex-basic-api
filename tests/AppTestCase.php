<?php

use Silex\WebTestCase;

class AppTestCase extends WebTestCase
{
    /**
     * Creates the application.
     *
     * @return HttpKernelInterface
     */
    
    private $entityManager;

    public function createApplication()
    {
        return require __DIR__ . '/../resources/config/bootstrap.php';
    }
}
