<?php
/**
 * Class to test flags service
 *
 * PHP version 7
 *
 * @category Test
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace Unit\App\Services;
use App\Entities\Flag;
use App\Repository\Setting\FlagRepository;
use App\Services\FlagService;
use MongoId;
use Doctrine\ORM\EntityManager;
use Silex\Application;

/**
 * Class FlagTest
 *
 * PHP version 7
 *
 * @category Test
 * @package  Acme
 * @author   Mauricio Rodrigues <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
class FlagTest extends \Codeception\Test\Unit
{
    protected $flagService;

    protected $entityManager;

    /**
     * Method to create a mock of entityManager and flagService
     *
     * @return null
     */
    protected function _before() // @codingStandardsIgnoreLine
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->flagService = new FlagService();
    }

    /**
     * Method to check if flagBuild method really return a flag object
     *
     * @param array $flagContent The content received from flagArray Method
     *
     * @dataProvider flagArray An provider to send flagContent automatically
     *
     * @return null
     */
    public function testFlagBuildMustReturnAFlagObject(array $flagContent)
    {
        $flagRepository = new FlagRepository('a');
        $flag = new Flag();
        $flag->setStatus(true);
        $flag->setUserId(new MongoId($flagContent['userId']));
        $flag->setProductId($flagContent['productId']);
        $flag->setType($flagContent['type']);
        $flag->setCreatedAt();
        $realFlag = $flagRepository->flagBuild($flagContent);
        $this->assertEquals($flag, $realFlag);
    }

    /**
     * Method to return an array with information to create a flag
     *
     * @return array With content to create a flag
     */
    public function flagArray()
    {
        return [
            [
                [
                    'productId' => '5a78928bf70bdb02a9186022',
                    'userId' => '590820c0753bd9005006c938',
                    'type' => 'work'
                ],
                'response' => true
            ],

        ];
    }
}