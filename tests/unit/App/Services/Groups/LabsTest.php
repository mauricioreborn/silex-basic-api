<?php

namespace Unit\App\Services\Groups;

use App\Services\Groups\LabService;
use App\Repository\Group\LabRepository;
use App\Entities\Lab;
use Faker\Factory;

class LabsTest extends \AppTestCase
{
    public $paginate;
    public $mockLab;
    public $labs;

    /**
     * This method will setUp LabsTest properties
     */
    public function setUp()
    {
        $this->mockLab = $this->createMock(LabRepository::class);
        $faker = Factory::create();

        $this->labs = [
            [
                'title' => '123456',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-01-30 18:50:36.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'alou',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' =>'2018-01-31 20:45:37.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'Netherworld',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-02-01 18:56:46.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'Dengei Sentai Changeman',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '',
                'status' => 'approved'
            ]
        ];

        $this->mockLab->method('getLabAndClassAdmin')
            ->willReturn(
                [
                    'lab' => [
                        [
                            'id' => 'Lab identifier'
                        ],
                        [
                            'id' => 'Lab identifier2'
                        ],
                        [
                            'id' => 'Lab identifier3'
                        ],
                        [
                            'id' => 'Lab identifier4'
                        ],
                    ]
                ]
            );
        $this->mockLab->method('findLab')
            ->willReturn(new Lab());
        $this->mockLab->method('buildLabOrClass')
            ->will(
                $this->onConsecutiveCalls(
                    $this->labs[0],
                    $this->labs[1],
                    $this->labs[2],
                    $this->labs[3]
                )
            );

        $this->paginate['page'] = 1;
        $this->paginate['limit'] = 4;
        $this->paginate['skip'] = 0;
        $this->paginate['userId'] = '123456789123456789123456';
    }

    /**
     * This method test if the Labs are sorted alphabetically ascending
     */
    public function testShouldGetLabAsAdminSortedAlphabeticallyASC()
    {
        $labService = new LabService();
        $this->paginate['sort'] = 'asc';
        $labService->getLabAdmin($this->paginate, $this->mockLab);

        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->labs as $title) {
            $sortByTitle[] = $title['title'];
        }

        natcasesort($sortByTitle);

        foreach ($sortByTitle as $title) {
            foreach ($this->labs as $lab) {
                if ($lab['title'] == $title) {
                    $sortedAlphabetical[] = $lab;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $labService->getMessage());
    }

    /**
     * This method test if the Labs are sorted alphabetically descending
     */
    public function testShouldGetLabAsAdminSortedAlphabeticallyDESC()
    {
        $labService = new LabService();
        $this->paginate['sort'] = 'desc';
        $labService->getLabAdmin($this->paginate, $this->mockLab);
        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->labs as $title) {
            $sortByTitle[] = $title['title'];
        }

        rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);

        foreach ($sortByTitle as $title) {
            foreach ($this->labs as $lab) {
                if ($lab['title'] == $title) {
                    $sortedAlphabetical[] = $lab;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $labService->getMessage());
    }

    /**
     * This method test if the Labs are returned in creation order
     */
    public function testShouldGetLabAsAdminSortByCreation()
    {
        $labService = new LabService();
        $this->paginate['sort'] = null;
        $labService->getLabAdmin($this->paginate, $this->mockLab);

        $this->assertEquals($this->labs, $labService->getMessage());
    }

    /**
     * This method will set all properties as null
     */
    public function tearDown()
    {
        $this->paginate = null;
        $this->mockLab = null;
        $this->labs = null;
    }
}
