<?php

namespace Unit\App\Services\Groups;

use App\Services\Groups\ClassRoomService;
use App\Repository\Group\ClassRoomRepository;
use App\Entities\ClassRoom;
use Faker\Factory;

class ClassRoomTest extends \AppTestCase
{
    public $paginate;
    public $mockClassRoom;
    public $classrooms;

    /**
     * This method will setUp ClassRoomTest properties
     */
    public function setUp()
    {
        $this->mockClassRoom = $this->createMock(ClassRoomRepository::class);
        $faker = Factory::create();

        $this->classrooms = [
            [
                'title' => '123456',
                'id' => 'Classroom identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-01-30 18:50:36.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'alou',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'Classroom owner',
                'createAt' =>'2018-01-31 20:45:37.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'Netherworld',
                'id' => 'Classroom identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-02-01 18:56:46.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'Dengei Sentai Changeman',
                'id' => 'Classroom identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '',
                'status' => 'approved'
            ]
        ];

        $this->mockClassRoom->method('getLabAndClassAdmin')
            ->willReturn(
                [
                    'classRoom' => [
                        [
                            'id' => 'Classroom identifier'
                        ],
                        [
                            'id' => 'Classroom identifier2'
                        ],
                        [
                            'id' => 'Classroom identifier3'
                        ],
                        [
                            'id' => 'Classroom identifier4'
                        ],
                    ]
                ]
            );
        $this->mockClassRoom->method('findClassRoom')
            ->willReturn(new ClassRoom());
        $this->mockClassRoom->method('buildLabOrClass')
            ->will(
                $this->onConsecutiveCalls(
                    $this->classrooms[0],
                    $this->classrooms[1],
                    $this->classrooms[2],
                    $this->classrooms[3]
                )
            );

        $this->paginate['page'] = 1;
        $this->paginate['limit'] = 4;
        $this->paginate['skip'] = 0;
        $this->paginate['userId'] = '123456789123456789123456';
    }

    /**
     * This method test if the classrooms are sorted alphabetically ascending
     */
    public function testShouldGetClassAsAdminSortedAlphabeticallyASC()
    {
        $classService = new ClassRoomService();
        $this->paginate['sort'] = 'asc';
        $classService->getClassAdmin($this->paginate, $this->mockClassRoom);

        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->classrooms as $title) {
            $sortByTitle[] = $title['title'];
        }

        natcasesort($sortByTitle);

        foreach ($sortByTitle as $title) {
            foreach ($this->classrooms as $classroom) {
                if ($classroom['title'] == $title) {
                    $sortedAlphabetical[] = $classroom;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $classService->getMessage());
    }

    /**
     * This method test if the classrooms are sorted alphabetically descending
     */
    public function testShouldGetClassAsAdminSortedAlphabeticallyDESC()
    {
        $classService = new ClassRoomService();
        $this->paginate['sort'] = 'desc';
        $classService->getClassAdmin($this->paginate, $this->mockClassRoom);
        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->classrooms as $title) {
            $sortByTitle[] = $title['title'];
        }

        rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);

        foreach ($sortByTitle as $title) {
            foreach ($this->classrooms as $classroom) {
                if ($classroom['title'] == $title) {
                    $sortedAlphabetical[] = $classroom;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $classService->getMessage());
    }

    /**
     * This method test if the classrooms are returned in creation order
     */
    public function testShouldGetClassAsAdminSortByCreation()
    {
        $classService = new ClassRoomService();
        $this->paginate['sort'] = null;
        $classService->getClassAdmin($this->paginate, $this->mockClassRoom);

        $this->assertEquals($this->classrooms, $classService->getMessage());
    }

    /**
     * This method will set all properties as null
     */
    public function tearDown()
    {
        $this->paginate = null;
        $this->mockClassRoom = null;
        $this->classrooms = null;
    }
}
