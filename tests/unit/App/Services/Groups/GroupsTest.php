<?php

namespace Unit\App\Services\Groups;

use App\Services\Groups\GroupService;
use App\Repository\Group\GroupRepository;
use App\Entities\Lab;
use App\Entities\ClassRoom;
use Faker\Factory;

class GroupsTest extends \AppTestCase
{
    public $paginate;
    public $mockGroup;
    public $groups;

    /**
     * This method will setUp GroupTest properties
     */
    public function setUp()
    {
        $this->mockGroup = $this->createMock(GroupRepository::class);
        $faker = Factory::create();

        $this->groups = [
            [
                'title' => '123456',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-01-30 18:50:36.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'alou',
                'id' => 'Lab identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' =>'2018-01-31 20:45:37.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'Gensokyo',
                'id' => 'Classroom identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '2018-02-01 18:56:46.000000',
                'status' => 'approved'
            ],
            [
                'title' => 'eintein',
                'id' => 'Classroom identifier',
                'description' => $faker->text,
                'coverImage' => $faker->url,
                'user' => $faker->rgbColorAsArray,
                'owner' => 'User owner',
                'createAt' => '',
                'status' => 'approved'
            ]
        ];

        $this->mockGroup->method('getLabAndClassAdmin')
            ->willReturn(
                [
                    'lab' => [
                        [
                            'id' => 'Lab identifier'
                        ],
                        [
                            'id' => 'Lab identifier2'
                        ],
                    ],
                    'classRoom' => [
                        [
                            'id' => 'Classroom identifier'
                        ],
                        [
                            'id' => 'Classroom identifier2'
                        ],
                    ]
                ]
            );
        $this->mockGroup->method('findLab')
            ->willReturn(new Lab());
        $this->mockGroup->method('buildLabOrClass')
            ->will(
                $this->onConsecutiveCalls(
                    $this->groups[0],
                    $this->groups[1],
                    $this->groups[2],
                    $this->groups[3]
                )
            );
        $this->mockGroup->method('findClassRoom')
            ->willReturn(new ClassRoom());

        $this->paginate['page'] = 1;
        $this->paginate['limit'] = 4;
        $this->paginate['skip'] = 0;
    }

    /**
     * This method test if the Groups are sorted alphabetically ascending
     */
    public function testShouldGetLabAndClassAsAdminSortedAlphabeticallyASC()
    {
        $groupService = new GroupService();
        $this->paginate['sort'] = 'asc';
        $groupService->getLabAndClassAdmin($this->paginate, $this->mockGroup);

        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->groups as $title) {
            $sortByTitle[] = $title['title'];
        }

        natcasesort($sortByTitle);

        foreach ($sortByTitle as $title) {
            foreach ($this->groups as $group) {
                if ($group['title'] == $title) {
                    $sortedAlphabetical[] = $group;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $groupService->getMessage());
    }

    /**
     * This method test if the Groups are sorted alphabetically descending
     */
    public function testShouldGetLabAndClassAsAdminSortedAlphabeticallyDESC()
    {
        $groupService = new GroupService();
        $this->paginate['sort'] = 'desc';
        $groupService->getLabAndClassAdmin($this->paginate, $this->mockGroup);
        $sortedAlphabetical = [];
        $sortByTitle = [];

        foreach ($this->groups as $title) {
            $sortByTitle[] = $title['title'];
        }

        rsort($sortByTitle, SORT_NATURAL | SORT_FLAG_CASE);

        foreach ($sortByTitle as $title) {
            foreach ($this->groups as $group) {
                if ($group['title'] == $title) {
                    $sortedAlphabetical[] = $group;
                }
            }
        }

        $this->assertEquals($sortedAlphabetical, $groupService->getMessage());
    }

    /**
     * This method test if the Groups are returned in creation order
     */
    public function testShouldGetLabAndClassAsAdminSortByCreation()
    {
        $groupService = new GroupService();
        $this->paginate['sort'] = null;
        $groupService->getLabAndClassAdmin($this->paginate, $this->mockGroup);

        $this->assertEquals($this->groups, $groupService->getMessage());
    }

    /**
     * This method will set all properties as null
     */
    public function tearDown()
    {
        $this->paginate = null;
        $this->mockGroup = null;
        $this->groups = null;
    }
}
