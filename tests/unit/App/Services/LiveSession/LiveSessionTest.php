<?php

namespace Unit\App\Services\LiveSession;

use AppTestCase;
use App\Services\LiveSession\LiveSessionService;
use App\Repository\LiveSessionRepository;

class LiveSessionTest extends AppTestCase
{
    public $mockSession;
    public $sessionInfo;

    public function setUp()
    {
        $this->mockSession = $this->createMock(LiveSessionRepository::class);
        $this->sessionInfo = [
            'sessionId' => 'Session database unique identifier',
            'userId' => 'User database unique identifier',
            'level' => 'User level',
            'cover' => 'cover url'
        ];
    }

    public function testShouldChangeCover()
    {
        $this->mockSession->method('uploadCover')
            ->willReturn(true);

        $sessionService = new LiveSessionService();
        $sessionService->uploadCover($this->sessionInfo, $this->mockSession);

        $this->assertEquals('Cover updated!', $sessionService->getMessage());
    }

    public function testShouldNotChangeCover()
    {
        $this->mockSession->method('uploadCover')
            ->willReturn(false);
        $sessionService = new LiveSessionService();
        $sessionService->uploadCover($this->sessionInfo, $this->mockSession);

        $this->assertEquals(
            'Cover was not updated',
            $sessionService->getMessage()
        );
    }


    public function tearDown()
    {
        $this->mockSession = null;
    }
}