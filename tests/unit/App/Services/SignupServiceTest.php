<?php

namespace Unit\App\Services;

use App\Entities\ClassRoom;
use App\Entities\User;
use App\Repository\ClassRoomRepository;
use App\Repository\UserRepository;
use App\Services\SignupService;

class SignupServiceTest extends \AppTestCase
{
    public $paginate;
    public $mockClassroom;
    public $mockUser;
    public $classroom;
    public $user;


    public function setUp()
    {
        $this->mockClassroom = $this->createMock(ClassRoomRepository::class);

        $this->classroom[1] = new ClassRoom();
        $this->classroom[1]->setId('Classroom Identifier');
        $this->classroom[1]->setTitle('Old Hunters');
        $this->classroom[1]->setDescription('pending');
        $this->classroom[1]->setTotalMembers(3);
        $this->classroom[1]->setCoverImage('old.png');
        $this->classroom[1]->setEmailEducator('Old@mailinator.com');

        $this->classroom[2] = new ClassRoom();
        $this->classroom[2]->setId('Classroom Identifier');
        $this->classroom[2]->setTitle('Hunters');
        $this->classroom[2]->setDescription('pending');
        $this->classroom[2]->setTotalMembers(3);
        $this->classroom[2]->setCoverImage('hunter.png');
        $this->classroom[2]->setEmailEducator('hunter@mailinator.com');

        $this->user[1] = new User();
        $this->user[1]->setId('User Identifier');
        $this->user[1]->setFirstName('German');
        $this->user[1]->setLastName('Old');
        $this->user[1]->setEmail('Old@mailinator.com');
        $this->user[1]->setUserPhoto('german.jpg');

        $this->user[2] = new User();
        $this->user[2]->setId('User Identifier');
        $this->user[2]->setFirstName('Lady');
        $this->user[2]->setLastName('Maria');
        $this->user[2]->setEmail('hunter@mailinator.com');
        $this->user[2]->setUserPhoto('ladymary.jpg');


        $this->mockClassroom->method('getClassrooms')
            ->willReturn(
                [
                    $this->classroom[1],
                    $this->classroom[2]
                ]
            );

        $this->mockUser = $this->createMock(UserRepository::class);
        $this->mockUser->method('getUserByEmail')
            ->will(
                $this->onConsecutiveCalls(
                    $this->user[1],
                    $this->user[2]
                )
            );

        $this->paginate['limit'] = 4;
        $this->paginate['skip'] = 0;
    }

    public function testShouldGetAllClassrooms()
    {
        $SignupService = new SignupService($this->app);
        $classrooms = $SignupService->classroomsInformation(
            $this->paginate,
            $this->mockClassroom,
            $this->mockUser
        );

        $this->assertEquals(2, count($classrooms));
    }

    public function testShouldCheckClassroomData()
    {
        $SignupService = new SignupService($this->app);
        $classrooms = $SignupService->classroomsInformation(
            $this->paginate,
            $this->mockClassroom,
            $this->mockUser
        );

        $classObject = 1;

        foreach ($classrooms as $classroom) {
            $this->assertEquals(
                $this->classroom[$classObject]->getId(),
                $classroom['classroom']['id']
            );
            $this->assertEquals(
                $this->classroom[$classObject]->getTitle(),
                $classroom['classroom']['title']
            );
            $this->assertEquals(
                $this->classroom[$classObject]->getDescription(),
                $classroom['classroom']['description']
            );
            $this->assertEquals(
                $this->classroom[$classObject]->getTotalMembers(),
                $classroom['classroom']['totalMembers']
            );
            $this->assertEquals(
                $this->classroom[$classObject]->getCoverImage(),
                $classroom['classroom']['cover']
            );

            $classObject ++;
        }
    }

    public function testShouldCheckTeacherData()
    {
        $SignupService = new SignupService($this->app);
        $classrooms = $SignupService->classroomsInformation(
            $this->paginate,
            $this->mockClassroom,
            $this->mockUser
        );

        $userObject = 1;

        foreach ($classrooms as $classroom) {
            $this->assertEquals(
                $this->user[$userObject]->getId(),
                $classroom['teacher']['id']
            );
            $this->assertEquals(
                $this->user[$userObject]->getFirstName(),
                $classroom['teacher']['firstName']
            );
            $this->assertEquals(
                $this->user[$userObject]->getLastName(),
                $classroom['teacher']['lastName']
            );
            $this->assertEquals(
                $this->user[$userObject]->getEmail(),
                $classroom['teacher']['email']
            );
            $this->assertEquals(
                $this->user[$userObject]->getUserPhoto(),
                $classroom['teacher']['profileImage']
            );

            $userObject ++;
        }
    }


    public function tearDown()
    {
        $this->paginate = null;
        $this->mockClassroom = null;
        $this->mockUser = null;
        $this->classroom = null;
        $this->user = null;
    }
}