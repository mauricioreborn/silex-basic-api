<?php

namespace Unit\App\Controllers;

use App\Validators\LoginValidator;
use Silex\Application;
use App\Services\Auth;

class SignInTest extends \AppTestCase
{

    /**
     * @dataProvider emailsProvider
     */
    public function testShouldBeReceivedAValidEmail($email, $result)
    {
        $login = new LoginValidator($this->app);

        $this->assertEquals($result, $login->validEmail($email));
    }

    public function emailsProvider()
    {
        return [
            ['mauricio.reborn@hotmail.com', true],
            ['asdasdasdasdasd', false],
            ['asdasdasdasd@asdasdasda.com', true],
            ['', false],
        ];
    }
    
    public function passwordsProvider()
    {
        return [
            ['Hadouken', true],
            ['1234', false],
            ['aaaa', false],
            ['', false],
            ['12345',true],
            ['123456dffd',true]
        ];
    }

    /**
     * @dataProvider passwordsProvider
     */
    public function testShouldBeReceivedAValidPassword($password, $result)
    {
        $login = new LoginValidator($this->app);
        $this->assertEquals($result, $login->validPassword($password));
    }
}
