<?php

namespace Unit\App\Validators\Work;

use App\Validators\Work\CommentValidator;

class CommentTest extends \AppTestCase
{

    /**
     * Test to verify if validator of work id is working
     *
     * @dataProvider getWorkId
     */
    public function testShouldValidateIfIsAValidWorkId($workId, $expected)
    {
        $this->markTestIncomplete('incomplete test');

        $repository = $this->createMock(\Doctrine\ODM\MongoDB\DocumentRepository::class);
        $repository->method('findOneBy')
            ->will($this->returnValue($workId));

        $commentValidator = new CommentValidator();
        $response = $commentValidator->validateWorkId($workId, $repository);

        $this->assertEquals($expected, $response);
    }


    public function getWorkId()
    {
        return [
            [
                '', false],
            [
                '322343', true
            ],
            [
               'blabla', true
            ],
        ];
    }
}