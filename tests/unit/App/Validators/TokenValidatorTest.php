<?php
/**
 * Token Validator Test Doc Comment
 *
 * PHP version 7
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
namespace Unit\App\Validators;

use App\Services\Auth;
use App\Validators\Token;

/**
 * Token Validator Test Doc Comment
 *
 * PHP version 7
 *
 * @category Validator
 * @package  Acme
 * @author   Display Name <mrodrigues@2mundos.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acme.com/
 */
class TokenValidatorTest extends \AppTestCase
{

    /**
     * Test should return a valid instance of token generator
     *
     * @return void
     */
    public function testShouldBeReturnedAValidInstanceOfTokenGenerator()
    {
        $tokenBuild = $this->getTokenBuilder();
        $tokenGenerator = new Auth\TokenGenerator($tokenBuild);
        $this->assertInstanceOf(Auth\TokenGenerator::class, $tokenGenerator);
    }

    /**
     * Test should return a valid token
     *
     * @return void
     */
    public function testShouldBeReturnedAValidAccessToken()
    {
        $tokenBuild = $this->getTokenBuilder();
        $tokenGenerator = new Auth\TokenGenerator($tokenBuild);
        $token = $tokenGenerator->getToken();
        $validToken = $tokenGenerator->readToken($token);
        $tokenValidator = new Token($validToken);
        $this->assertTrue($tokenValidator->validateToken());
    }

    /**
     * Test should return a valid instance of token generator
     *
     * @param string  $time   time
     * @param boolean $result type
     *
     * @dataProvider getTimesToExpireToken
     *
     * @return void
     */
    public function testShouldVerifyIfAnAccessTokenHasBeenExpired($time, $result)
    {
        $tokenBuild = $this->getTokenBuilder();
        $tokenGenerator = new Auth\TokenGenerator($tokenBuild);
        $tokenGenerator->setExpiration($time);
        $token = $tokenGenerator->getToken();
        $validToken = $tokenGenerator->readToken($token);
        $tokenValidator = new Token($validToken);

        $this->assertEquals($result, $tokenValidator->tokenHasExpired());
    }


    /**
     * Return time to expiration test
     *
     * @return array
     */
    public function getTimesToExpireToken()
    {
        return [
            [3600, true],
            [-100, false],
            [-400, false],
            [1, true],
            [7200, true]
        ];
    }


    /**
     * Return tokenBuilder instance
     *
     * @return object
     */
    public function getTokenBuilder()
    {
        return new Auth\TokenBuilder();
    }
}
