<?php

namespace Unit\App\Validators;

use App\Validators\SignUpValidator;

class SignUpValidatorTest extends \AppTestCase
{
    /**
     * @dataProvider getClassQueryProvider
     */
    public function testShouldValidateGetClassQueryMethod($query, $response)
    {
        $signUp = new SignUpValidator();
        $this->assertEquals(
            $response,
            $signUp->validateGetClassQuery($query, $this->app)
        );
    }
    
    public function getClassQueryProvider()
    {
        return [
            [
                [
                    'limit' => 1,
                    'skip' => 1
                ],
                'response' => true
            ],
            [
                [
                    'limit' => null,
                    'skip' => 1
                ],
                'response' => false
            ],
            [
                [
                    'limit' => 1,
                    'skip' => null
                ],
                'response' => false
            ]
        ];
    }
}