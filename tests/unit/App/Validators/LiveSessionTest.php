<?php

namespace Unit\App\Validators;

use App\Validators\SessionValidator;
use Symfony\Component\HttpFoundation\File\File;

class LiveSessionTest extends \AppTestCase
{
    /**
     * @dataProvider sessionInput
     */
    public function testShouldValidateSessionInput($data, $status)
    {
        $liveValidator = new SessionValidator();
        $result = $liveValidator->requiredFields($this->createApplication(), $data);
        
        $this->assertEquals($status, $result);
    }

    public function sessionInput()
    {
        return [
            [
                'data' => [
                    'title' => 'test validator',
                    'type'  => 'public',
                    'description' => 'bla bla bla xyz bla bla',
                    'date' => '10/10/2017',
                    'startTime' => '17:20',
                    'endTime' => '17:55',
                    'thumbnail' => 'http://path-to-my-image.com',
                    'speaker' =>  'name of the speaker'
                ],
                'status' => true
            ]
        ];
    }
}
