<?php

namespace Unit\App\Controllers;

use App\Validators\OrganizationValidator;

class OrganizationValidatorTest extends \AppTestCase
{

    /**
     * @dataProvider firstNameProvider
     */
    public function testShouldReceiveValidContactFirstName($firstName, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationContactFirstName($firstName));
    }


    public function firstNameProvider()
    {
        return [
            ['William Tahira Rabaldeli dos Santos', true],
            ['', false],
            ['William Tahira Rabaldeli dos Santos Souza Silva Sauro Teste de oitenta chars no maximo', false],
        ];
    }

    /**
     * @dataProvider lastNameProvider
     */
    public function testShouldReceiveValidContactLastName($lastName, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationContactLastName($lastName));
    }



    public function lastNameProvider()
    {
        return [
            ['William Tahira Rabaldeli dos Santos', true],
            ['', false],
            ['William Tahira Rabaldeli dos Santos Souza Silva Sauro Teste de oitenta chars no maximo', false],
        ];
    }

    /**
     * @dataProvider titleNameProvider
     */
    public function testShouldReceiveValidContactTitle($title, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationContactTitle($title));
    }



    public function titleNameProvider()
    {
        return [
            ['Manager', true],
            ['', false],
            ['Director of financials human resources', false],
        ];
    }

    /**
     * @dataProvider organizationNameProvider
     */
    public function testShouldReceiveValidOrganizationName($firstName, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationContactFirstName($firstName));
    }


    public function organizationNameProvider()
    {
        return [
            ['William Tahira Rabaldeli dos Santos', true],
            ['', false],
            ['William Tahira Rabaldeli dos Santos Souza Silva Sauro Teste de oitenta chars no maximo', false],
        ];
    }

    /**
     * @dataProvider organizationSchoolNumberProvider
     */
    public function testShouldReceiveValidOrganizationSchoolNumber($number, $result)
    {
        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationSchoolNumber($number));
    }


    public function organizationSchoolNumberProvider()
    {
        return [
            [9999999999, true],
            [10, true],
            ['', false],

        ];
    }

    /**
     * @dataProvider organizationClassroomNumberProvider
     */
    public function testShouldReceiveValidOrganizationClassroomSchoolNumber($number, $result)
    {
        $number = (integer)$number;

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationSchoolClassroomNumber($number));
    }


    public function organizationClassroomNumberProvider()
    {
        return [
            [9999999999, true],
            [10, true],
            ['', false],
        ];
    }

    /**
     * @dataProvider organizationEducationalStageProvider
     */
    public function testShouldReceiveValidOrganizationEducationalStage($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationEducationalStage($choice));
    }


    public function organizationEducationalStageProvider()
    {
        return [
            ['k12district', true],
            ['middle_school', true],
            ['high_school', true],
            ['college', true],
            ['999', false],
            ['', false]
        ];
    }

    /**
     * @dataProvider organizationDurationProvider
     */
    public function testShouldReceiveValidOrganizationDuration($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationDuration($choice));
    }


    public function organizationDurationProvider()
    {
        return [
            [2, true],
            [4, true],
            ['', false],
            ['999', false]
        ];
    }

    /**
     * @dataProvider organizationInvolvimentProvider
     */
    public function testShouldReceiveValidOrganizationInvolviment($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationInvolved($choice));
    }


    public function organizationInvolvimentProvider()
    {
        return [
            [true, true],

            ['', false],

        ];
    }

    /**
     * @dataProvider organizationInterestedProvider
     */
    public function testShouldReceivevalidOrganizationInterestInvolvement($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationInterestInvolvement($choice));
    }


    public function organizationInterestedProvider()
    {
        return [
            [true, true],
            ['', false],

        ];
    }

    /**
     * @dataProvider organizationInterestedCommunityProvider
     */
    public function testShouldReceiveValidOrganizationInterestedCommunity($community, $result)
    {
        $repository = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repository->expects($this->once())
            ->method('findBy')
            ->will($this->returnValue(array($community)));

        $organization = new OrganizationValidator($this->app);
        $this->assertEquals($result, $organization->validOrganizationIntestedCommunity($community, $repository));
    }


    public function organizationInterestedCommunityProvider()
    {
        return [
            ['animation', true],
            ['engineering', true],
            ['law', true]
        ];
    }

    /**
     * @dataProvider organizationFoundAboutProvider
     */
    public function testShouldReceiveValidOrganizationFoundAbout($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationFoundAbout($choice));
    }


    public function organizationFoundAboutProvider()
    {
        return [
            ['wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawdawdwadawawdawdawddawdawdwadawdawaaw', true],
            ['wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawawaawwdawaawdawdawdwadawawdawdawddawdawdwadaw
            dawaawww', false],
            ['law', true],
            ['', false]
        ];
    }

    /**
     * @dataProvider organizationCommentsProvider
     */
    public function testShouldReceiveValidOrganizationComments($choice, $result)
    {

        $organization = new OrganizationValidator($this->app);

        $this->assertEquals($result, $organization->validOrganizationComments($choice));
    }


    public function organizationCommentsProvider()
    {
        return [
            ['wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawdawdwadawawdawdawddawdawdwadawdawaaw', true],
            ['wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawawaawwdawaawdawdawdwadawawdawdawddawdawdwadawdawaawww
            wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawawaawwdawaawdawdawdwadawawdawdawddawdawdwadawdawaawww
            wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawawaawwdawaawdawdawdwadawawdawdawddawdawdwadawdawaawww
            wewdawdawdawdadawdawwdawdwadawdarefesqsadadawdawawaawwdawaawdawdawdwadawawdawdawddawdawdwadawdawaawww
            ', false],
            ['law', true],

        ];
    }
}
