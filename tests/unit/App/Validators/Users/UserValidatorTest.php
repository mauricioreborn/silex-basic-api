<?php

namespace Unit\App\Validators\Users;

use  App\Validators\UserValidator;

class UserValidatorTest extends \AppTestCase
{

    /**
     * @dataProvider getUserId
     */
    public function testValidateUserId($id, $status)
    {
        $this->markTestIncomplete();
        $userValidator = new UserValidator();
        $this->assertEquals($userValidator->validateMongoId($id),$status);
    }

    public function getUserId()
    {
        return [
            [
               'id' => new \MongoId(),
                'status' => true,
            ],
            [
                'id' =>'',
                'status' => false,
            ],
            [
                'id' => 232323,
                'status' => false,
            ],
            [
                'id' => "58dd246096fbde008624d233",
                'status' => true,
            ],
        ];
    }
}