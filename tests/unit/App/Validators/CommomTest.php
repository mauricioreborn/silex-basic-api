<?php

namespace Unit\App\Validators;

use App\Validators\Commom;
use Symfony\Component\HttpFoundation\File\File;
use Silex\Application;
use App\Services\Auth;
use Doctrine\ORM\EntityRepository;

class CommomTest extends \AppTestCase
{

    /**
     * @dataProvider profileProvider
     */
    public function testShouldReceiveValidProfile($profile, $result)
    {
        $repository = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repository->expects($this->once())
            ->method('findBy')
            ->will($this->returnValue(array($profile)));

        $commom = new Commom($this->app);
        $this->assertEquals($result, $commom->validProfile($profile, $repository));
    }

    public function profileProvider()
    {
        return [
            ['student',true],
            ['Educator',true],
            ['Professional',true],
            ['Recruiter',true]
        ];
    }

    /**
     * @dataProvider communityProvider
     */
    public function testShouldReceiveValidCommunity($community, $result)
    {

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('findBy')
            ->will($this->returnValue(array($community)));

        $commom = new Commom($this->createApplication());

        $this->assertEquals($result, $commom->validCommunity($community, $repository));
    }


    public function communityProvider()
    {
        return [
            ['Animation',true],
            ['Engineering',true],
            ['Law',true]
        ];
    }

     /**
     * @dataProvider photosProvider
     */
    public function testShouldReceivedAValidPhoto($photo, $result)
    {
        $filePath = __DIR__ . '/../../../Files/images/' . $photo;

        $image = new File(
            $filePath,
            $photo
        );

        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validPhoto($image));
    }

    public function photosProvider()
    {
        return [
            ['teste1.png', true],
            ['teste2.jpg', true],
            ['teste3.jpeg', true],
            ['teste4.txt', false],
            ['teste5.jpg', false]
        ];
    }
    /**
     * @dataProvider nameProvider
     */
    public function testShouldReceiveValidName($name, $result)
    {


        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validNameLength($name));
    }

    /**
     * @dataProvider nameProvider
     */
    public function testShouldReceiveValidLastName($lastName, $result)
    {


        $commom = new Commom($this->createApplication());

        $this->assertEquals($result, $commom->validNameLength($lastName));
    }


    public function nameProvider()
    {
        return [
            ['William Tahira Rabaldeli dos Santos', true],
            ['', false],
            ['William Tahira Rabaldeli dos Santos Souza Silva Sauro', false],
        ];
    }

    /**
     * @dataProvider genderProvider
     */
    public function testShouldReceiveValidGender($gender, $result)
    {


        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validGender($gender));
    }

    public static function getGenders()
    {
        return array('M', 'F');
    }

    public function genderProvider()
    {
        return [
            ['M', true],
            ['F', true],
            ['Maybe', false],
        ];
    }


    /**
     * @dataProvider dateProvider
     */
    public function testShouldReceiveValidBirthday($birthDay, $result)
    {

        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validBirthdayformat($birthDay));
    }

    public function dateProvider()
    {
        return [
            ['12/28/1985', true],
            ['10/25/1986', true],
            ['13/10/2020', false],
            ['31/31/1985', false],
            ['02/30/1985', false],
            ['02/28/1985', true],
            ['', false]
        ];
    }


    /**
     * @dataProvider phoneProvider
     */
    public function testShouldReceiveValidPhoneNumber($phone, $result)
    {

        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validPhoneNumber($phone));
    }

    public function phoneProvider()
    {
        return [
            [1231232131, true],
            ['1231232131', false],
            ['(123) 123-1234', false],
        ];
    }

    /**
     * @dataProvider phonesProvider
     */
    public function testShouldReceiveValidPhoneNumberQtyChars($phone, $result)
    {

        $commom = new Commom($this->app);

        $this->assertEquals($result, $commom->validPhoneChars($phone));
    }

    public function phonesProvider()
    {
        return [
            [1234567892, true],
            [21231232131, false],
            ['(123) 123-1234', false],
        ];
    }

    /**
     * @dataProvider agreementsProvider
     */
    public function testShouldReceiveValidAgreement($agreement, $result)
    {
//        $commom = new Commom($this->app);
//
//        $this->assertEquals($result, $commom->validateAgreement($agreement));
    }

    public function agreementsProvider()
    {
        return [
            [true, true],

        ];
    }
}
