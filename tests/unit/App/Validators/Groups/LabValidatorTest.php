<?php

namespace Unit\App\Validators\Groups;

use App\Validators\Group\LabValidator;
use AppTestCase;
use Faker\Factory;

class LabValidatorTest extends AppTestCase
{

    /**
     * @dataProvider getLabFaker
     */
    public function testShouldValidateAddLabInputs($lab, $response)
    {
        $labValidator = new LabValidator();
        $this->assertEquals($response, $labValidator->validateLabInput(
            $lab,
            $this->app
        ));
    }

    /**
     * @dataProvider getTaskFaker
     */
    public function testShouldValidateCreateTask($task, $response)
    {
        $labValidator = new LabValidator();
        $this->assertEquals($response, $labValidator->validateTask(
            $task,
            $this->app
        ));
    }

    /**
     * @dataProvider getTurninFaker
     */
    public function testShouldValidateTurnin($turnin, $response)
    {
        $labValidator = new LabValidator();
        $this->assertEquals($response, $labValidator->validateTurnin(
            $turnin,
            $this->app
        ));
    }
    
    public function getLabFaker()
    {
        $faker = Factory::create();
        
        return [
            [
                [
                    'title' => $faker->text,
                    'description' => $faker->text,
                    'userId' => '5a67435764061400b801fae3',
                    'educatorEmail' => $faker->email,
                    'schoolId' => '5a67414e64061400ba7234b2',
                    'proEmail' => $faker->email,
                    'type' => 'lab'
                ],
                'response' => true
            ],
            [
                [
                    'title' => 'a',
                    'description' => $faker->text,
                    'userId' => '5a67435764061400b801fae3',
                    'educatorEmail' => $faker->email,
                    'schoolId' => '5a67414e64061400ba7234b2',
                    'proEmail' => $faker->email,
                    'type' => 'lab'
                ],
                'response' => false
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => $faker->text,
                    'userId' => 'Wrong',
                    'educatorEmail' => $faker->email,
                    'schoolId' => '5a67414e64061400ba7234b2',
                    'proEmail' => $faker->email,
                    'type' => 'lab'
                ],
                'response' => false
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => $faker->text,
                    'userId' => '5a67435764061400b801fae3',
                    'educatorEmail' => $faker->email,
                    'schoolId' => '5a67414e64061400ba7234b2',
                    'proEmail' => $faker->email,
                ],
                'response' => false
            ]
        ];
    }

    public function getTaskFaker()
    {
        $faker = Factory::create();
        
        return [
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'title' => $faker->text,
                    'description' => $faker->text,
                    'dueDate' => $faker->date('m-d-Y')
                ],
                'response' => true
            ],
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'title' => 'nop',
                    'description' => $faker->text,
                    'dueDate' => $faker->date('m-d-Y')
                ],
                'response' => false
            ],
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'title' => $faker->text,
                    'description' => $faker->text,
                ],
                'response' => false
            ],
            [
                [
                    'labId' => 'a',
                    'title' => $faker->text,
                    'description' => $faker->text,
                    'dueDate' => $faker->date('m-d-Y')
                ],
                'response' => false
            ],
        ];
    }
    
    public function getTurninFaker()
    {
        return [
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'taskId' => '5a67569c64061400e259dbb2',
                    'userId' => '5a67435764061400b801fae3',
                    'workId' => '5a67590164061400f3361482'
                ],
                'response' => true
            ],
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'taskId' => '5a67569c64061400e259dbb2',
                    'userId' => 'Wrong',
                    'workId' => '5a67590164061400f3361482'
                ],
                'response' => false
            ],
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'userId' => '5a67435764061400b801fae3',
                    'workId' => '5a67590164061400f3361482'
                ],
                'response' => false
            ],
            [
                [
                    'labId' => '5a67546464061400cc465992',
                    'taskId' => '',
                ],
                'response' => false
            ],
        ];
    }
}
