<?php

namespace Unit\App\Validators;

use App\Validators\Work;
use Symfony\Component\HttpFoundation\File\File;

class AddWorkTest extends \AppTestCase
{

    /**
     * @dataProvider getValidVideoExtension
     */
    public function testShouldVerifyIfIsAValidVideo($video, $response)
    {
        $workValidator = new Work();
        $testFilePath =  getcwd() . '/tests/Files/video/' . $video;
        $video = new File(
            $testFilePath,
            $video
        );
        $this->assertEquals($response, $workValidator->validateVideoExtension($this->app, $video));
    }

    /**
     * @dataProvider getWorkFaker
     */
    public function testShouldValidateAddWorkInputs($work, $response)
    {
        $workValidator = new Work();
        $this->assertEquals($response, $workValidator->validateWorkInputs($this->app, $work));
    }

    public function getValidVideoExtension()
    {
        return [
            [
                'extension' => '2mundos-demoreel.mpeg' , 'response' => true
            ],
            [
                'extension' => '2mundos-demoreel_1.mp4' , 'response' => true
            ]
        ];
    }

    public function getWorkFaker()
    {
        $faker = \Faker\Factory::create();
        return [
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'userId' => '58dd246096fbde008624d261',
                    'studentLevel' => 'Student Level 3',
                    'levelNumber' => '3',
                    'type' => 'new',
                    'labId' => '5a65df12533c89008f1c1e22',
                    'isPrivate' => $faker->boolean
                ],
                'response' => true
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => 'Wrong',
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'isPrivate' => $faker->boolean
                ],
                'response' => false
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'token' => $faker->sha256,
                    'isPrivate' => $faker->boolean
                ],
                'response' => false
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'userId' => '58dd246096fbde008624d261',
                    'studentLevel' => 'Student Level 3',
                    'levelNumber' => '3',
                    'type' => 'new',
                    'isPrivate' => $faker->boolean
                ],
                'response' => true
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'token' => $faker->sha256,
                    'isPrivate' => $faker->boolean
                ],
                'response' => false
            ],
            [
                [
                    'workId' => '5a68b84964061402427efc3c',
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'userId' => '58dd246096fbde008624d261',
                    'studentLevel' => 'Student Level 3',
                    'levelNumber' => '3',
                    'type' => 'new',
                    'isPrivate' => $faker->boolean
                ],
                'response' => true
            ],
            [
                [
                    'workId' => '5a68b84964061402427efc3c',
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'attachments' => [
                        [
                            'name' => 'boitata.png',
                            'path' =>  $faker->url,
                            'type' => 'png'
                        ]
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'labId' => '5a65df12533c89008f1c1e22',
                    'userId' => '58dd246096fbde008624d261',
                    'studentLevel' => 'Student Level 3',
                    'levelNumber' => '3',
                    'type' => 'new',
                    'coWorkers' => [],
                    'isPrivate' => $faker->boolean
                ],
                'response' => true
            ],
            [
                [
                    'workId' => '5a68b84964061402427efc3c',
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                        $faker->text
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => $faker->text,
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'userId' => '58dd246096fbde008624d261',
                    'studentLevel' => 'Student Level 3',
                    'levelNumber' => '3',
                    'type' => 'new',
                    'coWorkers' => [
                        '590820c0753bd9005006c936'
                    ],
                    'isPrivate' => $faker->boolean
                ],
                'response' => true
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => 'resource',
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'userId' => '58dd246096fbde008624d261',
                    'levelNumber' => 'admin',
                    'type' => 'new',
                    'coWorkers' => [],
                ],
                'response' => true
            ],
            [
                [
                    'title' => $faker->text,
                    'description' => [
                        $faker->text,
                    ],
                    'category' => $faker->text,
                    'level' => $faker->text,
                    'group' => 'proTip',
                    'mainFile' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'thumbnail' => [
                        'path' => $faker->url,
                        'type' => 'test.jpg',
                        'name' => $faker->name
                    ],
                    'availableReview' => $faker->boolean,
                    'sendPortfolio' => $faker->boolean,
                    'utcOffset' => -0200,
                    'userId' => '58dd246096fbde008624d261',
                    'levelNumber' => 'admin',
                    'type' => 'new',
                    'coWorkers' => [],
                ],
                'response' => true
            ]
        ];
    }
    
    
}