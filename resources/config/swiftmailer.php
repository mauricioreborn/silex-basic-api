<?php
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
));

$app['swiftmailer.options'] = [
    'host' => getenv('EMAIL_HOST'),
    'port' => getenv('EMAIL_PORT'),
    'username' => getenv('EMAIL_USER'),
    'password' => getenv('EMAIL_PASSWORD'),
    'encryption' => getenv('EMAIL_ENCRYPTION'),
    'auth_mode' => getenv('EMAIL_AUTH')
];
