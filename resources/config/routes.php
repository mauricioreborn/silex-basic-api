<?php

$app['annot.controllers'] = array(
    App\Controllers\SignUp::class,
    App\Controllers\Resource::class,
    App\Controllers\Login::class,
    App\Controllers\SignupOrganization::class,
    App\Controllers\ForgotPassword::class,
    App\Controllers\User::class,
    App\Controllers\Work\Work::class,
    App\Controllers\Scripts\Script::class,
    App\Controllers\Work\Comment::class,
    App\Controllers\Profiles::class,
    App\Controllers\Work\Like::class,
    App\Controllers\Work\View::class,
    App\Controllers\Community\RecentActivity::class,
    App\Controllers\Community\Highlight::class,
    App\Controllers\LiveSession\LiveSession::class,
    App\Controllers\Events\Events::class,
    App\Controllers\Groups\Lab::class,
    App\Controllers\Groups\Classroom::class,
    App\Controllers\Groups\Group::class,
    App\Controllers\FeedBack\FeedBack::class,
    App\Controllers\Settings\Settings::class,
    App\Controllers\Comment\Comment::class,
    App\Controllers\Messages\Messages::class,
    App\Controllers\BackOffice\BackOffice::class,
    App\Controllers\BackOffice\BWork::class,
    App\Controllers\BackOffice\BSession::class,
    App\Controllers\BackOffice\BOrganization::class,
    App\Controllers\BackOffice\BRegister::class,
    App\Controllers\BackOffice\BLabs::class,
    App\Controllers\BackOffice\BMessages::class,
    App\Controllers\BackOffice\BDashBoard::class,
    App\Controllers\BackOffice\BNotes::class,
    App\Controllers\BackOffice\BSettings::class
);

$app->register(new DDesrosiers\SilexAnnotations\AnnotationServiceProvider(), array(
    'annot.controllerDir' => realpath('./src/Controllers'),
));
