<?php

use Symfony\Component\HttpFoundation\Request;
use App\Validators\Token;
use App\Services\Auth\TokenBuilder;
use App\ACL\AccessControlList;
use App\Services\Auth\TokenGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use DDesrosiers\SilexAnnotations\Annotations as SLX;


$app->before(function (Request $request) use ($app) {
    $token = $request->headers->get('token');
    if (empty($token) || $token == 'null' || $token == null) {
        $token = "noToken";
    }
    if ($request->getMethod() != 'OPTIONS') {
        $acl = new AccessControlList($token, $request->attributes->get('_controller'));
        if ($acl->validatePermission() === false) {
            return new JsonResponse(
                ['message' => 'Access Not Allowed Or Invalid Token'],
                401
            );
        }
    }
    if ($request->getMethod() != 'OPTIONS' && $token != "noToken") {
        if ($request->getMethod() == 'POST' || $request->getMethod() == 'DELETE') {
            $entity = $app['mongodbodm.dm'];
            $userService = new \App\Services\UserService();
            $tokenGenerator = new TokenGenerator(new TokenBuilder());
            $token = $tokenGenerator->readToken($token);
            $userService->updateActivity($entity->createQueryBuilder(\App\Entities\User::class), $token->getClaim('context')->user_id);
        }
    }


    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});
