<?php

$directory = __DIR__ . '/../../migrations';

$app->register(
    new \Kurl\Silex\Provider\DoctrineMigrationsProvider(),

    array(
        'migrations.directory' => $directory,
        'migrations.namespace' => 'Acme\Migrations',
    )
);
